<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_project_destinasis', function (Blueprint $table) {
            $table->integerIncrements('ticketing_project_destinasi_id');
            $table->integer('tpr_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->date('departure_date');
            $table->date('return_date')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_project_destinasis');
    }
};
