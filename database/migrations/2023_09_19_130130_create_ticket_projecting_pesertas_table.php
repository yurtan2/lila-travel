<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_projecting_pesertas', function (Blueprint $table) {
            $table->integerIncrements('ticket_projecting_peserta_id');
            $table->integer('customer_id');
            $table->integer('tpr_id');
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('nik',100);
            $table->string('tanggal_lahir',100);
            $table->string('jabatan',100);
            $table->string('airline',100);
            $table->string('flight',100);
            $table->string('class',100);
            $table->string('booking_code',100);
            $table->string('status_peserta',100)->default('Active')->comment('Active/Issue/Cancel/Expired');
            $table->dateTime('time_limit');
            $table->Time('etd');
            $table->Time('eta');
            $table->bigInteger('net_price');
            $table->bigInteger('mark_up');
            $table->bigInteger('selling_price');
            $table->string('invoice',100);
            $table->string('remark',100);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_projecting_pesertas');
    }
};
