<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //inclusion & exclusion
        Schema::create('terms', function (Blueprint $table) {
            $table->integerIncrements('term_id');
            $table->string('term_name',100);
            $table->integer('term_jenis');//1 = inclusion && 2 = exclusion
            $table->integer('status')->default(1);
            $table->integer("pr_category_id")->unsigned();
            $table->foreign('pr_category_id')->references('pr_category_id')->on('product_categories');
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
};
