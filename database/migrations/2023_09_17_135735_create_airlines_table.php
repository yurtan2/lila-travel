<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airlines', function (Blueprint $table) {
            $table->integerIncrements('airline_id');
            $table->string('airline_name',200);
            $table->string('airline_code_iata',200);
            $table->string('airline_url_api',300);
            $table->text('airline_url_logo');
            $table->string('airline_url_pic_name',200);
            $table->string('airline_url_pic_email',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airlines');
    }
};
