<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms_conditions', function (Blueprint $table) {
            $table->integerIncrements('terms_condition_id');
            $table->string('terms_condition_name',100);
            $table->text('terms_condition_detail',20);
            $table->integer("pr_category_id")->unsigned();
            $table->foreign('pr_category_id')->references('pr_category_id')->on('product_categories');
            $table->integer('status')->default(1);
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms_conditions');
    }
};
