<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_tailors', function (Blueprint $table) {
            $table->integerIncrements('booking_tailor_id');
            $table->string('booking_tailor_kode');
            $table->string('booking_tailor_request',255);
            $table->string('booking_tailor_nomor',50);
            $table->string('booking_tailor_email',100);
            $table->text('booking_tailor_alamat');
            $table->integer('booking_tailor_dewasa')->default(0);
            $table->integer('booking_tailor_anak')->default(0);
            $table->integer('booking_tailor_bayi')->default(0);
            $table->text('booking_tailor_remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_tailors');
    }
};
