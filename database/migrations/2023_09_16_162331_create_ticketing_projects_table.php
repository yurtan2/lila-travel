<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_projects', function (Blueprint $table) {
            $table->integerIncrements('ticketing_project_id');
            $table->string('ticketing_project_name',200);
            $table->integer('corporate_id');
            $table->integer('customer_id');
            $table->integer('user_id');
            $table->integer('ticketing_project_adult')->default(0);
            $table->integer('ticketing_project_child')->default(0);
            $table->integer('ticketing_project_infant')->default(0);
            $table->integer('ticketing_project_total_amount')->default(0);
            $table->integer('status')->default(1);
            $table->string('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_projects');
    }
};
