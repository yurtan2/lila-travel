<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->integerIncrements('invoice_id');
            $table->datetime('invoice_tanggal');
            $table->datetime('invoice_due_date');
            $table->string('invoice_kode',100);
            $table->bigInteger('invoice_total',100);
            $table->integer('invoice_jenis')->comment("1 = Flight, 2= Tour");
            $table->integer('invoice_status')->comment("1 = Aging,2= Block,3= Exception");
            $table->integer('invoice_payment')->comment("1 = Credit Balance,2= Cash");
            $table->text('invoice_remark')->default('');
            $table->integer('status')->default(1);
            $table->string('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
