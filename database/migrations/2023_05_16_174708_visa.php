<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visas', function (Blueprint $table) {
            $table->integerIncrements('visa_id');
            $table->string('visa_name',100);
            $table->text('visa_detail');
            $table->text('visa_need');
            $table->integer("country_id")->unsigned();
          //  $table->foreign('country_id')->references('country_id')->on('countries');
          $table->string('updated_by',100)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
