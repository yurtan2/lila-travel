<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporates', function (Blueprint $table) {
            $table->integerIncrements('corporate_id');
            $table->string('corporate_email',150);
            $table->text('corporate_address');
            $table->string('corporate_nomor',50);
            $table->integer('corporate_admin_fee');
            $table->integer('corporate_hide_logo')->default(0);
            $table->integer('corporate_use_own_logo')->default(0);
            $table->text('corporate_logo');
            $table->text('corporate_akta');
            $table->text('corporate_nib');
            $table->text('corporate_sk');
            $table->text('corporate_npwp');
            $table->integer('status')->default(1);
            $table->string('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporates');
    }
};
