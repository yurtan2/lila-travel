<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_departure_details', function (Blueprint $table) {
            $table->integerIncrements('tour_departure_detail_id');
            $table->integer('tour_departure_id');
            $table->integer('tour_departure_detail_jenis')->comment("1 = tour_price, 2 = add on");
            $table->string('tour_departure_detail_name',100)->comment("Nama Price");
            $table->string('tour_departure_detail_pax',100)->comment("Khusus Fit Tour, termasuk pax 2-5 dkk");
            $table->integer('tour_departure_detail_price');
            $table->integer('tour_departure_detail_disc');
            $table->integer('tour_departure_detail_total');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_departure_details');
    }
};
