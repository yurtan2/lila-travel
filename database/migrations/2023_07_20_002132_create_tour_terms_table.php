<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //incl,excl,term jadi satu dipisahkan jenis
        Schema::create('tour_terms', function (Blueprint $table) {
            $table->integerIncrements('tour_terms_id');
            $table->date('tour_terms_date');
            $table->integer('tour_terms_jenis')->nullable()->comment("1 = Inclusion, 2 = Exclusion, 3 = Term & Condition");
            $table->integer('tour_id')->comment("Tour ID");
            $table->integer('tour_terms_jenis_id')->comment("Parent ID");
            $table->longText('tour_terms_value');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_terms');
    }
};
