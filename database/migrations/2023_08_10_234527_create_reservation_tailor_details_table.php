<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('reservation_tailor_details', function (Blueprint $table) {
            $table->integerIncrements('reservation_tailor_detail_id');
            $table->string('reservation_tailor_detail_category',100);
            $table->integer('reservation_tailor_detail_customer_id');
            $table->string('reservation_tailor_detail_first_name',200);
            $table->string('reservation_tailor_detail_last_name',200);
            $table->string('reservation_tailor_detail_nik',200);
            $table->string('reservation_tailor_detail_nomor',30);
            $table->date('reservation_tailor_detail_tanggal_lahir',50);
            $table->integer('reservation_tailor_detail_umur');
            $table->string('reservation_tailor_detail_address',100);
            $table->string('reservation_tailor_detail_referensi_makan',255);
            $table->string('reservation_tailor_detail_passport_number',100);
            $table->string('reservation_tailor_detail_poi',100);
            $table->date('reservation_tailor_detail_doi');
            $table->date('reservation_tailor_detail_doe');
            $table->text('reservation_tailor_detail_ktp');
            $table->text('reservation_tailor_detail_passport');
            $table->text('reservation_tailor_detail_note');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_tailor_details');
    }
};
