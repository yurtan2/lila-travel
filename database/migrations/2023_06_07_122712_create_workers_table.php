<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->integerIncrements('worker_id');
            $table->string('worker_kode',50);
            $table->string('worker_name',100);
            $table->string('worker_type',50);
            $table->string('worker_phone',50);
            $table->string('worker_email',100);
            $table->string('worker_address',200);
            $table->string('worker_language1',100);
            $table->string('worker_language2',100);
            $table->string('worker_language3',100);
            $table->text('worker_photo');
            $table->text('worker_ktp');
            $table->text('worker_passport');
            $table->text('worker_drive');
            $table->text('worker_guide');
            $table->integer('status')->default(1);
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
};
