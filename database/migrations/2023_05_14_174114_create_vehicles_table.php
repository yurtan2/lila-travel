<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->integerIncrements('vehicle_id');
            $table->string('vehicle_name',100);
            $table->integer("vehicle_type_id")->unsigned();
            $table->foreign('vehicle_type_id')->references('vehicle_type_id')->on('vehicle_types');
            $table->integer('status')->default(1);
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
};
