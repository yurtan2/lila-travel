<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_black_outs', function (Blueprint $table) {
            $table->integerIncrements('tour_black_out_id');
            $table->integer('tour_departure_id');
            $table->dateTime('tour_black_out_start');
            $table->dateTime('tour_black_out_end');
            $table->text('tour_black_out_remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_black_outs');
    }
};
