<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_family_tree_details', function (Blueprint $table) {
            $table->integerIncrements('customer_family_tree_detail_id');
            $table->integer('customer_family_tree_id');
            $table->integer('customer_id');
            $table->string('customer_family_tree_detail_position',100);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_family_tree_details');
    }
};
