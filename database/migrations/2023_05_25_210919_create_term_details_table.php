<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_details', function (Blueprint $table) {
            $table->integerIncrements('term_detail_id');
            $table->string('term_detail',100);
            $table->integer('status')->default(1);
            $table->integer("term_id")->unsigned();
            $table->foreign('term_id')->references('term_id')->on('terms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term_details');
    }
};
