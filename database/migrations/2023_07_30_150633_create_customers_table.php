<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->integerIncrements('customer_id');
            
            $table->string('customer_first_name',100);
            $table->string('customer_last_name',100);
            $table->string('customer_nik',100);
            $table->string('customer_email',200)->nullable();
            $table->string('customer_password',255)->nullable();
            $table->string('customer_religion',255)->nullable();
            $table->string('customer_gender',20);
            $table->string('customer_prefix_nomor',100);
            $table->string('customer_nomor',100);
            $table->date('customer_tanggal_lahir');
            $table->integer('customer_umur');
            $table->text('customer_alamat');
            $table->string('customer_referensi_makanan',200);
            $table->string('customer_passport_number',50);
            $table->string('customer_poi',100);
            $table->date('customer_doi',100);
            $table->date('customer_doe',100);
            $table->longText('customer_catatan');
            $table->text('customer_ktp')->nullable();
            $table->text('customer_passport')->nullable();
            $table->text('customer_visa')->nullable();
            $table->string('customer_jenis',100)->default("Indonesia")->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
