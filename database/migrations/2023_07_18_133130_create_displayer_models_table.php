<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('displayers', function (Blueprint $table) {
            $table->integerIncrements('displayer_id');
            $table->string('displayer_title',200);
            $table->string('displayer_category',200);
            $table->dateTime('displayer_validity');
            $table->integer('displayer_order');
            $table->integer('displayer_total')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('displayers');
    }
};
