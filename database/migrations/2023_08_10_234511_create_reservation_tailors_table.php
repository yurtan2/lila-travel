<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_tailors', function (Blueprint $table) {
            $table->integerIncrements('reservation_tailor_id');
            $table->string('reservation_tailor_request',200);
            $table->integer('reservation_tailor_customer_id');
            $table->string('reservation_tailor_phone',50);
            $table->string('reservation_tailor_email',150);
            $table->string('reservation_tailor_address',255);
            $table->integer('reservation_tailor_dewasa');
            $table->integer('reservation_tailor_anak');
            $table->integer('reservation_tailor_bayi');
            $table->text('reservation_tailor_remark');
            $table->string('reservation_tailor_status',100)->default("Leads");
            $table->integer('status')->default(0);
            $table->string('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_tailors');
    }
};
