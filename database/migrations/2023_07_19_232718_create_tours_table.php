<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->integerIncrements('displayer_detail_id');
            $table->integer('tour_jenis')->nullable()->comment("1 = FIX, 2 = FIT");
            $table->string('tour_name',300)->nullable();
            $table->string('tour_category',100)->nullable();
            $table->string('tour_type',100)->nullable();
            $table->string('tour_price_gimick',100)->nullable();
            $table->dateTime('tour_date_start')->nullable();
            $table->dateTime('tour_date_end')->nullable();
            $table->integer('tour_days')->nullable();
            $table->integer('tour_nights')->nullable();
            $table->integer('tour_air_ticket')->default(0);
            $table->integer('tour_hotel')->default(0);
            $table->integer('tour_meal')->default(0);
            $table->integer('tour_tour')->default(0);
            $table->integer('tour_land_trans')->default(0);
            $table->integer('tour_water_trans')->default(0);
            $table->integer('tour_visa')->default(0);
            $table->integer('tour_insurance')->default(0);
            $table->longText('tour_itinerary')->nullable();
            $table->longText('keyword_id')->nullable();
            $table->longText('tour_cover')->nullable();
            $table->longText('tour_banner')->nullable();
            $table->integer('status')->default(1);
            $table->integer('displayer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
};
