<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_departures', function (Blueprint $table) {
            $table->integerIncrements('tour_departure_id');
            $table->integer('tour_id');
            $table->string('tour_departure_name',100)->comment("Nama Hotel untuk Fit Tour");
            $table->date('tour_departure_date_start');
            $table->date('tour_departure_date_end');
            $table->integer('tour_departure_deposit');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_departures');
    }
};
