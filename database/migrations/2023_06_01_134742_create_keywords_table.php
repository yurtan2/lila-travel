<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords', function (Blueprint $table) {
            $table->integerIncrements('keyword_id');
            $table->string('keyword_name',100);
            $table->integer("keyword_category_id")->unsigned();
            $table->foreign('keyword_category_id')->references('keyword_category_id')->on('keyword_categories');
            $table->longText('keyword_detail');
            $table->integer('status')->default(1);
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keywords');
    }
};
