<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_logs', function (Blueprint $table) {
            $table->integerIncrements('corporate_logs_id');
            $table->integer('corporate_log_category')->comment("1=credit limit");
            $table->integer('corporate_id');
            $table->integer('corporate_log_amount');
            $table->string('corporate_log_user',100);
            $table->string('corporate_log_due_date_type',100);
            $table->string('corporate_log_due_date',100);
            $table->dateTime('corporate_log_time');
            $table->text('corporate_log_message')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporate_logs');
    }
};
