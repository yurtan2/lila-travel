<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_project_routes', function (Blueprint $table) {
            $table->integerIncrements('tpr_id');
            $table->integer('tpr_jenis')->comment("1=One Way, 2 = Return, 3= Multi City");
            $table->integer('tpr_total_sales');
            $table->integer('tpr_airline');
            $table->integer('tpr_dewasa');
            $table->integer('tpr_anak');
            $table->integer('tpr_bayi');
            $table->integer('status')->default(1);
            $table->string('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_project_routes');
    }
};
