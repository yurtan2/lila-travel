<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->integerIncrements('supplier_id');
            $table->string('supplier_name',100);
            $table->string('supplier_phone',20);
            $table->string('supplier_email',100);
            $table->text('supplier_address');

            $table->longText('supplier_pic');

            $table->string('supplier_bank_name',100);
            $table->string('supplier_bank_account',20);
            $table->string('supplier_swift_code',100);
            $table->string('supplier_curency',100);
            $table->string('supplier_benificary_name',100);

            $table->integer("sp_category_id")->unsigned();
            $table->foreign('sp_category_id')->references('sp_category_id')->on('supplier_categories');
            $table->integer("country_id")->unsigned();
          //  $table->foreign('country_id')->references('country_id')->on('countries');
          $table->string('updated_by',100)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
};
