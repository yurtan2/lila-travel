<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table('users')->insert([
            'user_username' => "Admin",
            'user_email' => 'admin@lila.co.id',
            'user_password' => md5('admin'),
        ]);
        
        $path = public_path('data.sql');
        $sql = file_get_contents($path);
        DB::unprepared($sql);

    }
}
