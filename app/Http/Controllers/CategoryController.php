<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\corporate_position;
use App\Models\hotel_meal;
use App\Models\hotel_room;
use App\Models\hotel_service;
use App\Models\ProductCategory;
use App\Models\SupplierCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\Undefined;

class CategoryController extends Controller
{

    //produk category
   public function product_category()
   {
        return view('Dashboard_Admin.Master.product_category');
   }

   public function get_data_product_category(Request $req)
   {
        $p = new ProductCategory();
        $data = $p->get_category([
            "pr_category_nama"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
          "data" => $data["data"],
          "recordsFiltered" => $data["count"],
          "recordsTotal" =>$data["count"]
      ));
   }

   public function insertProductCategory(Request $req)
   {
        $p = new ProductCategory();
        $data = $p->insertProduct($req->all());
        return $data;
   }
   public function editProductCategory(Request $req)
   {
        $p = new ProductCategory();
        $data = $p->updateProduct($req->all());
        return $data;
   }
   public function deleteProductCategory(Request $req)
   {
        $p = new ProductCategory();
        $data = $p->deleteProduct($req->all());
        return $data;
   }

   public function exportDataProductCategory(Request $req)
   {
        $p = new ProductCategory();
        $data_category = $p->get_category([
            "pr_category_nama"=>$req->nama,
            "search"=>["pr_category_nama","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data_category["data"],
            "header"=>["Nama Category","Tanggal Pembuatan"]
        ]),"Master_Product_Category.xls");
   }

   //Supplier Category
   public function supplier_category()
   {
        return view('Dashboard_Admin.Master.supplier_category');
   }
   public function get_data_supplier_category(Request $req)
   {
        $p = new SupplierCategory();
        $data = $p->get_category([
            "pr_category_id"=>$req->pr_category_id,
            "sp_category_nama"=>$req->sp_category_nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }
   public function insertSupplierCategory(Request $req)
   {
        $p = new SupplierCategory();
        $data_category = $p->insertCategory($req->all());
        return $data_category;
   }
   public function editSupplierCategory(Request $req)
   {
        $p = new SupplierCategory();
        $data_category = $p->editCategory($req->all());
        return $data_category;
   }
   public function deleteSupplierCategory(Request $req)
   {
        $p = new SupplierCategory();
        $data_category = $p->deleteCategory($req->all());
        return $data_category;
   }
   public function exportDataSupplierCategory(Request $req)
   {
        $pi = $req->pr_category_id=="undefined"?null:$req->pr_category_id;
        $p = new SupplierCategory();
        $data_category = $p->get_category([
            "sp_category_nama"=>$req->nama,
            "pr_category_id"=>$pi,
            "search"=>["pr_category_nama","sp_category_nama","supplier_categories.created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data_category["data"],
            "header"=>["Product Category","Supplier Category","Tanggal Pembuatan"]
        ]),"Master_Supplier_Category.xls");
   }

   // Hotel
   public function hotel_room_category()
   {
        return view('Dashboard_Admin.Master.hotel_room_category');
   }

   public function get_data_hotel_room(Request $req)
   {
        $p = new hotel_room();
        $data = $p->get_hotel_room([
            "hotel_room_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
          "data" => $data["data"],
          "recordsFiltered" => $data["count"],
          "recordsTotal" =>$data["count"]
      ));
   }

   public function insertHotelRoom(Request $req)
   {
        $p = new hotel_room();
        $data = $p->insertHotelRoom($req->all());
        return $data;
   }
   public function editHotelRoom(Request $req)
   {
        $p = new hotel_room();
        $data = $p->updateHotelRoom($req->all());
        return $data;
   }
   public function deleteHotelRoom(Request $req)
   {
        $p = new hotel_room();
        $data = $p->deleteHotelRoom($req->all());
        return $data;
   }

   public function exportDataHotelRoom(Request $req)
   {
        $p = new hotel_room();
        $data_category = $p->get_hotel_room([
            "hotel_room_name"=>$req->nama,
            "search"=>["hotel_room_name","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data_category["data"],
            "header"=>["Nama Category","Tanggal Pembuatan"]
        ]),"Master_Hotel_Room.xls");
   }

   //hotel meal
   public function hotel_meal_category()
   {
        return view('Dashboard_Admin.Master.hotel_meal_category');
   }

   public function get_data_hotel_meal(Request $req)
   {
        $p = new hotel_meal();
        $data = $p->get_hotel_meal([
            "hotel_meal_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
          "data" => $data["data"],
          "recordsFiltered" => $data["count"],
          "recordsTotal" =>$data["count"]
      ));
   }

   public function insertHotelMeal(Request $req)
   {
        $p = new hotel_meal();
        $data = $p->insertHotelMeal($req->all());
        return $data;
   }

   public function editHotelMeal(Request $req)
   {
        $p = new hotel_meal();
        $data = $p->updateHotelMeal($req->all());
        return $data;
   }

   public function deleteHotelMeal(Request $req)
   {
        $p = new hotel_meal();
        $data = $p->deleteHotelMeal($req->all());
        return $data;
   }

   public function exportDataHotelMeal(Request $req)
   {
        $p = new hotel_meal();
        $data_category = $p->get_hotel_meal([
            "hotel_meal_name"=>$req->nama,
            "search"=>["hotel_meal_name","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data_category["data"],
            "header"=>["Nama Category","Tanggal Pembuatan"]
        ]),"Master_Hotel_Meal.xls");
   }

   //hotel service
   public function hotel_service()
   {
        return view('Dashboard_Admin.Master.hotel_service');
   }

   public function get_data_hotel_service(Request $req)
   {
        $p = new hotel_service();
        $data = $p->get_hotel_service([
            "hotel_service_name"=>$req->nama,
            "hotel_service_category"=>$req->hotel_service_category,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
          "data" => $data["data"],
          "recordsFiltered" => $data["count"],
          "recordsTotal" =>$data["count"]
      ));
   }

   public function insertHotelService(Request $req)
   {
        $p = new hotel_service();
        $data = $p->insertHotelService($req->all());
        return $data;
   }

   public function editHotelService(Request $req)
   {
        $p = new hotel_service();
        $data = $p->updateHotelService($req->all());
        return $data;
   }

   public function deleteHotelService(Request $req)
   {
        $p = new hotel_service();
        $data = $p->deleteHotelService($req->all());
        return $data;
   }

   public function exportDataHotelService(Request $req)
   {
        $p = new hotel_service();
        $data_category = $p->get_hotel_service([
          "hotel_service_name"=>$req->nama,
          "hotel_service_category"=>$req->hotel_service_category,
          "search"=>["hotel_meal_name","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data_category["data"],
            "header"=>["Nama Category","Tanggal Pembuatan"]
        ]),"Master_Hotel_Service.xls");
   }

   //hotel service
   public function corporate_position()
   {
        return view('Dashboard_Admin.Master.corporate_position_category');
   }

   public function get_data_corporate_position(Request $req)
   {
        $p = new corporate_position();
        $data = $p->get_corporate_position([
            "corporate_position_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);

          echo json_encode(array(
               "data" => $data["data"],
               "recordsFiltered" => $data["count"],
               "recordsTotal" =>$data["count"]
          ));
      
   }

   public function insertCorporatePosition(Request $req)
   {
        $p = new corporate_position();
        $data = $p->insertCorporatePosition($req->all());
        return $data;
   }

   public function editCorporatePosition(Request $req)
   {
        $p = new corporate_position();
        $data = $p->updateCorporatePosition($req->all());
        return $data;
   }

   public function deleteCorporatePosition(Request $req)
   {
        $p = new corporate_position();
        $data = $p->deleteCorporatePosition($req->all());
        return $data;
   }

   public function exportDataCorporatePosition(Request $req)
   {
        $p = new corporate_position();
        $data = $p->get_corporate_position([
          "corporate_position_name"=>$req->nama,
          "search"=>["corporate_position_name","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Nama Position","Tanggal Pembuatan"]
        ]),"Master_Position.xls");
   }
}
