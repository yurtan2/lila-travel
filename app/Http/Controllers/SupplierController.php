<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Imports\importSupplier;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SupplierController extends Controller
{
    //Supplier
    public function supplier()
    {
        
         return view('Dashboard_Admin.Master.supplier');
    }

    public function get_data_supplier(Request $req)
    {
         $p = new Supplier();
         $data = $p->get_data_supplier([
            "supplier_name"=>$req->nama,
            "country_id"=>$req->country_id,
            "sp_category_id"=>$req->sp_category_id,
            "start" => $req->start,
            "length" => $req->length
         ]);
         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertSupplier(Request $req)
    {
         $p = new Supplier();
         $data = $p->insertSupplier($req->all());
         return $data;
    }

    public function editSupplier(Request $req)
    {
         $p = new Supplier();
         $data = $p->updateSupplier($req->all());
         return $data;
    }

    public function deleteSupplier(Request $req)
    {
         $p = new Supplier();
         $data = $p->deleteSupplier($req->all());
         return $data;
    }

    public function exportDataSupplier(Request $req)
    {
        $pi = $req->country_id=="null"?null:$req->country_id;
        $p2 = $req->sp_category_id=="null"?null:$req->sp_category_id;
         $p = new Supplier();
         $data = $p->get_data_supplier([
            "supplier_name"=>$req->nama,
            "country_id"=>$pi,
            "sp_category_id"=>$p2,
            "search"=>["suppliers.supplier_name","suppliers.supplier_phone","suppliers.supplier_email","c.name","suppliers.supplier_address","sc.sp_category_nama","suppliers.supplier_bank_name","suppliers.supplier_bank_account","suppliers.supplier_swift_code","suppliers.supplier_curency","suppliers.supplier_benificary_name","suppliers.created_at"]
         ]);

         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Supplier Name","Supplier Phone","Supplier Email","Country Name","Supplier Address","Supplier Category","Bank Name","Account Number","Swift Code","Curency","Benificary Name","Tanggal Pembuatan"]
         ]),"Master_Supplier.xls");
    }

    public function exportDataSupplierPIC(Request $req)
    {
        $pi = $req->country_id=="null"?null:$req->country_id;
        $p2 = $req->sp_category_id=="null"?null:$req->sp_category_id;
        $p = new Supplier();
        $temp = [];

        $data = $p->get_data_supplier([
           "supplier_name"=>$req->nama,
           "country_id"=>$pi,
           "sp_category_id"=>$p2,
           "search"=>["suppliers.supplier_name","suppliers.supplier_pic"]
        ]);

        foreach ($data["data"] as $key => $value) {
           $pic = json_decode($value->supplier_pic,true);
           foreach ($pic as $key => $item) {
               array_push($temp,[
                   "supplier_name" => $value["supplier_name"],
                   "pic_name" => $item["pic_name"],
                   "pic_phone" => $item["pic_phone"],
                   "pic_email" => $item["pic_email"],
                   "pic_jabatan" => $item["pic_jabatan"]
               ]);
           }
        }

        $final = new Collection($temp);

         return Excel::download(new MasterExport([
             "data"=>$final,
             "header"=>["Supplier Name","PIC Name","PIC Phone","PIC Email","PIC Jabtan"]
         ]),"Master_PIC_Supplier.xls");
    }

    public function importSupplier(Request $req)
    {
        
        try {
            $data = Excel::import(new importSupplier,  $req->file('file'));
            $msg="Berhasil Insert!";
            return 1;
            
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return response()->json([
                'error' => true,
                'message' => $msg,
                'code' => 400
            ], 400);
        }
        
        
    }
}
