<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\Displayer;
use App\Models\displayer_detail;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DisplayerController extends Controller
{
    public function displayer()
    {
         return view('Dashboard_Admin.Master.displayer');
    }

    public function displayer_detail($id)
    {
        $d = new displayer_detail();
        $param["data"] = Displayer::find($id);
        $param["data_detail"] = $d->get_data_displayer_detail(["displayer_id"=>$id])["data"];
        return view('Dashboard_Admin.Master.displayer_detail')->with($param);
    }
 
    public function get_data_displayer(Request $req)
    {
         $p = new Displayer();

         $data = $p->get_data_displayer([
            "displayer_category" =>$req->displayer_category,
            "displayer_validty"=>$req->displayer_validity,
            "start" => $req->start,
            "length" => $req->length
         ]);
         
         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertDisplayer(Request $req)
    {
         $p = new Displayer();
         $data = $p->insertDisplayer($req->all());
         return $data;
    }
 
    public function editDisplayer(Request $req)
    {
         $p = new Displayer();
         $data = $p->updateDisplayer($req->all());
         return $data;
    }
 
    public function deleteDisplayer(Request $req)
    {
         $p = new Displayer();
         $data = $p->deleteDisplayer($req->all());
         return $data;
    }

   public function updateDisplayerDetail(Request $req)
    {
         $p = new displayer();
         $p->updateDisplayer($req->all());

         
         $p = new displayer_detail();
         $i = array_filter(json_decode($req->insert,true));//array list data insert
         $d = array_filter(json_decode($req->delete,true));//array list data delete

         if(sizeof($i)>0)$p->insertDisplayerDetail(json_decode($req->insert,true),$req->displayer_id);
         if(sizeof($d)>0)$p->deleteDisplayerDetail(json_decode($req->delete,true));

         $d = Displayer::find($req->displayer_id);
         $d->displayer_total = $p->get_data_displayer_detail(["displayer_id"=>$req->displayer_id])["count"];
         $d->save();
     }

     public function exportDisplayer(Request $req)
     {
          $p1 = $req->keyword_category_id=="null"?null:$req->keyword_category_id;
          $p = new displayer();
          
          
         $data = $p->get_data_displayer([
               "displayer_category" =>$req->nama,
               "displayer_validty"=>$req->validty,
               "select"=>["displayer_id","displayer_title","displayer_category","displayer_validity","displayer_total","created_at"]
          ]);
          foreach ($data["data"] as $key => $item) {
               unset($item->displayer_id);
          }
         
          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["Displayer Title","Displayer Category","Validity","Total Product","Tanggal Pembuatan"]
          ]),"Master Displayer.xls");
     }
  
}
