<?php

namespace App\Http\Controllers;

use App\Models\ticket_projecting_peserta;
use App\Models\ticketing_project;
use App\Models\ticketing_project_route;
use Illuminate\Http\Request;

class TicketProjectController extends Controller
{
    function ticketingProject() {
        return view('Dashboard_Admin.Booking.ticket_projecting');
    }

    function ticketingProjectEdit() {
        return view('Dashboard_Admin.Booking.ticket_projecting_edit');
    }

    function ticketingProjectDetail($id) {
        $p = new ticketing_project();
        $r = new ticketing_project_route();
        $param["header"] = $p->get_data_ticketing_project([
            "ticketing_project_id"=>$id,
        ])["data"][0];
        $param["routes"] = $r->get_data_route([
            "ticketing_project_id"=>$id,
        ])["data"];
        $param["ticketing_project_id"] = $id;
        return view('Dashboard_Admin.Booking.ticket_projecting_detail')->with($param);
    }

   public function get_data_ticketing_project(Request $req)
   {
        $p = new ticketing_project();
        $data = $p->get_data_ticketing_project([
            "ticketing_project_name"=>$req->ticketing_project_name,
            "bulan"=>$req->bulan,
            "tahun"=>$req->tahun,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

  public function insertTicketingProject(Request $req)
  {
       $p = new ticketing_project();
       $data = $p->insertTicketingProject($req->all());
       return $data;
  }

   public function editTicketingProject(Request $req)
   {
        $p = new ticketing_project();
        $data = $p->updateTicketingProject($req->all());
        return $data;
   }

   public function deleteTicketingProject(Request $req)
   {
        $p = new ticketing_project();
        $data = $p->deleteTicketingProject($req->all());
        return $data;
   }

   //Route
   function insertRoute(Request $req) {
        $p = new ticketing_project_route();
        $data = $p->insertRoute($req->all());
        return $data;
   }
   function deleteRoute(Request $req) {
        $p = new ticketing_project_route();
        $data = $p->deleteRoute($req->all());
        return $data;
   }

   //Peserta
   function managementPeserta(Request $req) {
        $p = new ticket_projecting_peserta();
        foreach (json_decode($req->data_delete,true) as $key => $value) {
            $p->deletePeserta([
                "tpp_id"=>$value,
                "tpr_id"=>$req->trp_id
            ]);
        }
        $id = [];
        foreach (json_decode($req->data,true) as $key => $value) {
           if(isset($value["tpp_id"])&&$value["tpp_id"]!="null") {
                
                $p->UpdatePeserta($value);
                $i =$value["tpp_id"];
           }
           else {
            
            $i  = $p->insertPeserta($value);
           }
           array_push($id,$i);
        }
        return [
            "header"=>ticketing_project_route::find($req->tpr_id),
            "detail"=>$id
        ];
   }
}
