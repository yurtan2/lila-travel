<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\airlines;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FlightTicket extends Controller
{
    public function get_data_airlines(Request $req)
    { 
        $p = new airlines();
        $data = $p->get_data_airlines([
            "airlines_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
    }

    public function insertAirlines(Request $req)
    {
        $data = $req->all();
        if(isset($req->airline_logo)&&$req->airline_logo!="undefined")$data["airline_url_logo"] = $this->insertFile($req->airline_logo,"airlines");
        $p = new airlines();
        $data = $p->insertAirlines($data);
        return $data;
    }

    public function editAirlines(Request $req)
    {
        $data = $req->all();
        if(isset($req->airline_logo)&&$req->airline_logo!="undefined")$data["airline_url_logo"] = $this->insertFile($req->airline_logo,"airlines");
        $p = new airlines();
        $data = $p->updateAirlines($data);
        return $data;
    }

    public function deleteAirlines(Request $req)
    {
            $p = new airlines();
            $data = $p->deleteAirlines($req->all());
            return $data;
    }

    public function exportDataAirplanes(Request $req)
    {
            $p = new airlines();
            $data = $p->get_data_airplanes([
                "vehicle_type_name"=>$req->nama,
                "search"=>["vehicle_type_name","created_at"]
            ]);

            return Excel::download(new MasterExport([
                "data"=>$data["data"],
                "header"=>["Vehicle Type","Tanggal Pembuatan"]
            ]),"Master_Vehicle_Type.xls");
    }

    function flight_ticket() {
        return view('Dashboard_Admin.Produk.FlightTIcket.flight_ticket');
    }
    function flight_ticket_tambah() {
        return view('Dashboard_Admin.Produk.FlightTIcket.flight_ticket_tambah');
    }
     
     //insert
     public function insertFile($file,$type)
     {
           try {
                $fileName = uniqid().'.'.$file->extension();
                $filePath = 'assets/'.$type."/".$fileName;
                $file->move(public_path('assets/'.$type), $fileName);
                return $filePath;
           } catch (\Throwable $th) {
                return -1;
           }
     }
}
