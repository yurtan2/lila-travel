<?php

namespace App\Http\Controllers;

use App\Models\airlines;
use App\Models\City;
use App\Models\corporate;
use App\Models\corporate_pic;
use App\Models\corporate_position;
use App\Models\Country;
use App\Models\customer;
use App\Models\Displayer;
use App\Models\Document;
use App\Models\keyword;
use App\Models\keywordCategory;
use App\Models\States;
use App\Models\ProductCategory;
use App\Models\Supplier;
use App\Models\SupplierCategory;
use App\Models\term;
use App\Models\terms_condition;
use App\Models\tour;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AutocompleteController extends Controller
{
    public function autocompleteProductCategory(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new ProductCategory();
        $data = $p->get_category([
            "pr_category_nama"=>$keyword
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["pr_category_id"];
            $r->text = $r["pr_category_nama"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteSupplierCategory(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new SupplierCategory();
        $data = $p->get_category([
            "sp_category_nama"=>$keyword
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["sp_category_id"];
            $r->text = $r["sp_category_nama"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteCountry(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new Country();
        $data = $p->get_data_country([
            "country_name"=>$keyword
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["id"];
            $r->text = $r["name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteLocation(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new Country();
        $final  = [];


        $p = new City();
        $data_city = $p->get_data_simple_city([
            "city_name"=>$keyword
        ]);


        foreach ($data_city['data'] as $r) {
            $r->id = $r["id"];
            $r->text = $r["name"];
            $r->type_data = "city";
            array_push($final,$r);
        };

        $final = array_unique($final);

        echo json_encode(array(
            "data" => $final
        ));
    }

    public function autocompleteStates(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new States();
        $data = $p->get_data_states([
            "state_name"=>$keyword,
            "country_id"=>$req->country_id,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["id"];
            $r->text = $r["name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteVehicleType(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new VehicleType();
        $data = $p->get_data_vehicle_Type([
            "vehicle_type_name"=>$keyword
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["vehicle_type_id"];
            $r->text = $r["vehicle_type_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteDocument(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new Document();
        $data = $p->get_data_vehicle_Type([
            "document_name"=>$keyword
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["document_id"];
            $r->text = $r["document_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteKeywordCategory(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new keywordCategory();
        $data = $p->get_data_keyword_category([
            "keyword_category_name"=>$keyword
        ]);
        foreach ($data as $r) {
            $r->id = $r["keyword_category_id"];
            $r->text = $r["keyword_category_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteSupplier(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new Supplier();
        $data = $p->get_data_supplier([
            "supplier_name"=>$keyword,
            "country_id"=>$req->country_id,
         ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["supplier_id"];
            $r->text = $r["supplier_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    public function autocompleteDisplayer(Request $req)
    {
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new Displayer();
        $data = $p->get_data_displayer([
            "displayer_title"=>$keyword,
            "country_id"=>$req->country_id,
         ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["displayer_id"];
            $r->text = $r["displayer_title"]." - ".$r["displayer_validity"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteInclusion(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new term();
        $data = $p->get_data_term([
            "term_name"=>$keyword,
            "jenis"=>1,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["term_id"];
            $r->text = $r["term_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteExclusion(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new term();
        $data = $p->get_data_term([
            "term_name"=>$keyword,
            "jenis"=>2,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["term_id"];
            $r->text = $r["term_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }
    function autocompleteTermCondition (Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new terms_condition();
        $data = $p->get_data_term([
            "terms_condition_name"=>$keyword,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["terms_condition_id"];
            $r->text = $r["terms_condition_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteTour(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new tour();
        $data = $p->get_data_tour([
            "tour_name"=>$keyword,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["tour_id"];
            $r->text = $r["tour_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteCustomer(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new customer();

        $data = $p->get_data_customer([
            "customer_name"=>$keyword,
            "unique"=>intval($req->unique),
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["customer_id"];
            $r->text = $r["customer_first_name"]." ".$r["customer_last_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteAirport(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;

        $data = DB::table('airports');

        if($keyword!=null)$data->where('city_name','like',"%".$keyword."%")->orwhere('city_code','like',"%".$keyword."%");
        $data = $data->get();

        foreach ($data as $r) {
            $r->id = $r->city_code;
            $r->text = $r->airport_name." (".$r->airport_code.")"."-".$r->city_name;
            $r->html = $r->airport_name." (".$r->airport_code.")"."-".$r->city_name;
        };
        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompletePosition(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new corporate_position();

        $data = $p->get_corporate_position([
            "corporate_position_name"=>$keyword,
        ]);
        foreach ($data['data'] as $r) {
            $r->id = $r["corporate_position_id"];
            $r->text = $r["corporate_position_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteKeyword(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new keyword();

        $data = $p->get_data_keyword([
            "keyword_name"=>$keyword,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["keyword_id"];
            $r->text = $r["keyword_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteCorporate(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new corporate();

        $data = $p->get_data_corporate([
            "corporate_name"=>$keyword,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["corporate_id"];
            $r->text = $r["corporate_name"];
        };

        echo json_encode(array(
            "data" => $data
        ));
    }

    function autocompleteCorporatePIC(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new corporate_pic();

        $data = $p->get_corporate_pic([
            "corporate_id"=>$req->corporate_id,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["customer_id"];
            $r->text = $r["customer_first_name"].$r["customer_last_name"];
        };

        echo json_encode(array(
            "data" => $data["data"]
        ));
    }

    function autocompleteAirlines(Request $req){
        $keyword = isset($req->keyword)?$req->keyword:null;
        $p = new airlines();

        $data = $p->get_data_airlines([
            "airlines_name"=>$req->keyword,
        ]);

        foreach ($data['data'] as $r) {
            $r->id = $r["airline_id"];
            $r->text = $r["airline_name"];
        };

        echo json_encode(array(
            "data" => $data["data"]
        ));
    }

}
