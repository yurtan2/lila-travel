<?php

namespace App\Http\Controllers;

use App\Models\Passport;
use App\Models\terms_condition;
use App\Models\Visa;
   use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class PDFController extends Controller
{
   public function exportPdfVisa($id)
   {
        $v = Visa::find($id);
        $param["visa"] = $v;
        $param["content"] = $v->visa_detail;
        $pdf =  Pdf::loadView('PDF.exportVisa',$param);
        return $pdf->download('Persyaratan Visa.pdf');
   }

   public function exportPdfTerms($id)
   {
        $p = terms_condition::find($id);
        $v = $p->get_data_term();
        $param["term"] = $v["data"][0];

        $pdf =  Pdf::loadView('PDF.exportTerm',$param);
        return $pdf->download('Terms & Condition.pdf');
   }

   public function exportPdfPassport($id)
   {
        $v = Passport::find($id);
        $param["content"] = $v->passport_syarat;
        $pdf = app('dompdf.wrapper');
        $pdf =  Pdf::loadView('PDF.exportGeneral',$param);
        return $pdf->download('Persyaratan Passport.pdf');
   }
}
