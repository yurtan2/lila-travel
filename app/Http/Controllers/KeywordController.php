<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\keyword;
use App\Models\keywordCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class KeywordController extends Controller
{
   //keyword
   public function keyword()
   {
        return view('Dashboard_Admin.Master.keyword');
   }

   public function get_data_keyword(Request $req)
   {
        $p = new keyword();
        $data = $p->get_data_keyword([
            "keyword_name"=>$req->nama,
            "keyword_category_id"=>$req->keyword_category_id,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

  public function insertKeyword(Request $req)
   {
        $p = new keyword();
        $data = $p->insertKeyword($req->all());
        return $data;
   }

   public function editKeyword(Request $req)
   {
        $p = new keyword();
        $data = $p->updateKeyword($req->all());
        return $data;
   }

   public function deleteKeyword(Request $req)
   {
        $p = new keyword();
        $data = $p->deleteKeyword($req->all());
        return $data;
   }
   public function exportDataKeyword(Request $req)
   {
        $p1 = $req->keyword_category_id=="null"?null:$req->keyword_category_id;
        $p = new keyword();
        
        $data = $p->get_data_keyword([
          "keyword_name"=>$req->nama,
          "keyword_category_id"=>$p1,
          "search"=>["kc.keyword_category_name","keywords.keyword_name","keywords.keyword_detail","keywords.created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Keyword Category","Keyword Name","Keyword","Tanggal Pembuatan"]
        ]),"Master_Keyword.xls");
   }

   //keyword category
   public function get_data_keyword_category()
   {
         $p = new keywordCategory();
         return $p->get_data_keyword_category();
   }

   public function insertKeywordCategory(Request $req)
   {
        $p = new keywordCategory();
        $data = $p->insertKeywordCategory($req->all());
        return $data;
   }

   public function deleteKeywordCategory(Request $req)
   {
        $p = new keywordCategory();
        $data = $p->deleteKeywordCategory($req->all());
        return $data;
   }
}
