<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\corporate;
use App\Models\corporate_log;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CorporateController extends Controller
{
     //Corporate
     function corporate() {
          return view('Dashboard_Admin.Master.corporate');
       }

     public function get_data_corporate(Request $req)
     {
          $p = new corporate();

          $data = $p->get_data_corporate([
             "corporate_name" =>$req->filter_corporate_name,
             "corporate_nomor" =>$req->filter_corporate_nomor,
             "start" => $req->start,
             "length" => $req->length
          ]);

          echo json_encode(array(
              "data" => $data["data"],
              "recordsFiltered" => $data["count"],
              "recordsTotal" =>$data["count"]
          ));
     }

     public function insertCorporate(Request $req)
     {
         $data = $req->all();

         if(isset($req->logo)&&$req->logo!="undefined")$data["corporate_logo"] = $this->insertFile($req->logo,"logo");
         if(isset($req->akta)&&$req->akta!="undefined")$data["corporate_akta"] = $this->insertFile($req->akta,"akta");
         if(isset($req->nib)&&$req->nib!="undefined")$data["corporate_nib"] = $this->insertFile($req->nib,"nib");
         if(isset($req->sk)&&$req->sk!="undefined")$data["corporate_sk"] = $this->insertFile($req->sk,"sk");
         if(isset($req->npwp)&&$req->npwp!="undefined")$data["corporate_npwp"] = $this->insertFile($req->npwp,"npwp");

         $p = new corporate();
         $data = $p->insertCorporate($data);
         return $data;
     }

     public function editCorporate(Request $req)
     {
         $data = $req->all();
         if(isset($req->logo)&&$req->logo!="undefined")$data["corporate_logo"] = $this->insertFile($req->logo,"logo");
         if(isset($req->akta)&&$req->akta!="undefined")$data["corporate_akta"] = $this->insertFile($req->akta,"akta");
         if(isset($req->nib)&&$req->nib!="undefined")$data["corporate_nib"] = $this->insertFile($req->nib,"nib");
         if(isset($req->sk)&&$req->sk!="undefined")$data["corporate_sk"] = $this->insertFile($req->sk,"sk");
         if(isset($req->npwp)&&$req->npwp!="undefined")$data["corporate_npwp"] = $this->insertFile($req->npwp,"npwp");

         $p = new corporate();
         $data = $p->updateCorporate($data);
         return $data;
     }

     public function deleteCorporate(Request $req)
     {
          $p = new corporate();
          $data = $p->deleteCorporate($req->all());
          return $data;
     }

     public function exportDataCorporate(Request $req)
      {
           $p = new corporate();
          $data = $p->get_data_corporate([
               "corporate_name" =>$req->filter_corporate_name,
               "corporate_nomor" =>$req->filter_corporate_nomor,
               "select"=>["corporate_name","status","corporate_email","corporate_address","corporate_admin_fee","created_at","corporate_nomor","corporate_prefix","corporate_id"]
           ]);
           foreach ($data["data"] as $key => $item) {
               $item["corporate_admin_fee"] = "Rp.".number_format($item["corporate_admin_fee"],0,',','.');
               if(!isset($item["total_pic"])||$item["total_pic"]==0)$item["total_pic"]="0";
               $item["status"] = $item["corporate_nomor_full"];
               unset($item["corporate_nomor_full"]);
               unset($item["corporate_prefix"]);
               unset($item["corporate_nomor"]);
               unset($item["corporate_id"]);
               unset($item["detail_pic"]);
           }
           return Excel::download(new MasterExport([
               "data"=>$data["data"],
               "header"=>["Corporate Name","Nomor","Email","Address","Admin Fee","Total PIC","Tanggal Pembuatan"]
           ]),"Master Corporate.xls");
      }

    function credit_corporate() {
        return view('Dashboard_Admin.Master.credit_corporate');
    }

    function billing_control() {
        return view("Dashboard_Admin.Master.billing_control");
    }

    function editCorporateCredit(Request $req) {
        $p = new corporate();
        $data = $p->editCorporateCredit($req->all());
    }

    //log
    public function get_data_corporate_log(Request $req)
    {
         $p = new corporate_log();

         $data = $p->get_data_corporate_log([   
            "corporate_log_category" => $req->corporate_log_category,
            "corporate_id" => $req->corporate_id,
            "start" => $req->start,
            "length" => $req->length
         ]);

         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }
    public function exportDataCorporateCredit(Request $req)
    {
         $p = new corporate();
        $data = $p->get_data_corporate([
             "corporate_name" =>$req->filter_corporate_name,
             "corporate_nomor" =>$req->filter_corporate_nomor,
             "select"=>["corporate_name","corporate_credit_limit","corporate_credit_used","corporate_credit_available","corporate_due_type","corporate_due_date"]
         ]);
         foreach ($data["data"] as $key => $item) {
             $item["corporate_credit_limit"] = "Rp.".number_format($item["corporate_credit_limit"],0,',','.');
             $item["corporate_credit_used"] = "Rp.".number_format($item["corporate_credit_used"],0,',','.');
             $item["corporate_credit_available"] = "Rp.".number_format($item["corporate_credit_available"],0,',','.');
             unset($item["corporate_nomor_full"]);
         }
         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Corporate Name","Credit Limit","Credit Used","Credit Available","Due Type","Due Date"]
         ]),"Master Corporate Credit.xls");
    }
    
     //insert
    public function insertFile($file,$type)
    {
          try {
               $fileName = uniqid().'.'.$file->extension();
               $filePath = 'assets/'.$type."/".$fileName;
               $file->move(public_path('assets/'.$type), $fileName);
               return $filePath;
          } catch (\Throwable $th) {
               return -1;
          }
    }
}
