<?php

namespace App\Http\Controllers;

use thiagoalessio\TesseractOCR\TesseractOCR;
use App\Exports\TemplateSupplier;
use App\Models\customer;
use App\Models\Displayer;
use App\Models\tour;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Facades\Socialite;
use Intervention\Image\Image;
use Werk365\IdentityDocuments\Interfaces\OCR;
use Werk365\IdentityDocuments\Responses\OcrResponse;

class GeneralController extends Controller
{
   public function landing_page()
   {
        $d = new Displayer();
        $t = new tour();
        $param["data2"] = $t->get_data_tour();
        $param["data"] = $d->get_data_displayer([
          "date"=>date("Y/m/d"),
          "display"=>1,
          "displayer_detail"=>1
        ])["data"];
        return view('landing_page.landing_page')->with($param);
   }

    function landing_page_tour() {
        return view('landing_page.landing_page_tour');
    }

    function detail($id) {
        $t = new tour();
        $param["data"] = $t->get_detail_tour($id);

        return view('landing_page.tour_detail')->with($param);
    }

    function order($id) {
        $t = new tour();
        $param["data"] = $t->get_detail_tour($id);

        return view('landing_page.order_page')->with($param);
    }

   public function aboutUs()
   {
        return view('landing_page.about_us');
   }

   public function contact()
   {
        return view('landing_page.contact');
   }

   public function login()
   {
        if(Session::has("user"))return redirect('/master/product_category');
        return view('General.login');
   }

   public function mekanismeLogin(Request $req)
   {
        $u = new User();
        $valid = $u->login_user([
            "user_email"=>$req->email,
            "user_password"=>md5($req->password)
        ]);
        if(count($valid)>0){
            Session::put("user",$valid[0]);
            return 1;
        }
        else{
            return -1;
        }
   }

   public function logout()
   {
        Session::flush();
        return redirect('/');
   }

   public function downloadTemplate()
   {
     return Excel::download(new TemplateSupplier(),"import_data_supplier.xls");
   }

   public function testOCR()
   {
      return view("ocr");
   }

   public function uploadOCR(Request $req)
   {
         $image = $req->file('file');
         // Store your image in a temp folder
         $fileName = uniqid().'.'.$image->extension();
         $filePath = 'temp/'.$fileName;
         $image->move(public_path('temp/'), $fileName);
         // Use Tesseract to create text response

         $response = (new TesseractOCR(public_path($filePath)));
         $response->lang('eng')->run();
         //File::delete($filePath);

         // Return the new response
        // return new OcrResponse($response);
   }

   public function loginGoogle()
   {
        return Socialite::driver('google')->redirect();
   }

   public function callbackGoogle()
   {
     /*
       try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();

            if($finduser){


                return redirect('/');

            }else{
                $newUser = customer::insert([
                        'name' => $user->name,
                        'google_id'=> $user->id,
                        'password' => encrypt('123456dummy')
                    ]);


                    return redirect();
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }*/
   }

   function get_bin() {
        $tables = DB::select('SHOW TABLES');
        foreach ($tables as $table) {
            foreach ($table as $key => $value){
                echo $value."<br>";
            }

        }
   }

}
