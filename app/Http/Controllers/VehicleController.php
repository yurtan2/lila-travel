<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class VehicleController extends Controller
{
    //vehicle_type
   public function vehicle_type()
   {
        return view('Dashboard_Admin.Master.vehicle_type');
   }

   public function get_data_vehicle_type(Request $req)
   { 
        $p = new VehicleType();
        $data = $p->get_data_vehicle_type([
            "vehicle_type_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

   public function insertVehicleType(Request $req)
   {
        $p = new VehicleType();
        $data = $p->insertVehicleType($req->all());
        return $data;
   }

   public function editVehicleType(Request $req)
   {
        $p = new VehicleType();
        $data = $p->updateVehicleType($req->all());
        return $data;
   }

   public function deleteVehicleType(Request $req)
   {
        $p = new VehicleType();
        $data = $p->deleteVehicleType($req->all());
        return $data;
   }

   public function exportDataVehicleType(Request $req)
   {
        $p = new VehicleType();
        $data = $p->get_data_vehicle_type([
            "vehicle_type_name"=>$req->nama,
            "search"=>["vehicle_type_name","created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Vehicle Type","Tanggal Pembuatan"]
        ]),"Master_Vehicle_Type.xls");
   }


   //vehicle_type
   public function vehicle()
   {
        return view('Dashboard_Admin.Master.vehicle');
   }

   public function get_data_vehicle(Request $req)
   {
        $p = new vehicle();
        $data = $p->get_data_vehicle([
            "vehicle_type_name"=>$req->nama,
            "vehicle_type"=>$req->vehicle_type_id,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

   public function insertVehicle(Request $req)
   {
        $p = new Vehicle();
        $data = $p->insertVehicle($req->all());
        return $data;
   }

   public function editVehicle(Request $req)
   {
        $p = new Vehicle();
        $data = $p->updateVehicle($req->all());
        return $data;
   }

   public function deleteVehicle(Request $req)
   {
        $p = new Vehicle();
        $data = $p->deleteVehicle($req->all());
        return $data;
   }

   public function exportDataVehicle(Request $req)
   {
        $p = new Vehicle();
        $data = $p->get_data_vehicle([
            "vehicle_type_name"=>$req->nama,
            "vehicle_type"=>$req->vehicle_type_id,
            "search"=>["v.vehicle_type_name","vehicles.vehicle_name","vehicles.created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Vehicle Type","Vehicle Name","Tanggal Pembuatan"]
        ]),"Master_Vehicle.xls");
   }

}
