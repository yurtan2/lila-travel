<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class LocationController extends Controller
{
   //country
   public function country()
   {
        return view('Dashboard_Admin.Master.country');
   }

   public function get_data_country(Request $req)
   {
        $p = new Country();
        $data = $p->get_data_country([
            "country_name"=>$req->nama,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

   public function insertCountry(Request $req)
   {
        $p = new Country();
        $data = $p->insertCountry($req->all());
        return $data;
   }

   public function editCountry(Request $req)
   {
        $p = new Country();
        $data = $p->updateCountry($req->all());
        return $data;
   }

   public function deleteCountry(Request $req)
   {
        $p = new Country();
        $data = $p->deleteCountry($req->all());
        return $data;
   }

   public function exportDataCountry(Request $req)
   {
        $p = new Country();
        $data = $p->get_data_country([
            "country_name"=>$req->nama,
            "search"=>["id","name","created_at"]
        ]);
        foreach ($data["data"] as $key => $item) {
            unset($item->keterangan_visa);

           if($item->with_visa==0) $item->with_visa ="Tidak Pakai Visa";
           else $item->with_visa ="Pakai Visa";
        }
        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Country Code","Country","Visa","Tanggal Pembuatan"]
        ]),"Master_Country.xls");
   }

   //city
   public function city()
   {
        return view('Dashboard_Admin.Master.city');
   }

   public function get_data_city(Request $req)
   {
        $p = new City();
        $data = $p->get_data_city([
            "city_name"=>$req->nama,
            "country_id"=>$req->country_id,
            "state_id"=>$req->state_id,
            "start" => $req->start,
            "length" => $req->length
        ]);
        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
   }

   public function insertCity(Request $req)
   {
        $p = new City();
        $data = $p->insertCity($req->all());
        return $data;
   }

   public function editCity(Request $req)
   {
        $p = new City();
        $data = $p->updateCity($req->all());
        return $data;
   }

   public function deleteCity(Request $req)
   {
        $p = new City();
        $data = $p->deleteCity($req->all());
        return $data;
   }

   public function exportDataCity(Request $req)
   {
        $pi = $req->country_id=="undefined"?null:$req->country_id;
        $pi2 = $req->state_id=="undefined"?null:$req->state_id;

        $p = new City();
        $data = $p->get_data_city([
            "city_name"=>$req->nama,
            "country_id"=>$pi,
            "state_id"=>$pi2,
            "search"=>["c.name as country_name","s.name","cities.name as city_name","cities.created_at"]
        ]);

        return Excel::download(new MasterExport([
            "data"=>$data["data"],
            "header"=>["Country","State","City","Tanggal Pembuatan"]
        ]),"Master_City.xls");
   }
}
