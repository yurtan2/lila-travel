<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    function hotel() {
        return view("Dashboard_Admin.Produk.Hotel.hotel");
    }
    function tambah_hotel() {
        $c = new Country();
        $param['data'] = $c->get_data_country();
        return view("Dashboard_Admin.Produk.Hotel.hotel_tambah")->with($param);
    }
}
