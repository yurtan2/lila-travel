<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\term;
use App\Models\terms_condition;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TermsController extends Controller
{
     //Terms & Condition
     public function term_condition()
     {
          return view('Dashboard_Admin.Master.term_condition');
     }

     public function get_data_term_condition(Request $req)
     {
          $p = new terms_condition();
          $data = $p->get_data_term([
             "terms_condition_name"=>$req->nama,
             "pr_category_id"=>$req->pr_category_id,
             "start" => $req->start,
             "length" => $req->length
          ]);
          echo json_encode(array(
              "data" => $data["data"],
              "recordsFiltered" => $data["count"],
              "recordsTotal" =>$data["count"]
          ));
     }

     public function insertTerm(Request $req)
     {
          $p = new terms_condition();
          $data = $p->insertTerm($req->all());
          return $data;
     }

     public function editTerm(Request $req)
     {
          $p = new terms_condition();
          $data = $p->editTerm($req->all());
          return $data;
     }

     public function deleteTerm(Request $req)
     {
          $p = new terms_condition();
          $data = $p->deleteTerm($req->all());
          return $data;
     }

     public function exportDataTerms(Request $req)
     {
         $p2 = $req->pr_category_id=="null"?null:$req->pr_category_id;
          $p = new terms_condition();
          $data = $p->get_data_term([
             "term_condition_name"=>$req->nama,
             "pr_category_id"=>$p2,
             "search"=>["terms_conditions.terms_condition_name","p.pr_category_nama","terms_conditions.terms_condition_detail","terms_conditions.created_at"]
          ]);

        foreach ($data["data"] as $key => $value) {
            $value->terms_condition_detail = strip_tags($value->terms_condition_detail);
        }
        
          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["Terms & Condition Name","Category","Details","Tanggal Pembuatan"]
          ]),"Master_Terms_Condition.xls");
     }

     //Inclusion
     public function inclusion()
     {
          return view('Dashboard_Admin.Master.inclusion');
     }
     public function get_data_term(Request $req)
     {
          $p = new term();
          $data = $p->get_data_term([
             "term_name"=>$req->nama,
             "jenis"=>1,
             "pr_category_id"=>$req->pr_category_id,
             "start" => $req->start,
             "length" => $req->length
          ]);
          echo json_encode(array(
              "data" => $data["data"],
              "recordsFiltered" => $data["count"],
              "recordsTotal" =>$data["count"]
          ));
     }

     public function insertInclusion(Request $req)
     {
          $p = new term();
          $data = $p->insertTerm($req->all());
          return $data;
     }

     public function editInclusion(Request $req)
     {
          $p = new term();
          $data = $p->editTerm($req->all());
          return $data;
     }

     public function deleteInclusion(Request $req)
     {
          $p = new term();
          $data = $p->deleteTerm($req->all());
          return $data;
     }

     public function exportDataInclusion(Request $req)
     {
         $p2 = $req->pr_category_id=="null"?null:$req->pr_category_id;
          $p = new term();
          $data = $p->get_data_term([
               "term_name"=>$req->nama,
               "jenis"=>1,
               "pr_category_id"=>$p2,
               "search"=>["terms.term_name","p.pr_category_nama","term_id","terms.created_at"]
          ]);

          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["Inclusion Name","Category","Details","Tanggal Pembuatan"]
          ]),"Master_Inclusion.xls");
     }

     //Exclusion
     public function exclusion()
     {
          return view('Dashboard_Admin.Master.exclusion');
     }

     public function get_data_exclusion(Request $req)
     {
          $p = new term();
          $data = $p->get_data_term([
             "term_name"=>$req->nama,
             "jenis"=>2,
             "pr_category_id"=>$req->pr_category_id,
             "start" => $req->start,
             "length" => $req->length
          ]);
          echo json_encode(array(
              "data" => $data["data"],
              "recordsFiltered" => $data["count"],
              "recordsTotal" =>$data["count"]
          ));
     }

     public function insertExclusion(Request $req)
     {
          $p = new term();
          $data = $p->insertTerm($req->all());
          return $data;
     }

     public function editExclusion(Request $req)
     {
          $p = new term();
          $data = $p->editTerm($req->all());
          return $data;
     }

     public function deleteExclusion(Request $req)
     {
          $p = new term();
          $data = $p->deleteTerm($req->all());
          return $data;
     }

     public function exportDataExclusion(Request $req)
     {
         $p2 = $req->pr_category_id=="null"?null:$req->pr_category_id;
          $p = new term();
          $data = $p->get_data_term([
               "term_name"=>$req->nama,
               "jenis"=>2,
               "pr_category_id"=>$p2,
               "search"=>["terms.term_name","p.pr_category_nama","term_id","terms.created_at"]
          ]);

          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["Exclusion Name","Category","Details","Tanggal Pembuatan"]
          ]),"Master_Exclusion.xls");
     }

}
