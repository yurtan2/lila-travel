<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\Document;
use App\Models\Passport;
use App\Models\Visa;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TravelDocumentsController extends Controller
{

    //document
    public function document()
    {
        return view('Dashboard_Admin.Master.document');
    }

    public function get_data_document(Request $req)
    {
         $p = new Document();
         $data = $p->get_data_document([
             "document_name"=>$req->nama,
             "start" => $req->start,
             "length" => $req->length
         ]);
         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertDocument(Request $req)
    {
         $p = new Document();
         $data = $p->insertDocument($req->all());
         return $data;
    }

    public function editDocument(Request $req)
    {
         $p = new Document();
         $data = $p->updateDocument($req->all());
         return $data;
    }

    public function deleteDocument(Request $req)
    {
         $p = new Document();
         $data = $p->deleteDocument($req->all());
         return $data;
    }

    public function exportDataDocument(Request $req)
    {
         $p = new Document();
         $data = $p->get_data_document([
             "document_name"=>$req->nama,
             "search"=>["document_name","created_at"]
         ]);
         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Jenis Document","Tanggal Pembuatan"]
         ]),"Master_Jenis_Document.xls");
    }

    //passport
    public function passport()
    {
        return view('Dashboard_Admin.Master.passport');
    }

    public function get_data_passport(Request $req)
    {
         $p = new Passport();
         $data = $p->get_data_passport([
             "passport_name"=>$req->nama,
             "start" => $req->start,
             "length" => $req->length
         ]);
         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertPassport(Request $req)
    {
         $p = new Passport();
         $data = $p->insertPassport($req->all());
         return $data;
    }

    public function editPassport(Request $req)
    {
         $p = new Passport();
         $data = $p->updatePassport($req->all());
         return $data;
    }

    public function deletePassport(Request $req)
    {
         $p = new Passport();
         $data = $p->deletePassport($req->all());
         return $data;
    }

    public function exportDataPassport(Request $req)
    {
         $p = new Passport();
         $data = $p->get_data_passport([
            "passport_name"=>$req->nama,
             "search"=>["passport_name","created_at"]
         ]);

         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Jenis Passport","Tanggal Pembuatan"]
         ]),"Master_Passport.xls");
    }

    //visa
    public function visa()
    {
        return view('Dashboard_Admin.Master.visa');
    }

    public function get_data_visa(Request $req)
    {
         $p = new Visa();
         $data = $p->get_data_visa([
             "visa_name"=>$req->nama,
             "country"=>$req->country,
             "start" => $req->start,
             "length" => $req->length
         ]);
         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertVisa(Request $req)
    {
         $p = new Visa();
         $data = $p->insertVisa($req->all());
         return $data;
    }

    public function editVisa(Request $req)
    {
         $p = new Visa();
         $data = $p->updateVisa($req->all());
         return $data;
    }

    public function deleteVisa(Request $req)
    {
         $p = new Visa();
         $data = $p->deleteVisa($req->all());
         return $data;
    }

    public function exportDataVisa(Request $req)
    {
        $pi = $req->country_id=="undefined"?null:$req->country_id;
         $p = new Visa();
         $data = $p->get_data_visa([
            "visa_name"=>$req->nama,
            "country"=>$pi,
            "search"=>["c.country_name","visas.visa_name","visas.visa_detail","visas.visa_need","visas.created_at"]
         ]);
         foreach ($data["data"] as $key => $value) {
            $value->visa_detail = strip_tags($value->visa_detail);
            $list_need = json_decode($value->visa_need,true);
            $list = "";
            for ($i=1; $i <= count($list_need); $i++) {
                $list .= $list_need[$i]["document_name"];
                if($i<count($list_need)) $list.=", ";
            }
            $value->visa_need = $list;
         }
         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Country","Visa","Detail","Dokumen yang Diperlukan","Tanggal Pembuatan"]
         ]),"Master_Visa.xls");
    }
}
