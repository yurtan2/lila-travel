<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\customer;
use App\Models\customer_family_tree;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    //customer
    public function customer()
    {
         return view('Dashboard_Admin.Master.customer');
    }

    public function get_data_customer(Request $req)
    {
         $p = new customer();

         $data = $p->get_data_customer([
            "family_id" =>$req->family_id,
            "corporate_id" =>$req->corporate_id,
            "passport_status" =>$req->passport_status,
            "customer_name" =>$req->customer_name,
            "customer_nik" =>$req->customer_nik,
            "customer_passport_number" =>$req->customer_passport_number,
            "customer_nomor" =>$req->customer_nomor,
            "customer_tanggal_lahir"=>$req->customer_tanggal_lahir,
            "start" => $req->start,
            "length" => $req->length
         ]);

         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }

    public function insertCustomer(Request $req)
    {
        $data = $req->all();
        if(isset($req->ktp)&&$req->ktp!="undefined")$data["customer_ktp"] = $this->insertFile($req->ktp,"ktp");
        if(isset($req->passport)&&$req->passport!="undefined")$data["customer_passport"] = $this->insertFile($req->passport,"passport");
        if(isset($req->visa)&&$req->visa!="undefined")$data["customer_visa"] = $this->insertFile($req->visa,"visa");

        $p = new customer();
        $data = $p->insertCustomer($data);
        return $data;
    }

    public function editCustomer(Request $req)
    {
        $data = $req->all();
        if(isset($req->ktp))$data["customer_ktp"] = $this->insertFile($req->ktp,"ktp");
        if(isset($req->passport))$data["customer_passport"] = $this->insertFile($req->passport,"passport");
        if(isset($req->visa))$data["customer_visa"] = $this->insertFile($req->visa,"visa");

        $p = new customer();
        $data = $p->updateCustomer($data);
        return $data;
    }

    public function deleteCustomer(Request $req)
    {
         $p = new customer();
         $data = $p->deleteCustomer($req->all());
         return $data;
    }

    function cekCustomer(Request $req) {
          $valid=1;
          $data = $req->all();
          $c = customer::where(function ($query) use ($data) {
               $query->where('customer_first_name', '=', $data["customer_first_name"])
                     ->Where('customer_first_name', '=', $data["customer_last_name"]);
           })->orwhere("customer_nomor","=",$req->customer_nomor)->orwhere("customer_nik","=",$req->customer_nik)->get();
           if(isset($c)&&count($c)>0){
               foreach ($c as $key => $item) {
                    if($item["status"]==1)$valid=-1;
               }
           }
          return $valid;
    }

    public function exportDataCustomer(Request $req)
     {
          $p = new customer();

         $data = $p->get_data_customer([
               "customer_name" =>$req->customer_name,
               "customer_tanggal_lahir"=>$req->customer_tanggal_lahir,
               "select"=>["status","customer_first_name","customer_last_name","customer_gender","customer_nomor","customer_alamat","customer_tanggal_lahir","customer_referensi_makanan","customer_nik","customer_passport_number","customer_poi","customer_doi","customer_doe","customer_catatan","created_at","customer_id"]
          ]);
          foreach ($data["data"] as $key => $item) {
               if($item["customer_family_tree_id"]!="-")$item["status"] = "FT".str_pad($item["customer_family_tree_id"],5,STR_PAD_LEFT);
               unset($item["customer_family_tree_id"]);
               unset($item["customer_id"]);
          }
          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["Family ID","First Name","Last Name","Gender","Nomor","Alamat"," Tanggal Lahir","Referensi Makanan","NIK","Passport Number","POI","DOI","DOE","Catatan","Tanggal Pembuatan"]
          ]),"Master Displayer.xls");
     }

     //customer family tree
     public function family_tree()
     {
          return view('Dashboard_Admin.Master.customer_family_tree');
     }

     public function get_data_family_tree(Request $req)
     {
          $p = new customer_family_tree();

          $data = $p->get_data_customer_family_tree([

               "customer_name" =>$req->customer_name,
               "customer_family_id" =>$req->customer_family_id,
               "start" => $req->start,
               "length" => $req->length
          ]);

          echo json_encode(array(
               "data" => $data["data"],
               "recordsFiltered" => $data["count"],
               "recordsTotal" =>$data["count"]
          ));
     }

     public function getDetailFamilyTree(Request $req)
     {
          $p = new customer();

          $data = $p->get_data_customer([
               "customer_id" =>$req->customer_id,
          ]);

          echo $data["data"][0];
     }

     public function insertFamilyTree(Request $req)
     {
          $data = $req->all();

          $p = new customer_family_tree();
          $data = $p->insertFamilyTree($data);
          return $data;
     }

     public function editFamilyTree(Request $req)
     {
          $data = $req->all();

          $p = new customer_family_tree();
          $data = $p->updateFamilyTree($data);
          return $data;
     }

     public function deleteFamilyTree(Request $req)
     {
          $p = new customer_family_tree();
          $data = $p->deleteFamilyTree($req->all());
          return $data;
     }

     public function exportDataFamilyTree(Request $req)
     {
          $p = new customer_family_tree();

          $data = $p->get_data_customer_family_tree([
               "customer_name" =>$req->customer_name,
               "customer_tanggal_lahir"=>$req->customer_tanggal_lahir,
               "select"=>["customer_first_name","customer_last_name","customer_gender","customer_nomor","customer_alamat","customer_tanggal_lahir","customer_referensi_makanan","customer_nik","customer_passport_number","customer_poi","customer_doi","customer_doe","customer_catatan","created_at"]
          ]);
          $final = [];
          foreach ($data["data"] as $key => $value) {
               $p = new customer();
               $data2 = $p->get_data_customer([
                   "customer_id" =>$value["customer_id"],
                   "select"=>["status","updated_by","customer_first_name","customer_last_name","customer_gender","customer_nomor","customer_alamat","customer_tanggal_lahir","customer_referensi_makanan","customer_nik","customer_passport_number","customer_poi","customer_doi","customer_doe","customer_catatan","created_at","customer_id"]
                ]); 

               foreach ($data2["data"] as $key => $item) {
                    $item["updated_by"] = "Head Of Member";
                    if($item["customer_family_tree_id"]!="-")$item["status"] = "FT".str_pad($item["customer_family_tree_id"],5,STR_PAD_LEFT);
                    unset($item["customer_family_tree_id"]);     
                    unset($item["customer_id"]);
               }
               
               if(count($data2["data"])>0){
                    array_push($final,$data2["data"][0]);
                    foreach ($value->detail as $key => $detail) {
     
                         $data2 = $p->get_data_customer([
                              "customer_id" =>$detail["customer_id"],
                              "select"=>["status","updated_by","customer_first_name","customer_last_name","customer_gender","customer_nomor","customer_alamat","customer_tanggal_lahir","customer_referensi_makanan","customer_nik","customer_passport_number","customer_poi","customer_doi","customer_doe","customer_catatan","created_at","customer_id"]
                        ]);
                        foreach ($data2["data"] as $key => $item) {
                             if($item["customer_family_tree_id"]!="-")$item["status"] = "FT".str_pad($item["customer_family_tree_id"],5,STR_PAD_LEFT);
                             $item["updated_by"] = $detail["customer_family_tree_detail_position"];
                             unset($item["customer_family_tree_id"]);
                             unset($item["customer_id"]);
                        }
     
                        if(sizeof($data2["data"])>0)array_push($final,$data2["data"][0]);
                    }     
               }
               
          }
          return Excel::download(new MasterExport([
               "data"=>$data["data"],
               "header"=>["Family ID","Member Type","First Name","Last Name","Gender","Nomor","Alamat"," Tanggal Lahir","Referensi Makanan","NIK","Passport Number","POI","DOI","DOE","Catatan","Tanggal Pembuatan"]
          ]),"Master Family Tree.xls");
     }


     //insert
    public function insertFile($file,$type)
    {
          try {
               $fileName = uniqid().'.'.$file->extension();
               $filePath = 'assets/'.$type."/".$fileName;
               $file->move(public_path('assets/'.$type), $fileName);
               return $filePath;
          } catch (\Throwable $th) {
               return -1;
          }
    }


    //dashboard customer
    function dashboard() {
          return view('Dashboard_Customer/dashboard_customer');
    }

    function profile_customer() {
          return view("Dashboard_Customer.profile");
    }
}
