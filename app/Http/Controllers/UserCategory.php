<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\userCategory as ModelsUserCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserCategory extends Controller
{
     public function user_category()
     {
          return view('Dashboard_Admin.Master.user_category');
     }
  
     public function get_data_user_category(Request $req)
     {
          $p = new ModelsUserCategory();
          $data = $p->get_data_user_category([
              "user_category_name"=>$req->nama,
              "start" => $req->start,
              "length" => $req->length
          ]);
          echo json_encode(array(
              "data" => $data["data"],
              "recordsFiltered" => $data["count"],
              "recordsTotal" =>$data["count"]
          ));
     }
  
     public function insertUserCategory(Request $req)
     {
          $p = new ModelsUserCategory();
          $data = $p->insertUserCategory($req->all());
          return $data;
     }
  
     public function editUserCategory(Request $req)
     {
          $p = new ModelsUserCategory();
          $data = $p->editUserCategory($req->all());
          return $data;
     }
  
     public function deleteUserCategory(Request $req)
     {
          $p = new ModelsUserCategory();
          $data = $p->deleteUserCategory($req->all());
          return $data;
     }
  
     public function exportDataUserCategory(Request $req)
     {
          $p = new ModelsUserCategory();
          $data = $p->get_data_user_category([
            "user_category_name"=>$req->nama,
            "search"=>["user_category_name","created_at"]
          ]);
  
          return Excel::download(new MasterExport([
              "data"=>$data["data"],
              "header"=>["User Category Name","Tanggal Pembuatan"]
          ]),"Master_Category_Name.xls");
     }
  
}
