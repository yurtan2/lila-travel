<?php

namespace App\Http\Controllers;

use App\Exports\MasterExport;
use App\Models\worker;
use Faker\Core\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;

class WorkerController extends Controller
{
    public function worker()
    {
         return view('Dashboard_Admin.Master.worker');
    }
 
    public function get_data_worker(Request $req)
    {
         $p = new worker();
         $data = $p->get_data_worker([
             "worker_kode" =>$req->kode,
             "worker_name" =>$req->nama,
             "worker_type" =>$req->type,
             "start" => $req->start,
             "length" => $req->length
         ]);

         echo json_encode(array(
             "data" => $data["data"],
             "recordsFiltered" => $data["count"],
             "recordsTotal" =>$data["count"]
         ));
    }
 
    public function insertWorker(Request $req)
    {
         $data = $req->all();
         if(isset($req->ktp))$data["ktp_url"] = $this->insertFile($req->ktp,"ktp");
         if(isset($req->photo))$data["photo_url"] = $this->insertFile($req->photo,"photo");
         if(isset($req->passport))$data["passport_url"] = $this->insertFile($req->passport,"passport");
         if(isset($req->drive))$data["drive_url"] = $this->insertFile($req->drive,"drive");
         if(isset($req->guide))$data["guide_url"] = $this->insertFile($req->guide,"guide");
         $p = new worker();
         $data = $p->insertWorker($data);
         return $data;
    }
 
    public function editWorker(Request $req)
    {
          $data = $req->all();
          if(isset($req->ktp))$data["ktp_url"] = $this->insertFile($req->ktp,"ktp");
          if(isset($req->photo))$data["photo_url"] = $this->insertFile($req->photo,"photo");
          if(isset($req->passport))$data["passport_url"] = $this->insertFile($req->passport,"passport");
          if(isset($req->drive))$data["drive_url"] = $this->insertFile($req->drive,"drive");
          if(isset($req->guide))$data["guide_url"] = $this->insertFile($req->guide,"guide");
         $p = new worker();
         $data = $p->editWorker($data);
         return $data;
    }
 
    public function deleteWorker(Request $req)
    {
         $p = new worker();
         $data = $p->deleteWorker($req->all());
         return $data;
    }
 
    public function exportDataWorker(Request $req)
    {
         $p = new worker();
         $data = $p->get_data_worker([
            "worker_kode" =>$req->id,
            "worker_name" =>$req->nama,
            "worker_type" =>$req->type,
           "search"=>["worker_name","worker_kode","worker_email","worker_phone","worker_address","worker_language1","worker_language2","worker_language3","created_at","updated_at"]
         ]);

         return Excel::download(new MasterExport([
             "data"=>$data["data"],
             "header"=>["Patner Name","Patner ID","Patner Email","Patner Phone","Patner address","Patner Language 1","Patner Language 2","Patner Language 3","Tanggal Pembuatan","Last Update"]
         ]),"Patner.xls");
    }

    public function insertFile($file,$type)
    {
          try {
               $fileName = uniqid().'.'.$file->extension();  
               $filePath = 'assets/'.$type."/".$fileName;  
               $file->move(public_path('assets/'.$type), $fileName);
               return $filePath;
          } catch (\Throwable $th) {
               return -1;
          }
    }

     public function downloadDocument(Request $req)
     {
        return response()->download(public_path($req->file_name));
     }
}
