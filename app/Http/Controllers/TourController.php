<?php

namespace App\Http\Controllers;

use App\Models\displayer_detail;
use App\Models\tour;
use App\Models\tour_departure;
use App\Models\tour_destination;
use App\Models\tour_galery;
use App\Models\tour_keyword;
use App\Models\tour_term;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Mockery\Undefined;

class TourController extends Controller
{
    function DD(Request $req)
    {
        dd($req->all());
    }

    function fix_departure()
    {   
        return view('Dashboard_Admin.Produk.Tour.fix_departure');
    }

    function fix_departure_tambah_baru()
    {
        $param["mode"]=1;
        return view('Dashboard_Admin.Produk.Tour.fix_departure_tambah')->with($param);
    }

    function fit_tour()
    {
        return view('Dashboard_Admin.Produk.Tour.fit_tour');
    }

    function fit_tour_tambah_baru()
    {
        $param["mode"]=1;
        return view('Dashboard_Admin.Produk.Tour.fit_tour_tambah')->with($param);
    }

    //fix
    function get_data_fix(Request $req) {
        $p = new tour();
        $data = $p->get_data_tour([
           "tour_name"=>$req->tour_name,
           "date"=>$req->date,
           "tour_jenis"=>1,
           "start" => $req->start,
           "length" => $req->length
        ]);

        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
    }

    function detailFixDepature($id) {
        $t = new tour();
        $param["tour"]= $t->get_detail_tour($id);
        $param["mode"]=2;
        return view('Dashboard_Admin.Produk.Tour.fix_departure_tambah')->with($param);
    }


    function insertFixDepature(Request $req) {
        $data = $req->all();
        $remove = ["validity","term_condition","_token","tour_destination","tour_galery","tour_inclusion","tour_exclusion","tour_term_condition"];
        $data_insert = array_diff_key($data, array_flip($remove));
        $data_insert["tour_cover"] = $this->insertFile($req->tour_cover,"cover");
        $data_insert["tour_banner"] = $this->insertFile($req->tour_banner,"banner");

        $t = new tour();
        $tour_id = $t->insertTour($data_insert);
        if($tour_id==-1) return ["error_code"=>"500","msg"=>"Terjadi Kesalahan pada sistem!"];

        //insert ke displayer
        $d = new displayer_detail();
        if($data_insert["displayer_id"]!="null"&&$data_insert["displayer_id"]!=null){
            $d->InsertDisplayerDetail([[
                "tour_id"=>$tour_id
            ]],$data_insert["displayer_id"]);
        }
       
       
        //insert ke destinasi
        $temp = json_decode($req->tour_destination,true);
        $d = new tour_destination();
        $d->insertTourDestination($temp,$tour_id);

        //insert Keyword
        foreach (json_decode($req->keyword_id,true) as $key => $item) {
            $temp = [];
            $temp["tour_id"]=$tour_id;
            $temp["keyword_id"]=$item;
            $tr = new tour_keyword();
            $tr->insertTourKeyword($temp);
        }

        //insert foto galery
        if(isset($req->tour_galery)){
            foreach ($req->tour_galery as $key => $value) {
                $path = $this->insertFile($value,"galery");
                $tg = new tour_galery();
                $tg->insertGalery([
                    "tour_id"=>$tour_id,
                    "tour_images"=>$path,
                ]);
            }    
        }

        //insert inclusion
        if(isset($req->tour_inclusion)){
            foreach (json_decode($req->tour_inclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 1,
                    "tour_terms_jenis_id" => $value["id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }

        //insert exclusion
        if(isset($req->tour_exclusion)){
            foreach (json_decode($req->tour_exclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 2,
                    "tour_terms_jenis_id" => $value["id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }
        
        //insert term_condition
        if(isset($req->tour_term_condition)){
            foreach (json_decode($req->tour_term_condition,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 3,
                    "tour_terms_jenis_id" => $value["id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }
        
        //insert Departure (price)
        foreach (json_decode($req->validity,true) as $key => $item) {
            $item["tour_id"]=$tour_id;
            $item["tour_jenis"]=1;
            $tr = new tour_departure();
            $tr->insertTourDeparture($item);
        }
        
    }

    function updateFixDepature(Request $req) {
        $data = $req->all();
        $tour_id = $req->tour_id;
        $remove = ["validity","term_condition","_token","displayer_detail_id","tour_destination","tour_galery","tour_inclusion","tour_exclusion","tour_term_condition","inclusion_id","exclusion_id","term_condition_id"];
        $data_insert = array_diff_key($data, array_flip($remove));
        
        if($req->tour_cover!="undefined") $data_insert["tour_cover"] = $this->insertFile($req->tour_cover,"cover");
        else unset($data_insert["tour_cover"]);
        if($req->tour_banner!="undefined") $data_insert["tour_banner"] = $this->insertFile($req->tour_banner,"banner");
        else unset($data_insert["tour_banner"]);
       // $data_insert["status"]=1;//dikembalikan active karena sudah ada pengecekan expired ketika query
        
        $t = new tour();
        $t->updateTour($data_insert);
        
        if($tour_id==-1) return ["error_code"=>"500","msg"=>"Terjadi Kesalahan pada sistem!"];
       
        //insert ke displayer
        if($data_insert["displayer_id"]!="null"&&$data_insert["displayer_id"]!=null){
        
            $d = displayer_detail::find($req->displayer_detail_id);
            $d->displayer_id =  $req->displayer_id;
            $d->save();
        }
        
      
        //insert ke destinasi
        $temp = json_decode($req->tour_destination,true);
        $d = new tour_destination();
        $d->updateTourDestination($temp,$tour_id);
        
        //update dan insert(bila ada) Keyword
        $temp = [];
        $temp["tour_id"]=$tour_id;
        $temp["keyword_id"]=json_decode($req->keyword_id,true);
        $tr = new tour_keyword();
        $tr->updateTourKeyword($temp);

        
        //update foto galery
        if(isset($req->tour_galery)){
            foreach ($req->tour_galery as $key => $value) {
                $path = $this->insertFile($value,"galery");
                $tg = new tour_galery();
                $tg->insertGalery([
                    "tour_id"=>$tour_id,
                    "tour_images"=>$path,
                ]);
            }    
        }
      
        //update inclusion
     
        if(isset($req->tour_inclusion)){
            $tr = new tour_term();
            $tr->updateTerm(json_decode($req->tour_inclusion,true),$tour_id);
        }
        //update exclusion
        if(isset($req->tour_exclusion)){
            $tr = new tour_term();
            $tr->updateTerm(json_decode($req->tour_exclusion,true),$tour_id);
        }
       
        //update term_condition
        if(isset($req->tour_term_condition)){
            $tr = new tour_term();
            $tr->updateTerm(json_decode($req->tour_term_condition,true),$tour_id);
        }
        //update Departure (price)
        $v = new Collection();
        $tr = new tour_departure();
     
        foreach (json_decode($req->validity,true) as $key => $item) {
            $item["tour_jenis"]=1;
            $v->push((object)$item);
            //array_push($v,$item);
        }
        $tr->updateTourDeparture($v,$tour_id);
    }

    function insertFitDepature(Request $req) {
        $data = $req->all();
        $data["tour_air_ticket"] = $data["tour_flight"];

        $remove = ["validity","tour_flight","term_condition","_token","tour_destination","tour_galery","tour_inclusion","tour_exclusion","tour_term_condition"];
        $data_insert = array_diff_key($data, array_flip($remove));
        
       
        if($req->tour_cover!="undefined") $data_insert["tour_cover"] = $this->insertFile($req->tour_cover,"cover");
        else unset($data_insert["tour_cover"]);
        if($req->tour_banner!="undefined") $data_insert["tour_banner"] = $this->insertFile($req->tour_banner,"banner");
        else unset($data_insert["tour_banner"]);

        $t = new tour();
        $tour_id = $t->insertTour($data_insert);
        if($tour_id==-1) return ["error_code"=>"500","msg"=>"Terjadi Kesalahan pada sistem!"];

        //insert ke displayer
        $d = new displayer_detail();
        if($data_insert["displayer_id"]!="null"&&$data_insert["displayer_id"]!=null){
            $d->InsertDisplayerDetail([[
                "tour_id"=>$tour_id
            ]],$data_insert["displayer_id"]);
        }
        //insert ke destinasi
        $temp = json_decode($req->tour_destination,true);
        
        $d = new tour_destination();
        $d->insertTourDestination($temp,$tour_id);

        //insert Keyword
        foreach (json_decode($req->keyword_id,true) as $key => $item) {
            $temp = [];
            $temp["tour_id"]=$tour_id;
            $temp["keyword_id"]=$item;
            $tr = new tour_keyword();
            $tr->insertTourKeyword($temp);
        }

        //insert foto galery
        if(isset($req->tour_galery)){
            foreach ($req->tour_galery as $key => $value) {
                $path = $this->insertFile($value,"galery");
                $tg = new tour_galery();
                $tg->insertGalery([
                    "tour_id"=>$tour_id,
                    "tour_images"=>$path,
                ]);
            }    
        }

        //insert inclusion
        if(isset($req->tour_inclusion)){
            foreach (json_decode($req->tour_inclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 1,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }

        //insert exclusion
        if(isset($req->tour_exclusion)){
            foreach (json_decode($req->tour_exclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 2,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }
        
        //insert term_condition
        if(isset($req->tour_term_condition)){
            foreach (json_decode($req->tour_term_condition,true) as $key => $value) {
               $tr = new tour_term();
               $tr->insertTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 3,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ]);
            }
        }

        //insert Departure (price)
        foreach (json_decode($req->validity,true) as $keys => $item) {
            $validity = [//yang sudah diformat sesuai database
                "tour_start_date"=>$item["tour_start_date"][0],
                "tour_end_date"=>$item["tour_start_date"][0],
                "tour_id"=>$tour_id,
                "tour_jenis"=>$req->tour_jenis,
                "tour_departure_name"=>$item["hotel_name"],
                "tour_departure_deposit"=>$item["deposit_amount"][0],
                "black_out"=>json_decode($item["black_out"],true),
                "price"=>[]
            ];
            
            foreach ($item["room"] as $key => $value) {
                $price = [
                    "tour_departure_detail_jenis"=>1,
                    "tour_departure_detail_name"=>$value[0],
                    "tour_departure_detail_price"=>isset($item["price"][$key])?$item["price"][$key][0]:0,
                    "tour_departure_detail_pax_2"=>isset($item["pax_2"][$key])?$item["pax_2"][$key][0]:0,
                    "tour_departure_detail_pax_5"=>isset($item["pax_5"][$key])?$item["pax_5"][$key][0]:0,
                    "tour_departure_detail_pax_9"=>isset($item["pax_5"][$key])?$item["pax_9"][$key][0]:0,
                ];
                array_push($validity['price'],$price);
            }
            foreach ($item["add_on_type"] as $key => $value) {
               
                $price = [
                    "tour_departure_detail_jenis"=>2,
                    "tour_departure_detail_name"=>$value[0],
                    "tour_departure_detail_price"=>isset($item["add_on_price"][$key])?$item["add_on_price"][$key][0]:0,
                    "tour_departure_detail_total"=>isset($item["add_on_price"][$key])?$item["add_on_price"][$key][0]:0,
                ];
                array_push($validity['price'],$price);
            }
            $tr = new tour_departure();
            $tr->insertTourDeparture($validity);
        }
    }

    function updateFitDepature(Request $req){
        $data = $req->all();
        $data["tour_air_ticket"] = $data["tour_flight"];

        $remove = ["validity","tour_flight","displayer_detail_id","term_condition","_token","tour_destination","tour_galery","tour_inclusion","tour_exclusion","tour_term_condition"];
        $data_insert = array_diff_key($data, array_flip($remove));
        
       
        if($req->tour_cover!="undefined") $data_insert["tour_cover"] = $this->insertFile($req->tour_cover,"cover");
        else unset($data_insert["tour_cover"]);
        if($req->tour_banner!="undefined") $data_insert["tour_banner"] = $this->insertFile($req->tour_banner,"banner");
        else unset($data_insert["tour_banner"]);

        $t = new tour();
        $tour_id = $t->updateTour($data_insert);
        if($tour_id==-1) return ["error_code"=>"500","msg"=>"Terjadi Kesalahan pada sistem!"];

        //insert ke displayer
        $d = displayer_detail::find($req->displayer_detail_id);
        if($data_insert["displayer_id"]!="null"&&$data_insert["displayer_id"]!=null){
            $d->displayer_id =  $data_insert["displayer_id"];
        }

        //insert ke destinasi
        $temp = json_decode($req->tour_destination,true);
        
        $d = new tour_destination();
        $d->updateTourDestination($temp,$tour_id);

        //insert Keyword
        $temp = [];
        $temp["tour_id"]=$tour_id;
        $temp["keyword_id"]=json_decode($req->keyword_id,true);
        $tr = new tour_keyword();
        $tr->updateTourKeyword($temp);

        //insert foto galery
        if(isset($req->tour_galery)){
            foreach ($req->tour_galery as $key => $value) {
                $path = $this->insertFile($value,"galery");
                $tg = new tour_galery();
                $tg->insertGalery([
                    "tour_id"=>$tour_id,
                    "tour_images"=>$path,
                ]);
            }    
        }
   
        if(isset($req->tour_inclusion)){
            $dt = [];
            foreach (json_decode($req->tour_inclusion,true) as $key => $value) {
                $tr = new tour_term();
                array_push($dt,[
                     "tour_terms_date" => json_encode($value["tour_terms_date"]),
                     "tour_id" => $tour_id,
                     "tour_terms_jenis" => 1,
                     "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                     "tour_terms_value" => $value["tour_terms_value"]
                ]);
             }
             
            $tr = new tour_term();
            $tr->updateTerm($dt,$req->tour_id);
        }
        
        //update exclusion
        if(isset($req->tour_exclusion)){
            $dt = [];
            foreach (json_decode($req->tour_exclusion,true) as $key => $value) {
                $tr = new tour_term();
                array_push($dt,[
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 2,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
                ]);
             }
            $tr = new tour_term();
            $tr->updateTerm($dt,$req->tour_id);
        }
       
        //update term_condition
        if(isset($req->tour_term_condition)){
            $dt = [];
            foreach (json_decode($req->tour_exclusion,true) as $key => $value) {
                $tr = new tour_term();
                array_push($dt,[
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 3,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
                ]);
             }
            $tr = new tour_term();
            $tr->updateTerm($dt,$tour_id);
        }
        /*
        //insert inclusion
        if(isset($req->tour_inclusion)){
            foreach (json_decode($req->tour_inclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->updateTerms([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 1,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ],$data["tour_id"]);
               
            }
        }

        //insert exclusion
        if(isset($req->tour_exclusion)){
            foreach (json_decode($req->tour_exclusion,true) as $key => $value) {
               $tr = new tour_term();
               $tr->updateTerm([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 2,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ],$data["tour_id"]);
            }
        }
        
        //insert term_condition
        if(isset($req->tour_term_condition)){
            foreach (json_decode($req->tour_term_condition,true) as $key => $value) {
               $tr = new tour_term();
               $tr->updateTerm([
                    "tour_terms_date" => json_encode($value["tour_terms_date"]),
                    "tour_id" => $tour_id,
                    "tour_terms_jenis" => 3,
                    "tour_terms_jenis_id" => $value["tour_terms_jenis_id"],
                    "tour_terms_value" => $value["tour_terms_value"]
               ],$data["tour_id"]);
            }
        }*/
        $va = [];
        //insert Departure (price)
        foreach (json_decode($req->validity,true) as $keys => $item) {
            $validity = [//yang sudah diformat sesuai database
                "tour_start_date"=>$item["tour_start_date"][0],
                "tour_departure_id"=>$item["tour_departure_id"][0],
                "tour_end_date"=>$item["tour_start_date"][0],
                "tour_id"=>$tour_id,
                "tour_jenis"=>$req->tour_jenis,
                "tour_departure_name"=>$item["hotel_name"],
                "tour_departure_deposit"=>$item["deposit_amount"][0],
                "black_out"=>json_decode($item["black_out"],true),
                "price"=>[],
                "black_out"=>[]
            ];
            
            foreach ($item["room"] as $key => $value) {
                $price = [
                    "tour_jenis"=>2,
                    "tour_departure_detail_jenis"=>1,
                    "tour_departure_detail_name"=>$value[0],
                    "tour_departure_detail_price"=>isset($item["price"][$key])?$item["price"][$key][0]:0,
                    "tour_departure_detail_pax_2"=>isset($item["pax_2"][$key])?$item["pax_2"][$key][0]:0,
                    "tour_departure_detail_pax_5"=>isset($item["pax_5"][$key])?$item["pax_5"][$key][0]:0,
                    "tour_departure_detail_pax_9"=>isset($item["pax_5"][$key])?$item["pax_9"][$key][0]:0,
                ];
                array_push($validity['price'],$price);
            }
            foreach ($item["add_on_type"] as $key => $value) {
               
                $price = [
                    "tour_jenis"=>2,
                    "tour_departure_detail_jenis"=>2,
                    "tour_departure_detail_name"=>$value[0],
                    "tour_departure_detail_price"=>isset($item["add_on_price"][$key])?$item["add_on_price"][$key][0]:0,
                    "tour_departure_detail_total"=>isset($item["add_on_price"][$key])?$item["add_on_price"][$key][0]:0,
                ];
                array_push($validity['price'],$price);
            }

            foreach (json_decode($item["black_out"]) as $key => $value) {
            
                $black_out = [
                    "date_start"=>$value->date_start,
                    "date_end"=>$value->date_end,
                    "remark"=>$value->remark,
                ];
                if(isset($value->tour_black_out_id)) $black_out["tour_black_out_id"] = $value->tour_black_out_id;
                array_push($validity['black_out'],$black_out);
            }
           
            array_push($va,$validity);
        }
        $tr = new tour_departure();
        $tr->updateFITTourDeparture($va,$req->tour_id);
        
    }

    //fit
    function get_data_fit(Request $req) {
        $p = new tour();
        $data = $p->get_data_tour([
           "tour_name"=>$req->tour_name,
           "date"=>$req->date,
           "tour_jenis"=>2,
           "start" => $req->start,
           "length" => $req->length
        ]);

        echo json_encode(array(
            "data" => $data["data"],
            "recordsFiltered" => $data["count"],
            "recordsTotal" =>$data["count"]
        ));
    }

    function detailFitTour($id) {
        $t = new tour();
        $param["tour"]= $t->get_detail_tour($id);
        $param["mode"]=2;
        $final = [];
  
        foreach ($param["tour"]["tour_validity"] as $key => $item) {
      
            $ada=-1;
            foreach ($final as $keys => $value) {
                if($value["tour_departure_name"]==$item["tour_departure_name"]){
                    array_push($final[$keys],$item);
                    $ada=1;
                }
            }
            if($ada==-1){
                array_push($final,[
                    "tour_departure_name"=>$item["tour_departure_name"],
                    "item"=>array($item)
                ]);
            }
            /*
            if(array_key_exists($value["tour_departure_name"],$final)){
                $ada=1;
                array_push($final[$value["tour_departure_name"]],$value);
            }
            else{
                $final[$value["tour_departure_name"]] = [];
                array_push($final[$value["tour_departure_name"]],$value);
            }*/
        }
       
        $param["tour"]["tour_validity"] = $final;
     
        return view('Dashboard_Admin.Produk.Tour.fit_tour_tambah')->with($param);
    }

    //general 
    function deleteTour(Request $req) {
        $t = new tour();
        $t->deleteTour($req->all());
    }

    //lain lain
    public function insertFile($file,$type)
    {
          try {
               $fileName = uniqid().'.'.$file->extension();  
               $filePath = 'assets/'.$type."/".$fileName;  
               $file->move(public_path('assets/'.$type), $fileName);
               return $filePath;
          } catch (\Throwable $th) {
               return -1;
          }
    }


}
