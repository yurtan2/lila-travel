<?php

namespace App\Http\Controllers;

use App\Models\reservation_tailor;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function booking_tour()
    {
         return view('Dashboard_Admin.Booking.tour');
    }

    public function tambah_booking($id)
    {
        return view('Dashboard_Admin.Master.displayer_detail');
    }

    public function reservation_fit() {
        return view('Dashboard_Admin.Booking.reservation_fit');
    }

    public function reservation_fix() {
        return view('Dashboard_Admin.Booking.reservation_fix');
    }

    public function reservation_tailor() {
        return view('Dashboard_Admin.Booking.reservation_tailor');
    }

    public function insertReservationTailor(Request $req) {

        $t = new reservation_tailor();
        $t->insertReservationTailor($req->all());
    }

    public function get_data_hotel_service(Request $req)
    {
        $p = new reservation_tailor();
        $data = $p->get_reservation_tailor([
            "reservation_tailor_status"=>$req->reservation_tailor_status,
            "start" => $req->start,
            "length" => $req->length
        ]);

        echo json_encode(array(
          "data" => $data["data"],
          "recordsFiltered" => $data["count"],
          "recordsTotal" =>$data["count"]
      ));
    }
}
