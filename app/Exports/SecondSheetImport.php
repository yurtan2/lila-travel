<?php

namespace App\Exports;

use App\Models\Country;
use App\Models\SupplierCategory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SecondSheetImport implements FromCollection, WithHeadings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $ct = Country::all();
        $sp = SupplierCategory::where("status","=",1)->select("sp_category_nama")->get();
        $c = new Collection([]);

        $length = count($ct);
        if($length<count($sp))$length = count($sp);
        
        for ($i=0; $i < $length; $i++) { 
            $c_name = $c_supplier =  "";
            if(isset($ct[$i]->name)) $c_name = $ct[$i]->name;
            if(isset($sp[$i]->sp_category_nama)) $c_supplier = $sp[$i]->sp_category_nama;

            $temp = [
                "location"=>$c_name,
                "a"=>"",
                "b"=>"",
                "Supplier"=>$c_supplier,
            ];
            $c->push($temp);
            
        }
        return $c;
    }

    public function headings(): array
    {
        return ["Location (Country)","","","Supplier Category"];
    }
}
