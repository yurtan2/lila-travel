<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class FirstSheetImport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents
{
    public function collection()
    {
        return new Collection();
    }
    
    public function headings(): array
    {
        return ["Supplier Name","Phone No.","Email","Supplier Category","Location","Address","Bank Name","Account Number","SWIFT Code","Currency"
            ,"Supplier Name*","Supplier Email*","Supplier Jabatan*","","* Supplier Lebih dari 1 bisa dipisahkan dengan ' ; ' untuk setiap datanya"];
    }
    
    public function columnWidths(): array
    {
          return [
             'A' => 25,
             'B' => 15,
             'C' => 20,
             'D' => 20,
             'F' => 25,
             'G' => 15,
             'H' => 20,
             'I' => 15,
             'J' => 15,
             'K' => 25,
             'L' => 25,
             'M' => 25,
         ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
   
                $event->sheet->getDelegate()->getStyle('A1:M1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
   
            },
        ];
    }
}
