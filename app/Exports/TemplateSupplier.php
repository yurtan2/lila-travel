<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TemplateSupplier implements WithMultipleSheets, ShouldAutoSize
{
    public function sheets(): array
    {
        return [
            "Data Supplier"=> new FirstSheetImport(),
            "Category Supplier & Location" => new SecondSheetImport(),
        ];
    }
}
