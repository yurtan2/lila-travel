<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MasterExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $data;

    public function __construct($data)
    {
        $this->data=$data;
    }

    public function collection()
    {
        foreach ($this->data["data"] as $key => $item) {
            if(isset($item->created_at)){
                 $dt = strtotime($item->created_at);
                 unset($item->created_at);
                 $item->tanggal_pembuatan = strval(date ("Y/d/m H:i", $dt));
            }
            if(isset($item->updated_at)){
                $dt = strtotime($item->updated_at);
                unset($item->updated_at);
                $item->last_update = strval(date ("Y/d/m H:i", $dt));
           }
         }
        return $this->data["data"];
    }

    public function headings(): array
    {
        return $this->data["header"];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
   
                $event->sheet->getDelegate()->freezePane('A2');
   
                $event->sheet->getDelegate()->getStyle('A:Z')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
   
            },
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

        ];
    }

}
