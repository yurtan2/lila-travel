<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class term extends Model
{
    use HasFactory;
    protected $table ="terms";
    protected $primaryKey ="term_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_term($data=[])
    {
        $data = array_merge(array(
            "term_name" =>null,
            "jenis" =>1,//1=inclusion,2=exclusion
            "search"=>null,
            "pr_category_id"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = term::where('terms.status','=',1);
        if($data["term_name"]) $pc->where("terms.term_name","like","%".$data["term_name"]."%");
        if($data["pr_category_id"]) $pc->where("terms.pr_category_id","like","%".$data["pr_category_id"]."%");
        if($data["jenis"]) $pc->where("terms.term_jenis","=",$data["jenis"]);
        
        $pc->leftjoin('product_categories as p','p.pr_category_id','terms.pr_category_id');
        if($data["search"])$pc->select($data["search"]);
        else $pc->select("p.pr_category_id","p.pr_category_nama","terms.*");

        $result =  $pc->get();
        $total_rows =$pc->count();

        foreach ($result as $key => $item) {
            $item->detail = term_detail::where("status","=",1)->where('term_id',"=",$item->term_id)->select('term_detail as inclusion_detail','term_detail_id')->get();     
        }
        if($data["search"]){ 
            //export 
            foreach ($result as $key => $value) {
                $txt = "";
                for ($i=0; $i < count($value->detail); $i++) { 
                    $txt.= $value->detail[$i]["inclusion_detail"];
                    if($i<count($value->detail)-1)$txt .= ", ";
                    $item->detail_text = $txt;
                }
                unset($value->term_id);
                unset($value->detail);
            }
        }
        
        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$result,
            "count"=>$total_rows
        ];
    }

    public function insertTerm($data)
    {
        $count_data =  term::where('status','=',1)->where('term_name','=',$data["nama"])->where('term_jenis','=',$data["jenis"])->count();
        if($count_data>0) return -1;
        $pc = new term();
        $pc->term_name = $data["nama"];
        $pc->term_jenis = $data["jenis"];
        $pc->pr_category_id = $data["pr_category_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
       
        foreach (json_decode($data["detail"],true) as $key => $item) {
            $d = new term_detail();
            $dt = [
                "term_detail"=>$item["inclusion_detail"],
                "term_id"=>$pc->term_id
            ];
            $d->insertDetailTerm($dt);
        }

        return 1;
    }

    public function editTerm($data)
    {
        $count_data =  term::where('status','=',1)->where('term_id','!=',$data["term_id"])->where('term_name','=',$data["nama"])->where('term_jenis','=',$data["jenis"])->count();
        if($count_data>0) return -1;
        $pc = term::find($data["term_id"]);
        $pc->term_name = $data["nama"];
        $pc->pr_category_id = $data["pr_category_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
       
        foreach (json_decode($data["detail"],true) as $key => $item) {
            $d = new term_detail();
            if($item["term_detail_id"]=="-"){
                $dt = [
                    "term_detail"=>$item["inclusion_detail"],
                    "term_id"=>$pc->term_id
                ];
                $d->insertDetailTerm($dt);
            }
        }
        foreach (json_decode($data["list_delete"],true) as $key => $item) {
            $d = new term_detail();
            $d->deleteDetailTerm([
                "term_detail_id"=>$item["term_detail_id"]
            ]);
        }
        return 1;
    }

    public function deleteTerm($data)
    {
        $pc = term::find($data["term_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
