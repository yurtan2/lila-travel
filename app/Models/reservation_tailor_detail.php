<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reservation_tailor_detail extends Model
{
    use HasFactory;
    protected $table ="reservation_tailor_details";
    protected $primaryKey ="reservation_tailor_detail_id";
    public $timestamps = true;
    public  $incrementing = true;
   
    function insertDetailTailor($data){
        $d = new reservation_tailor_detail(); 
        $d->reservation_tailor_id = $data["reservation_tailor_id"]; 
        $d->reservation_tailor_detail_category = $data["reservation_tailor_detail_category"]; 
        $d->reservation_tailor_detail_customer_id = $data["reservation_tailor_detail_customer_id"];
        $d->reservation_tailor_detail_first_name = $data["reservation_tailor_detail_first_name"];
        $d->reservation_tailor_detail_last_name = $data["reservation_tailor_detail_last_name"];
        $d->reservation_tailor_detail_nik = $data["reservation_tailor_detail_nik"];
        $d->reservation_tailor_detail_nomor = $data["reservation_tailor_detail_nomor"];
        $d->reservation_tailor_detail_tanggal_lahir = $data["reservation_tailor_detail_tanggal_lahir"];
        $d->reservation_tailor_detail_umur = explode(" ",$data["reservation_tailor_detail_umur"])[0];
        $d->reservation_tailor_detail_address = $data["reservation_tailor_detail_address"];
        $d->reservation_tailor_detail_referensi_makan = $data["reservation_tailor_detail_referensi_makan"];
        $d->reservation_tailor_detail_passport_number = $data["reservation_tailor_detail_passport_number"];
        $d->reservation_tailor_detail_poi = $data["reservation_tailor_detail_poi"];
        $d->reservation_tailor_detail_doe = $data["reservation_tailor_detail_doe"];
        if(isset($data["reservation_tailor_detail_ktp"]))$d->reservation_tailor_detail_ktp = $data["reservation_tailor_detail_ktp"];
        if(isset($data["reservation_tailor_detail_passport"]))$d->reservation_tailor_detail_passport = $data["reservation_tailor_detail_passport"];
        $d->reservation_tailor_detail_note = $data["reservation_tailor_detail_note"];
        $d->save();
    }
}
