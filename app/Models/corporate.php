<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class corporate extends Model
{
    use HasFactory;
    protected $table ="corporates";
    protected $primaryKey ="corporate_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_corporate($data=[])
    {
        $data = array_merge(array(
            "corporate_name" =>null,
            "corporate_nomor" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = corporate::where('status','=',1);
        if($data["corporate_name"]) $pc->where("corporate_name","like","%".$data["corporate_name"]."%");
        if($data["corporate_nomor"]) $pc->where(DB::raw("CONCAT(corporate_prefix,corporate_nomor)"),"like","%".$data["corporate_nomor"]."%");
        if($data["select"]) $pc->select($data["select"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();

        foreach ($data as $key => $value) {
            $q = corporate_pic::where("corporate_pics.corporate_id",'=',$value->corporate_id)->where('corporate_pics.status','=',1);
            $value->corporate_nomor_full =  $value->corporate_prefix.$value->corporate_nomor;
            $value->total_pic = $q->count();
            $value->detail_pic = $q->leftjoin('customers as c','c.customer_id','corporate_pics.customer_id')->leftjoin('corporate_position as cp','cp.corporate_position_id','corporate_pics.jabatan_id')->get();
        }
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertCorporate($data)
    {
        $count_data =  corporate::where('status','=',1)->where('corporate_name','=',$data["corporate_name"])->count();
        if($count_data>0) return -1;
        $pc = new corporate();
        $pc->corporate_name = $data["corporate_name"];
        $pc->corporate_email = $data["corporate_email"];
        $pc->corporate_address = $data["corporate_address"];
        $pc->corporate_prefix = $data["corporate_prefix"];
        $pc->corporate_nomor = $data["corporate_nomor"];
        $pc->corporate_admin_fee = $data["corporate_admin_fee"];
        $pc->corporate_hide_logo = $data["corporate_hide_logo"];
        $pc->corporate_use_own_logo = $data["corporate_own_logo"];
        if(isset($data["corporate_logo"]))$pc->corporate_logo = $data["corporate_logo"];
        if(isset($data["corporate_akta"]))$pc->corporate_akta = $data["corporate_akta"];
        if(isset($data["corporate_nib"]))$pc->corporate_nib = $data["corporate_nib"];
        if(isset($data["corporate_sk"]))$pc->corporate_sk = $data["corporate_sk"];
        if(isset($data["corporate_npwp"]))$pc->corporate_npwp = $data["corporate_npwp"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        foreach (json_decode($data["corporate_pic"],true) as $key => $value) {
            $value["corporate_id"] = $pc->corporate_id;
            $d = new corporate_pic();
            $d->insertCorporatePIC($value);
        }
        return 1;
    }

    public function editCorporateCredit($data)
    {
        $pc = corporate::find($data["corporate_id"]);
        $last  =  corporate_log::where('corporate_id','=',$data["corporate_id"])
        ->where('corporate_log_category','=',1)
        ->orderby("created_at","desc")
        ->first();
        $amount  = isset($last->corporate_log_amount)?$last->corporate_log_amount:0;
        $selisih = $data["corporate_credit_limit"] - $amount;

        $pc->corporate_credit_limit = $data["corporate_credit_limit"];
        $pc->corporate_due_type = $data["corporate_due_type"];
        $pc->corporate_due_date = $data["corporate_due_date"];
        $pc->corporate_credit_available = intval($pc->corporate_credit_limit) - intval($pc->corporate_credit_used);
        $pc->save();

        $c = new corporate_log();
      
        $c->insertCorporateLog([
            "corporate_id"=>$data["corporate_id"],
            "corporate_log_category"=>1,
            "corporate_log_amount"=>$selisih,
            "corporate_log_user"=>Session::get("user")->user_username,
            "corporate_log_time"=>now(),
            "corporate_log_due_date_type"=>$data["corporate_due_type"],
            "corporate_log_due_date"=>$data["corporate_due_date"],
        ]);

        return 1;
    }

    public function updateCorporate($data)
    {
        $count_data =  corporate::where('status','=',1)->where('corporate_id','!=',$data["corporate_id"])->where('corporate_name','=',$data["corporate_name"])->count();
        if($count_data>0) return -1;
        $pc = corporate::find($data["corporate_id"]);
        $pc->corporate_name = $data["corporate_name"];
        $pc->corporate_email = $data["corporate_email"];
        $pc->corporate_address = $data["corporate_address"];
        $pc->corporate_prefix = $data["corporate_prefix"];
        $pc->corporate_nomor = $data["corporate_nomor"];
        $pc->corporate_admin_fee = $data["corporate_admin_fee"];
        $pc->corporate_hide_logo = $data["corporate_hide_logo"];
        $pc->corporate_use_own_logo = $data["corporate_own_logo"];
        if(isset($data["corporate_logo"]))$pc->corporate_logo = $data["corporate_logo"];
        if(isset($data["corporate_akta"]))$pc->corporate_akta = $data["corporate_akta"];
        if(isset($data["corporate_nib"]))$pc->corporate_nib = $data["corporate_nib"];
        if(isset($data["corporate_sk"]))$pc->corporate_sk = $data["corporate_sk"];
        if(isset($data["corporate_npwp"]))$pc->corporate_npwp = $data["corporate_npwp"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();

        $pic = json_decode($data["corporate_pic"],true);
        $id = [];
        foreach ($pic as $key => $item) {
            if(isset($item["corporate_pic_id"])){
                array_push($id,$item["corporate_pic_id"]);
            }
        }   
        if(count($id)>0)corporate_pic::where('customer_id','=',$pc->customer_id)->whereNotIn('corporate_pic_id',$id)->update(["status"=>0]);
        foreach ($pic as $key => $value) {
            $value["corporate_id"] = $pc->corporate_id;
            $d = new corporate_pic();
            if(!isset($value["corporate_pic_id"]))$d->insertCorporatePIC($value);
        }

        return 1;
    }

    public function deleteCorporate($data)
    {
        $pc = corporate::find($data["corporate_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }
}
