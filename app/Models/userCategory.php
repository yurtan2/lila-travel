<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class userCategory extends Model
{

    use HasFactory;
    protected $table ="user_categories";
    protected $primaryKey ="user_category_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_user_category($data=[])
    {
        $data = array_merge(array(
            "user_category_name" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = userCategory::where('status','=',1);
        if($data["user_category_name"]) $pc->where("user_category_name","like","%".$data["user_category_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertUserCategory($data)
    {
        $count_data =  userCategory::where('status','=',1)->where('user_category_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new userCategory();
        $pc->user_category_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function editUserCategory($data)
    {
        $count_data =  userCategory::where('status','=',1)->where('user_category_id','!=',$data["user_category_id"])->where('user_category_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = userCategory::find($data["user_category_id"]);
        $pc->user_category_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteUserCategory($data)
    {
        $pc = userCategory::find($data["user_category_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }

}
