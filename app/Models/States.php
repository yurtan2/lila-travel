<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class states extends Model
{
    use HasFactory;
    protected $table ="states";
    protected $primaryKey ="id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_states($data=[])
    {
        $data = array_merge(array(
            "state_name" =>null,
            "country_id" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = States::where('status','=',1);
        if($data["state_name"]) $pc->where("name","like","%".$data["state_name"]."%");
        if($data["country_id"]) $pc->where("country_id","=",$data["country_id"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();
     
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }
}
