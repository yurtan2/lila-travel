<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasFactory;

    protected $table = 'users';
    protected $primaryKey = 'user_id';
    public $incrementing = true;
    public $timestamps = true;

    public function login_user($data=[])
    {
        $u = User::where("status","=",1);

        $u->where("user_email","=",$data["user_email"]);
        $u->where("user_password","=",$data["user_password"]);

        return $u->get();
    }
    public function insert($data)
    {
        $u = new User();
        $u->user_email = $data["user_email"];
        $u->user_username = $data["user_username"];
        $u->user_password = md5($data["password"]);
        $u->save();
    }
    public function changePassword($id,$password)
    {
        $u = User::find($id);
        $u->user_password = md5($password);
        $u->save();
    }
}
