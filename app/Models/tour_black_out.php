<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_black_out extends Model
{
    use HasFactory;
    protected $table ="tour_black_outs";
    protected $primaryKey ="tour_black_out_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertBlackOut($data) {
        $i = new tour_black_out();
        $i->tour_departure_id = $data["tour_departure_id"];
        $i->tour_black_out_start = $data["date_start"];
        $i->tour_black_out_end = $data["date_end"];
        $i->tour_black_out_remark = $data["remark"];
        $i->save(); 
    }
    function updateBlackOut($data) {
        $id = [];
        foreach ($data as $key => $value) {
           if(isset($value["tour_black_out_id"])) array_push($id,$value["tour_black_out_id"]);
        }
       
        tour_black_out::whereNotIn('tour_black_out_id',$id)->delete();

        
        foreach ($data as $key => $value) {
            if(isset($value["tour_black_out_id"])){
               
                $i = tour_black_out::find($value["tour_black_out_id"]);
                $i->tour_departure_id = $value["tour_departure_id"];
                $i->tour_black_out_start = $value["date_start"];
                $i->tour_black_out_end = $value["date_end"];
                $i->tour_black_out_remark = $value["remark"];
                $i->save();
            }
            else{
                $this->insertBlackOut($value);
            }
        }
       
    }
}
