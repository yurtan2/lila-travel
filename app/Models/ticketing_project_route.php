<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticketing_project_route extends Model
{
    use HasFactory;
    protected $table ="ticketing_project_routes";
    protected $primaryKey ="tpr_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_route($data=[])
    {
        $data = array_merge(array(
            "ticketing_project_id" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = ticketing_project_route::where('ticketing_project_routes.status','=',1);
        if($data["ticketing_project_id"]) $pc->where("ticketing_project_routes.ticketing_project_id","=",$data["ticketing_project_id"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();
        $pc->leftjoin('airlines as a','a.airline_id','ticketing_project_routes.tpr_airline');
        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);
        $dt =$pc->get();
        
        foreach ($dt as $key => $value) {
            $d = new ticketing_project_destinasi();
            $value->destinasi =$d->get_data_destinasi(["tpr_id"=>$value->tpr_id]);
        }

        foreach ($dt as $key => $value) {
            $d = new ticket_projecting_peserta();
            $value->peserta =$d->get_data_peserta(["tpr_id"=>$value->tpr_id]);
        }
        
        return [
            "data"=>$dt,
            "count"=>$total_rows
        ];
    }


    function insertRoute($data){
     
        $n = new ticketing_project_route();
        $n->ticketing_project_id = $data["ticketing_project_id"];
        $n->tpr_jenis = $data["tpr_jenis"];
        $n->tpr_total_sales = 0;
        $n->tpr_airline = $data["tpr_airline"];
        $n->tpr_dewasa = $data["tpr_dewasa"];
        $n->tpr_anak = $data["tpr_anak"];
        $n->tpr_bayi = $data["tpr_bayi"];
        $n->save();
        $total = intval($data["tpr_dewasa"]) + intval($data["tpr_anak"]) + intval($data["tpr_bayi"]);

        $d = new ticketing_project_destinasi();
        foreach (json_decode($data["destinasi"],true) as $key => $value) {
            $value["tpr_id"] = $n->tpr_id;
            if($data["tpr_jenis"]==1||$data["tpr_jenis"]==3)$value["return_date"]=null;
            $d->insertDestinasi($value);
        }
        $id = [];
        for ($i=0; $i < $total; $i++) { 
            $t = new ticket_projecting_peserta();
            array_push($id,$t->insertPesertaNull(["tpr_id"=>$n->tpr_id]));
        }

        $p = ticketing_project::find($data["ticketing_project_id"]);
        $p->ticketing_project_adult += $data["tpr_dewasa"];
        $p->ticketing_project_child += $data["tpr_anak"];
        $p->ticketing_project_infant += $data["tpr_bayi"]; 
        $p->save();

        return [
            "tpr_id"=>$n->tpr_id,
            "tpp_id"=>$id,
        ];
    }
    function deleteRoute($data) {
        $n = ticketing_project_route::find($data["tpr_id"]);
        $n->status=0;
        $n->save();
    }
}
