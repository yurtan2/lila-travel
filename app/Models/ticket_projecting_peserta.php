<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPUnit\Framework\isNull;

class ticket_projecting_peserta extends Model
{
    use HasFactory;
    protected $table ="ticket_projecting_pesertas";
    protected $primaryKey ="ticket_projecting_peserta_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_peserta($data=[])
    {
        $data = array_merge(array(
            "tpr_id"=>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = ticket_projecting_peserta::where('ticket_projecting_pesertas.status','=',1);
        if($data["tpr_id"]) $pc->where("ticket_projecting_pesertas.tpr_id","=",$data["tpr_id"]);
        if($data["search"]) $pc->select($data["search"]);

        
        $total_rows =$pc->count();
        $pc->leftJoin("airlines as a",'a.airline_id','ticket_projecting_pesertas.airline');
        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    function insertPeserta($data) {
        $n = new ticket_projecting_peserta();
        if($data["customer_id"]!=null)$n->customer_id = $data["customer_id"];
        $n->tpr_id = $data["tpr_id"];
        $n->first_name = $data["first_name"];
        $n->last_name = $data["last_name"];
        $n->nik = $data["nik"];
        $n->tanggal_lahir = $data["tanggal_lahir"];
        $n->jabatan = $data["jabatan"];
        $n->airline = $data["airline"];
        $n->flight = $data["flight"];
        $n->class = $data["class"];
        $n->booking_code = $data["booking_code"];
        $n->status_peserta = $data["status_peserta"];
        if(isset($data["time_limit"]))$n->time_limit = $data["time_limit"];
        $n->etd = $data["etd"];
        $n->eta = $data["eta"];
        $n->net_price = $data["net_price"];
        $n->mark_up = $data["mark_up"];
        $n->selling_price = $data["selling_price"];
        $n->invoice = $data["invoice"];
        $n->remark = $data["remark"];
        $n->save();
        $this->updateProject($data["tpr_id"]);
        return $n->ticket_projecting_peserta_id;
    }

    function insertPesertaNull($data) {
        $tp = ticketing_project_route::find($data["tpr_id"]);
        $n = new ticket_projecting_peserta();
        $n->tpr_id = $data["tpr_id"];
        $n->customer_id = null;
        $n->tpr_id = null;
        $n->first_name = "-";
        $n->last_name = "-";
        $n->nik = "-";
        $n->tanggal_lahir = "-";
        $n->jabatan = "-";
        $n->airline = $tp->tpr_airline;
        $n->flight ="-";
        $n->class = "-";
        $n->booking_code = "-";
        $n->status_peserta = "Active";
        $n->time_limit = null;
        $n->etd = null;
        $n->eta = null;
        $n->net_price = 0;
        $n->mark_up =0;
        $n->selling_price =0;
        $n->invoice = "-";
        $n->remark ="-";
        $n->save();
        $this->updateProject($data["tpr_id"]); 
        return $n->ticket_projecting_peserta_id;
    }

    function UpdatePeserta($data) {
        $n = ticket_projecting_peserta::find($data["tpp_id"]);
        if($data["customer_id"]!=null)$n->customer_id = $data["customer_id"];
        try {
            if($data["tpr_id"]!=null)$n->tpr_id = $data["tpr_id"];
        } catch (\Throwable $th) {
            dd($data);
        }
       
        $n->first_name = $data["first_name"];
        $n->last_name = $data["last_name"];
        $n->nik = $data["nik"];
        $n->tanggal_lahir = $data["tanggal_lahir"];
        $n->jabatan = $data["jabatan"];
        $n->airline = $data["airline"];
        if(isset($data["flight"]))$n->flight = $data["flight"];
        if(isset($data["class"]))$n->class = $data["class"];
        if(isset($data["booking_code"]))$n->booking_code = $data["booking_code"];
        if(isset($data["status_peserta"]))$n->status_peserta = $data["status_peserta"];
        if(isset($data["time_limit"]))$n->time_limit = $data["time_limit"];
        if(isset($data["etd"]))$n->etd = $data["etd"];
        if(isset($data["eta"]))$n->eta = $data["eta"];
        if(isset($data["net_price"]))$n->net_price = $data["net_price"];
        if(isset($data["mark_up"]))$n->mark_up = $data["mark_up"];
        if(isset($data["selling_price"]))$n->selling_price = $data["selling_price"];
        if(isset($data["invoice"]))$n->invoice = $data["invoice"];
        if(isset($data["remark"]))$n->remark = $data["remark"];
        $n->save();
        $this->updateProject($data["tpr_id"]);
    }

    function updateProject($tpr_id) {
        
        $h = ticketing_project_route::find($tpr_id);
        
        $total = ticket_projecting_peserta::where('tpr_id','=',$tpr_id)->where('status','=',1)->sum("selling_price");
        $h->tpr_total_sales = $total;
        $h->save();
        $list_route =  ticketing_project_route::where('ticketing_project_id','=',$h->ticketing_project_id)->where('status','=',1)->get();
        
        foreach ($list_route as $key => $value) {
            $dt = ticket_projecting_peserta::where('tpr_id','=',$value["tpr_id"])->where('status','=',1)->get();
            $h = ticketing_project_route::find($value["tpr_id"]);
            $h->tpr_dewasa=0;
            $h->tpr_anak=0;
            $h->tpr_bayi=0;
            foreach ($dt as $key => $item) {   
                $y = date('Y', strtotime($item["tanggal_lahir"]));
                $y_now = date('Y', strtotime(now()));
                if($y_now-$y>=17)$h->tpr_dewasa++;
                else if($y_now-$y>0&&$y_now-$y<17)$h->tpr_anak++;
                else $h->tpr_bayi++;
            }
            $h->save();
        }
        
        $p = ticketing_project::find($h->ticketing_project_id);
        $p->ticketing_project_total_amount = ticketing_project_route::where('ticketing_project_id','=',$h->ticketing_project_id)->where('status','=',1)->sum("tpr_total_sales");
        $p->ticketing_project_adult = ticketing_project_route::where('ticketing_project_id','=',$h->ticketing_project_id)->where('status','=',1)->sum("tpr_dewasa");
        $p->ticketing_project_child = ticketing_project_route::where('ticketing_project_id','=',$h->ticketing_project_id)->where('status','=',1)->sum("tpr_anak");
        $p->ticketing_project_infant = ticketing_project_route::where('ticketing_project_id','=',$h->ticketing_project_id)->where('status','=',1)->sum("tpr_bayi");
        $p->save();

        return $p;
    }
    function DeletePeserta($data) {
        $n = ticket_projecting_peserta::find($data["tpp_id"]);
        $n->status=0;
        $n->save();
        $this->updateProject($data["tpr_id"]);
    }
}
