<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class customer_family_tree_detail extends Model
{
    use HasFactory;
    protected $table ="customer_family_tree_details";
    protected $primaryKey ="customer_family_tree_detail_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function insertFamilyTreeDetail($data)
    {
       
        $pc = new customer_family_tree_detail();
        $pc->customer_family_tree_id = $data["customer_family_tree_id"];
        $pc->customer_id = $data["customer_id"];
        $pc->customer_family_tree_detail_position = $data["customer_family_tree_detail_position"];
        $pc->save();

        
        return 1;
    }

    function updateFamilyTreeDetail($data) {
        if(intval($data["customer_family_tree_detail_id"])!=-1){
            $pc = customer_family_tree_detail::find($data["customer_family_tree_detail_id"]);
            $pc->customer_id = $data["customer_id"];
            $pc->customer_family_tree_detail_position = $data["customer_family_tree_detail_position"];
            $pc->save();
        }
        else{
            $this->insertFamilyTreeDetail($data);
        }
    }

}
