<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class terms_condition extends Model
{
    use HasFactory;
    protected $table ="terms_conditions";
    protected $primaryKey ="terms_condition_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_term($data=[])
    {
        $data = array_merge(array(
            "terms_condition_name" =>null,
            "search"=>null,
            "pr_category_id"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = terms_condition::where('terms_conditions.status','=',1);
        if($data["terms_condition_name"]) $pc->where("terms_conditions.terms_condition_name","like","%".$data["terms_condition_name"]."%");
        if($data["pr_category_id"]) $pc->where("terms_conditions.pr_category_id","like","%".$data["pr_category_id"]."%");
    
        $pc->leftjoin('product_categories as p','p.pr_category_id','terms_conditions.pr_category_id');

        if($data["search"]) $pc->select($data["search"]);
        else $pc->select("p.pr_category_id","p.pr_category_nama","terms_conditions.*");

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertTerm($data)
    {
        $count_data =  terms_condition::where('status','=',1)->where('terms_condition_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new terms_condition();
        $pc->terms_condition_name = $data["nama"];
        $pc->terms_condition_detail = $data["detail"];
        $pc->pr_category_id = $data["pr_category_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function editTerm($data)
    {
        $count_data =  terms_condition::where('status','=',1)->where('terms_condition_id','!=',$data["terms_condition_id"])->where('terms_condition_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = terms_condition::find($data["terms_condition_id"]);
        $pc->terms_condition_name = $data["nama"];
        $pc->terms_condition_detail = $data["detail"];
        $pc->pr_category_id = $data["pr_category_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteTerm($data)
    {
        $pc = terms_condition::find($data["terms_condition_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
