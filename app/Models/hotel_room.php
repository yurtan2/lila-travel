<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class hotel_room extends Model
{
    use HasFactory;
    protected $table ="hotel_rooms";
    protected $primaryKey ="hotel_room_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_hotel_room($data=[])
    {
        $data = array_merge(array(
            "hotel_room_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = hotel_room::where('status','=',1);
        if($data["hotel_room_name"]) $pc->where("hotel_room_name","like","%".$data["hotel_room_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertHotelRoom($data)
    {
        $count_data =  hotel_room::where('status','=',1)->where('hotel_room_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new hotel_room();
        $pc->updated_by = Session::get("user")->user_username;
        $pc->hotel_room_name = $data["nama"];
        $pc->save();
        return 1;
    }

    public function updateHotelRoom($data)
    {
        $count_data =  hotel_room::where('status','=',1)->where('hotel_room_id','!=',$data["hotel_room_id"])->where('hotel_room_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = hotel_room::find($data["hotel_room_id"]);
        $pc->hotel_room_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteHotelRoom($data)
    {
        $pc = hotel_room::find($data["hotel_room_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
