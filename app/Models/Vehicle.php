<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Vehicle extends Model
{
    use HasFactory;

    protected $table = 'vehicles';
    protected $primaryKey = 'vehicle_id';
    public $incrementing = true;
    public $timestamps = true;


    public function get_data_vehicle($data=[])
    {
        $data = array_merge(array(
            "vehicle_name" =>null,
            "vehicle_type" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Vehicle::where('vehicles.status','=',1);
        if($data["vehicle_name"]) $pc->where("vehicles.vehicle_name","like","%".$data["vehicle_name"]."%");
        if($data["vehicle_type"]) $pc->where("vehicles.vehicle_type_id","like","%".$data["vehicle_type"]."%");
        
        $pc->leftjoin('vehicle_types as v','v.vehicle_type_id','vehicles.vehicle_type_id');
        
        if($data["search"]) $pc->select($data["search"]);
        else $pc->select("v.vehicle_type_name",'v.vehicle_type_id',"vehicles.*");

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }


    public function insertVehicle($data)
    {
        $count_data =  Vehicle::where('status','=',1)->where('vehicle_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new Vehicle();
        $pc->vehicle_name = $data["nama"];
        $pc->vehicle_type_id = $data["vehicle_type_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updateVehicle($data)
    {
        $count_data =  Vehicle::where('status','=',1)->where('vehicle_id','!=',$data["vehicle_id"])->where('vehicle_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = Vehicle::find($data["vehicle_id"]);
        $pc->vehicle_name = $data["nama"];
        $pc->vehicle_type_id = $data["vehicle_type_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteVehicle($data)
    {
        $pc = Vehicle::find($data["vehicle_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
