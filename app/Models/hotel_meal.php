<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class hotel_meal extends Model
{
    use HasFactory;
    protected $table ="hotel_meals";
    protected $primaryKey ="hotel_meal_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_hotel_meal($data=[])
    {
        $data = array_merge(array(
            "hotel_meal_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = hotel_meal::where('status','=',1);
        if($data["hotel_meal_name"]) $pc->where("hotel_meal_name","like","%".$data["hotel_meal_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);
     
        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertHotelMeal($data)
    {
        $count_data =  hotel_meal::where('status','=',1)->where('hotel_meal_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new hotel_meal();
        $pc->updated_by = Session::get("user")->user_username;
        $pc->hotel_meal_name = $data["nama"];
        $pc->save();
        return 1;
    }

    public function updateHotelMeal($data)
    {
        $count_data =  hotel_meal::where('status','=',1)->where('hotel_meal_id','!=',$data["hotel_meal_id"])->where('hotel_meal_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = hotel_meal::find($data["hotel_meal_id"]);
        $pc->hotel_meal_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteHotelMeal($data)
    {
        $pc = hotel_meal::find($data["hotel_meal_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
