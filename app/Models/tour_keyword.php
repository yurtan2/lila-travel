<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_keyword extends Model
{
    use HasFactory;
    protected $table ="tour_keywords";
    protected $primaryKey ="tour_keyword_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertTourKeyword($data) {
        $i = new tour_keyword();
        $i->tour_id = $data["tour_id"];
        $i->keyword_id = $data["keyword_id"];
        $i->save();
    }

    function updateTourKeyword($data) { 
        
        tour_keyword::where('tour_id','=',$data["tour_id"])
        ->whereNotIn('keyword_id',$data["keyword_id"])->delete();
       
        foreach ($data["keyword_id"] as $key => $value) {
            $ada = tour_keyword::where('tour_id','=',$data["tour_id"])
            ->where('keyword_id','=',$value)->count();
            
            if($ada<=0){
                $this->insertTourKeyword([
                    "tour_id"=>$data["tour_id"],
                    "keyword_id"=>$value
                ]);
            }
        }
    }
}
