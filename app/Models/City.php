<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table ="cities";
    protected $primaryKey ="id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_city($data=[])
    {
        $data = array_merge(array(
            "city_name" =>null,
            "country_id" =>null,
            "state_id" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = City::where('cities.status','=',1);
        $pc->leftjoin('states as s','s.id','cities.state_id');
        $pc->leftjoin('countries as c','c.id','cities.country_id');

        if($data["city_name"]!=null) $pc->where("cities.name","like","%".$data["city_name"]."%");
        if($data["state_id"]!=null) $pc->where("cities.state_id","=",$data["state_id"]);
        if($data["country_id"]!=null) $pc->where("cities.country_id","=",$data["country_id"]);
        if($data["search"]!=null) $pc->select($data["search"]);
        else $pc->select('cities.*','s.name as state_name','s.id as state_id','c.name as country_name','c.id as country_id');

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function get_data_simple_city($data=[])
    {
        $data = array_merge(array(
            "city_name" =>null,
        ), $data);
        
        $pc = City::where('cities.status','=',1);
        if($data["city_name"]!=null) $pc->where("cities.name","like","%".$data["city_name"]."%");
        return [
            "data"=>$pc->get(),
            "count"=>$pc->count()
        ];
    }

    public function insertCity($data)
    {
        $count_data =  City::where('status','=',1)->where('name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new City();
        $pc->name = $data["nama"];
        $pc->state_id = $data["states"];
        $pc->country_id = $data["country_id"];
        $pc->save();
        return 1;
    }

    public function updateCity($data)
    {
        $count_data =  City::where('status','=',1)->where('id','!=',$data["city_id"])->where('name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = City::find($data["city_id"]);
        $pc->name = $data["nama"];
        $pc->state_id = $data["states"];
        $pc->country_id = $data["country_id"];
        $pc->save();
        return 1;
    }

    public function deleteCity($data)
    {
        $pc = City::find($data["city_id"]);
        $pc->status = 0;
        $pc->save();

        $ci = City::where('id','=',$data["city_id"])->update([
            "status"=>0
        ]);

        return 1;
    }
}
