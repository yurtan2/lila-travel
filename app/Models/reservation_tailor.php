<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reservation_tailor extends Model
{
    use HasFactory;
    protected $table ="reservation_tailors";
    protected $primaryKey ="reservation_tailor_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertReservationTailor($data){
        $r = new reservation_tailor();
        $r->reservation_tailor_request = $data["reservation_tailor_request"];
        $r->reservation_tailor_customer_id = $data["reservation_tailor_customer_id"];
        $r->reservation_tailor_phone = $data["reservation_tailor_phone"];
        $r->reservation_tailor_email = $data["reservation_tailor_email"];
        $r->reservation_tailor_address = $data["reservation_tailor_address"];
        $r->reservation_tailor_dewasa = $data["reservation_tailor_dewasa"];
        $r->reservation_tailor_anak = $data["reservation_tailor_anak"];
        $r->reservation_tailor_bayi = $data["reservation_tailor_bayi"];
        $r->reservation_tailor_remark = $data["reservation_tailor_remark"];
        $r->save();
        
        foreach (json_decode($data["detail"],true) as $key => $value) {
            $value["reservation_tailor_id"] = $r->reservation_tailor_id;
            $d = new reservation_tailor_detail();
            $d->insertDetailTailor($value);
        }
        
    }

    function updateReservationTailor($data){
        $r = reservation_tailor::find($data["reservation_tailor_id"]);
        $r->reservation_tailor_request = $data["reservation_tailor_request"];
        $r->reservation_tailor_customer_id = $data["reservation_tailor_customer_id"];
        $r->reservation_tailor_phone = $data["reservation_tailor_phone"];
        $r->reservation_tailor_email = $data["reservation_tailor_email"];
        $r->reservation_tailor_address = $data["reservation_tailor_address"];
        $r->reservation_tailor_dewasa = $data["reservation_tailor_dewasa"];
        $r->reservation_tailor_anak = $data["reservation_tailor_anak"];
        $r->reservation_tailor_bayi = $data["reservation_tailor_bayi"];
        $r->save();

        $d = new reservation_tailor_detail();
        $d->updateDetailTailor($data["detail"]);
    }

    public function get_reservation_tailor($data=[])
    {
        $data = array_merge(array(
            "reservation_tailor_status" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = reservation_tailor::where('status','=',1);
        if($data["reservation_tailor_status"]) $pc->where("reservation_tailor_status","=",$data["reservation_tailor_status"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }
}
