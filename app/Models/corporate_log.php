<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class corporate_log extends Model
{
    use HasFactory;

    use HasFactory;
    protected $table ="corporate_logs";
    protected $primaryKey ="corporate_log_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_corporate_log($data=[])
    {
        $data = array_merge(array(
            "corporate_log_category"=>null,
            "corporate_id"=>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = corporate_log::where('status','=',1);
        if($data["corporate_log_category"]) $pc->where("corporate_log_category","=",$data["corporate_log_category"]);
        if($data["corporate_id"]) $pc->where("corporate_id","=",$data["corporate_id"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);
        $pc->orderBy("created_at","desc");
        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }


    public function insertCorporateLog($data)
    {
        $pc = new corporate_log();
        $pc->corporate_id = $data["corporate_id"];
        $pc->corporate_log_category = $data["corporate_log_category"];
        $pc->corporate_log_amount = $data["corporate_log_amount"];
        $pc->corporate_log_user = $data["corporate_log_user"];
        $pc->corporate_log_time = $data["corporate_log_time"];
        $pc->corporate_log_due_date_type = $data["corporate_log_due_date_type"];
        $pc->corporate_log_due_date = $data["corporate_log_due_date"];
        if(isset($pc->corporate_log_message))$pc->corporate_log_message = $data["corporate_log_message"];
        $pc->save();
    }

}
