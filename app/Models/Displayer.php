<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Displayer extends Model
{
    use HasFactory;
    protected $table ="displayers";
    protected $primaryKey ="displayer_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_displayer($data=[])
    {
        $data = array_merge(array(
            "displayer_category" =>-1,
            "displayer_validty"=>null,
            "displayer_title"=>null,
            "displayer_detail"=>0,
            "display"=>0,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = Displayer::where('status','=',1);
        if($data["displayer_title"]) $pc->where("displayer_title","=","%".$data["displayer_category"]."%");
        if($data["displayer_category"]!=-1) $pc->where("displayer_category","=",$data["displayer_category"]);
        if($data["displayer_validty"]) $pc->where("displayer_validity","<=",$data["displayer_validty"]);
        if($data["select"]) $pc->select($data["select"]);
        if($data["display"]==1) $pc->where("displayer_validity",">=",now());
        $pc->orderBy('displayer_order', 'asc');

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data_displayer = $pc->get();
        if($data["displayer_detail"]==1){
            foreach ($data_displayer as $key => $item) {
                $d = new displayer_detail();
                $data_detail = $d->get_data_displayer_detail([
                    "displayer_id"=>$item->displayer_id
                ])["data"];
                $item->detail=[];
                if($data["display"]==1){
                    $t = [];
                    foreach ($data_detail as $key => $values) {
                        if($values->tour_status==1){
                            array_push($t,$values);
                        }
                    }
                    $item->detail = $t;
                }
                else $item->detail = $data_detail;
                if(sizeof($item->detail)<=0&&$data["display"]==1){
                    unset($data_displayer[$key]);
                    //if(sizeof($data_displayer)>0)$data_displayer = array_values($data_displayer);
                }
            }
        }
        foreach ($data_displayer as $key => $item) {
            $d = Displayer::find($item->displayer_id);
            $d->displayer_total = displayer_detail::where('displayer_id','=',$item->displayer_id)->where('status','=',1)->count();
            $d->save();
            $item->displayer_total = $d->displayer_total;
        }
        return [
            "data"=>$data_displayer,
            "count"=>$total_rows
        ];
    }


    function insertDisplayer($data){
        $count_data =  Displayer::where('status','=',1)->where('displayer_title','=',$data["displayer_title"])->count();
        if($count_data>0) return -1;
        $pc = new Displayer();
        $pc->displayer_title = $data["displayer_title"];
        $pc->displayer_category = $data["displayer_category"];
        $pc->displayer_validity = $data["displayer_validity"];
        $pc->displayer_order = $data["displayer_order"];
        $pc->save();
        return 1;
    }

    function updateDisplayer($data){
        $count_data =  Displayer::where('status','=',1)->where('displayer_id','!=',$data["displayer_id"])->where('displayer_title','=',$data["displayer_title"])->count();
        if($count_data>0) return -1;
        $pc = Displayer::find($data["displayer_id"]);
        $pc->displayer_title = $data["displayer_title"];
        $pc->displayer_category = $data["displayer_category"];
        $pc->displayer_validity = $data["displayer_validity"];
        $pc->displayer_order = $data["displayer_order"];
        $pc->displayer_total = isset($data["displayer_total"])?$data["displayer_total"]:0;
        $pc->save();
        return 1;
    }

    function deleteDisplayer($data){
        $pc = Displayer::find($data["displayer_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }
}
