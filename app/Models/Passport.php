<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Passport extends Model
{
    use HasFactory;
    protected $table ="passports";
    protected $primaryKey ="passport_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_passport($data=[])
    {
        $data = array_merge(array(
            "passport_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Passport::where('status','=',1);
        if($data["passport_name"]) $pc->where("passport_name","like","%".$data["passport_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();

        foreach ($data as $key => $item) {
           $item->passport_syarat_text = Strip_tags($item->passport_syarat);
           if(strlen($item->passport_syarat_text) >=100){
                $item->passport_syarat_text = substr($item->passport_syarat_text,0,100)." ...";
           }
        }

        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertPassport($data)
    {
        $count_data =  Passport::where('status','=',1)->where('passport_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new Passport();
        $pc->passport_name = $data["nama"];
        $pc->passport_syarat = $data["syarat"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updatePassport($data)
    {
        $count_data =  Passport::where('status','=',1)->where('passport_id','!=',$data["passport_id"])->where('passport_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = Passport::find($data["passport_id"]);
        $pc->passport_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->passport_syarat = $data["syarat"];
        $pc->save();
        return 1;
    }

    public function deletePassport($data)
    {
        $pc = Passport::find($data["passport_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }
}
