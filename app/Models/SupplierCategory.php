<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class SupplierCategory extends Model
{
    use HasFactory;
    protected $table ="supplier_categories";
    protected $primaryKey ="sp_category_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_category($data=[])
    {
        $data = array_merge(array(
            "sp_category_nama" =>null,
            "search"=>null,
            "pr_category_id"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = SupplierCategory::where('supplier_categories.status','=',1);
        if($data["sp_category_nama"]) $pc->where("supplier_categories.sp_category_nama","like","%".$data["sp_category_nama"]."%");
        if($data["pr_category_id"]) $pc->where("supplier_categories.pr_category_id","like","%".$data["pr_category_id"]."%");
        
        $pc->leftjoin('product_categories as p','p.pr_category_id','supplier_categories.pr_category_id');
        
        if($data["search"]) $pc->select($data["search"]);
        else $pc->select("p.pr_category_nama","p.pr_category_id","supplier_categories.*");

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }


    public function insertCategory($data)
    {
        $count_data =  SupplierCategory::where('status','=',1)->where('sp_category_nama','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new SupplierCategory();
        $pc->sp_category_nama = $data["nama"];
        $pc->pr_category_id = $data["product_category"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function editCategory($data)
    {
        $count_data =  SupplierCategory::where('status','=',1)->where('sp_category_id','!=',$data["sp_category_id"])->where('sp_category_nama','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = SupplierCategory::find($data["sp_category_id"]);
        $pc->sp_category_nama = $data["nama"];
        $pc->pr_category_id = $data["product_category"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteCategory($data)
    {
        $pc = SupplierCategory::find($data["sp_category_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
