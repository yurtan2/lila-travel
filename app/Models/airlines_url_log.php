<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class airlines_url_log extends Model
{
    use HasFactory;
    protected $table ="airlines_url_logs";
    protected $primaryKey ="airline_url_log_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertLogUrl($data) {
        $lg = new airlines_url_log();
        $lg->airline_id = $data["airline_id"];
        $lg->airline_url_log_api = $data["airline_url_log_api"];
        $lg->save();
    }

    function get_data_log_url($data){
        $lg = airlines_url_log::where('airline_id','=',$data["airline_id"]);
        return $lg->get();
    }
}
