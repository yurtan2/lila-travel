<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class corporate_pic extends Model
{
    use HasFactory;
    protected $table ="corporate_pics";
    protected $primaryKey ="corporate_pic_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function insertCorporatePIC($data)
    {
        $pc = new corporate_pic();
        $pc->corporate_id = $data["corporate_id"];
        $pc->customer_id = $data["customer_id"];
        $pc->jabatan_id = $data["jabatan_id"];
        $pc->status = 1;
        $pc->save();
        return $pc->corporate_pic_id;
    }

    public function get_corporate_pic($data=[])
    {
        $data = array_merge(array(
            "corporate_id" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = corporate_pic::where('corporate_pics.status','=',1);
        $pc->leftJoin("customers as c",'c.customer_id','corporate_pics.customer_id');
        if($data["corporate_id"]) $pc->where("corporate_pics.corporate_id","=",$data["corporate_id"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }


}
