<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class term_detail extends Model
{
    use HasFactory;
    protected $table ="term_details";
    protected $primaryKey ="term_detail_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function insertDetailTerm($data)
    {
        $pc = new term_detail();
        $pc->term_detail = $data["term_detail"];
        $pc->term_id = $data["term_id"];
        $pc->save();
        return 1;
    }

    public function deleteDetailTerm($data)
    {
        $pc = term_detail::find($data["term_detail_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
