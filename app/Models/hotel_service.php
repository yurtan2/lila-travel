<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class hotel_service extends Model
{
    use HasFactory;
    protected $table ="hotel_services";
    protected $primaryKey ="hotel_service_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_hotel_service($data=[])
    {
        $data = array_merge(array(
            "hotel_service_name"=>null,
            "hotel_service_category"=>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = hotel_service::where('status','=',1);
        if($data["hotel_service_name"]) $pc->where("hotel_service_name","like","%".$data["hotel_service_name"]."%");
        if($data["hotel_service_category"]) $pc->where("hotel_service_category","=",$data["hotel_service_category"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertHotelService($data)
    {
        $count_data =  hotel_service::where('status','=',1)->where('hotel_service_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new hotel_service();
        $pc->updated_by = Session::get("user")->user_username;
        $pc->hotel_service_name = $data["nama"];
        $pc->hotel_service_category = $data["hotel_service_category"];
        $pc->hotel_service_icon = $data["hotel_service_icon"];
        $pc->save();
        return 1;
    }

    public function updateHotelService($data)
    {
        $count_data =  hotel_service::where('status','=',1)->where('hotel_service_id','!=',$data["hotel_service_id"])->where('hotel_service_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = hotel_service::find($data["hotel_service_id"]);
        $pc->hotel_service_name = $data["nama"];
        $pc->hotel_service_category = $data["hotel_service_category"];
        $pc->hotel_service_icon = $data["hotel_service_icon"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteHotelService($data)
    {
        $pc = hotel_service::find($data["hotel_service_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
