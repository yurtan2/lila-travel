<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_term extends Model
{
    use HasFactory;
    protected $table ="tour_terms";
    protected $primaryKey ="tour_terms_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertTerms($data) {
       
        if(isset($data["tour_terms_jenis_id"])&&$data["tour_terms_jenis_id"]!="null"&&$data["tour_terms_jenis_id"]!=null){
          
            $i = new tour_term();
            $i->tour_terms_date = $data["tour_terms_date"];
            $i->tour_id = $data["tour_id"];
            $i->tour_terms_jenis = $data["tour_terms_jenis"];
            $i->tour_terms_jenis_id = $data["tour_terms_jenis_id"];
            $i->tour_terms_value = $data["tour_terms_value"];
            $i->save();
            
        }
    }

    function updateTerm($data,$tour_id) {
        $id = [];
        foreach ($data as $key => $value) {
           if(isset($value["tour_terms_id"])) array_push($id,$value["tour_terms_id"]);
        }
        tour_term::where('tour_id','=',$tour_id)->where('tour_terms_jenis','=',$data[0]["tour_terms_jenis"])
        ->whereNotIn('tour_terms_id',$id)->delete();

        foreach ($data as $key => $value) {
            if(isset($value["tour_terms_id"])){
                $i = tour_term::find($value["tour_terms_id"]);
                    $i->tour_terms_date = json_encode($value["tour_terms_date"]);
                    $i->tour_id = $tour_id;
                    $i->tour_terms_jenis = $value["tour_terms_jenis"];
                    $i->tour_terms_jenis_id = isset($value["id"])?$value["id"]:$value["tour_terms_jenis_id"];
                    $i->tour_terms_value = $value["tour_terms_value"];
                    $i->save();
            }
            else{
                if(isset($value["tour_terms_jenis_id"])&&$value["tour_terms_jenis_id"]!="null"&&$value["tour_terms_jenis_id"]!=null){
     
                    $value["tour_id"] = $tour_id;
                    $value["tour_terms_jenis_id"] =  isset($value["id"])?$value["id"]:$value["tour_terms_jenis_id"];
                    $value["tour_terms_date"] = json_encode($value["tour_terms_date"]);
                    
                    $this->insertTerms($value);
                }
            }
        }
       
    }
}
