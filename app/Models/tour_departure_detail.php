<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class tour_departure_detail extends Model
{
    use HasFactory;
    protected $table ="tour_departure_details";
    protected $primaryKey ="tour_departure_detail_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertTourDepartureDetail($data) {

        $i = new tour_departure_detail();
        $i->tour_departure_id = $data["tour_departure_id"];
        $i->tour_departure_detail_jenis = $data["tour_departure_detail_jenis"];
        $i->tour_departure_detail_name = $data["tour_departure_detail_name"];
        $i->tour_departure_detail_price = $data["tour_departure_detail_price"];
        
        if($data["tour_jenis"]==1||$data["tour_jenis"]==2&&$data["tour_departure_detail_jenis"]==2){
            $i->tour_departure_detail_disc = isset($data["tour_departure_detail_disc"])?$data["tour_departure_detail_disc"]:0;
            $i->tour_departure_detail_total = $data["tour_departure_detail_total"];
        }
        else{
            $i->tour_departure_detail_pax_2 = $data["tour_departure_detail_pax_2"];
            $i->tour_departure_detail_pax_5 = $data["tour_departure_detail_pax_5"];
            $i->tour_departure_detail_pax_9 = $data["tour_departure_detail_pax_9"];
        }
        $i->save();
    }

    function updateTourDepartureDetail($data,$tour_departure_id) {

        $id = [];
        foreach ($data as $key => $value) {
           if(isset($value->tour_departure_detail_id)) array_push($id,$value->tour_departure_detail_id);
        }
        
        tour_departure_detail::where('tour_departure_id','=',$tour_departure_id)
        ->whereNotIn('tour_departure_detail_id',$id)->delete();
        
        foreach ($data as $key => $value) {
            if(isset($value->tour_departure_detail_id)){
                $i = tour_departure_detail::find($value->tour_departure_detail_id);
                    $i->tour_departure_id = $tour_departure_id;
                    $i->tour_departure_detail_jenis = $value->tour_departure_detail_jenis;
                    $i->tour_departure_detail_name = $value->tour_departure_detail_name;
                    $i->tour_departure_detail_price =  isset($value->tour_departure_detail_price)?$value->tour_departure_detail_price:0;
                    $i->tour_departure_detail_disc = isset($value->tour_departure_detail_disc)?$value->tour_departure_detail_disc:0;
                    $i->tour_departure_detail_total =isset($value->tour_departure_detail_total)?$value->tour_departure_detail_total:0;
                    $i->tour_departure_detail_pax_2 = isset($value->tour_departure_detail_pax_2)?$value->tour_departure_detail_pax_2:0;
                    $i->tour_departure_detail_pax_5 = isset($value->tour_departure_detail_pax_5)?$value->tour_departure_detail_pax_5:0;
                    $i->tour_departure_detail_pax_9 = isset($value->tour_departure_detail_pax_9)?$value->tour_departure_detail_pax_9:0;
                    $i->save();
            }
            else{
                if(is_array($value)){
                    $value = json_decode(json_encode($value), FALSE);
           
                }
                $temp = [
                    "tour_departure_id" => $tour_departure_id,
                    "tour_departure_detail_jenis" => $value->tour_departure_detail_jenis,
                    "tour_departure_detail_name" =>  $value->tour_departure_detail_name,
                    "tour_departure_detail_price" => isset($value->tour_departure_detail_price)?$value->tour_departure_detail_price:0,
                    "tour_departure_detail_disc" =>  isset($value->tour_departure_detail_disc)?$value->tour_departure_detail_disc:0,
                    "tour_departure_detail_total" => isset($value->tour_departure_detail_total)?$value->tour_departure_detail_total:0,
                    "tour_departure_detail_pax_2" => isset($value->tour_departure_detail_pax_2)?$value->tour_departure_detail_pax_2:0,
                    "tour_departure_detail_pax_5" => isset($value->tour_departure_detail_pax_5)?$value->tour_departure_detail_pax_5:0,
                    "tour_departure_detail_pax_9" => isset($value->tour_departure_detail_pax_9)?$value->tour_departure_detail_pax_9:0
                    
                ];
                
                if(isset($value->tour_jenis)&&$value->tour_jenis==2){
                    $temp["tour_jenis"] = 2;
                }
                else{
                    $temp["tour_jenis"] = 1;
                }
                $this->insertTourDepartureDetail($temp);
            }
        }
    }
}
