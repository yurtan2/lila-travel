<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class ticketing_project extends Model
{
    use HasFactory;
    protected $table ="ticketing_projects";
    protected $primaryKey ="ticketing_project_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_ticketing_project($data=[])
    {
        $data = array_merge(array(
            "ticketing_project_id" =>null,
            "ticketing_project_name" =>null,
            "bulan" =>null,
            "tahun" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        $pc = ticketing_project::where('ticketing_projects.status','=',1);
        if($data["ticketing_project_id"]) $pc->where("ticketing_projects.ticketing_project_id","=",$data["ticketing_project_id"]);
        if($data["ticketing_project_name"]) $pc->where("ticketing_projects.ticketing_project_name","like","%".$data["ticketing_project_name"]."%");
        if($data["bulan"]) $pc->whereMonth("ticketing_projects.created_at","=",$data["bulan"]);
        if($data["tahun"]) $pc->whereYear("ticketing_projects.created_at","=",$data["tahun"]);
        
        $pc->leftjoin('corporates as cp','cp.corporate_id','ticketing_projects.corporate_id');
        $pc->leftjoin('customers as c','c.customer_id','ticketing_projects.customer_id');

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);
        $dt = $pc->get();
        return [
            "data"=>$dt,
            "count"=>$total_rows
        ];
    }

    public function insertTicketingProject($data)
    {
        $count_data =  ticketing_project::where('status','=',1)->where('ticketing_project_name','=',$data["ticketing_project_name"])->count();
        if($count_data>0) return -1;
        $pc = new ticketing_project();
        $pc->ticketing_project_name = $data["ticketing_project_name"];
        $pc->corporate_id = $data["corporate_id"];
        $pc->customer_id = $data["customer_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return $pc->ticketing_project_id;
    }

    public function editTicketing_project($data)
    {
        $count_data =  ticketing_project::where('status','=',1)->where('ticketing_project_id','!=',$data["ticketing_project_id"])->where('ticketing_project_name','=',$data["ticketing_project_name"])->count();
        if($count_data>0) return -1;
        $pc = ticketing_project::find($data["ticketing_project_id"]);
        $pc->ticketing_project_name = $data["ticketing_project_name"];
        $pc->corporate_id = $data["corporate_id"];
        $pc->customer_id = $data["customer_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteTicketing_project($data)
    {
        $pc = ticketing_project::find($data["ticketing_project_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
