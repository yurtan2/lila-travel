<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_destination extends Model
{
    use HasFactory;
    protected $table ="tour_destinations";
    protected $primaryKey ="tour_destination_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertTourDestination($data_insert,$id) {
        foreach ($data_insert as $key => $data) {
            $i = new tour_destination();
            $i->tour_id = $id;
            $i->country_id = $data["country_id"];
            $i->supplier_id = $data["supplier_id"];
            $i->with_visa = $data["is_visa"]==false?0:1;
            $i->offline_code = $data["offline_code"];
            $i->save();
        }
    }
    function updateTourDestination($data,$tour_id) {

        $id = [];
        foreach ($data as $key => $value) {
            if(isset($value["tour_destination_id"])) array_push($id,$value["tour_destination_id"]);
        }
        tour_destination::where('tour_id','=',$tour_id)
        ->whereNotIn('tour_destination_id',$id)->delete();

        foreach ($data as $key => $value) {
            if(isset($value["tour_destination_id"])){
                $u = tour_destination::find($value["tour_destination_id"]);
                $u->tour_id = $tour_id;
                $u->country_id = $value["country_id"];
                $u->supplier_id = $value["supplier_id"];
                $u->with_visa = $value["is_visa"];
                $u->offline_code = $value["offline_code"];
                $u->save();
            }
            else{
                $this->insertTourDestination([$value],$tour_id);
            }
        }
       
    }
}
