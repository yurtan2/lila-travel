<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class corporate_position extends Model
{
    use HasFactory;
    protected $table ="corporate_position";
    protected $primaryKey ="corporate_position_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_corporate_position($data=[])
    {
        $data = array_merge(array(
            "corporate_position_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = corporate_position::where('status','=',1);
        if($data["corporate_position_name"]) $pc->where("corporate_position_name","like","%".$data["corporate_position_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertCorporatePosition($data)
    {
        $count_data =  corporate_position::where('status','=',1)->where('corporate_position_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new corporate_position();
        $pc->updated_by = Session::get("user")->user_username;
        $pc->corporate_position_name = $data["nama"];
        $pc->save();
        return 1;
    }

    public function updateCorporatePosition($data)
    {
        $count_data =  corporate_position::where('status','=',1)->where('corporate_position_id','!=',$data["corporate_position_id"])->where('corporate_position_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = corporate_position::find($data["corporate_position_id"]);
        $pc->corporate_position_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteCorporatePosition($data)
    {
        $pc = corporate_position::find($data["corporate_position_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
