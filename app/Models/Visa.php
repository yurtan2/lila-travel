<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Visa extends Model
{
    use HasFactory;
    protected $table = 'visas';
    protected $primaryKey = 'visa_id';
    public $incrementing = true;
    public $timestamps = true;

    public function get_data_visa($data=[])
    {
        $data = array_merge(array(
            "visa_name" =>null,
            "country" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Visa::where('visas.status','=',1);
        $pc->leftjoin("countries as c","c.id","visas.country_id");
        if($data["country"]) $pc->where("c.id","like","%".$data["country"]."%");
        if($data["visa_name"]) $pc->where("visas.visa_name","like","%".$data["visa_name"]."%");
        if($data["search"]) $pc->select($data["search"]);
        else $pc->select("visas.*","c.name as country_name","c.id as country_id");
        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertVisa($data)
    {
        $count_data =  Country::where('status','=',1)->where('name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new Visa();
        $pc->visa_name = $data["nama"];
        $pc->visa_detail = $data["detail"];
        $pc->country_id = $data["country"];
        $pc->visa_need = json_encode($data["dokumen"]);
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updateVisa($data)
    {
        $count_data =  Visa::where('status','=',1)->where('visa_id','!=',$data["visa_id"])->where('visa_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = Visa::find($data["visa_id"]);
        $pc->visa_name = $data["nama"];
        $pc->visa_detail = $data["detail"];
        $pc->country_id = $data["country"];
        $pc->visa_need = json_encode($data["dokumen"]);
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteVisa($data)
    {
        $pc = Visa::find($data["visa_id"]);
        $pc->status = 0;
        $pc->save();


        return 1;
    }

}
