<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table ="countries";
    protected $primaryKey ="id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_country($data=[])
    {
        $data = array_merge(array(
            "country_code" =>null,
            "country_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Country::where('status','=',1);
        if($data["country_code"]) $pc->where("iso3","like","%".$data["country_code"]."%");
        if($data["country_name"]) $pc->where("name","like","%".$data["country_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();
        foreach ($data as $key => $item) {
            $item->with_visa=0;
            $item->visa_name="";
            $item->visa_detail="";
            $item->visa_need="";

            $v = Visa::where("country_id","=",$item->id)->where('status',"=",1)->orderby('created_at',"desc")->get();
            if(count($v)>0){
                $item->with_visa=1;
                $item->visa_id=$v[0]->visa_id;
                $item->visa_name=$v[0]->visa_name;
                $item->visa_detail=$v[0]->visa_detail;
                $item->visa_need=$v[0]->visa_need;
            }
        }
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertCountry($data)
    {
        $count_data =  Country::where('status','=',1)->where('name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new Country();
        $pc->name = $data["nama"];
        $pc->iso3 = $data["code"];
        $pc->phonecode = $data["phone"];
        $pc->save();
        return 1;
    }

    public function updateCountry($data)
    {
        $count_data =  Country::where('status','=',1)->where('id','!=',$data["country_id"])->where('name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = Country::find($data["country_id"]);
        $pc->name = $data["nama"];
        $pc->iso3 = $data["code"];
        $pc->phonecode = $data["phone"];
        $pc->save();
        return 1;
    }

    public function deleteCountry($data)
    {
        $pc = Country::find($data["country_id"]);
        $pc->status = 0;
        $pc->save();

        $ci = City::where('country_id','=',$data["country_id"])->update([
            "status"=>0
        ]);

        return 1;
    }

}
