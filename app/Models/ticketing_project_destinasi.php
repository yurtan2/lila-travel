<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticketing_project_destinasi extends Model
{
    use HasFactory;
    protected $table ="ticketing_project_destinasis";
    protected $primaryKey ="tpd_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertDestinasi($data){
        $n = new ticketing_project_destinasi();
        $n->tpr_id = $data["tpr_id"];
        $n->from_id = $data["from_id"];
        $n->to_id = $data["to_id"];
        $n->departure_date = $data["departure_date"];
        $n->return_date = $data["return_date"];
        $n->save();
    }

    function get_data_destinasi($data) {
        $g = ticketing_project_destinasi::where('ticketing_project_destinasis.tpr_id','=',$data["tpr_id"])
        ->leftJoin("airports as f","f.airport_code","ticketing_project_destinasis.from_id")
        ->leftJoin("airports as t","t.airport_code","ticketing_project_destinasis.to_id")
        ->select("ticketing_project_destinasis.*","f.city_code as from_city_code","f.airport_name as from_airport_name","f.city_name as from_city_name","t.city_code as to_city_code","t.airport_name as to_airport_name","t.city_name as to_city_name");
        return $g->get();
    }
}