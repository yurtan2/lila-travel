<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class airlines extends Model
{
    use HasFactory;
    
    protected $table ="airlines";
    protected $primaryKey ="airline_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_airlines($data=[])
    {
        $data = array_merge(array(
            "airlines_name" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = airlines::where('status','=',1);

        if($data["airlines_name"]!=null) $pc->where("airline_name","like","%".$data["airlines_name"]."%");
        if($data["search"]!=null) $pc->select($data["search"]);
        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $value = $pc->get();
        foreach ($value as $key => $item) {
            $l = new airlines_url_log();
            $item->log =  $l->get_data_log_url([
                "airline_id"=>$item->airline_id,
            ]);
        }
        return [
            "data"=>$value,
            "count"=>$total_rows
        ];
    }

    public function insertAirlines($data)
    {
        $count_data =  airlines::where('status','=',1)->where('airline_name','=',$data["airline_name"])->count();
        if($count_data>0) return -1;
        $pc = new airlines();
        $pc->airline_name = $data["airline_name"];
        $pc->airline_code_iata = $data["airline_code_iata"];
        $pc->airline_url_api = $data["airline_url_api"];
        if($data["airline_url_logo"])$pc->airline_url_logo = $data["airline_url_logo"];
        $pc->airline_pic_name = $data["airline_pic_name"];
        $pc->airline_pic_email = $data["airline_pic_email"];
        $pc->save();
        return 1;
    }

    public function updateAirlines($data)
    {
        $count_data =  airlines::where('status','=',1)->where('airline_id','!=',$data["airline_id"])->where('airline_name','=',$data["airline_name"])->count();
        if($count_data>0) return -1;
        $pc = airlines::find($data["airline_id"]);
        if($pc->airline_url_api!=$data["airline_url_api"]){
            $l = new airlines_url_log();
            $l->insertLogUrl([
                "airline_id"=>$data["airline_id"],
                "airline_url_log_api"=>$pc->airline_url_api,
            ]);
        }
        $pc->airline_name = $data["airline_name"];
        $pc->airline_code_iata = $data["airline_code_iata"];
        $pc->airline_url_api = $data["airline_url_api"];
        if(isset($data["airline_url_logo"]))$pc->airline_url_logo = $data["airline_url_logo"];
        $pc->airline_pic_name = $data["airline_pic_name"];
        $pc->airline_pic_email = $data["airline_pic_email"];
        $pc->save();
        return 1;
    }

    public function deleteAirlines($data)
    {
        $pc = airlines::find($data["airlines_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
