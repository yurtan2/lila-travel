<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class customer_family_tree extends Model
{
    use HasFactory;

    use HasFactory;
    protected $table ="customer_family_trees";
    protected $primaryKey ="customer_family_tree_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_customer_family_tree($data=[])
    {
        $data = array_merge(array(
            "customer_id" =>null,
            "customer_name" =>null,
            "customer_family_id" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        
        $pc = customer_family_tree::where('customer_family_trees.status','=',1);
        if($data["customer_id"]) $pc->where("customer_family_trees.customer_id","=",$data["customer_id"]);
        if($data["customer_family_id"]) $pc->where("customer_family_trees.customer_family_tree_id","=",intval(substr($data["customer_family_id"]."",2)));
        
        if($data["select"]) $pc->select($data["select"]);

        $total_rows =$pc->count();
        $pc->leftJoin('customers as c','c.customer_id','customer_family_trees.customer_id');
        $pc->select('customer_family_trees.*','c.*');
        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        if($data["customer_name"]) {
            $detail = customer_family_tree_detail::where('customer_family_tree_details.status','=',1)
            ->leftjoin('customers as c','c.customer_id','customer_family_tree_details.customer_id');
    
      
            $detail->where(function ($query) use ($data) {
                $query->where("c.customer_first_name","like","%".$data["customer_name"]."%")
                      ->orwhere("c.customer_last_name","like","%".$data["customer_name"]."%");
            });
            $detail =  $detail->select('customer_family_tree_details.customer_family_tree_id','c.customer_first_name','c.customer_last_name')->get();
            
            $head = customer_family_tree::where('customer_family_trees.status','=',1)
            ->leftjoin('customers as c','c.customer_id','customer_family_trees.customer_id');
            
            $head->where(function ($query) use ($data) {
                $query->where("c.customer_first_name","like","%".$data["customer_name"]."%")
                      ->orwhere("c.customer_last_name","like","%".$data["customer_name"]."%");
            });
            $head =  $head->select('customer_family_trees.customer_family_tree_id')->get();
           
            $id  = $id_head = [];
            foreach ($detail as $key => $value) {
                array_push($id,$value->customer_family_tree_id);
            }
            foreach ($head as $key => $value) {
                array_push($id_head,$value->customer_family_tree_id);
            }
            $pc->where(function ($query) use ($id,$id_head) {
                $query->whereIn('customer_family_trees.customer_family_tree_id',$id)
                      ->orwhereIn('customer_family_trees.customer_family_tree_id',$id_head);
            });
        }

        $data = $pc->get();

        foreach ($data as $key => $value) {
            $detail = customer_family_tree_detail::where('customer_family_tree_details.customer_family_tree_id','=',$value->customer_family_tree_id)
            ->where('customer_family_tree_details.status','=',1)
            ->leftJoin('customers as c','c.customer_id','customer_family_tree_details.customer_id')
            ->select('customer_family_tree_details.*','c.customer_first_name','c.customer_last_name','c.status as customer_status')->get();
            $data[$key]->detail = $detail;
           $data[$key]->total  = ($detail->count()+1)." Member";
        }
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertFamilyTree($data)
    {
        $count_data =  customer_family_tree::where('status','=',1)->where('customer_id','=',$data["customer_id"])->count();
        if($count_data>0) return -1;
        
        $pc = new customer_family_tree();
        $pc->customer_id = $data["customer_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();

        foreach (json_decode($data["detail"],true) as $key => $value) {
            $value["customer_family_tree_id"] = $pc->customer_family_tree_id;
            $p = new customer_family_tree_detail();
            $p->insertFamilyTreeDetail($value);
        }
        
        return 1;
    }

    public function updateFamilyTree($data)
    {
        $count_data =  customer_family_tree::where('status','=',1)->where('customer_family_tree_id','!=',$data["customer_family_tree_id"])->where('customer_id','=',$data["customer_id"])->count();
        if($count_data>0) return -1;
        $pc = customer_family_tree::find($data["customer_family_tree_id"]);
        $pc->customer_id = $data["customer_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();

        $detail = json_decode($data["detail"],true);
        $detail = array_filter($detail);
        $id = [];
        foreach ($detail as $key => $value) {
            array_push($id,$value["customer_family_tree_detail_id"]);
        }
        customer_family_tree_detail::where('customer_family_tree_id','=',$data["customer_family_tree_id"])->whereNotIn('customer_family_tree_detail_id',$id)->update(["status"=>0]);
        foreach ($detail as $key => $value) {
            $value["customer_family_tree_id"] = $pc->customer_family_tree_id;
            $p = new customer_family_tree_detail();
            $p->updateFamilyTreeDetail($value);
        }
        return 1;
    }

    public function deleteFamilyTree($data)
    {
        $pc = customer_family_tree::find($data["customer_family_tree_id"]);
        $pc->status = 0;
        $pc->save();
        customer_family_tree_detail::where('customer_family_tree_id','=',$data["customer_family_tree_id"])->update(["status"=>0]);
        return 1;
    }
}
