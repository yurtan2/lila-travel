<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour extends Model
{
    use HasFactory;
    protected $table ="tours";
    protected $primaryKey ="tour_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_tour($data=[])
    {

        $data = array_merge(array(
            "tour_name" =>null,
            "tour_jenis" =>null,
            "date" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        //check expired
        tour::whereDate('tour_date_end','<',date("Y-m-d H:i:s"))->where('status','=',1)->update(["status"=>2]);

        $pc = tour::where('status','>=',1);
        if($data["tour_name"]) $pc->where("tour_name","like","%".$data["tour_name"]."%");
        if($data["date"]!=null&&$data["date"]!="Undefined") $pc->whereDate('tour_date_start','<=',$data["date"])->whereDate('tour_date_end','>=',$data["date"]);
        if($data["tour_jenis"]) $pc->where("tour_jenis","=",$data["tour_jenis"]);
        if($data["search"]) $pc->select($data["search"]);
        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $total_rows =$pc->count();
        $result = $pc->get();

        foreach ($result as $key => $item) {
            $dt = tour_destination::where('tour_destinations.tour_id','=',$item->tour_id)->leftjoin('countries as c','c.id','tour_destinations.country_id')->first();
            if($dt){
                $item->country = $dt->name;
            }
            else{
                $item->country = null;
            }
            if($item->tour_price_gimick==null){
                $item->tour_price_gimick = tour_departure::where('tour_departures.tour_id','=',$item->tour_id)
                                           ->leftJoin('tour_departure_details as td','td.tour_departure_id','tour_departures.tour_departure_id')
                                           ->min('td.tour_departure_detail_price');
            }
            $item->tour_price_highest_price = tour_departure::where('tour_departures.tour_id','=',$item->tour_id)
                                           ->leftJoin('tour_departure_details as td','td.tour_departure_id','tour_departures.tour_departure_id')
                                           ->max('td.tour_departure_detail_price');
        }




        return [
            "data"=>$result,
            "count"=>$total_rows
        ];
    }

    function get_detail_tour($id) {
        $data = tour::find($id);

        //keyword
        $data->keyword = tour_keyword::where('tour_keywords.tour_id','=',$id)
        ->leftjoin('keywords as k','k.keyword_id','tour_keywords.keyword_id')
        ->select('k.keyword_name','k.keyword_id')->get();

        //destinasi
        $data->destinasi = tour_destination::where('tour_destinations.tour_id','=',$id)
        ->leftjoin('countries as c','c.id','tour_destinations.country_id')
        ->leftjoin('suppliers as s','s.supplier_id','tour_destinations.supplier_id')
        ->select('tour_destinations.tour_destination_id','c.id','c.name','s.supplier_name','s.supplier_id','tour_destinations.with_visa','tour_destinations.offline_code')->get();

        //destinasi
        $data->displayer = Displayer::find($data->displayer_id);
        $dt = displayer_detail::where('displayer_id','=',$data->displayer_id)->where('tour_id','=',$data->tour_id)->get();
        if(isset($dt[0]["displayer_detail_id"]))$data->displayer_detail_id = $dt[0]["displayer_detail_id"];

        //inclusion
        $data->tour_inclusion = tour_term::where('tour_terms.tour_id','=',$id)
        ->where('tour_terms.tour_terms_jenis','=',1)
        ->leftjoin('terms as t','t.term_id','tour_terms.tour_terms_jenis_id')
        ->select('tour_terms.*','t.term_name','t.term_id')->get();


        //exclusion
        $data->tour_exclusion = tour_term::where('tour_terms.tour_id','=',$id)
        ->where('tour_terms.tour_terms_jenis','=',2)
        ->leftjoin('terms as t','t.term_id','tour_terms.tour_terms_jenis_id')
        ->select('tour_terms.*','t.term_name','t.term_id')->get();

        //term
        $data->tour_term = tour_term::where('tour_terms.tour_id','=',$id)
        ->where('tour_terms.tour_terms_jenis','=',3)
        ->leftjoin('terms_conditions as t','t.terms_condition_id','tour_terms.tour_terms_jenis_id')
        ->select('tour_terms.*','t.terms_condition_name','t.terms_condition_id')->get();
        
       
        //term
        $data->tour_term = tour_term::where('tour_terms.tour_id','=',$id)
        ->where('tour_terms.tour_terms_jenis','=',3)
        ->leftjoin('terms_conditions as t','t.terms_condition_id','tour_terms.tour_terms_jenis_id')
        ->select('tour_terms.*','t.terms_condition_name','t.terms_condition_id')->get();
        
        //term
        $data->tour_term = tour_term::where('tour_terms.tour_id','=',$id)
        ->where('tour_terms.tour_terms_jenis','=',3)
        ->leftjoin('terms_conditions as t','t.terms_condition_id','tour_terms.tour_terms_jenis_id')
        ->select('tour_terms.*','t.terms_condition_name','t.terms_condition_id')->get();

        //Tour Galery
        $data->tour_galery = tour_galery::where('tour_id','=',$id)->get();

        //departure
        $final = [];
        $temp = tour_departure::where('tour_departures.tour_id','=',$id)
        ->where('status',"=",1)
        ->get();
        foreach ($temp as $key => $item) {
            $item->default_price = tour_departure_detail::where('tour_departure_id','=',$item->tour_departure_id)->where('tour_departure_detail_jenis','=',1)->get();
            $item->add_on = tour_departure_detail::where('tour_departure_id','=',$item->tour_departure_id)->where('tour_departure_detail_jenis','=',2)->get();
            $item->black_out = tour_black_out::where('tour_departure_id','=',$item->tour_departure_id)->get();
        }
        $data->tour_validity = $temp;

        return $data;
    }

    public function insertTour($data)
    {
        foreach ($data as $key => $item) {
            if($item=="true")$data[$key]=1;
            else if($item=="false")$data[$key]=0;
            if($item=="null")$data[$key]=null;
        }
        try {
            return tour::insertGetId($data);
        } catch (\Throwable $th) {
            dd($th);
            return -1;
        }
    }

    public function updateTour($data)
    {
        foreach ($data as $key => $item) {
            if($item=="true")$data[$key]=1;
            else if($item=="false")$data[$key]=0;
            if($item=="null")$data[$key]=null;
        }
        try {
            return tour::where('tour_id','=',$data["tour_id"])->update($data);
        } catch (\Throwable $th) {
            dd($th);
            return -1;
        }
    }

    public function deleteTour($data)
    {
        $tour  = tour::find($data["tour_id"]);
        $tour->status=0;
        $tour->save();
        displayer_detail::where('tour_id','=',$tour->tour_id)->update(["status"=>0]);
    }
}
