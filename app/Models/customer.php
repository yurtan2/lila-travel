<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class customer extends Model
{
    use HasFactory;

    use HasFactory;
    protected $table ="customers";
    protected $primaryKey ="customer_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_customer($data=[])
    {
        $data = array_merge(array(
            "family_id" =>null,
            "customer_id" =>null,
            "customer_name" =>null,
            "customer_nomor" =>null,
            "customer_nik" =>null,
            "customer_passport_number" =>null,
            "customer_tanggal_lahir" =>null,
            "unique"=>0,
            "corporate_id" =>null,
            "passport_status" =>null,
            "search"=>null,
            "all"=>0,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        if($data==0)$pc = customer::where('customers.status','=',1);
        else $pc = customer::where('customers.status','>=',0);
        
        if($data["customer_name"]) {
            $pc->where(function ($query) use ($data) {
                $query->where("customers.customer_first_name","like","%".$data["customer_name"]."%")
                      ->orwhere("customers.customer_last_name","like","%".$data["customer_name"]."%");
            });
        }
        if($data["customer_nik"]) $pc->where("customers.customer_nik","like","%".$data["customer_nik"]."%");
        if($data["customer_nomor"]) $pc->where(DB::raw("CONCAT(customers.customer_prefix_nomor,customers.customer_nomor)"),"like","%".$data["customer_nomor"]."%");
        if($data["passport_status"]=="1") {
            $pc->whereDate(DB::raw("DATE_SUB(customer_doe, INTERVAL 6 MONTH)"),">=",date("Y-m-d"));
        }
        else if($data["passport_status"]=="-1") {
            $pc->where(function ($query) use ($data) {
                $query->whereDate(DB::raw("DATE_SUB(customer_doe, INTERVAL 6 MONTH)"),"<=",date("Y-m-d"))
                      ->orWhereNull("customer_doe");
            });
        }
        if($data["customer_passport_number"]) $pc->where("customers.customer_passport_number","like","%".$data["customer_passport_number"]."%");
        if($data["customer_tanggal_lahir"]) $pc->where("customers.customer_tanggal_lahir","=",$data["customer_tanggal_lahir"]);
        if($data["customer_id"]) $pc->where("customers.customer_id","=",$data["customer_id"]);
        if($data["select"]) $pc->select($data["select"]);
        else $pc->select("customers.*");

        if($data["family_id"]) {
            $head = customer_family_tree::where('status','=',1)->where('customer_family_tree_id','=',intval(substr($data["family_id"]."",2)))->select('customer_id')->get();
            $detail = customer_family_tree_detail::where('status','=',1)->where('customer_family_tree_id','=',intval(substr($data["family_id"]."",2)))->select('customer_id')->get();

            $id  = [];
            foreach ($head as $key => $value) {
                array_push($id,$value->customer_id);
            }
            foreach ($detail as $key => $value) {
                array_push($id,$value->customer_id);
            }

            $pc->whereIn('customers.customer_id',$id);
        }

        if($data["corporate_id"]) {
            $head = corporate_pic::where('status','=',1)->where('corporate_id','=',intval(substr($data["corporate_id"]."",2)))->select('customer_id')->get();

            $id  = [];
            foreach ($head as $key => $value) {
                array_push($id,$value->customer_id);
            }
            $pc->whereIn('customers.customer_id',$id);
        }

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data_customer = $pc->get();

        if($data["unique"]==1){
            $temp = [];
            foreach ($data_customer as $key => $value) {

                $c = customer_family_tree::where('customer_id','=',$value["customer_id"])->where('status','=',1)->count();
                if($c<=0)array_push($temp,$value);
            }
            $data_customer = $temp;
        }
        foreach ($data_customer as $key => $value) {
            $c = customer_family_tree::where('customer_id','=',$value["customer_id"])->where('status','=',1)->first();
            try {
                if($c){
                    $value->customer_family_tree_id = $c["customer_family_tree_id"];
                }
                else{
                    $c = customer_family_tree_detail::where('customer_id','=',$value["customer_id"])->where('status','=',1)->first();
                    $value->customer_family_tree_id = $c["customer_family_tree_id"];
                }
            } catch (\Throwable $th) {
                $value->customer_family_tree_id = "-";
            }
            $d = corporate_pic::where('corporate_pics.corporate_pic_id','=',$value->corporate_pic_id)->leftJoin('corporate_position as cp','cp.corporate_position_id','corporate_pics.jabatan_id')->select("cp.corporate_position_name")
            ->first();
            if(!$d)$d="-";
            else $d = $d->corporate_position_name;
            $value->jabatan = $d;
            $value->list_pic = corporate_pic::where('corporate_pics.status','=',1)->where('corporate_pics.customer_id','=',$value->customer_id)
            ->leftJoin('corporates as c','c.corporate_id','corporate_pics.corporate_id')
            ->leftJoin('corporate_position as cp','cp.corporate_position_id','corporate_pics.jabatan_id')->get();
        }

        // dd($data_customer);
        return [
            "data"=>$data_customer,
            "count"=>$total_rows
        ];
    }

    public function insertCustomer($data)
    {
        $count_data =  customer::where('status','=',1)->where('customer_first_name','=',$data["customer_first_name"])->where('customer_last_name','=',$data["customer_last_name"])->count();
        if($count_data>0) return -1;
        $pc = new customer();
        $pc->customer_title = $data["customer_title"];
        $pc->customer_prefix_nomor = $data["customer_prefix_nomor"];
        $pc->customer_first_name = $data["customer_first_name"];
        $pc->customer_last_name = $data["customer_last_name"];
        $pc->customer_nik = $data["customer_nik"];
        $pc->customer_nomor = $data["customer_phone"];
        $pc->customer_gender = $data["customer_gender"];
        $pc->customer_tanggal_lahir = $data["customer_tanggal_lahir"];
        $pc->customer_religion = $data["customer_religion"];
        $pc->customer_email = $data["customer_email"];
        $pc->customer_umur = $data["customer_umur"];
        $pc->customer_alamat = $data["customer_alamat"];
        $pc->customer_jenis = $data["customer_jenis"];
        $pc->customer_referensi_makanan = $data["customer_referensi_makanan"];
        $pc->customer_passport_number = $data["customer_passport_number"];
        $pc->customer_poi = $data["customer_poi"];
        $pc->customer_doi = $data["customer_doi"];
        $pc->customer_doe = $data["customer_doe"];

        if($data["ktp"]!="undefined")$pc->customer_ktp = $data["customer_ktp"];//GAMBAR KTP
        if($data["passport"]!="undefined")$pc->customer_passport = $data["customer_passport"];//GAMBAR PASSPORT
        if($data["visa"]!="undefined")$pc->customer_visa = $data["customer_visa"];//GAMBAR Visa
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();

        $pic = json_decode($data["list_pic"],true);
        foreach ($pic as $key => $value) {
            $value["customer_id"] = $pc->customer_id;
            $d = new corporate_pic();
            $value["corporate_pic_id"] = $d->insertCorporatePIC($value);
            if($value["default"]==1){
                $pc->corporate_pic_id = $value["corporate_pic_id"];
                $pc->corporate_id= $value["corporate_id"];
            }
        }

        return 1;
    }

    public function updateCustomer($data)
    {
        $count_data =  customer::where('status','=',1)->where('customer_id','!=',$data["customer_id"])->where('customer_first_name','=',$data["customer_first_name"])->where('customer_last_name','=',$data["customer_last_name"])->count();
        if($count_data>0) return -1;
        $pc = customer::find($data["customer_id"]);
        $pc->customer_title = $data["customer_title"];
        $pc->customer_prefix_nomor = $data["customer_prefix_nomor"];
        $pc->customer_first_name = $data["customer_first_name"];
        $pc->customer_last_name = $data["customer_last_name"];
        $pc->customer_nik = $data["customer_nik"];
        $pc->customer_nomor = $data["customer_phone"];
        $pc->customer_gender = $data["customer_gender"];
        $pc->customer_tanggal_lahir = $data["customer_tanggal_lahir"];
        $pc->customer_umur = $data["customer_umur"];
        $pc->customer_alamat = $data["customer_alamat"];
        $pc->customer_religion = $data["customer_religion"];
        $pc->customer_email = $data["customer_email"];
        $pc->customer_jenis = $data["customer_jenis"];
        $pc->customer_referensi_makanan = $data["customer_referensi_makanan"];
        $pc->customer_passport_number = $data["customer_passport_number"];
        $pc->customer_poi = $data["customer_poi"];
        $pc->customer_doi = $data["customer_doi"];
        $pc->customer_doe = $data["customer_doe"];
        if($data["ktp"]!="undefined")$pc->customer_ktp = $data["customer_ktp"];//GAMBAR KTP
        if($data["passport"]!="undefined")$pc->customer_passport = $data["customer_passport"];//GAMBAR PASSPORT
        if($data["visa"]!="undefined")$pc->customer_visa = $data["customer_visa"];//GAMBAR Visa
        $pc->updated_by = Session::get("user")->user_username;


        $pic = json_decode($data["list_pic"],true);
        $id = [];
        foreach ($pic as $key => $item) {
            if(isset($item["corporate_pic_id"])){
                array_push($id,$item["corporate_pic_id"]);

                if($pc->corporate_pic_id==$item["corporate_pic_id"]){
                    $pc->corporate_pic_id=$pc->corporate_pic_id=null;
                }
            }
        }
        $list_delete = [];
        
        $list = corporate_pic::where('customer_id','=',$pc->customer_id)
        ->whereNotIn('corporate_pic_id',$id)
        ->where('status',"=",1)
        ->select("corporate_pic_id")->get();
        
        foreach ($list as $key => $value) {
            array_push($list_delete,$value->corporate_pic_id);
        }
        if(count($id)>0)corporate_pic::whereNotIn('corporate_pic_id',$id)->where('customer_id','=',$pc->customer_id)->update(["status"=>0]);
        $empty = 1;
        foreach ($pic as $key => $value) {

            $value["customer_id"] = $pc->customer_id;
            $d = new corporate_pic();
            if(!isset($value["corporate_pic_id"]))$value["corporate_pic_id"] = $d->insertCorporatePIC($value);
            if($value["default"]==1&&array_search($value["corporate_pic_id"],$list_delete)==false){
                $pc->corporate_pic_id = $value["corporate_pic_id"];
                $pc->corporate_id= $value["corporate_id"];
                $empty=0;
            }

        }
        if($empty==1){
            $pc->corporate_pic_id = null;
            $pc->corporate_id= null;
        }
        $pc->save();

        return 1;
    }

    public function deleteCustomer($data)
    {
        $pc = customer::find($data["customer_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }


    public function loginCustomer($data)
    {
        return customer::where('customer_email','=',$data["customer_email"])
        ->where('customer_password','=',$data["customer_password"])->get();
    }

}
