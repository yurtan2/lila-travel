<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_departure extends Model
{
    use HasFactory;
    protected $table ="tour_departures";
    protected $primaryKey ="tour_departure_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertTourDeparture($data) {
        $i = new tour_departure();
        $i->tour_id = $data["tour_id"];
        $i->tour_departure_date_start = $data["tour_start_date"];
        $i->tour_departure_date_end = $data["tour_end_date"];
        $i->tour_departure_deposit = intval(str_replace('.','',$data["tour_departure_deposit"]));
        $i->tour_departure_name = isset($data["tour_departure_name"])?$data["tour_departure_name"]:null;
        $i->save();

        
        foreach ($data["price"] as $key => $value) {
            $value["tour_departure_id"] = $i->tour_departure_id;
            $value["tour_jenis"] = $data["tour_jenis"];
            $td = new tour_departure_detail();
            $td->insertTourDepartureDetail($value);
        }

        if($data["tour_jenis"]==2){
            foreach ($data["black_out"] as $key => $value) {
                $value["tour_departure_id"] = $i->tour_departure_id;    
                $tb = new tour_black_out();
                $tb->insertBlackOut($value);
            }
        }

        return $i->tour_departure_id;
    }
    
    function updateTourDeparture($data,$tour_id) {
        $id = [];
        foreach ($data as $key => $value) {
           if(isset($value->tour_departure_id)) array_push($id,$value->tour_departure_id);
        }
       
        tour_departure::where('tour_id','=',$tour_id)
        ->whereNotIn('tour_departure_id',$id)->delete();
        
        foreach ($data as $key => $value) {   
            if(isset($value->tour_departure_id)){
                if(isset($value->tour_departure_name)&&$value->tour_jenis==2||$value->tour_jenis==1){
                    $i = tour_departure::find($value->tour_departure_id);
                    $i->tour_id = $tour_id;
                    $i->tour_departure_date_start = $value->tour_start_date;
                    $i->tour_departure_date_end = $value->tour_end_date;
                    $i->tour_departure_name = isset($value->tour_departure_name)?$value->tour_departure_name:null;
                    $i->tour_departure_deposit = intval(str_replace(['.','Rp'],'',$value->tour_departure_deposit));
                    $i->save();
                }
            }
            else{
                $temp = [
                    "tour_jenis"=>isset($value->tour_jenis)?$value->tour_jenis:1,
                    "tour_id"=>$tour_id,
                    "tour_start_date"=>$value->tour_start_date,
                    "tour_end_date"=>$value->tour_end_date,
                    "tour_departure_name"=>isset($value->tour_departure_name)?$value->tour_departure_name:null,
                    "tour_departure_deposit"=>intval(str_replace(['.','Rp'],'',$value->tour_departure_deposit)),
                    "price"=>[]
                ];
                $value->tour_departure_id = $this->insertTourDeparture($temp);
            }
            
            $td = new tour_departure_detail();
            $td->updateTourDepartureDetail($value->price,$value->tour_departure_id);
        }
      
    }
    
    function updateFITTourDeparture($data,$tour_id) {
        $id = [];
        foreach ($data as $key => $value) {
           if(isset($value["tour_departure_id"])) array_push($id,$value["tour_departure_id"]);
        }
       
        tour_departure::where('tour_id','=',$tour_id)
        ->whereNotIn('tour_departure_id',$id)->delete();

        foreach ($data as $key => $value) {
           
            if(isset($value["tour_departure_id"])){
                
                $i = tour_departure::find($value["tour_departure_id"]);

                $i->tour_id = $tour_id;
                $i->tour_departure_date_start = $value["tour_start_date"];
                $i->tour_departure_date_end = $value["tour_end_date"];
                $i->tour_departure_name = $value["tour_departure_name"];
                $i->tour_departure_deposit = intval(str_replace(['.','Rp'],'',$value["tour_departure_deposit"]));
                $i->save();
                
            }
            else{
                
                $temp = [
                    "tour_id"=>$tour_id,
                    "tour_jenis"=>$value["tour_jenis"],
                    "tour_start_date"=>$value["tour_start_date"],
                    "tour_end_date"=>$value["tour_end_date"],
                    "tour_departure_name"=>$value["tour_departure_name"],
                    "tour_departure_deposit"=>intval(str_replace(['.','Rp'],'',$value["tour_departure_deposit"])),
                    "price"=>[]
                ];
                $value["tour_departure_id"] = $this->insertTourDeparture($temp);
            }
   
            $td = new tour_departure_detail();
            $td->updateTourDepartureDetail($value["price"],$value["tour_departure_id"]);
            $bo = new tour_black_out();
            foreach ($value["black_out"] as $key => $item) {
                $value["black_out"][$key]["tour_departure_id"] = $value["tour_departure_id"];
            }
            $bo->updateBlackOut($value["black_out"]);
        }
      
    }
}
