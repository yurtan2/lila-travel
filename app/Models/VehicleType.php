<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class VehicleType extends Model
{
    use HasFactory;


    protected $table = 'vehicle_types';
    protected $primaryKey = 'vehicle_type_id';
    public $incrementing = true;
    public $timestamps = true;

    public function get_data_vehicle_type($data=[])
    {
        $data = array_merge(array(
            "vehicle_type_nama" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = VehicleType::where('status','=',1);
        if($data["vehicle_type_name"]) $pc->where("vehicle_type_name","like","%".$data["vehicle_type_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertVehicleType($data)
    {
        $count_data =  VehicleType::where('status','=',1)->where('vehicle_type_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new VehicleType();
        $pc->vehicle_type_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updateVehicleType($data)
    {
        $count_data =  VehicleType::where('status','=',1)->where('vehicle_type_id','!=',$data["vehicle_type_id"])->where('vehicle_type_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = VehicleType::find($data["vehicle_type_id"]);
        $pc->vehicle_type_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteVehicleType($data)
    {
        $pc = VehicleType::find($data["vehicle_type_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
