<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Document extends Model
{
    use HasFactory;
    protected $table ="documents";
    protected $primaryKey ="document_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_document($data=[])
    {
        $data = array_merge(array(
            "document_name" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Document::where('status','=',1);
        if($data["document_name"]) $pc->where("document_name","like","%".$data["document_name"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertDocument($data)
    {
        $count_data =  Document::where('status','=',1)->where('document_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new Document();
        $pc->document_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updateDocument($data)
    {
        $count_data =  Document::where('status','=',1)->where('document_id','!=',$data["document_id"])->where('document_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = Document::find($data["document_id"]);
        $pc->document_name = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteDocument($data)
    {
        $pc = Document::find($data["document_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }
}
