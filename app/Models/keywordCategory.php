<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class keywordCategory extends Model
{
    use HasFactory;
    protected $table ="keyword_categories";
    protected $primaryKey ="keyword_category_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_keyword_category($data=[])
    {
        $data = array_merge(array(
            "keyword_category_name" =>null,
        ), $data);

        $k = keywordCategory::where('status',"=",1);
        if($data["keyword_category_name"]!=null) $k->where("keyword_category_name","like","%".$data["keyword_category_name"]."%");
        
        return $k->get();
    }

    public function insertKeywordCategory($data = [])
    {
        $k = new keywordCategory();
        $k->keyword_category_name = $data["keyword_category_name"];
        $k->save();
        return keywordCategory::where("status","=",1)->get();
    }

    public function deleteKeywordCategory($data = [])
    {
        $k = keywordCategory::find($data["keyword_category_id"]);   
        $k->status = 0;
        $k->save();
        return keywordCategory::where("status","=",1)->get();
    }

}
