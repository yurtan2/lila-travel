<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Supplier extends Model
{
    use HasFactory;
    protected $table ="suppliers";
    protected $primaryKey ="supplier_id";
    public $timestamps = true;
    public  $incrementing = true;


    public function get_data_supplier($data=[])
    {
        $data = array_merge(array(
            "supplier_name" =>null,
            "country_id" =>null,
            "sp_category_id" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = Supplier::where('suppliers.status','=',1);
        if($data["supplier_name"]) $pc->where("suppliers.supplier_name","like","%".$data["supplier_name"]."%");
        if($data["country_id"]) $pc->where("suppliers.country_id","=",$data["country_id"]);
        if($data["sp_category_id"]) $pc->where("suppliers.sp_category_id","=",$data["sp_category_id"]);

        $total_rows =$pc->count();
        $pc->leftjoin('supplier_categories as sc',"sc.sp_category_id","suppliers.sp_category_id");
        $pc->leftjoin('countries as c',"c.id","suppliers.country_id");

        if($data["search"]) $pc->select($data["search"]);
        else $pc->select('suppliers.*','sc.sp_category_nama','sc.sp_category_id',"c.name as country_name","c.id as country_id");

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();

        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    public function insertSupplier($data)
    {
        $count_data =  Supplier::where('status','=',1)->where('supplier_name','=',$data["supplier_name"])->count();
        if($count_data>0) return -1;
        $pc = new Supplier();
        $pc->supplier_name = $data["supplier_name"];
        $pc->supplier_phone = $data["supplier_phone"];
        $pc->supplier_email = $data["supplier_email"];
        $pc->supplier_address = $data["supplier_address"];
        $pc->supplier_pic = $data["supplier_pic"];
        $pc->supplier_bank_name = $data["supplier_bank_name"];
        $pc->supplier_bank_account = $data["supplier_bank_account"];
        $pc->supplier_swift_code = $data["supplier_swift_code"];
        $pc->supplier_curency = $data["supplier_curency"];
        $pc->supplier_benificary_name = $data["supplier_benificary_name"];
        $pc->sp_category_id = $data["sp_category_id"];
        $pc->country_id = $data["country_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function updateSupplier($data)
    {
        $count_data =  Supplier::where('status','=',1)->where('supplier_id','!=',$data["supplier_id"])->where('supplier_name','=',$data["supplier_name"])->count();
        if($count_data>0) return -1;
        $pc = Supplier::find($data["supplier_id"]);
        $pc->supplier_name = $data["supplier_name"];
        $pc->supplier_phone = $data["supplier_phone"];
        $pc->supplier_email = $data["supplier_email"];
        $pc->supplier_address = $data["supplier_address"];
        $pc->supplier_pic = $data["supplier_pic"];
        $pc->supplier_bank_name = $data["supplier_bank_name"];
        $pc->supplier_bank_account = $data["supplier_bank_account"];
        $pc->supplier_swift_code = $data["supplier_swift_code"];
        $pc->supplier_curency = $data["supplier_curency"];
        $pc->supplier_benificary_name = $data["supplier_benificary_name"];
        $pc->sp_category_id = $data["sp_category_id"];
        $pc->country_id = $data["country_id"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteSupplier($data)
    {
        $pc = Supplier::find($data["supplier_id"]);
        $pc->status = 0;
        $pc->save();

        return 1;
    }
}
