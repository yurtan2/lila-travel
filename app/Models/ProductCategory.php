<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class ProductCategory extends Model
{
    use HasFactory;
    protected $table ="product_categories";
    protected $primaryKey ="pr_category_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_category($data=[])
    {
        $data = array_merge(array(
            "pr_category_nama" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = ProductCategory::where('status','=',1);
        if($data["pr_category_nama"]) $pc->where("pr_category_nama","like","%".$data["pr_category_nama"]."%");
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function insertProduct($data)
    {
        $count_data =  ProductCategory::where('status','=',1)->where('pr_category_nama','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new ProductCategory();
        $pc->updated_by = Session::get("user")->user_username;
        $pc->pr_category_nama = $data["nama"];
        $pc->save();
        return 1;
    }

    public function updateProduct($data)
    {
        $count_data =  ProductCategory::where('status','=',1)->where('pr_category_id','!=',$data["pr_category_id"])->where('pr_category_nama','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = ProductCategory::find($data["pr_category_id"]);
        $pc->pr_category_nama = $data["nama"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteProduct($data)
    {
        $pc = ProductCategory::find($data["pr_category_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }
}
