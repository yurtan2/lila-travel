<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class keyword extends Model
{
    use HasFactory;
    protected $table ="keywords";
    protected $primaryKey ="keyword_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_keyword($data=[])
    {
        $data = array_merge(array(
            "keyword_name" =>null,
            "keyword_category_id" =>null,
            "search"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = keyword::where('keywords.status','=',1);
        if($data["keyword_name"]) $pc->where("keywords.keyword_name","like","%".$data["keyword_name"]."%");
        if($data["keyword_category_id"]) $pc->where("keywords.keyword_category_id","=",$data["keyword_category_id"]);
        
        $pc->leftjoin('keyword_categories as kc','kc.keyword_category_id','keywords.keyword_category_id');
        if($data["search"]) $pc->select($data["search"]);
        else $pc->select("kc.keyword_category_id","kc.keyword_category_name","keywords.*");

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get();
        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    
    public function insertKeyword($data = [])
    {
        $k = new keyword();
        $k->keyword_name = $data["nama"];
        $k->keyword_category_id = $data["category"];
        $k->keyword_detail = $data["keyword"];
        $k->updated_by = Session::get("user")->user_username;
        $k->save();
        return 1;
    }

    public function updateKeyword($data = [])
    {
        $k = keyword::find($data["keyword_id"]);
        $k->keyword_name = $data["nama"];
        $k->keyword_category_id = $data["category"];
        $k->keyword_detail = $data["keyword"];
        $k->updated_by = Session::get("user")->user_username;
        $k->save();
        return 1;
    }

    public function deleteKeyword($data = [])
    {
        $k = keyword::find($data["keyword_id"]);   
        $k->status = 0;
        $k->save();
        return 1;
    }
}
