<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class displayer_detail extends Model
{
    use HasFactory;
    protected $table ="displayer_details";
    protected $primaryKey ="displayer_detail_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_displayer_detail($data=[])
    {
        $data = array_merge(array(
            "displayer_id"=>null,
            "select"=>null,
            "start"=> null,
            "length"=>null
        ), $data);
        $this->checkExpired();
        $pc = displayer_detail::where('displayer_details.status','=',1);//0 = delete,1 = aktif, 2= expired
        if($data["displayer_id"]) $pc->where('displayer_details.displayer_id','=',$data["displayer_id"]);
        if($data["select"]) $pc->select($data["select"]);
        else $pc->select('displayer_details.*','t.*','t.status as tour_status');

        $total_rows =$pc->count();
        $pc->leftjoin("tours as t",'t.tour_id','displayer_details.tour_id');

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        $data = $pc->get(); 
        foreach ($data as $key => $item) {
            $dt = tour_destination::where('tour_destinations.tour_id','=',$item->tour_id)->leftjoin('countries as c','c.id','tour_destinations.country_id')->first();
          
            if(isset($dt->name))$item->country = $dt->name;
            if($item->tour_price_gimick==null){
                $item->tour_price_gimick = tour_departure::where('tour_departures.tour_id','=',$item->tour_id)
                                           ->leftJoin('tour_departure_details as td','td.tour_departure_id','tour_departures.tour_departure_id')
                                           ->min('td.tour_departure_detail_price');
            }
            $item->tour_price_highest_price = tour_departure::where('tour_departures.tour_id','=',$item->tour_id)
                                           ->leftJoin('tour_departure_details as td','td.tour_departure_id','tour_departures.tour_departure_id')
                                           ->max('td.tour_departure_detail_price');
           
        }

        return [
            "data"=>$data,
            "count"=>$total_rows
        ];
    }

    function checkExpired() {
        displayer_detail::where('displayer_details.status','=',1)
        ->leftjoin("tours as t",'t.tour_id','displayer_details.tour_id')
        ->where('t.status','=',2)->update(["displayer_details.status"=>2]);
       
    }
    
    function InsertDisplayerDetail($data,$id){
        foreach ($data as $key => $value) {
            $pc = new displayer_detail();
            $pc->tour_id=$value["tour_id"];
            $pc->displayer_id=$id;
            $pc->save();
        }
    }

    function deleteDisplayerDetail($data){
        if($data==null) return "";
        foreach ($data as $key => $value) {
            /*
            $pc = Displayer::where('tour_id','=',$value["tour_id"])
            ->where('displayer_id','=',$value["displayer_id"])
            ->update(["status"=>1]);*/

            if(isset($value["displayer_detail_id"])){
                $pc = displayer_detail::find($value["displayer_detail_id"]);
                $pc->status = 0;
                $pc->save();
            }
        }

    }

}
