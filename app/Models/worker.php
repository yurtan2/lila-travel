<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class worker extends Model
{
    use HasFactory;

    protected $table ="workers";
    protected $primaryKey ="worker_id";
    public $timestamps = true;
    public  $incrementing = true;

    public function get_data_worker($data=[])
    {
        $data = array_merge(array(
            "worker_kode" =>null,   
            "worker_name" =>null,
            "worker_type" =>null,
            "search"=>null,
            "start"=> null,
            "length"=>null
        ), $data);

        $pc = worker::where('status','=',1);
        if($data["worker_kode"]) $pc->where("worker_kode","like","%".$data["worker_kode"]."%");
        if($data["worker_name"]) $pc->where("worker_name","like","%".$data["worker_name"]."%");
        if($data["worker_type"]) $pc->where("worker_type","=",$data["worker_type"]);
        if($data["search"]) $pc->select($data["search"]);

        $total_rows =$pc->count();

        if($data["start"]!=null) $pc->offset($data["start"]);
        if($data["length"]!=null) $pc->limit($data["length"]);

        return [
            "data"=>$pc->get(),
            "count"=>$total_rows
        ];
    }

    public function get_kode($type)
    {
        $ty = "LG";
        if($type=="Tour Leader")$ty="LT";
        else if($type=="Driver")$ty="LD";

        $urut = worker::where('worker_type',"=",$type)->orderby("created_at","desc")->first();
        if($urut)$urut = substr($urut->worker_kode,-3,3)+1;
        else $urut = 1;

        return $ty.str_pad($urut,3,"0",STR_PAD_LEFT);
    }

    public function insertWorker($data)
    {
        $data = array_merge(array(
            "photo_url" =>null,
            "ktp_url" =>null,
            "passport_url" =>null,
            "drive_url" =>null,
            "guide_url" =>null,
        ), $data);

        $count_data =  worker::where('status','=',1)->where('worker_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = new worker();
        $pc->worker_name = $data["nama"];
        $pc->worker_kode = $this->get_kode($data["type"]);
        $pc->worker_type = $data["type"];
        $pc->worker_phone = $data["phone"];
        $pc->worker_email = $data["email"];
        $pc->worker_address = $data["address"];
        $pc->worker_language1 = $data["language1"];
        $pc->worker_language2 = $data["language2"];
        $pc->worker_language3 = $data["language3"];
        $pc->worker_photo = $data["photo_url"];
        $pc->worker_ktp = $data["ktp_url"];
        $pc->worker_passport = $data["passport_url"];
        $pc->worker_drive = $data["drive_url"];
        $pc->worker_guide = $data["guide_url"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function editWorker($data)
    {
        $count_data =  worker::where('status','=',1)->where('worker_id','!=',$data["worker_id"])->where('worker_name','=',$data["nama"])->count();
        if($count_data>0) return -1;
        $pc = worker::find($data["worker_id"]);
        $pc->worker_name = $data["nama"];
        $pc->worker_kode = $this->get_kode($data["type"]);
        $pc->worker_type = $data["type"];
        $pc->worker_phone = $data["phone"];
        $pc->worker_email = $data["email"];
        $pc->worker_address = $data["address"];
        $pc->worker_language1 = $data["language1"];
        $pc->worker_language2 = $data["language2"];
        $pc->worker_language3 = $data["language3"];
        if(isset($data["photo_url"]))$pc->worker_photo = $data["photo_url"];
        if(isset($data["ktp_url"]))$pc->worker_ktp = $data["ktp_url"];
        if(isset($data["passport_url"]))$pc->worker_passport = $data["passport_url"];
        if(isset($data["drive_url"]))$pc->worker_drive = $data["drive_url"];
        if(isset($data["guide_url"]))$pc->worker_guide = $data["guide_url"];
        $pc->updated_by = Session::get("user")->user_username;
        $pc->save();
        return 1;
    }

    public function deleteWorker($data)
    {
        $pc = worker::find($data["worker_id"]);
        $pc->status = 0;
        $pc->save();
        return 1;
    }

}
