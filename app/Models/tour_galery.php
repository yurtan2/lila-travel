<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour_galery extends Model
{
    use HasFactory;
    protected $table ="tour_galeries";
    protected $primaryKey ="tour_galery_id";
    public $timestamps = true;
    public  $incrementing = true;

    function insertGalery($data) {
        $i = new tour_galery();
        $i->tour_id = $data["tour_id"];
        $i->tour_images = $data["tour_images"];
        $i->save();
    }
}
