<?php

namespace App\Imports;

use App\Models\Country;
use App\Models\Supplier;
use App\Models\SupplierCategory;
use Exception;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use ZipArchive;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Importable;

class importSupplier implements ToCollection, WithStartRow
{
    use Importable;
    public function collection(Collection $row)
    {
        $success=1;
        foreach($row as $row) {
            if($row->filter()->isNotEmpty()&&isset($row[4])&&isset($row[3])){
                
                $s = new Supplier();
                $location_id = "0";
                $sp_category_id = "0";
                $pic = [];
                
                if($row[3]!=null&&$row[3]!=""){
                    $data = SupplierCategory::where("sp_category_nama","like","%".$row[3]."%")->first();
                    if($data!=null)$sp_category_id = $data->sp_category_id;
                    else throw new Exception("Periksa Kembali Nama Category Anda!",401);
                    
                }
                if($row[4]!=null&&$row[4]!=""){
                    $data = Country::where("name","like","%".$row[4]."%")->first();
                    if($data!=null)$location_id = $data->id;
                    else throw new Exception("Periksa Kembali Nama Country Anda!",401);
                }

                $name = explode(';',$row[11]);
                $no = explode(';',$row[12]);
                $email = explode(';',$row[13]);
                $jabatan = explode(';',$row[14]);

                for ($i=0; $i < count($name); $i++) { 
                    array_push($pic,[
                        "pic_name"=>$name[$i],
                        "pic_phone"=>$no[$i],
                        "pic_email"=>$email[$i],
                        "pic_jabatan"=>$jabatan[$i]
                    ]);
                }

                $data = $s->insertSupplier([
                    "supplier_name"=>$row[0],
                    "supplier_phone"=>$row[1],
                    "supplier_email"=>$row[2],
                    "sp_category_id"=>$sp_category_id,
                    "country_id"=>$location_id,
                    "supplier_address"=>$row[5],
                    "supplier_bank_name"=>$row[6],
                    "supplier_benificary_name"=>$row[7],
                    "supplier_bank_account"=>$row[8],
                    "supplier_swift_code"=>$row[9],
                    "supplier_curency"=>$row[10],
                    "supplier_pic"=>json_encode($pic)
                ]);
                if($data==-1)throw new Exception("Terdapat Nama Supplier Kembar!");
               
            }
            
        }   
        return $success;
    }

    public function startRow(): int
    {
        return 2;
    }
}
