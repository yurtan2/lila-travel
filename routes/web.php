<?php

use App\Http\Controllers\AutocompleteController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\KeywordController;
use App\Http\Controllers\BuktiPengeluaranController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CorporateController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DisplayerController;
use App\Http\Controllers\FlightTicket;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TermsController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\TravelDocumentsController;
use App\Http\Controllers\UserCategory;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\WorkerController;
use App\Http\Controllers\TicketProjectController;
use App\Models\keywordCategory;
use App\Models\worker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//general
Route::get('/', [GeneralController::class, 'landing_page'])->name('landing_page');
Route::get('/landing_page/tour', [GeneralController::class, 'landing_page_tour'])->name('landing_page_tour');
Route::get('/aboutUs', [GeneralController::class, 'aboutUs'])->name('aboutUs');
Route::get('/faq', [GeneralController::class, 'faq'])->name('faq');
Route::get('/contact', [GeneralController::class, 'contact'])->name('contact');
Route::get('/login', [GeneralController::class, 'login'])->name('login');
Route::get('/logout', [GeneralController::class, 'logout'])->name('logout');
Route::post('/mekanismeLogin', [GeneralController::class, 'mekanismeLogin'])->name('mekanismeLogin');

Route::get('/loginGoogle', [GeneralController::class, 'loginGoogle'])->name('loginGoogle');
Route::post('/callbackGoogle', [GeneralController::class, 'callbackGoogle'])->name('callbackGoogle');

//AutocomplateController
Route::post('/autocompleteProductCategory', [AutocompleteController::class, 'autocompleteProductCategory'])->name('autocompleteProductCategory');
Route::post('/autocompleteSupplierCategory', [AutocompleteController::class, 'autocompleteSupplierCategory'])->name('autocompleteSupplierCategory');
Route::post('/autocompleteSupplier', [AutocompleteController::class, 'autocompleteSupplier'])->name('autocompleteSupplier');
Route::post('/autocompleteCountry', [AutocompleteController::class, 'autocompleteCountry'])->name('autocompleteCountry');
Route::post('/autocompleteVehicleType', [AutocompleteController::class, 'autocompleteVehicleType'])->name('autocompleteVehicleType');
Route::post('/autocompleteDocument', [AutocompleteController::class, 'autocompleteDocument'])->name('autocompleteDocument');
Route::post('/autocompleteStates', [AutocompleteController::class, 'autocompleteStates'])->name('autocompleteStates');
Route::post('/autocompleteKeywordCategory', [AutocompleteController::class, 'autocompleteKeywordCategory'])->name('autocompleteKeywordCategory');
Route::post('/autocompleteLocation', [AutocompleteController::class, 'autocompleteLocation'])->name('autocompleteLocation');
Route::post('/autocompleteDisplayer', [AutocompleteController::class, 'autocompleteDisplayer'])->name('autocompleteDisplayer');
Route::post('/autocompleteTermCondition', [AutocompleteController::class, 'autocompleteTermCondition'])->name('autocompleteTermCondition');
Route::post('/autocompleteInclusion', [AutocompleteController::class, 'autocompleteInclusion'])->name('autocompleteInclusion');
Route::post('/autocompleteExclusion', [AutocompleteController::class, 'autocompleteExclusion'])->name('autocompleteExclusion');
Route::post('/autocompleteTour', [AutocompleteController::class, 'autocompleteTour'])->name('autocompleteTour');
Route::post('/autocompleteCustomer', [AutocompleteController::class, 'autocompleteCustomer'])->name('autocompleteCustomer');
Route::post('/autocompleteAirport', [AutocompleteController::class, 'autocompleteAirport'])->name('autocompleteAirport');
Route::post('/autocompletePosition', [AutocompleteController::class, 'autocompletePosition'])->name('autocompletePosition');
Route::post('/autocompleteKeyword', [AutocompleteController::class, 'autocompleteKeyword'])->name('autocompleteKeyword');
Route::post('/autocompleteCorporate', [AutocompleteController::class, 'autocompleteCorporate'])->name('autocompleteCorporate');
Route::post('/autocompleteCorporatePIC', [AutocompleteController::class, 'autocompleteCorporatePIC'])->name('autocompleteCorporatePIC');
Route::post('/autocompleteAirlines', [AutocompleteController::class, 'autocompleteAirlines'])->name('autocompleteAirlines');

//Dashboard Customer
Route::get('/dashboard', [CustomerController::class, 'dashboard'])->name('dashboard');
Route::get('/profile_customer', [CustomerController::class, 'profile_customer'])->name('profile_customer');

//Dashboard Admin
// Tour Detail
Route::get("/tour/detail/{id}", [GeneralController::class, "detail"]);
Route::get("/tour/order/{id}", [GeneralController::class, "order"]);

Route::middleware(['checkSession'])->prefix('master')->group(function () {
    //Product Category
    Route::get('/product_category', [CategoryController::class, 'product_category'])->name('product_category');
    Route::get('/get_data_product_category', [CategoryController::class, 'get_data_product_category'])->name('get_data_product_category');
    Route::post('/insertProductCategory', [CategoryController::class, 'insertProductCategory'])->name('insertProductCategory');
    Route::post('/editProductCategory', [CategoryController::class, 'editProductCategory'])->name('editProductCategory');
    Route::post('/deleteProductCategory', [CategoryController::class, 'deleteProductCategory'])->name('deleteProductCategory');
    Route::get('/exportDataProductCategory', [CategoryController::class, 'exportDataProductCategory'])->name('exportDataProductCategory');

    //Supplier Category
    Route::get('/supplier_category', [CategoryController::class, 'supplier_category'])->name('supplier_category');
    Route::get('/get_data_supplier_category', [CategoryController::class, 'get_data_supplier_category'])->name('get_data_supplier_category');
    Route::post('/insertSupplierCategory', [CategoryController::class, 'insertSupplierCategory'])->name('insertSupplierCategory');
    Route::post('/editSupplierCategory', [CategoryController::class, 'editSupplierCategory'])->name('editSupplierCategory');
    Route::post('/deleteSupplierCategory', [CategoryController::class, 'deleteSupplierCategory'])->name('deleteSupplierCategory');
    Route::get('/exportDataSupplierCategory', [CategoryController::class, 'exportDataSupplierCategory'])->name('exportDataSupplierCategory');


    //Location - Country
    Route::get('/country', [LocationController::class, 'country'])->name('country');
    Route::get('/get_data_country', [LocationController::class, 'get_data_country'])->name('get_data_country');
    Route::post('/insertCountry', [LocationController::class, 'insertCountry'])->name('insertCountry');
    Route::post('/editCountry', [LocationController::class, 'editCountry'])->name('editCountry');
    Route::post('/deleteCountry', [LocationController::class, 'deleteCountry'])->name('deleteCountry');
    Route::get('/exportDataCountry', [LocationController::class, 'exportDataCountry'])->name('exportDataCountry');

    //Location - Country
    Route::get('/city', [LocationController::class, 'city'])->name('city');
    Route::get('/get_data_city', [LocationController::class, 'get_data_city'])->name('get_data_city');
    Route::post('/insertCity', [LocationController::class, 'insertCity'])->name('insertCity');
    Route::post('/editCity', [LocationController::class, 'editCity'])->name('editCity');
    Route::post('/deleteCity', [LocationController::class, 'deleteCity'])->name('deleteCity');
    Route::get('/exportDataCity', [LocationController::class, 'exportDataCity'])->name('exportDataCity');

    //Vehicle - Vehicle Type
    Route::get('/vehicle_type', [VehicleController::class, 'vehicle_type'])->name('vehicle_type');
    Route::get('/get_data_vehicle_type', [VehicleController::class, 'get_data_vehicle_type'])->name('get_data_vehicle_type');
    Route::post('/insertVehicleType', [VehicleController::class, 'insertVehicleType'])->name('insertVehicleType');
    Route::post('/editVehicleType', [VehicleController::class, 'editVehicleType'])->name('editVehicleType');
    Route::post('/deleteVehicleType', [VehicleController::class, 'deleteVehicleType'])->name('deleteVehicleType');
    Route::get('/exportDataVehicleType', [VehicleController::class, 'exportDataVehicleType'])->name('exportDataVehicleType');

    //Vehicle - Vehicle
    Route::get('/vehicle', [VehicleController::class, 'vehicle'])->name('vehicle');
    Route::get('/get_data_vehicle', [VehicleController::class, 'get_data_vehicle'])->name('get_data_vehicle');
    Route::post('/insertVehicle', [VehicleController::class, 'insertVehicle'])->name('insertVehicle');
    Route::post('/editVehicle', [VehicleController::class, 'editVehicle'])->name('editVehicle');
    Route::post('/deleteVehicle', [VehicleController::class, 'deleteVehicle'])->name('deleteVehicle');
    Route::get('/exportDataVehicle', [VehicleController::class, 'exportDataVehicle'])->name('exportDataVehicle');

    //Travel Documents - Documents
    Route::get('/document', [TravelDocumentsController::class, 'document'])->name('document');
    Route::get('/get_data_document', [TravelDocumentsController::class, 'get_data_document'])->name('get_data_document');
    Route::post('/insertDocument', [TravelDocumentsController::class, 'insertDocument'])->name('insertDocument');
    Route::post('/editDocument', [TravelDocumentsController::class, 'editDocument'])->name('editDocument');
    Route::post('/deleteDocument', [TravelDocumentsController::class, 'deleteDocument'])->name('deleteDocument');
    Route::get('/exportDataDocument', [TravelDocumentsController::class, 'exportDataDocument'])->name('exportDataDocument');

    //Travel Documents - Passport
    Route::get('/passport', [TravelDocumentsController::class, 'passport'])->name('passport');
    Route::get('/get_data_passport', [TravelDocumentsController::class, 'get_data_passport'])->name('get_data_passport');
    Route::post('/insertPassport', [TravelDocumentsController::class, 'insertPassport'])->name('insertPassport');
    Route::post('/editPassport', [TravelDocumentsController::class, 'editPassport'])->name('editPassport');
    Route::post('/deletePassport', [TravelDocumentsController::class, 'deletePassport'])->name('deletePassport');
    Route::get('/exportDataPassport', [TravelDocumentsController::class, 'exportDataPassport'])->name('exportDataPassport');

    //Travel Documents - Visa
    Route::get('/visa', [TravelDocumentsController::class, 'visa'])->name('visa');
    Route::get('/get_data_visa', [TravelDocumentsController::class, 'get_data_visa'])->name('get_data_visa');
    Route::post('/insertVisa', [TravelDocumentsController::class, 'insertVisa'])->name('insertVisa');
    Route::post('/editVisa', [TravelDocumentsController::class, 'editVisa'])->name('editVisa');
    Route::post('/deleteVisa', [TravelDocumentsController::class, 'deleteVisa'])->name('deleteVisa');
    Route::get('/exportDataVisa', [TravelDocumentsController::class, 'exportDataVisa'])->name('exportDataVisa');

    //supplier
    Route::get('/supplier', [SupplierController::class, 'supplier'])->name('supplier');
    Route::get('/get_data_supplier', [SupplierController::class, 'get_data_supplier'])->name('get_data_supplier');
    Route::post('/insertSupplier', [SupplierController::class, 'insertSupplier'])->name('insertSupplier');
    Route::post('/importSupplier', [SupplierController::class, 'importSupplier'])->name('importSupplier');
    Route::post('/editSupplier', [SupplierController::class, 'editSupplier'])->name('editSupplier');
    Route::post('/deleteSupplier', [SupplierController::class, 'deleteSupplier'])->name('deleteSupplier');
    Route::get('/exportDataSupplier', [SupplierController::class, 'exportDataSupplier'])->name('exportDataSupplier');
    Route::get('/exportDataSupplierPIC', [SupplierController::class, 'exportDataSupplierPIC'])->name('exportDataSupplierPIC');

    //Terms - Term & Condition
    Route::get('/term_condition', [TermsController::class, 'term_condition'])->name('term_condition');
    Route::get('/get_data_term', [TermsController::class, 'get_data_term_condition'])->name('get_data_term_condition');
    Route::post('/insertTerm', [TermsController::class, 'insertTerm'])->name('insertTerm');
    Route::post('/editTerm', [TermsController::class, 'editTerm'])->name('editTerm');
    Route::post('/deleteTerm', [TermsController::class, 'deleteTerm'])->name('deleteTerm');
    Route::get('/exportDataTerms', [TermsController::class, 'exportDataTerms'])->name('exportDataTerms');

    //Terms - Inclusion
    Route::get('/inclusion', [TermsController::class, 'inclusion'])->name('inclusion');
    Route::get('/get_data_inclusion', [TermsController::class, 'get_data_term'])->name('get_data_term');
    Route::post('/insertInclusion', [TermsController::class, 'insertInclusion'])->name('insertInclusion');
    Route::post('/editInclusion', [TermsController::class, 'editInclusion'])->name('editInclusion');
    Route::post('/deleteInclusion', [TermsController::class, 'deleteInclusion'])->name('deleteInclusion');
    Route::get('/exportDataInclusion', [TermsController::class, 'exportDataInclusion'])->name('exportDataInclusion');

    //Terms - Exclusion
    Route::get('/exclusion', [TermsController::class, 'exclusion'])->name('exclusion');
    Route::get('/get_data_exclusion', [TermsController::class, 'get_data_exclusion'])->name('get_data_exclusion');
    Route::post('/insertExclusion', [TermsController::class, 'insertExclusion'])->name('insertExclusion');
    Route::post('/editExclusion', [TermsController::class, 'editExclusion'])->name('editExclusion');
    Route::post('/deleteExclusion', [TermsController::class, 'deleteExclusion'])->name('deleteExclusion');
    Route::get('/exportDataExclusion', [TermsController::class, 'exportDataExclusion'])->name('exportDataExclusion');

    //User Category
    Route::get('/user_category', [UserCategory::class, 'user_category'])->name('user_category');
    Route::get('/get_data_user_category', [UserCategory::class, 'get_data_user_category'])->name('get_data_user_category');
    Route::post('/insertUserCategory', [UserCategory::class, 'insertUserCategory'])->name('insertUserCategory');
    Route::post('/editUserCategory', [UserCategory::class, 'editUserCategory'])->name('editUserCategory');
    Route::post('/deleteUserCategory', [UserCategory::class, 'deleteUserCategory'])->name('deleteUserCategory');
    Route::get('/exportDataUserCategory', [UserCategory::class, 'exportDataUserCategory'])->name('exportDataUserCategory');

    //Keyword
    Route::get('/keyword', [KeywordController::class, 'keyword'])->name('keyword');
    Route::get('/get_data_keyword', [KeywordController::class, 'get_data_keyword'])->name('get_data_keyword');
    Route::get('/get_data_keyword_category', [KeywordController::class, 'get_data_keyword_category'])->name('get_data_keyword_category');
    Route::post('/insertKeyword', [KeywordController::class, 'insertKeyword'])->name('insertKeyword');
    Route::post('/insertKeywordCategory', [KeywordController::class, 'insertKeywordCategory'])->name('insertKeywordCategory');
    Route::post('/deleteKeywordCategory', [KeywordController::class, 'deleteKeywordCategory'])->name('deleteKeywordCategory');
    Route::post('/editKeyword', [KeywordController::class, 'editKeyword'])->name('editKeyword');
    Route::post('/deleteKeyword', [KeywordController::class, 'deleteKeyword'])->name('deleteKeyword');
    Route::get('/exportDataKeyword', [KeywordController::class, 'exportDataKeyword'])->name('exportDataKeyword');

    //TL / Guide / Driver
    Route::get('/worker', [WorkerController::class, 'worker'])->name('worker');
    Route::get('/get_data_worker', [WorkerController::class, 'get_data_worker'])->name('get_data_worker');
    Route::post('/insertWorker', [WorkerController::class, 'insertWorker'])->name('insertWorker');
    Route::post('/editWorker', [WorkerController::class, 'editWorker'])->name('editWorker');
    Route::post('/deleteWorker', [WorkerController::class, 'deleteWorker'])->name('deleteWorker');
    Route::get('/exportDataWorker', [WorkerController::class, 'exportDataWorker'])->name('exportDataWorker');

    //Download Document
    Route::get('/downloadDocument', [WorkerController::class, 'downloadDocument'])->name('downloadDocument');

    //Displayer
    Route::get('/displayer', [DisplayerController::class, 'displayer'])->name('displayer');
    Route::get('/displayer_detail/{id}', [DisplayerController::class, 'displayer_detail'])->name('displayer_detail');
    Route::get('/get_data_displayer', [DisplayerController::class, 'get_data_displayer'])->name('get_data_displayer');
    Route::post('/insertDisplayer', [DisplayerController::class, 'insertDisplayer'])->name('insertDisplayer');
    Route::post('/editDisplayer', [DisplayerController::class, 'editDisplayer'])->name('editDisplayer');
    Route::post('/deleteDisplayer', [DisplayerController::class, 'deleteDisplayer'])->name('deleteDisplayer');
    Route::get('/exportDisplayer', [DisplayerController::class, 'exportDisplayer'])->name('exportDisplayer');

    //Displayer detail
    Route::get('/displayer_detail/{id}', [DisplayerController::class, 'displayer_detail'])->name('displayer_detail');
    Route::get('/get_data_displayer_detail', [DisplayerController::class, 'get_data_displayer_detail'])->name('get_data_displayer_detail');
    Route::post('/updateDisplayerDetail', [DisplayerController::class, 'updateDisplayerDetail'])->name('updateDisplayerDetail');
    Route::get('/exportDisplayerDetail', [DisplayerController::class, 'exportDisplayerDetail'])->name('exportDisplayerDetail');

    //customer
    Route::get('/customer', [CustomerController::class, 'customer'])->name('customer');
    Route::get('/get_data_customer', [CustomerController::class, 'get_data_customer'])->name('get_data_customer');
    Route::post('/insertCustomer', [CustomerController::class, 'insertCustomer'])->name('insertCustomer');
    Route::post('/editCustomer', [CustomerController::class, 'editCustomer'])->name('editCustomer');
    Route::post('/deleteCustomer', [CustomerController::class, 'deleteCustomer'])->name('deleteCustomer');
    Route::get('/exportDataCustomer', [CustomerController::class, 'exportDataCustomer'])->name('exportDataCustomer');

    //customer
    Route::get('/family_tree', [CustomerController::class, 'family_tree'])->name('family_tree');
    Route::get('/get_data_family_tree', [CustomerController::class, 'get_data_family_tree'])->name('get_data_family_tree');
    Route::post('/insertFamilyTree', [CustomerController::class, 'insertFamilyTree'])->name('insertFamilyTree');
    Route::post('/editFamilyTree', [CustomerController::class, 'editFamilyTree'])->name('editFamilyTree');
    Route::post('/deleteFamilyTree', [CustomerController::class, 'deleteFamilyTree'])->name('deleteFamilyTree');
    Route::get('/exportDataFamilyTree', [CustomerController::class, 'exportDataFamilyTree'])->name('exportDataFamilyTree');
    Route::get('/getDetailFamilyTree', [CustomerController::class, 'getDetailFamilyTree'])->name('getDetailFamilyTree');
    Route::post('/cekCustomer', [CustomerController::class, 'cekCustomer'])->name('cekCustomer');

    // Corporate
    Route::get('/corporate', [CorporateController::class, 'corporate'])->name('corporate');
    Route::get('/credit_corporate', [CorporateController::class, 'credit_corporate'])->name('credit_corporate');
    Route::get('/get_data_corporate', [CorporateController::class, 'get_data_corporate'])->name('get_data_corporate');
    Route::post('/insertCorporate', [CorporateController::class, 'insertCorporate'])->name('insertCorporate');
    Route::post('/editCorporate', [CorporateController::class, 'editCorporate'])->name('editCorporate');
    Route::post('/editCorporateCredit', [CorporateController::class, 'editCorporateCredit'])->name('editCorporateCredit');
    Route::post('/deleteCorporate', [CorporateController::class, 'deleteCorporate'])->name('deleteCorporate');
    Route::get('/exportDataCorporate', [CorporateController::class, 'exportDataCorporate'])->name('exportDataCorporate');
    Route::get('/exportDataCorporateCredit', [CorporateController::class, 'exportDataCorporateCredit'])->name('exportDataCorporateCredit');
    Route::get('/get_data_corporate_log', [CorporateController::class, 'get_data_corporate_log'])->name('get_data_corporate_log');

    // Billing Control
    Route::get('/billing_control', [CorporateController::class, 'billing_control'])->name('billing_control');

    // Hotel Room Category
    Route::get('/hotel_room_category', [CategoryController::class, 'hotel_room_category'])->name('hotel_room_category');
    Route::get('/get_data_hotel_room', [CategoryController::class, 'get_data_hotel_room'])->name('get_data_hotel_room');
    Route::post('/insertHotelRoom', [CategoryController::class, 'insertHotelRoom'])->name('insertHotelRoom');
    Route::post('/editHotelRoom', [CategoryController::class, 'editHotelRoom'])->name('editHotelRoom');
    Route::post('/deleteHotelRoom', [CategoryController::class, 'deleteHotelRoom'])->name('deleteHotelRoom');
    Route::get('/exportDataHotelRoom', [CategoryController::class, 'exportDataHotelRoom'])->name('exportDataHotelRoom');

    // Hotel Room Meal Category
    Route::get('/hotel_meal_category', [CategoryController::class, 'hotel_meal_category'])->name('hotel_meal_category');
    Route::get('/get_data_hotel_meal', [CategoryController::class, 'get_data_hotel_meal'])->name('get_data_hotel_meal');
    Route::post('/insertHotelMeal', [CategoryController::class, 'insertHotelMeal'])->name('insertHotelMeal');
    Route::post('/editHotelMeal', [CategoryController::class, 'editHotelMeal'])->name('editHotelMeal');
    Route::post('/deleteHotelMeal', [CategoryController::class, 'deleteHotelMeal'])->name('deleteHotelMeal');
    Route::get('/exportDataHotelMeal', [CategoryController::class, 'exportDataHotelMeal'])->name('exportDataHotelMeal');


    Route::get('/hotel_service', [CategoryController::class, 'hotel_service'])->name('hotel_service');
    Route::get('/get_data_hotel_service', [CategoryController::class, 'get_data_hotel_service'])->name('get_data_hotel_service');
    Route::post('/insertHotelService', [CategoryController::class, 'insertHotelService'])->name('insertHotelService');
    Route::post('/editHotelService', [CategoryController::class, 'editHotelService'])->name('editHotelService');
    Route::post('/deleteHotelService', [CategoryController::class, 'deleteHotelService'])->name('deleteHotelService');
    Route::get('/exportDataHotelService', [CategoryController::class, 'exportDataHotelService'])->name('exportDataHotelService');


    Route::get('/corporate_position', [CategoryController::class, 'corporate_position'])->name('corporate_position');
    Route::get('/get_data_corporate_position', [CategoryController::class, 'get_data_corporate_position'])->name('get_data_corporate_position');
    Route::post('/insertCorporatePosition', [CategoryController::class, 'insertCorporatePosition'])->name('insertCorporatePosition');
    Route::post('/editCorporatePosition', [CategoryController::class, 'editCorporatePosition'])->name('editCorporatePosition');
    Route::post('/deleteCorporatePosition', [CategoryController::class, 'deleteCorporatePosition'])->name('deleteCorporatePosition');
    Route::get('/exportDataCorporatePosition', [CategoryController::class, 'exportDataCorporatePosition'])->name('exportDataCorporatePosition');


});

Route::get('/get_bin', [GeneralController::class, 'get_bin'])->name('get_bin');

// buat DD
Route::post('/dd', [TourController::class, 'DD']);

// Product Prefix
Route::middleware(['checkSession'])->prefix('product')->group(function () {

    // Tour
    Route::middleware(['checkSession'])->prefix('tour')->group(function () {

        // Tour - Fix Departure
        Route::prefix('fix_departure')->group(function () {
            Route::get('/', [TourController::class, "fix_departure"])->name('fix_departure');
            Route::get('/tambah_baru', [TourController::class, "fix_departure_tambah_baru"])->name('fix_departure_tambah_baru');
            Route::get('/detailFixDepature/{id}', [TourController::class, "detailFixDepature"])->name('detailFixDepature');
            Route::get('/get_data_fix', [TourController::class, 'get_data_fix'])->name('get_data_fix');
            Route::post('/insertFixDepature', [TourController::class, "insertFixDepature"])->name('insertFixDepature');
            Route::post('/updateFixDepature', [TourController::class, "updateFixDepature"])->name('updateFixDepature');
            Route::post('/deleteFixDepature', [TourController::class, 'deleteTour'])->name('deleteTour');
        });


        // Tour - Fit Departure
        Route::prefix('fit_tour')->group(function () {
            Route::get('/', [TourController::class, 'fit_tour'])->name('fit_tour');
            Route::get('/tambah_baru', [TourController::class, 'fit_tour_tambah_baru'])->name('fit_tour_tambah_baru');
            Route::get('/get_data_fit', [TourController::class, 'get_data_fit'])->name('get_data_fit');
            Route::post('/insertFitDepature', [TourController::class, "insertFitDepature"])->name('insertFitDepature');
            Route::post('/updateFitDepature', [TourController::class, "updateFitDepature"])->name('updateFitDepature');
            Route::get('/detailFitTour/{id}', [TourController::class, "detailFitTour"])->name('detailFitTour');

        });
    });

    // Hotel
    Route::get("/hotel", [HotelController::class, "hotel"])->name("hotel");
    Route::get("/tambah_hotel", [HotelController::class, "tambah_hotel"])->name("tambah_hotel");

    // Flight Ticket
    Route::get("/flight_ticket", [FlightTicket::class, "flight_ticket"])->name("flight_ticket");
    Route::get("/flight_ticket_tambah", [FlightTicket::class, "flight_ticket_tambah"])->name("flight_ticket_tambah");
    Route::get('/get_data_airlines', [FlightTicket::class, 'get_data_airlines'])->name('get_data_airlines');
    Route::post('/insertAirlines', [FlightTicket::class, 'insertAirlines'])->name('insertAirlines');
    Route::post('/editAirlines', [FlightTicket::class, 'editAirlines'])->name('editAirlines');
    Route::post('/deleteAirlines', [FlightTicket::class, 'deleteAirlines'])->name('deleteAirlines');
    Route::get('/exportDataAirlines', [FlightTicket::class, 'exportDataAirlines'])->name('exportDataAirlines');


});

Route::middleware(['checkSession'])->prefix('booking')->group(function () {
        // Tour
        Route::prefix('tour')->group(function () {
            Route::get('/', [BookingController::class, "booking_tour"])->name('booking_tour');
            Route::get('/tambah_booking', [BookingController::class, "tambah_booking"])->name('tambah_booking');
           // Route::get('/detailFixDepature/{id}', [BookingController::class, "detailFixDepature"])->name('detailFixDepature');
            Route::get('/reservation_fit', [BookingController::class, 'reservation_fit'])->name('reservation_fit');
            Route::get('/reservation_fix', [BookingController::class, 'reservation_fix'])->name('reservation_fix');
            Route::get('/reservation_tailor', [BookingController::class, 'reservation_tailor'])->name('reservation_tailor');
            Route::post('/insertReservationTailor', [BookingController::class, 'insertReservationTailor'])->name('insertReservationTailor');
            Route::post('/updateReservationTailor', [BookingController::class, 'updateReservationTailor'])->name('updateReservationTailor');
        });
});

//link storage dan public folder
Route::get('/linkstorage', function () {
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'storage linked!';
});


// Ticketing Project
Route::middleware(['checkSession'])->group(function () {
    //Header Project
    Route::get('/ticketingProject', [TicketProjectController::class, 'ticketingProject'])->name('ticketingProject');
    Route::get('/ticketingProject/edit', [TicketProjectController::class, 'ticketingProjectEdit'])->name('ticketingProjectEdit');
    Route::get('/ticketingProject/detail/{id}', [TicketProjectController::class, 'ticketingProjectDetail'])->name('ticketingProjectDetail');
    Route::get('/get_data_ticketing_project', [TicketProjectController::class, 'get_data_ticketing_project'])->name('get_data_hotel_service');
    Route::post('/insertTicketingProject', [TicketProjectController::class, 'insertTicketingProject'])->name('insertTicketingProject');
    Route::post('/editTicketingProject', [TicketProjectController::class, 'editTicketingProject'])->name('editTicketingProject');
    Route::post('/deleteTicketingProject', [TicketProjectController::class, 'deleteTicketingProject'])->name('deleteTicketingProject');

    //Route
    Route::post('/insertRoute', [TicketProjectController::class, 'insertRoute'])->name('insertRoute');
    Route::post('/deleteRoute', [TicketProjectController::class, 'deleteRoute'])->name('deleteRoute');
    Route::post('/managementPeserta', [TicketProjectController::class, 'managementPeserta'])->name('managementPeserta');
 
});

//export pdf
Route::get('/exportPdfVisa/{id}', [PDFController::class, 'exportPdfVisa'])->name('exportPdfVisa');
Route::get('/exportPdfPassport/{id}', [PDFController::class, 'exportPdfPassport'])->name('exportPdfPassport');
Route::get('/exportPdfTerms/{id}', [PDFController::class, 'exportPdfTerms'])->name('exportPdfTerms');

//Download File
Route::get('/downloadTemplate', [GeneralController::class, 'downloadTemplate'])->name('downloadTemplate');

//coba coba
Route::get('/scanDocument', [GeneralController::class, 'testOCR'])->name('scanDocument');
Route::post('/uploadOCR', [GeneralController::class, 'uploadOCR'])->name('uploadOCR');
