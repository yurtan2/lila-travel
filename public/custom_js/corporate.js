var list_pic = [];
function refreshPIC() {
    $('#body-pic').html("");
    list_pic.forEach((item,index) => {
        $('#body-pic').append(`
            <tr>
                <td class="td-name-${index}">${index+1}</td>
                <td class="td-name-${index}">${item.pic_name}</td>
                <td class="td-name-${index}">${item.pic_jabatan}</td>
                <td class="text-center"><button class="btn btn-outline-danger btn-sm me-2 btn-delete-pic" index="${index}" type="button">Delete</button>
                </td>
            </tr>
        `);
        $('.edit-mode').hide();
    });
}

get_data();
var mode=1; //1= insert, 2= delete
var file_name = "";

autocompleteCustomer('#pic_name');
autocompletePosition('#pic_jabatan');

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val(null).trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    list_pic = [];
    $("#body-pic").html("");
    $('#title').html("Tambah Corporate");
    $('.add').val("");
    $('#corporate_prefix').val("+62");
    $('#inlineRadio2').prop("checked",false);
    $('#inlineRadio1').prop("checked",false);
    $('input[type="file"]').val(null).trigger("change");
    $('.file').val(null).trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on('hidden.bs.modal', '#modalInsert', function(e){
    $('.is-invalid').removeClass('is-invalid');
});

$(document).on("click",".btn-delete-pic",function(){
    var idx = $(this).attr("index");
    console.log(idx);
    list_pic.splice(idx,1);
    refreshPIC();
});

$(document).on("keyup","#corporate_admin_fee",function(){
    $(this).val(formatRupiah($(this).val()+"","Rp."));
});

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Customer");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();

    if(data.corporate_hide_logo==1)$('#inlineRadio1').prop("checked",true);
    else $('#inlineRadio1').prop("checked",false);
    if(data.corporate_use_own_logo==2)$('#inlineRadio2').prop("checked",true);
    else $('#inlineRadio2').prop("checked",true);

    $('input[type="file"]').val(null)
    $('#corporate_name').val(data.corporate_name);
    $('#corporate_address').val(data.corporate_address);
    $('#corporate_admin_fee').val(formatRupiah(data.corporate_admin_fee+"","Rp."));
    $('#corporate_email').val(data.corporate_email);
    $('#corporate_prefix').val(data.corporate_prefix).trigger("change");
    $('#corporate_nomor').val(data.corporate_nomor);

    list_pic = [];
    data.detail_pic.forEach(item => {
        list_pic.push({
            "pic_name":item.customer_first_name+" "+item.customer_last_name,
            "customer_id":item.customer_id,
            "jabatan_id":item.corporate_position_id,
            "pic_jabatan":item.corporate_position_name,
            "corporate_pic_id":item.corporate_pic_id
        });
    });


    refreshPIC();
    $('#modalInsert').attr("edit_id",data.corporate_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.corporate_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click",".btn-view",function(){
    $('#title').html("Detail Customer");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    var own = hide= "No";
    if(data.corporate_hide_logo==1)hide="Yes";
    if(data.corporate_use_own_logo==1)own="Yes";

    $('#view_hide_logo').html(hide);
    $('#view_own').html(own);
    $('#view_name').html(data.corporate_name);
    $('#view_address').html(data.corporate_address);
    $('#view_admin_fee').html(formatRupiah(data.corporate_admin_fee+"","Rp."));
    $('#view_email').html(data.corporate_email);
    $('#view_nomor').html(data.corporate_prefix+data.corporate_nomor);

    $('#view_logo').attr("src",public+data.corporate_logo).attr("file_name",data.corporate_logo);
    $('#view_akta').attr("src",public+data.corporate_akta).attr("file_name",data.corporate_akta);
    $('#view_nib').attr("src",public+data.corporate_nib).attr("file_name",data.corporate_nib);
    $('#view_sk').attr("src",public+data.corporate_sk).attr("file_name",data.corporate_sk);
    $('#view_npwp').attr("src",public+data.corporate_npwp).attr("file_name",data.corporate_npwp);
    $('#view_pic').html(" ");
    data.detail_pic.forEach((item,index) => {
        $('#view_pic').append(`
            <tr>
                <td>${index+1}</td>
                <td>${item.customer_first_name+" "+item.customer_last_name}</td>
                <td>${item.corporate_position_name}</td>
            </tr>
        `);
    });
    $('#modalView').modal("show");
})

$(document).on("click",".btn-add-pic",function(){
    var valid=1;

    $('.has-danger').removeClass("has-danger");

    $('.input-pic .pic-check').each(function(){
        if($(this).select2("data").length == 0){
            $(this).closest('.form-group').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }


    list_pic.push({
        "pic_name":$('#pic_name').select2("data")[0]["text"],
        "customer_id":$('#pic_name').select2("data")[0]["id"],
        "jabatan_id":$('#pic_jabatan').select2("data")[0]["id"],
        "pic_jabatan":$('#pic_jabatan').select2('data')[0]["text"],
    });

    $('.input-pic .pic-check').val("");
    $('#pic_name').empty().trigger("change");
    $('#pic_jabatan').empty().trigger("change");

    refreshPIC();
});

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataCorporate?customer_name="+ $('#filter_customer_name').val()+"&&customer_tanggal_lahir="+ $('#filter_customer_tanggal_lahir').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCorporate",
        method:"post",
        data:{
            corporate_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("input", '.need-check', function(){
    let isEmpty = $(this).val() == '';
    $(this).toggleClass('is-invalid', isEmpty);
})

$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "insertCorporate";
    var gender ="Male";
    var customer_jenis = "Indonesia";

    $('.has-danger').removeClass("has-danger");

    $('#formInsert input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            $(this).closest('.form-floating').addClass("has-danger");
            valid=0;
        }
    });

    let isEmpty = false;
    $(".pic-check").each(function(){
        if($(this).val() == '' || $(this).val() == null)isEmpty = true;
    })

    if(!isEmpty){
        $(".btn-add-pic").click();
    }

    if(list_pic.length<=0){
        toastr.error("Minimal 1 PIC!","Terjadi Kesalahan");
        return false;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if($('#inlineRadio2').is(":checked"))gender="Female";

    var param = new FormData();
    var hide =own= 0;
    if($('#inlineRadio1').is(":checked"))hide=1;
    if($('#inlineRadio2').is(":checked"))own=1;
    param.append( 'corporate_name',$('#corporate_name').val());
    param.append( 'corporate_nomor',$('#corporate_nomor').val());
    param.append( 'corporate_address',$('#corporate_address').val());
    param.append( 'corporate_prefix',$('#corporate_prefix').val());
    param.append( 'corporate_address',$('#corporate_address').val());
    param.append( 'corporate_admin_fee', convertToAngka($('#corporate_admin_fee').val()));
    param.append( 'corporate_email',$('#corporate_email').val());
    param.append( 'corporate_own_logo',hide);
    param.append( 'corporate_hide_logo',own);
    param.append( 'corporate_pic',JSON.stringify(list_pic));
    param.append( 'logo', $( '#corporate_logo' )[0].files[0] );
    param.append( 'akta', $( '#corporate_akta' )[0].files[0] );
    param.append( 'npwp', $( '#corporate_npwp' )[0].files[0] );
    param.append( 'nib', $( '#corporate_nib' )[0].files[0] );
    param.append( 'sk', $( '#corporate_sk' )[0].files[0] );
    param.append( '_token',$('meta[name="csrf-token"]').attr('content'));

    if(mode==2){
        url = "editCorporate";
        param.append( 'corporate_id', $('#modalInsert').attr("edit_id"));
    }

    $('#btn-insert').append(`
        <div class="spinner-border text-light" role="status" style="width:10px;height:10px">
            <span class="visually-hidden">Loading...</span>
        </div>`);
    $('#btn-insert').attr("disabled","disabled");

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:param,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success:function(e){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                list_pic = [];
                $("#body-pic").html("");
                $('.add').val("");
                $('#corporate_prefix').val("+62");
                $('#inlineRadio2').prop("checked",false);
                $('#inlineRadio1').prop("checked",false);
                $('input[type="file"]').val('').trigger("change");
                $('.file').val(null).trigger("change");
                $('.has-danger').removeClass("has-danger");
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }

        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_corporate",
            type: "get",
            data:{
                filter_corporate_nomor:$('#filter_corporate_nomor').val(),
                filter_corporate_name:$('#filter_corporate_name').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button type="button" class="btn btn-info btn-view">View</button>
                        <button type="button" class="btn btn-warning btn-edit">Edit</button>
                        <button type="button" class="btn btn-danger btn-delete">Delete</button>
                    `;
                    json.data[i].corporate_nomor_text = json.data[i].corporate_prefix+" "+json.data[i].corporate_nomor;
                    json.data[i].corporate_admin_fee_text = formatRupiah(json.data[i].corporate_admin_fee,"Rp.");
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "corporate_name"},
            { data: "corporate_nomor_text"},
            { data: "corporate_admin_fee_text"},
            { data: "total_pic",className:"text-center"},
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "action" ,className:"text-center",width:"20%"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".detail",function(){
    var name = $(this).attr("name")+"_"+$('#view_name').html();
    $('#detail_dokumen').attr("src",$(this).attr("src"));
    $('#btn-download').attr("href",$(this).attr("src"));
    $('#btn-download').attr("download",name);
    $("#modalViewDetail").modal("show");
})

$(document).on("click","#btn-download",function(){
    e.preventDefault();
    /*
    $.ajax({
        url:"/master/downloadDocument",
        method:"post",
        data:{
            nama:$(this).attr("nama")
        },
        success:function(data){
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'myfile.pdf';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
*/

})

