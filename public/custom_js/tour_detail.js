var totalPassenger = [];

$(function(){
    $(".btn-add").click();
})

function formatDate(inputDate) {
    const months = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    let date = new Date(inputDate);
    let day = date.getDate().toString().padStart(2, '0');  // Get day and pad with 0 if needed
    let monthName = months[date.getMonth()];
    let year = date.getFullYear();

    return `${day} ${monthName} ${year}`;
}

function newRoom() {
    let option = '';
    console.log(data);
    if(data.tour_jenis==1){
        id.forEach(element => {
            start = formatDate(element.tour_departure_date_start);
            end = formatDate(element.tour_departure_date_end);
            option += `<option value="${element.tour_departure_id}" start="${start}" end="${end}">${start} s/d ${end}</option>`
        });
    }
    else{
        console.log(data.tour_date_start);
        console.log(data.tour_date_end);
        console.log(data.tour_days);
        start = moment(data.tour_date_start);
        end = moment(data.tour_date_end);
        var selisih = end.diff(start, 'days');
       
        while(moment(start)<end){
            var end_temp = moment(start).add('days',data.tour_days);
            option += `<option value="${data.tour_id}" start="${start}" end="${end}">${start.format("D MMMM YYYY")} s/d ${end_temp.format("D MMMM YYYY")}</option>`
            
            start = end_temp;
            start = moment(start).add('days',1);;
        }
        
    }

    let newElement = $(`
    <div class="row list_room">
        <div class="row mb-3 justify-content-between align-items-center">
            <div class="col-lg-4">
                <div class="h5 title-room">Room 1</div>
            </div>
            <div class="col-lg-3">
                <div class="h3">
                    <i class="bi bi-x float-end"></i>
                </div>
            </div>
            <div class="text-purple">
                <hr>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="h5">
                    <i class="bi bi-calendar-event-fill text-purple me-2"></i>
                    Date
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="form-floating">
                    <select class="form-select" id="date-floating" aria-label="Floating label select example">
                        ${option}
                    </select>
                    <label for="date-floating">Date</label>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="h5">
                    <i class="bi bi-people-fill me-2 text-purple"></i>
                    Participant
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-12 row-passager">
                <div class="row mb-2 align-items-center">
                    <div class="col-lg-4 col-md-4">
                        <div class="h5">Adults</div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-md-2 text-center minus-parent">
                                <div class="h3 text-purple float-start">
                                    <i class="bi bi-dash-circle cursor"></i>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 text-center">
                                <div class="h5 value-person">1</div>
                                <input type="hidden" name="adult[]" value="1">
                            </div>
                            <div class="col-lg-2 col-md-2 text-center plus-parent">
                                <div class="h3 text-purple float-end">
                                    <i class="bi bi-plus-circle cursor"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2 align-items-center">
                    <div class="col-lg-4 col-md-4">
                        <div class="h5">Children</div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-md-2 text-center minus-parent">
                                <div class="h3 text-purple float-start">
                                    <i class="bi bi-dash-circle cursor"></i>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 text-center">
                                <div class="h5 value-person">0</div>
                                <input type="hidden" name="children[]" value="0">
                            </div>
                            <div class="col-lg-2 col-md-2 text-center plus-parent">
                                <div class="h3 text-purple float-end">
                                    <i class="bi bi-plus-circle cursor"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-purple">
                    <hr>
                </div>
            </div>
        </div>
    </div>
    `);

    $("#order-room").append(newElement);
    totalPassenger.push(1);
}

function refreshRoom() {
    if($(".bi-x").length > 1){
        $(".bi-x").prop("hidden", false);
    }else{
        $(".bi-x").prop("hidden", true);
    }
    $(".title-room").each(function(index){
        $(this).text("Room " + (index+1));
    })
}

$(document).on("click", ".btn-add", function(){
    newRoom();
    refreshRoom();
    $(this).removeClass("btn-purple");
    $(this).addClass("btn-outline-purple");
})

$(document).on("click", ".bi-x", function(){
    $(this).closest(".list_room").remove();
    refreshRoom();
})

$(document).on("click", ".bi-dash-circle", function(){
    let input = $(this).closest(".minus-parent").next().find("input");
    name = input.attr("name");
    index = $(`input[name="${name}"]`).index(input);
    if(input.val() > 0){
        var idx = $(this).closest(".list_room").index();
        console.log(idx);
        $(`.list_room:eq(${idx}) .bi-plus-circle`).removeClass("text-gray");
        $(`.list_room:eq(${idx}) .bi-plus-circle`).addClass("text-purple");

        const $tooltipElement = $(`.list_room:eq(${idx}) .bi-plus-circle[data-bs-toggle="tooltip"]`);

        $tooltipElement.each(function() {
            const tooltipInstance = bootstrap.Tooltip.getInstance(this); // 'this' refers to the current DOM element in the loop
            if (tooltipInstance) {
                tooltipInstance.dispose(); // Use this to permanently remove the tooltip
                // tooltipInstance.hide(); // Use this if you just want to hide it without removing it entirely
            }
        });
        if(input.attr("name") == "adult[]" && input.val() ==1)return ;
        input.val(input.val() - 1);
        $(this).closest(".minus-parent").next().find(".value-person").html(input.val())
        $(`.btn-add`).addClass("btn-outline-purple");
        $(`.btn-add`).removeClass("btn-purple");
        totalPassenger[index]--;
    }
})

$(document).on("click", ".bi-plus-circle", function(){
    let input = $(this).closest(".plus-parent").prev().find("input");
    name = input.attr("name");
    index = $(`input[name="${name}"]`).index(input);
    if(totalPassenger[index] < 3) {
        input.val(Number(input.val()) + Number(1));
        $(this).closest(".plus-parent").prev().find(".value-person").html(input.val());
        totalPassenger[index]++;
    }
    if(totalPassenger[index] >= 3) {
        var idx = $(this).closest(".list_room").index();
        $(`.list_room:eq(${idx}) .bi-plus-circle`).addClass("text-gray");
        $(`.list_room:eq(${idx}) .bi-plus-circle`).removeClass("text-purple");
        $(`.list_room:eq(${idx}) .bi-plus-circle`).attr("data-bs-toggle", "tooltip");
        $(`.list_room:eq(${idx}) .bi-plus-circle`).attr("data-bs-placement", "top");
        $(`.list_room:eq(${idx}) .bi-plus-circle`).attr("data-bs-title", "Ruangan Penuh! Mohon tambah ruangan");
        $(`.btn-add`).removeClass("btn-outline-purple");
        $(`.btn-add`).addClass("btn-purple");

        const $tooltipElement = $(`.list_room:eq(${idx}) .bi-plus-circle[data-bs-toggle="tooltip"]`);

        $tooltipElement.each(function() {
            const tooltipInstance = new bootstrap.Tooltip(this); // 'this' refers to the current DOM element in the loop
            tooltipInstance.show();
        });
    }

})

$(document).on("click", "#btn-book", function(){
    temp = [];
    $(`input[name="adult[]"]`).each(function(index){
        temp.push({adult: $(this).val()});
    });
    $(`input[name="children[]"]`).each(function(index){
        temp[index].children = $(this).val();
    })
    console.log(temp);
    localStorage.setItem("room", JSON.stringify(temp));
    localStorage.setItem("date", $("#date-floating option:selected").attr("start") + " - " + $("#date-floating option:selected").attr("end"));
    url = window.location.href;
    window.location.href = `/tour/order/${data.tour_id}`;
})

$(document).on("click", ".discover", function(){
    $('html, body').animate({
        scrollTop: $('#content').offset().top
    }, 100);
})

$('.slide-down').click(function(){
    $(`#inner-${$(this).val()}`).slideToggle();
});

$("#inclusion").find("ul").addClass("list-group");
$("#inclusion").find("li").addClass("list-group-item");
$("#exclusion").find("ul").addClass("list-group");
$("#exclusion").find("li").addClass("list-group-item");
$("#term").find("p").addClass("h5");
$("#term").find("ul").addClass("list-group");
$("#term").find("li").addClass("list-group-item");

$("#date-floating").find("option").each(function(){
    $(this).text(formatDate($(this).attr("start")) + " s/d " + formatDate($(this).attr("end")));
})
