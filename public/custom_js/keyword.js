//get_data();
var list_category = [];
var mode=1; //1= insert, 2= delete
get_data_keyword_category();
get_data();
autocompleteKeywordCategory("#category","#modalInsert");
autocompleteKeywordCategory("#keyword_filter",null);

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click",".nav-link",function(){
    var menu  = $(this).attr("menu");
    if(menu=="category") refreshCategory();
    $('.has-danger').removeClass("has-danger");
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#keyword_filter').empty().trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    get_data_keyword_category();
    refreshCategory();
    $('#title').html("Tambah Keyword Category");
    $('.add').val("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit User Category");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#nama').val(data.keyword_name);
    $('#keyword').val(data.keyword_detail);
    $('#category').append(`<option value="${data.keyword_category_id}">${data.keyword_category_name}</option>`).trigger("change");
    $('#modalInsert').attr("edit_id",data.keyword_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.keyword_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataKeyword?nama="+ $('#nama_filter').val()+"&keyword_category_id="+$('#keyword_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteKeyword",
        method:"post",
        data:{
            keyword_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("User Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-add-category",function(){
    var nama = $('#keyword_category_name').val();
    var valid=1;

    $('.has-danger').removeClass("has-danger");
    if(nama==""||nama==null){
        $('#keyword_category_name').closest('.form-group').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    $.ajax({
        url:"/master/insertKeywordCategory",
        method:"post",
        data:{
            keyword_category_name :nama,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            toastr.success("Berhasil Insert data keyword category","Berhasil Delete");
            list_category =e;
            $('#keyword_category_name').val("");
            refreshCategory();
        }
    });
})

function get_data_keyword_category() {
    $.ajax({
        url:"/master/get_data_keyword_category",
        method:"get",
        success:function(e){
            list_category =e;
        }
    });
}
$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var keyword = $('#keyword').val();
    var category = $('#category').val();
    var valid = 1;
    var url = "insertKeyword";

    $('.has-danger').removeClass("has-danger");
    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }
    if(keyword==""||keyword==null){
        $('#keyword').closest('.form-group').addClass("has-danger");
        valid=0;
    }
    if(category==""||category==null){
        $('#category').closest('.form-group').addClass("has-danger");
        valid=0;
    }
    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    var data = {
        nama:nama,
        category:category,
        keyword:keyword,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    if(mode==2){
        url = "editKeyword";
        data.keyword_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("User Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_keyword",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                keyword_category_id:$('#keyword_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                    json.data[i].keyword_detail_text = json.data[i].keyword_detail.split(",").length;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "keyword_name"},
            { data: "keyword_category_name"},
            { data: "keyword_detail_text",width:"5%",className:"text-center"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".btn-delete-category",function(){
    var idx = $(this).attr("index");
    $.ajax({
        url:"/master/deleteKeywordCategory",
        method:"post",
        data:{
            keyword_category_id :list_category[idx].keyword_category_id,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){

            toastr.success("Berhasil Delete data category","Berhasil Delete");
            list_category = e;
            refreshCategory();

        }
    });


});

function refreshCategory() {
    $('#body-category').html("");
    list_category.forEach((item,index) => {
        $('#body-category').append(`
            <tr>
                <td>${item.keyword_category_name}</td>
                <td class="text-center"><button class="btn btn-outline-danger btn-sm me-2 btn-delete-category" index="${index}" type="button">Delete</button></td>
            </tr>
        `)
    });
}
