/**
 * Notes
 *
 */

// inisial variabel
var inputDestination = 0;
var countDestination = 0;
var countInclusion = 1;
var countExclusion = 1;
var countTermCondition = 1;
var countDepartureTab = 0;
var countDeparture = [];
var tab = 1;

var tabButton = '';
var tabContent = '';

var pillTab = 0;
var days = 2;
var nama_bulan = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"];

var inputTourDays = 2;

var itineraryUpdateMode = true;
var itineraryQuil = [];

var termQuil = [];
var inclusionQuil = [];
var exclusionQuil = [];

// Windows Load
$(function () {
    init();
    init2();
    //kalau mau paten mending dari js biar nanti pas edit lebih gampang
    autocompleteKeywords('#input_keyword', null);
    $('#input_tour_date_start').val(getCurrentDateTime());
    addDayTime('#input_tour_date_start', 2, "#input_tour_date_end");

    if(mode==2){
        //edit mode
        $('#input_tour_name').val(tour.tour_name);
        $('#input_tour_category').val(tour.tour_category).trigger("change");
        $('#input_tour_type').val(tour.tour_type).trigger("change");
        $('#input_tour_date_start').val(tour.tour_date_start).trigger("change");
        $('#input_tour_date_end').val(tour.tour_date_end).trigger("change");
        //tour & keyword
        tour.keyword.forEach(item => {
            $('#input_keyword').append(`<option value="${item.keyword_id}" selected>${item.keyword_name}</option>`).trigger("change");
        });
        if(tour.displayer)$('#select_displayer').append(`<option value="${tour.displayer.displayer_id}">${tour.displayer.displayer_title}</option>`).trigger("change");
        //destinasi
        tour.destinasi.forEach(item => {
            $('#btn_add_country').click();
            $('#destination_'+(inputDestination-1)).append(`<option value="${item.id}" >${item.name}</option>`).trigger("change");
            $('#supplier_'+(inputDestination-1)).append(`<option value="${item.supplier_id}">${item.supplier_name}</option>`).trigger("change");
            $('#offline_code_'+(inputDestination-1)).val(item.offline_code);
            $('#tour_destination_id'+(inputDestination-1)).val(item.tour_destination_id);
            if(item.with_visa==1)$('#visa_'+(inputDestination-1)).prop("checked",true);
            else $('#visa_'+(inputDestination-1)).prop("checked",false);
        });

        //include
        if(tour.tour_flight==1)$('#tour_flight').prop("checked",true);
        if(tour.tour_hotel==1)$('#tour_hotel').prop("checked",true);
        if(tour.tour_meal==1)$('#tour_meal').prop("checked",true);
        if(tour.tour_tour==1)$('#tour_tour').prop("checked",true);
        if(tour.tour_transport==1)$('#tour_transport').prop("checked",true);
        if(tour.tour_visa==1)$('#tour_visa').prop("checked",true);

        //day & night
        $('#input_tour_days').val(tour.tour_days).trigger("change");
        $('#input_tour_nights').val(tour.tour_nights).trigger("change");

        if(tour.tour_price_gimick>0){
            $('#check_gimmick_price').prop("checked",true).trigger("change");
            $('#input_gimmick_price').val(formatRupiah(tour.tour_price_gimick,"Rp."));
        }

        //inclsion
        tour.tour_inclusion.forEach((item,index) => {
            console.log(item);
            $('#btn_add_inclusion').click();
            inclusionQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-inclusion'+index).append(`<option value="${item.term_id}">${item.term_name}</option>`);
            $('#select-inclusion'+index).attr("tour_terms_id",item.tour_term_id);

        });
        if(tour.tour_inclusion.length<=0) $('#btn_add_inclusion').click();

        //exclusion
        tour.tour_exclusion.forEach((item,index) => {
            $('#btn_add_exclusion').click();
            exclusionQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-exclusion'+index).append(`<option value="${item.term_id}">${item.term_name}</option>`);
            $('#select-exclusion'+index).attr("tour_terms_id",item.tour_term_id);

        });
        if(tour.tour_exclusion.length<=0) $('#btn_add_exclusion').click();

        //term & condition
        tour.tour_term.forEach((item,index) => {
            $('#btn_add_term_condition').click();
            termQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-term'+index).append(`<option selected value="${item.terms_condition_id}">${item.terms_condition_name}</option>`);
            $('#select-term'+index).attr("tour_terms_id",item.tour_term_id);
            /*
            JSON.parse(item.tour_terms_date).forEach(element => {
                $(`#select-tanggal-term${index} option[value='${element}']`).attr("selected", "selected");
            });*/

        });
        if(tour.tour_term.length<=0) $('#btn_add_term_condition').click();

        //validity & price
        for (let i = 0; i < tour.tour_validity.length; i++) {
            $('#btn_add_tab').click();
            $('#btn_hotel_'+i).html(tour.tour_validity[i].tour_departure_name);
        }

        var seq = 0;
        tour.tour_validity.forEach((value,index) => {
            refreshDeparture();

            if(value.item.length<=0) seq++;

            value.item.forEach((item,index_item) => {


                    var departure = `.departure_date:eq(${seq})`;

                    $('.tour-departure-start').eq(seq).val(item.tour_departure_date_start).trigger("change");
                    $('.tour-departure-end').eq(seq).val(item.tour_departure_date_end).trigger("change");
                    $('.tour-deposit').eq(seq).val(formatRupiah(item.tour_departure_deposit+"")).trigger("change");

                    //check + masukan add on
                    if(item.add_on.length>0){
                        $('.check_addon_price').eq(index).prop('checked',true).trigger("change");
                        item.add_on.forEach((value_add_on,index_add_on) => {
                            $(`${departure} .add_on_type`).eq(index_add_on).val(value_add_on.tour_departure_detail_name);
                            $(`${departure} .add_on_price_input`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_price)).trigger("change");
                            $(`${departure} .add_on_disc_input`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_disc)).trigger("change");
                            $(`${departure} .add_on_total`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_total)).trigger("change");
                            if(index_add_on+1<item.add_on.length){
                                $('.btn_add_on').eq(index).click();
                            }
                        });
                    }
                    //check + masukan add on
                    if(item.black_out.length>0){
                        $('.check_black_out_date').eq(index).prop('checked',true).trigger("change");
                        item.black_out.forEach((value_black_out,index_black_out) => {
                            $(`${departure} .black-out-start`).eq(index_black_out).val(value_black_out.tour_black_out_start).trigger("change");
                            $(`${departure} .black-out-end`).eq(index_black_out).val(value_black_out.tour_black_out_end).trigger("change");
                            $(`${departure} .black-out-remark`).eq(index_black_out).val(value_black_out.tour_black_out_remark);
                            if(index_black_out+1<item.black_out.length){
                                $('.btn_black_out').eq(index).click();
                            }

                        });
                    }


                    /*
                    item.default_price.forEach((values,index_price) => {
                        if(values.tour_departure_detail_name!="Adult Single Accupancy"){
                            $(`input[name="input_tab_${index}_single_pax2_index_${index_price}[]"]`).val(formatRupiah(values.tour_departure_detail_pax_2+"",""));
                            $(`input[name="input_tab_${index}_single_pax5_index_${index_price}[]"]`).val(formatRupiah(values.tour_departure_detail_pax_5+"",""));
                            $(`input[name="input_tab_${index}_single_pax9_index_${index_price}[]"]`).val(formatRupiah(values.tour_departure_detail_pax_9+"",""));
                        }
                        else{
                            $(`input[name="input_tab_${index}_single_price${index_price}[]"]`).val(formatRupiah(values.tour_departure_detail_price+"",""));
                        }
                    });*/
                    if(index_item<value.item.length-1){
                        $('#btn-tambah-jadwal').click();

                    }

            });

            //masukan default price + extra bed
            $(`#tab_hotel_${index} .departure_date`).each(function(i){
                var depart = $(this);
                $(this).find(".room-type").each(function(j){
                    var type = $(this).find(":selected").attr("room");
                    console.log(`#tab_hotel_${index} .departure_date`);
                    if(type == "Adult Single Accupancy"){
                        depart.find(".single-price-"+index+"-"+j).val(formatRupiah(value.item[i].default_price[j].tour_departure_detail_price+"",""));
                    }
                    else{
                        try {
                            depart.find(".pax2-price-"+index+"-"+j).val(formatRupiah(value.item[i].default_price[j].tour_departure_detail_pax_2+"",""));
                            depart.find(".pax5-price-"+index+"-"+j).val(formatRupiah(value.item[i].default_price[j].tour_departure_detail_pax_5+"",""));
                            depart.find(".pax9-price-"+index+"-"+j).val(formatRupiah(value.item[i].default_price[j].tour_departure_detail_pax_9+"",""));
                        } catch (error) {
                        }
                    }

                });
            });


            seq++;
        });

        $(".tab-validate").click();

        //publish
        //1 = active
        //2 = expired
        //3 = draft
        //0 = delete
        if(tour.status==1){
            $('#inlineRadio1').prop("checked",true);
            $('#inlineRadio2').prop("checked",false);
        }
        else {
            $('#inlineRadio1').prop("checked",false);
            $('#inlineRadio2').prop("checked",true);
        }
    }
    else{
        $('#btn_add_inclusion').click();
        $('#btn_add_exclusion').click();
        $('#btn_add_term_condition').click();
        $('#btn_add_country').click();
        $('#btn_add_tab').click();
        refreshDeparture(pillTab);
        $(".tab-validate").click();

    }
});

function init() {
    autocompleteCountry(`#destination_${inputDestination}`);
    autocompleteSupplier(`#supplier_${inputDestination++}`);
    autocompleteDisplayer(`#select_displayer`, null);
    // autocompleteExclusion(`#select_displayer`,null);
    refreshItinerary()
    countDestination++;
}

function init2() {
    $(".js-example-basic-multiple").select2();
}

$(document).on("keyup", "#input_gimmick_price", function () {
    $(this).val(formatRupiah($(this).val(),"Rp."));
});

$(document).on("change", "#input_tour_date_start, #input_tour_date_end", function () {
    //refresh tanggal lagi karena tanggalnya berubah
    for (let i = 0; i < countInclusion; i++) {
        RefreshTanggal('#select-tanggal-inclusion' + i);
    }
    for (let i = 0; i < countExclusion; i++) {
        RefreshTanggal('#select-tanggal-exclusion' + i);
    }
    for (let i = 0; i < countTermCondition; i++) {
        RefreshTanggal('#select-tanggal-term' + i);
    }
});

$(document).on("click", ".tab-page", function () {
    tab = $(this).val();
});

$(document).on("click", "#btn_add_country", function () {
    var btn_delete = `
    <div class="col-lg-1">
            <div class="row">
                <label for="">&nbsp;</label>
            </div>
            <div class="row justify-content-start">
                <div class="col-2">
                    <button type="button" class="btn close-btn">
                    <i class="bi bi-x-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    `;
    if (inputDestination == 1) btn_delete = "";
    $element = $(`
    <div class="row mb-3 mt-3 input_destination">
        <div class="col-lg-5">
            <div class="mb-3">
                <label for="basicpill-phoneno-input" class="form-label">Destination</label>
                <select class="form-select need-check select2 country" name="input_tour_destination[]" index="${inputDestination}" id="destination_${inputDestination}">
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="basicpill-phoneno-input" class="form-label">Supplier</label>
                <select class="form-select select2" name="input_tour_supplier[]" id="supplier_${inputDestination}">
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="basicpill-firstname-input" class="form-label">Offline Code</label>
                <input type="text" class="form-control need-check" name="input_tour_offline_code[]" id="offline_code_${inputDestination}">
            </div>
        </div>
        ${btn_delete}
    </div>
    `);

    $(".input_destination:last").after($element);
    init();
});

//supplier berdasarkan supplier
$(document).on("change", ".country", function () {
    var idx = $(this).attr("index");
    //dapetin data supplier berdasarkan country
    autocompleteSupplier('#supplier_' + idx, null, $(this).val());

    //cek ada visa endak
    var temp = $(this).select2("data")[0];
    if (temp.with_visa == 1) $(`#visa_${idx}`).prop("checked", true);
    else $(`#visa_${idx}`).prop("checked", false);
    $(`#visa_${idx}`).attr("with_visa", temp.with_visa);
})

$(document).on("click", ".close-btn", function () {
    $(this).closest(".input_destination").remove();
    countDestination--;
});

$(document).on("change", "#input_tour_days", function(){
    $("#input_tour_nights").val($(this).val()-1);
})

$(document).on("blur", "#input_tour_days, #input_tour_nights", function () {
    if (inputTourDays == $("#input_tour_days").val()) {
        return;
    }
    itineraryUpdateMode = true;
    inputTourDays = $("#input_tour_days").val();

    $(".tour-departure-start").each(function(index){
        let newDate = moment(addDaysToDate($(this).val(), inputTourDays)).format("YYYY-MM-DD");
        $(".tour-departure-end").eq(index).val(newDate);
    })
});

$(document).on('change', '#check_gimmick_price', function(){
    if($(this).is(":checked")){
        $("#input_gimmick_price").prop('disabled', false)
    }else{
        $("#input_gimmick_price").prop('disabled', true)
    }
})

$(document).on("click", ".btn-next", function () {
    $(window).scrollTop(0);
    if (tab == 1) {
        $("#pills-itinerary-tab").click();
    } else if (tab == 2) {
        $("#pills-price-tab").click();
    } else if (tab == 3) {
        $("#pills-term-tab").click();
    }
});
$(document).on("click", ".btn-back", function () {
    $(window).scrollTop(0);
    $("#pills-price-tab").click();
});

$(document).on("click", "#pills-itinerary-tab", function () {
    if (!itineraryUpdateMode) return;
    refreshItinerary();
});

function refreshItinerary() {
    itineraryUpdateMode = false;
    itineraryQuil = [];
    $("#input_itenerary").html("");
    for (let i = 1; i <= $("#input_tour_days").val(); i++) {
        $element = $(`
            <div id="row${i}" class="row-itinerary">
                <div class="row mb-3" >
                    <div class="col-1">
                        <label for="">Days</label>
                        <div class="form-control" id="day${i}">
                        ${i.toString().padStart(2, "0")}
                        </div>
                    </div>
                    <div class="col-10">
                        <label for="">Highlight</label>
                        <input type="text" class="form-control need-check" id="input-highligt${i}" name="input_tour_highlight[]" placeholder="Tour Highlight">
                    </div>
                    <div class="col-1 pt-4 text-end">
                        <button type="button" class="btn btn-outline-danger btn-sm w-100 mt-1 btn-trash" id="trash${i}" index="${i}">
                            <ion-icon name="trash-outline" style="font-size:14pt;padding-top:2px"></ion-icon>
                        </button>
                    </div>
                </div>
                <div class="row mb-3 justify-content-between">
                    <div class="col-3">
                        <label for="">Short Description</label>
                    </div>
                    <div class="col-5">
                        <div class="row">
                            <div class="col-2"><label for="">Meals</label></div>
                            <div class="col-4">
                                <input class="form-check-input" type="checkbox" name="input_tour_breakfast[]" >
                                <label class="form-check-label" for="input_tour_breakfast">
                                    Breakfast
                                </label>
                            </div>
                            <div class="col-3">
                                <input class="form-check-input" type="checkbox" name="input_tour_lunch[]" >
                                <label class="form-check-label" for="input_tour_lunch">
                                    Lunch
                                </label>
                            </div>
                            <div class="col-3">
                                <input class="form-check-input" type="checkbox" name="input_tour_dinner[]" >
                                <label class="form-check-label" for="input_tour_dinner">
                                    Dinner
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-floating mb-4">
                        <div id="detail-${i}" class="" type="text-editor" style="height: 125px"></div>
                    </div>
                </div>
                <hr>
            </div>

        `);
        $("#input_itenerary").append($element);
        itineraryQuil.push(
            new Quill(`#detail-${i}`, {
                theme: "snow",
            })
        );
        if(mode==2){
            var temp = JSON.parse(tour.tour_itinerary);
            $('#input-highligt'+i).val(temp[i-1].highlight);
            itineraryQuil[i-1].pasteHTML(temp[i-1].detail);
            if(temp[i-1].breakfast==1)$('#breakfast'+i).prop("checked",true);
            else $('#breakfast'+i).prop("checked",false);

            if(temp[i-1].lunch==1)$('#lunch'+i).prop("checked",true);
            else $('#lunch'+i).prop("checked",false);

            if(temp[i-1].dinner==1)$('#dinner'+i).prop("checked",true);
            else $('#dinner'+i).prop("checked",false);
        }
    }
}
$(document).on("change", ".radio-departure", function () {
    if ($(this).val() == "disabled") {
        $(".ibjs-item").prop("disabled", true);
    } else {
        $(".ibjs-item").prop("disabled", false);
    }
});

$(document).on("click", ".tab-validate", function(){
    pillTab = $(this).attr('index');
})

function refreshPrice() {
    $(".departure_date").find(`.room-type`).each(function(){
        $parent = $(this).closest(".row");
        if($(this).val() > 0){
            $parent.find(`.single-price`).prop('disabled', true);
            $parent.find(`.pax-price`).prop('disabled', false);
            $parent.find(`.pax-price`).val(0);
        }else{
            $parent.find(`.single-price`).prop('disabled', false);
            $parent.find(`.pax-price`).prop('disabled', true);
            $parent.find(`.single-price`).val(0);
        }
    })
}

function newTab(id) {
    $element = '';
    for(i = 1; i<= 6; i++){
        value_1 = (i-1 == 0? 0 : "");
        value_2 = (i-1 == 0? "": 0 );
        $element += `
            <div class="row" class="room_tab_${id} room_tabs">
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Guest/Room Type</label>
                        <select class="form-select room-type "  name="input_tab_${id}_type${i}[]">
                            <option value="0"${(i-1==0?' selected': '')} room="Adult Single Accupancy">Adult Single Accupancy</option>
                            <option value="1"${(i-1==1?' selected': '')} room="Adult Twin / Triple Sharing">Adult Twin / Triple Sharing</option>
                            <option value="2"${(i-1==2?' selected': '')} room="Child With Bed">Child With Bed</option>
                            <option value="3"${(i-1==3?' selected': '')} room="Child No Bed">Child No Bed</option>
                            <option value="4"${(i-1==4?' selected': '')} room="Infant">Infant</option>
                            <option value="5"${(i-1==5?' selected': '')} room="Extra Bed">Extra Bed</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Single</label>
                        <div class="input-group">
                            <div class="input-group-text">Rp</div>
                            <input type="text" class="form-control need-check single-price single-price-${(id)}-${(i-1)} nominal" name="input_tab_${id}_single_price${i}[]" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="mb-3">
                        <label for="" class="form-label">02 - 04 Pax</label>
                        <div class="input-group">
                            <div class="input-group-text">Rp</div>
                            <input type="text" class="form-control need-check can-infant pax-price pax2-price pax2-price-${(id)}-${(i-1)} nominal" name="input_tab_${id}_single_pax2_index_${i}[]" placeholder="" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="mb-3">
                        <label for="" class="form-label">05 - 08 Pax</label>
                        <div class="input-group">
                            <div class="input-group-text">Rp</div>
                            <input type="text" class="form-control need-check can-infant pax-price pax5-price pax5-price-${(id)}-${(i-1)} nominal" name="input_tab_${id}_single_pax5_index_${i}[]" placeholder="" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="mb-3">
                        <label for="" class="form-label">09 - 12 Pax</label>
                        <div class="input-group">
                            <div class="input-group-text">Rp</div>
                            <input type="text" class="form-control need-check can-infant pax-price pax9-price pax9-price-${(id)}-${(i-1)} nominal" name="input_tab_${id}_single_pax9_index_${i}[]" placeholder="" disabled>
                        </div>
                    </div>
                </div>
            </div> \n
        `
    }
    let today = getCurrentDate();
    let later = moment(addDaysToDate(today, inputTourDays)).format('YYYY-MM-DD');

    $(`#tab_hotel_${id}`).append(`
        <div class="departure_date mt-0" >
            <div class="row align-items-center">
                <div class="col-11 d-flex">
                    <i class="mdi mdi-bullseye-arrow me-3" style="color:#43006a!important"></i>
                    <h5 class="my-0  departure_title" style="color:#43006a!important">Departure Date 1</h5>
                </div>
                <div class="col-1 text-end">
                    <button type="button" class="btn btn-outline-secondary btn-close-departure" aria-label="Close">X</button>
                </div>
            </div>
            <div class="mt-3">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="basicpill-vatno-input" class="form-label">Date</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input class="form-control need-check tour-departure-start" type="date" value="${today}" name="tour_departure_date_start_${id}[]">
                                </div>
                                <div class="col-lg-6">
                                    <input class="form-control need-check tour-departure-end" type="date" value="${later}" name="tour_departure_date_end_${id}[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="basicpill-vatno-input" class="form-label">Deposit Amount</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal tour-deposit" name="tour_deposit_amount_${id}[]" placeholder="" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Tour Price</b></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Selling Price</b></label>
                        </div>
                    </div>
                </div>
                ${$element}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-check form-switch form-switch-md mb-3" dir="ltr">
                            <label class="form-check-label" for="customSwitchsizemd">Add on Price</label>
                            <input type="checkbox" class="form-check-input check_addon_price check_tab_${id}_add_on_${countDeparture[id]-1}" id="check_tab_${id}_add_on_${countDeparture[id]-1}">
                        </div>
                    </div>
                </div>
                <div class="row add-on-price">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="mb-3">
                                    <label for="" class="form-label">Guest/Room Type</label>
                                    <input type="text" class="form-control need-check add_on_type input_tab_${id}_add_on_type_${countDeparture[id]-1}" name="input_tab_${id}_add_on_type_${countDeparture[id]-1}[]" placeholder="Add On Type" disabled>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="mb-3">
                                    <label for="" class="form-label float-start">Price</label>
                                    <button type="button" class="btn-close float-end btn_del_add_on" hidden></button>
                                    <div class="input-group">
                                        <div class="input-group-text">Rp</div>
                                        <input type="text" class="form-control need-check add_on_price_input nominal input_tab_${id}_add_on_price_${countDeparture[id]-1}" name="input_tab_${id}_add_on_price_${countDeparture[id]-1}[]" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12 ">
                        <button type="button" class="btn btn-light btn-rounded float-end btn_add_on">Add On Price +</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-check form-switch form-switch-md mb-3" dir="ltr">
                            <label class="form-check-label" for="check_black_out_date">Black Out Date</label>
                            <input type="checkbox" class="form-check-input check_black_out_date" id="check_tab_${id}_black_out_${countDeparture[id]-1}" name="check_tab_${id}_black_out_${countDeparture[id]-1}">
                        </div>
                    </div>
                </div>
                <div class="row black-out-date">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="mb-3">
                                    <label for="" class="form-label">From</label>
                                    <input type="datetime-local" class="form-control need-check black-out-start input_tab_${id}_blackout_start_${countDeparture[id]-1}" name="input_tab_${id}_blackout_start_${countDeparture[id]-1}" disabled>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="mb-3">
                                    <label for="" class="form-label">To</label>
                                    <input type="datetime-local" class="form-control need-check black-out-end input_tab_${id}_blackout_end_${countDeparture[id]-1}" name="input_tab_${id}_blackout_end_${countDeparture[id]-1}" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="mb-3">
                                    <label for="" class="form-label float-start">Remark</label>
                                    <button type="button" class="btn-close float-end btn_del_black_out" hidden></button>
                                    <div class="input-group">
                                        <input type="text" class="form-control need-check black-out-remark input_tab_${id}_remark_${countDeparture[id]-1}" name="input_tab_${id}_remark_${countDeparture[id]-1}" placeholder="Type your remkar here" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12 ">
                        <button type="button" class="btn btn-light btn-rounded float-end btn_black_out">Add More +</button>
                        <button type="button" class="btn btn-light btn_add_on_duplicate">Duplicate to Next</button>
                    </div>
                </div>
            </div>
            <hr>
        </div>

    `);

    refreshPrice();
}

function refreshDeparture(tab) {
    $element = $(`#tab_hotel_${tab}`)
    $element.find(".departure_date").each(function (index) {
        $(this)
            .find('.room-type')
            .each(function(type){
                $(this).attr('name', `input_tab_${tab}_type${type}[]`)
            })
        $(this)
            .find('.single-price')
            .each(function(type){
                $(this).attr('name', `input_tab_${tab}_single_price${type}[]`)
            })
        $(this)
            .find('.pax2-price')
            .each(function(type){
                $(this).attr('name', `input_tab_${tab}_single_pax2_index_${type}[]`)
            })
        $(this)
            .find('.pax5-price')
            .each(function(type){
                $(this).attr('name', `input_tab_${tab}_single_pax5_index_${type}[]`)
            })
        $(this)
            .find('.pax9-price')
            .each(function(type){
                $(this).attr('name', `input_tab_${tab}_single_pax9_index_${type}[]`)
            })
        $(this)
            .find(".departure_title")
            .text(`Departure Date ${index + 1}`);
        $(this)
            .find(".tour-departure-start")
            .attr('name', `tour_departure_date_start_${tab}[]`);
        $(this)
            .find(".tour-departure-end")
            .attr('name', `tour_departure_date_end_${tab}[]`);
        $(this)
            .find(".tour-deposit")
            .attr('name', `tour_deposit_amount_${tab}[]`);
        $(this)
            .find(".check_addon_price")
            .attr('id', `check_tab_${tab}_add_on_${index}`);
        $(this)
            .find(".add_on_type")
            .attr('name', `input_tab_${tab}_add_on_type_${index}[]`);
        $(this)
            .find(".add_on_price_input")
            .attr('name', `input_tab_${tab}_add_on_price_${index}[]`);
        $(this)
            .find(".add_on_type")
            .attr("name", `input_tab_${tab}_add_on_type_${index}[]`);
        $(this)
            .find(".add_on_price_input")
            .attr("name", `input_tab_${tab}_add_on_price_${index}[]`);
        $(this)
            .find(".check_black_out_date")
            .attr("name", `check_tab_${tab}_black_out_${index}[]`);
        $(this)
            .find(".black-out-start")
            .attr("name", `input_tab_${tab}_blackout_start_${index}[]`);
        $(this)
            .find(".black-out-end")
            .attr("name", `input_tab_${tab}_blackout_end_${index}[]`);
        $(this)
            .find(".black-out-remark")
            .attr("name", `input_tab_${tab}_remark_${index}[]`);
    });
}

$(document).on("dblclick", ".tab-validate", function(){
    if($(this).is('button')){
        var $input = $('<input type="text">');
        var attributes = this.attributes;
        $.each(attributes, function() {
            $input.attr(this.name, this.value);
        });

        $input.attr("type", 'text');
        $input.val($(this).text());
        $(this).replaceWith($input);
    }
});

$(document).on('blur', ".tab-validate", function(){
    if($(this).is('input')){
        var $button = $('<button type="button">');
        var attributes = this.attributes;
        // Loop through button's attributes and apply them to the input
        $.each(attributes, function() {
            $button.attr(this.name, this.value);
        });
        $button.attr('type', 'button');
        $button.text($(this).val());
        $(this).replaceWith($button);
    }
})

$(document).on('keypress', ".tab-validate", function(e){
    if(!$(this).is('input'))return;
    if(e.which  == 13) {
        e.preventDefault();
        $(this).blur();
    }
})

$(document).on("click", ".btn-del-tab-hotel", function(){
    // if()
    tabButton = $(this).prev();
    tabContent = $(`${tabButton.attr('data-bs-target')}`);
    tabButton = tabButton.parent();
    showModalDelete('Yakin mau menghapus hotel ini?', 'btn_confirm_del_tab');
})

$(document).on('click', '#btn_confirm_del_tab', function(){
    if(countDepartureTab == 1){
        toastr.error("Minimal 1 Tab Hotel", 'Gagal Delete');
        return;
    }
    closeModalDelete();
    index = tabButton.find('.tab-validate').attr('index');
    countDeparture.splice(index, 1);
    tabButton.remove();
    tabContent.remove();
    countDepartureTab--;
    refreshTab();
})

function refreshTab() {
    let $allButtonLink = $('.tab-validate');
    let $allTabContent = $('.validate-tab');
    for (let i = 0; i < countDepartureTab; i++) {
        $allButtonLink.eq(i).attr('id', 'btn_hotel_'+i);
        $allButtonLink.eq(i).attr('value', i);
        $allButtonLink.eq(i).attr('index', i);
        $allButtonLink.eq(i).attr('data-bs-target', '#tab_hotel_' + i);
        $allButtonLink.eq(i).attr('aria-controls', 'tab_hotel_' + i);
        if($allButtonLink.eq(i).text().split(' ')[0] != 'Hotel'){
            $allButtonLink.eq(i).text('Hotel ' + (i+1).toString().padStart(2, '0'));
        }
        $allTabContent.eq(i).attr('id', 'tab_hotel_' + i);
        $allTabContent.eq(i).attr('aria-labelledby', 'btn_hotel_' + i);
        refreshDeparture(i);
    }
}

$(document).on("click", "#btn_add_tab", function(){
    $(`#btn-add-tab`).before(`
        <li class="nav-item nav-validate me-3 btn-group align-items-center" role="presentation">
            <button class="nav-link tab-validate" id="btn_hotel_${countDepartureTab}" value="${countDepartureTab}" index="${countDepartureTab}" data-bs-toggle="pill" data-bs-target="#tab_hotel_${countDepartureTab}" type="button" role="tab" aria-controls="tab_hotel_${countDepartureTab}" aria-selected="true">
                Hotel ${(countDepartureTab+1).toString().padStart(2, '0')}
            </button>
            <button type="button" class="btn btn-del-tab-hotel">
                <i class="bi bi-x-square"></i>
            </button>
        </li>
    `);

    $(`#validate-tabContent`).append(`
        <div class="tab-pane fade show validate-tab" id="tab_hotel_${countDepartureTab}" role="tabpanel" aria-labelledby="btn_hotel_${countDepartureTab}" tabindex="0">

        </div>
    `);
    countDeparture.push(1);
    newTab(countDepartureTab++);
});

$(document).on("click", ".btn-close-departure", function () {
    if ($(this).closest(`#tab_hotel_${pillTab}`).find(`.departure_date`).length - 1 <= 0) {
        toastr.error("Minimal 1 Depature", "Gagal Insert");
        return false;
    }
    $(this).closest(".departure_date").remove();
    refreshDeparture(pillTab);
});

$(document).on('change', '.room-type', function(){
    $parent = $(this).closest(".row");
    $parent.find(`.single-price, .pax-price`).val('');
    if($(this).val() > 0){
        $parent.find(`.single-price`).prop('disabled', true);
        $parent.find(`.pax-price`).prop('disabled', false);
        $parent.find(`.pax-price`).val(0);
    }else{
        $parent.find(`.single-price`).prop('disabled', false);
        $parent.find(`.pax-price`).prop('disabled', true);
        $parent.find(`.single-price`).val(0);
    }
});

$(document).on("change", ".check_addon_price", function () {
    $element = $(this).closest(".departure_date").find(".add-on-price");
    if (!$(this).is(":checked")) {
        $element
            .find(".add_on_price_input, .add_on_type")
            .prop("disabled", true);
    } else {
        $element
            .find(".add_on_price_input, .add_on_type")
            .prop("disabled", false);
    }
});

$(document).on("click", ".btn_add_on", function () {
    $element = $(this).closest(".row").prev(".add-on-price").clone();
    $element.find(".add_on_type, .add_on_price_input").val("");
    $element.find(".btn_del_add_on").prop("hidden", false);
    $(this).closest(".row").prev(".add-on-price").after($element);
});

$(document).on("click", ".btn_del_add_on", function () {
    $(this).closest(".row.add-on-price").remove();
});

$(document).on("change", ".check_black_out_date", function () {
    $element = $(this).closest(".departure_date").find(".black-out-date");
    if (!$(this).is(":checked")) {
        $element
            .find(".form-control")
            .prop("disabled", true);
    } else {
        $element
            .find(".form-control")
            .prop("disabled", false);
    }
});

$(document).on("click", ".btn_black_out", function () {
    $element = $(this).closest(".row").prev(".black-out-date").clone();
    $element.find(".form-control").val("");
    $element.find(".btn_del_black_out").prop("hidden", false);
    $(this).closest(".row").prev(".black-out-date").after($element);

});

$(document).on("click", ".btn_del_black_out", function(){
    $(this).closest(".row.black-out-date").remove();
})

$(document).on("click", ".btn_add_on_duplicate", function () {
    $element = $(this).closest(".departure_date");
    $clonedElement = $element.clone();
    $element.find('select').each(function(index, originalSelect) {
        const selectedValue = $(originalSelect).val();

        const clonedSelect = $clonedElement.find('select').eq(index);
        clonedSelect.val(selectedValue);
      });
    $element.after($clonedElement);
    countDeparture[pillTab]++;
    refreshDeparture(pillTab);
});

$(document).on("click", "#btn-tambah-jadwal", function () {
    newTab(pillTab);
    refreshDeparture(pillTab);
});


$(document).on("click", "#btn_add_inclusion", function () {
    $element = $(`
        <div class="row inclusion mb-3">
            <div class="row">
                <label for="basicpill-namecard-input" class="form-label">Inclusion ${countInclusion}</label>
            </div>
            <div class="row">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-inclusion" index="${countExclusion-1}" id="select-inclusion${countInclusion - 1}">
                            </select>
                        </div>
                        <div class="col-3 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-2 align-self-center justify-content-end">
                            <input class="form-check-input radio-departure" type="radio" name="input_tour_inclusion_${countInclusion - 1}[]" id="formRadios111" checked value="disabled">
                            <label class="form-check-label" for="formRadios1">
                                All Departure
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="inclusion-wrapper-${countInclusion - 1}">
                                <div id="inclusion-text-${countInclusion - 1}" style="height: 250px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <small class="text-muted">*Pisahkan dengan enter</small>
        </div>
    `);
    $(".container-inclusion").append($element);
    autocompleteInclusion(`#select-inclusion${countInclusion - 1}`, null);
    inclusionQuil.push(
        new Quill(`#inclusion-text-${countInclusion - 1}`, {
            theme: "snow",
        })
    );
    countInclusion++;
});

$(document).on("change", ".select-inclusion", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr('index');
    var value = "";

    data_select.detail.forEach(item => {
        value += item.inclusion_detail + "\n";
    });

    inclusionQuil[index].pasteHTML(value);
});

$(document).on("click", "#btn_add_exclusion", function () {
    $element = $(`
        <div class="row exclusion mb-3">
            <div class="row">
                <label for="basicpill-namecard-input" class="form-label">Exclusion ${countExclusion}</label>
            </div>
            <div class="row ">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-exclusion" index="${countExclusion-1}" id="select-exclusion${countExclusion - 1}">
                            </select>
                        </div>
                        <div class="col-3 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-2 align-self-center justify-content-end">
                            <input class="form-check-input radio-departure" type="radio" name="input_tour_exclusion_${countExclusion - 1}[]" id="formRadios111" checked value="disabled">
                            <label class="form-check-label" for="formRadios1">
                                All Departure
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="exclusion-wrapper-${countExclusion - 1}">
                                <div id="exclusion-text-${countExclusion - 1}" style="height: 250px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <small class="text-muted">*Pisahkan dengan enter</small>
        </div>
    `);

    $(".container-exclusion").append($element);
    autocompleteExclusion(`#select-exclusion${countExclusion - 1}`, null);
    exclusionQuil.push(
        new Quill(`#exclusion-text-${countExclusion - 1}`, {
            theme: "snow",
        })
    );
    countExclusion++;
});

function RefreshTanggal(id) {

    var start = new moment($('#input_tour_date_start').val());
    var end = new moment($('#input_tour_date_end').val()).add(1, "days");
    var selisih = end.diff(start, 'days');// selisih hari
    var temp = [];

    var select = $(id);
    temp = select.val();//ambil data jika ada option yang select
    select.html("");//kosongkan
    for (let i = 0; i < selisih; i++) {
        //masukan date sesuai range date
        select.append(`<option  value="${start.format('MMM DD')}">${start.format('MMM DD')}</option>`);
        start.add(1, 'days');
    }

    if (temp && temp.length > 0) {//cek jika ada data select lama
        temp.forEach(item => {
            $(`${id} option[value='${item}']`).attr("selected", "selected");
        });
    }
}


$(document).on("change", ".select-exclusion", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr('index');
    var value = "";

    data_select.detail.forEach(item => {
        value += item.inclusion_detail + "\n";
    });
    console.log(index);
    exclusionQuil[index].pasteHTML(value);
});

$(document).on("click", "#btn_add_term_condition", function () {
    $element = $(`
        <div class="row term_condition">
            <div class="row">
                <label for="basicpill-namecard-input" class="form-label">Term & Condition ${countTermCondition}</label>
            </div>
            <div class="row mb-3">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-term" index="${countTermCondition - 1}" id="select-term${countTermCondition - 1}">
                            </select>
                        </div>
                        <div class="col-2 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-3 align-self-center">
                            <input class="form-check-input radio-departure" type="radio" name="input_tour_term_${countTermCondition - 1}[]" id="formRadios111" checked value="disabled">
                            <label class="form-check-label" for="formRadios1">
                                All Departure
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-floating mb-4">
                            <div id="term-${countTermCondition}" style="height: 250px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);
    $(".container_term_condition").append($element);
    termQuil.push(
        new Quill(`#term-${countTermCondition}`, {
            theme: "snow",
        })
    );
    autocompleteTermCondition(`#select-term${countTermCondition - 1}`, null);
    countTermCondition++;
});

$(document).on("click", ".select-tanggal option", function () {
    //masih bug
    // alert("test");
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr("index");
    var jumlah = countInclusion;
    var id = "select-tanggal-inclusion";
    if (data_select.term_jenis && data_select.term_jenis == 2) { jumlah = countExclusion; id = "select-tanggal-exclusion"; }
    if (data_select.terms_condition_id) { jumlah = countTermCondition; id = "select-tanggal-term"; }
    $(`.${id} option`).show();

    for (let i = 0; i < jumlah; i++) {
        if (i != index) {
            $(this).val().forEach(item => {
                $(`.${id}${i} option[value='${item}']`).hide();
            });
        }
    }
});

$(document).on("change", ".select-term", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr("index");
    var value = "";

    termQuil[index].pasteHTML(data_select.terms_condition_detail);
    $(this).closest(".exclusion").find(".text-area").val(value);
});

$(document).on("click", ".btn-trash", function () {
    var index = parseInt($(this).attr("index"));
    if (parseInt($("#input_tour_days").val()) - 1 <= 0) {
        toastr.error("Minimal 1 Hari / 1 Itinerary", "Gagal Insert");
        return false;
    }
    $("#input_tour_days").val(parseInt($("#input_tour_days").val()) - 1);
    $("#input_tour_nights").val(parseInt($("#input_tour_nights").val()) - 1);
    $('#row' + index).remove();

    //reindexing
    for (let i = index + 1; i <= parseInt($("#input_tour_days").val()) + 1; i++) {
        var new_val = i - 1;
        $('#day' + i).html((new_val + "").padStart(2, "0"));
        $('#row' + i).attr("id", "row" + new_val);
        $('#trash' + i).attr("index", new_val);
    }
    //scroll ke terakhir
    window.location.href = "#input-highligt" + $("#input_tour_days").val();
});

$(document).on("click", ".btn-tambah-hari", function () {
    $("#input_tour_days").val(parseInt($("#input_tour_days").val()) + 1);
    $("#input_tour_nights").val(parseInt($("#input_tour_nights").val()) + 1);
    refreshItinerary();
    window.location.href = "#input-highligt" + $("#input_tour_days").val();
});

$(document).on("keyup", ".price", function () {
    var pr = convertToAngka($(this).val());
    var disc = convertToAngka($(this).closest(".row").find('.disc').val());
    $(this).closest(".row").find('.total').val(formatRupiah((pr - disc) + ""));
});

$(document).on("keyup", ".disc", function () {
    var disc = convertToAngka($(this).val());
    var pr = convertToAngka($(this).closest(".row").find('.price').val());
    $(this).closest(".row").find('.total').val(formatRupiah((pr - disc) + ""));
});

$(document).on("keyup", ".nominal", function (e) {
    //setiap berbau nominal or rupiah dalam input ataupun dalam table harus ada Rp. dan sperator
    $(this).val(formatRupiah($(this).val() + ""), "Rp.");
});

$(document).on("click", "#btn_submit", function(){
    $("#tour_form").submit();
})

function isValid()
{
    let check = true;
    let toastSelect = false;
    let $tab = '';
    let $tab2 = '';
    let $element;
    $('.need-check').removeClass('is-invalid');
    $('.need-check').each(function()
    {
        isInfant = false;
        if($(this).hasClass('can-infant')){
            if($(this).closest(".row").find('.room-type').val() == 4||$(this).closest(".row").find('.room-type').val() == 5){
                isInfant = true;
            }
        }
        isEmpty = ($(this).val() == '' ||( $(this).val() == 0 && !isInfant)) && !$(this).prop('disabled');
        if(isEmpty)
        {
            check = false;
            if($(this).is('input'))
            {
                $(this).toggleClass('is-invalid', isEmpty);
            }
            else
            {
                toastSelect = true;
            }

            if($tab == '')
            {
                $element = $(this);
                $tab = $(this).closest('.tab-pane');
                console.log($tab);
                if($tab.hasClass('validate-tab'))
                {
                    $tab2 = $(`#pills-price-tab`);
                }
                $tab = $(`#${$tab.attr('aria-labelledby')}`);
            }
        }
    })

    for (let i = 0; i < inclusionQuil.length; i++)
    {
        const element = inclusionQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check=false;
            toastr.error('Ada Inclusion Kosong!');
            break;
        }
    }

    for (let i = 0; i < exclusionQuil.length; i++)
    {
        const element = exclusionQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check=false;
            toastr.error('Ada Exclusion Kosong!');
            break;
        }
    }

    for (let i = 0; i < termQuil.length; i++)
    {
        const element = termQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check=false;
            toastr.error('Ada Term & Condition Kosong!');
            break;
        }
    }

    if(!check)
    {
        $tab.click();
        $('html, body').animate({
            scrollTop: $element.offset().top
        }, 10);
        if($tab2 != "")
        {
            $tab2.click();
        }
        if(toastSelect)
        {
            toastr.error('Mohon pilih data');
        }
        toastr.error('Harap isi semua field', "Gagal Insert");
    }
    return check;
}

$(document).on("submit", "#tour_form", function (e) {
    e.preventDefault();
    submit(0);
});

$(document).on("click", "#btn-safe-draf", function (e) {
    submit(1);
});

function submit(draf=0) {
    var status = 1;
    if(draf==0&&$("#inlineRadio1").is(":checked")&&!isValid())
    {
        $("#modal-save").modal("hide");
        return;
    }
    if($("#inlineRadio2").is(":checked"))status=4;
    if(draf==1)status=3;

    let tour_destination = [];

    //Insert Tour Destination
    $("select[name='input_tour_destination[]']").each(function (index) {
        let temp = $(this).select2("data")[0];
        if(temp){
            tour_destination.push({
                country_name: temp.name,
                country_id: temp.id,
                is_visa: temp.with_visa == 1 ? true : false,
            });
        }

    });

    $("select[name='input_tour_supplier[]']").each(function (index) {
        let temp = $(this).select2("data")[0];
        if(temp){
            tour_destination[index].supplier_nama = temp.name;
            tour_destination[index].supplier_id = temp.id;
        }

    });

    $("input[name='input_tour_offline_code[]']").each(function (index) {

        if(tour_destination[index]) tour_destination[index].offline_code = $(this).val();
    });

   // Insert Itinerary
   var itinerary = [];
   $(".row-itinerary").each(function (index) {
       var idx = index+1;
       itinerary.push({
           highlight: $(`#input-highligt`+idx).val(),
           breakfast:$(`#breakfast`+idx).is(":checked"),
           lunch:$(`#lunch`+idx).is(":checked"),
           dinner:$(`#dinner`+idx).is(":checked"),
           detail: itineraryQuil[index].root.innerHTML
       });
   });

    // Insert Term & Condition
    term_condition = {
        inclusion: [],
        exclusion: [],
        termCondition: []
    };

    var date = [];
    var start = new moment($('#input_tour_date_start').val());
    var end = new moment($('#input_tour_date_end').val()).add(1, "days");
    var selisih = end.diff(start, 'days');// selisih hari

    for (let i = 0; i < selisih; i++) {
        //masukan date sesuai range date
        date.push(start.format('MMM DD'));
        start.add(1, 'days');
    }

    for (let i = 0; i < inclusionQuil.length; i++) {
        const element = inclusionQuil[i];

        term_condition.inclusion.push({
            "tour_terms_value":element.root.innerHTML,
            "tour_terms_jenis_id": $('#select-inclusion'+i).val(),
            "tour_terms_date":date
        });
    }

    for (let i = 0; i < exclusionQuil.length; i++) {
        const element = exclusionQuil[i];
        term_condition.exclusion.push({
            "tour_terms_value":element.root.innerHTML,
            "tour_terms_jenis_id": $('#select-exclusion'+i).val(),
            "tour_terms_date":date
        });
    }

    for (let i = 0; i < termQuil.length; i++) {
        const element = termQuil[i];
        term_condition.termCondition.push({
            "tour_terms_value":element.root.innerHTML,
            "tour_terms_jenis_id": $('#select-term'+i).val(),
            "tour_terms_date":date
        });
    }

    let data = {
        tour_jenis:2,
        tour_name: $("#input_tour_name").val(),
        tour_category: $("#input_tour_category").val(),
        tour_type: $("#input_tour_type").val(),
        tour_date_start: $("#input_tour_date_start").val(),
        tour_date_end: $("#input_tour_date_end").val(),
        tour_destination: JSON.stringify(tour_destination),
        tour_days: $("#input_tour_days").val(),
        tour_nights: $("#input_tour_nights").val(),
        tour_flight: $('#tour_flight').is(':checked'),
        tour_hotel: $('#tour_hotel').is(':checked'),
        tour_meal: $('#tour_meal').is(':checked'),
        tour_tour: $('#tour_tour').is(':checked'),
        tour_transport: $('#tour_transport').is(':checked'),
        tour_visa: $('#tour_visa').is(':checked'),
        tour_price_gimick: ($('#check_gimmick_price').is(":checked")? convertToAngka($('#input_gimmick_price').val()): 0),
        keyword_id: JSON.stringify($("select[name='input_tour_keyword[]']").val()),
        tour_itinerary: JSON.stringify(itinerary),
        tour_inclusion: JSON.stringify(term_condition.inclusion),
        tour_exclusion: JSON.stringify(term_condition.exclusion),
        tour_term_condition: JSON.stringify(term_condition.termCondition),
        displayer_id: $('#select_displayer').val(),
        status:status,
        _token : token
    }

    // insert Validity Price
    validity = [];
    for (let i = 0; i < countDepartureTab; i++) {
        /**
         * hotel = {
         *      tour_start_date: [
         *          0: date1,
         *          1: date2,
         *          2: date3
         *      ]
         *      tour_end_date: [
         *          0: date1,
         *          1: date2,
         *          2: date3
         *      ]
         *      deposit_ammount: [
         *          0: date1,
         *          1: date2,
         *          2: date3
         *      ]
         *      room : [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      price: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      pax_2: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      pax_5: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      pax_9: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      add_on_type: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         *      add_on_price: [
         *          0: [date1, date1, ...],
         *          1: [date2, date2, ...],
         *          2: [date3, date3, ...]
         *      ]
         * }
         */
        var hotel = {
            tour_departure_id: [],
            tour_start_date: [],
            tour_end_date: [],
            deposit_amount: [],
            room: [],
            price: [],
            pax_2: [],
            pax_5: [],
            pax_9: [],
            add_on_type: [],
            add_on_price: [],
            black_out: []
        };

        $(`input[name='tour_departure_date_start_${i}[]']`).each(function(index){
            if(mode==2&&tour.tour_validity[i].item[index]) hotel.tour_departure_id.push(tour.tour_validity[i].item[index].tour_departure_id);
            hotel.tour_start_date.push($(this).val());
        })

        $(`input[name='tour_departure_date_end_${i}[]']`).each(function(index){
            hotel.tour_end_date.push($(this).val());
        })

        $(`input[name="tour_deposit_amount_${i}[]"]`).each(function(index){
            hotel.deposit_amount.push(convertToAngka($(this).val()));
        })

        for(j = 0; j < 6; j++){
            $(`select[name='input_tab_${i}_type${j+1}[]']`).each(function(index){
                if(index % 6 == 0){
                    hotel.room.push([]);
                }
                hotel.room[hotel.room.length-1].push($(this).find(":selected").text());
            })

            $(`input[name='input_tab_${i}_single_price${j+1}[]']`).each(function(index){
                if(index % 6 == 0){
                    hotel.price.push([]);
                }
                if(!$(this).prop('disabled')){
                    hotel.price[hotel.price.length-1].push(convertToAngka($(this).val()));
                }else{
                    hotel.price[hotel.price.length-1].push(0);
                }
            })

            $(`input[name='input_tab_${i}_single_pax2_index_${j+1}[]']`).each(function(index){
                if(index % 6 == 0){
                    hotel.pax_2.push([]);
                }
                if(!$(this).prop('disabled')){
                    hotel.pax_2[hotel.pax_2.length-1].push(convertToAngka($(this).val()));
                }else{
                    hotel.pax_2[hotel.pax_2.length-1].push(0);
                }
            })

            $(`input[name='input_tab_${i}_single_pax5_index_${j+1}[]']`).each(function(index){
                if(index % 6 == 0){
                    hotel.pax_5.push([]);
                }
                if(!$(this).prop('disabled')){
                    hotel.pax_5[hotel.pax_5.length-1].push(convertToAngka($(this).val()));
                }else{
                    hotel.pax_5[hotel.pax_5.length-1].push(0);
                }

            })

            $(`input[name='input_tab_${i}_single_pax9_index_${j+1}[]']`).each(function(index){
                if(index % 6 == 0){
                    hotel.pax_9.push([]);
                }
                if(!$(this).prop('disabled')){
                    hotel.pax_9[hotel.pax_9.length-1].push(convertToAngka($(this).val()));
                }else{
                    hotel.pax_9[hotel.pax_9.length-1].push(0);
                }
            })
        }

        for(j = 0; j < countDeparture[i]; j++){
            if($(`.check_tab_${i}_add_on_${j}`).is(":checked")){

                tempType = [];
                $(`.input_tab_${i}_add_on_type_${j}`).each(function(){
                    tempType.push($(this).val());
                })

                tempPrice = [];
                $(`.input_tab_${i}_add_on_price_${j}`).each(function(){
                    tempPrice.push(convertToAngka($(this).val()));
                })
                hotel.add_on_type.push(tempType);
                hotel.add_on_price.push(tempPrice);
            }

            if($(`#check_tab_${i}_black_out_${j}`).is(":checked")){
                var temp =[];
                $(`.input_tab_${i}_blackout_start_${j}`).each(function(idx){
                    temp.push({"date_start" : $(this).val()});
                    if(mode==2){
                        if(tour.tour_validity[i].item[j].black_out[idx]){
                            temp[idx].tour_black_out_id = tour.tour_validity[i].item[j].black_out[idx].tour_black_out_id;
                        }
                    }
                });

                $(`.input_tab_${i}_blackout_end_${j}`).each(function(idx){
                    temp[idx].date_end = $(this).val();
                });

                $(`.input_tab_${i}_remark_${j}`).each(function(idx){
                    temp[idx].remark = $(this).val();
                });

                hotel.black_out = temp;
            }
        }

        hotel.hotel_name = $('#btn_hotel_'+i).html().trim().replace(/(\r\n|\n|\r)/gm, "");
        hotel.black_out = JSON.stringify(hotel.black_out);

        validity.push(hotel);
    }
    data.validity = JSON.stringify(validity);

    //mode edit
    var url = "insertFitDepature";
    if(mode==2){
        url = "updateFitDepature";
        data.tour_id = tour.tour_id;
        data.displayer_detail_id = tour.displayer_detail_id;
    }

    //convert data -> form data
    const fd = new FormData();
    for (const [key, value] of Object.entries(data)) {
        fd.append(key, value);
    }

    fd.append("tour_cover", $('#input_file_cover')[0].files[0]=="undefined"?null:$('#input_file_cover')[0].files[0]);
    fd.append("tour_banner", $('#input_file_banner')[0].files[0]=="undefined"?null:$('#input_file_cover')[0].files[0]);
    $.each($("#input_file_galery")[0].files, function(i, file) {
        fd.append('tour_galery[]', file);
    });

    //add loading
    $('#btn_submit').append(`
        <div class="spinner-border text-light ms-2 " id="" role="status" style="width: 10px;
        height: 10px;
        font-size: 10px;
        color: white!important;">
            <span class="visually-hidden">Loading...</span>
        </div>
    `);

    $('#btn_submit').attr("disabled","disabled");
    $.ajax({
        url: "/product/tour/fit_tour/"+url,
        method: "post",
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        data: fd,
        success: function () {
            $('#btn_submit').removeAttr("disabled");
            $('#btn_submit').html("Submit Data");
            localStorage.setItem("success","Berhasil Edit Data!");
            window.location.href = "/product/tour/fit_tour";
        },
        error: function (e) {
            console.log(e);
            $('#btn_submit').html("Submit Data");
            $('#btn_submit').removeAttr("disabled");
            toastr.error("", "Terjadi Kesalahan");
        },
    });
};

$(document).on("click", ".btn-draft", function (e) {
    console.log("test");
    submit(1);
});

window.onmousedown = function (e) {
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();

        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');

        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
}

