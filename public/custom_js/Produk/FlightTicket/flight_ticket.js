var mode=1; //1= insert, 2= delete

get_data();

$(document).on("click", "#btn-batal", function(){
    $("#modalInsert").find('input').val();
    $("#modalInsert").find('img').attr("src", uploadImageUrl);
})
$(document).on("click",".btn-delete",function(){
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.airline_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})
$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val("").trigger("change");
    get_data();
})
$(document).on("change", ".upload_gambar", function(){
    let reader = new FileReader();
    let inputElement = $(this);

    reader.onload = function(e) {
        inputElement.prev().attr("src", e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Flight Ticket");
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#airline_nama').val(data.airline_name);
    $('#code-iata').val(data.airline_code_iata);
    $('#url-api').val(data.airline_url_api);
    $('#pic-name').val(data.airline_pic_name);
    $('#pic-email').val(data.airline_pic_email);
    $('#preview').attr("src",public+data.airline_url_logo);
    $('#log_container').html(" ");
    data.log.forEach(item => {
        $('#log_container').append(`<label>- ${item.airline_url_log_api}</label><br>`);
    });
    $('#modalInsert').attr("edit_id",data.airline_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Flight Ticket");
    $('.add').val("");
    $('#log_container').html(" ");
    $("#modalInsert").find('input').val();
    $("#modalInsert").find('img').attr("src", uploadImageUrl);
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})


$(document).on("click", ".btn-delete-photo", function(){
    let inputFile = $("#modalInsert");
    inputFile.find("input[type='file']").val('');
    inputFile.find("img").attr("src", uploadImageUrl);
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/product/deleteAirlines",
        method:"post",
        data:{
            airlines_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("","Gagal Insert");
            }
        }
    });
})

 $(document).on("click","#btn-insert",function(){
     var valid = 1;
     var url = "/product/insertAirlines";
     $('.has-danger').removeClass("has-danger");
     
     if(valid==0){
         toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
         return false;
     }
    const fd = new FormData();
     fd.append("airline_name", $('#airline_nama').val());
     fd.append("airline_code_iata", $('#code-iata').val());
     fd.append("airline_url_api", $('#url-api').val());
     fd.append("airline_pic_name", $('#pic-name').val());
     fd.append("airline_pic_email", $('#pic-email').val());
     fd.append("_token", $('meta[name="csrf-token"]').attr('content'));
     fd.append("airline_logo", $('#logo')[0].files[0]=="undefined"?null:$('#logo')[0].files[0]);
    
     if(mode==2){
         url = "/product/editAirlines";
         fd.append("airline_id", $('#modalInsert').attr("edit_id"));
     }
     $.ajax({
         url:url,
         method:"post",
         data:fd,
         processData: false,  // tell jQuery not to process the data
         contentType: false,  // tell jQuery not to set contentType
         success:function(e){
             if(e==1){
                 if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                 else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                 get_data();
                 $('.modal').modal("hide");
             }
             else{
                 toastr.error("Airlines sudah terdaftar","Gagal Insert");
             }
         }
     })
 })

function get_data() {
    $("#tb-document").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/product/get_data_airlines",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].airline_url_logo_img = `<div class="p-2"><img src="${public+json.data[i].airline_url_logo}" style="width:100px"></div>`;
                    json.data[i].airline_pic_name_full = `<b>${json.data[i].airline_pic_name}</b><br>${json.data[i].airline_pic_email}`;
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "airline_code_iata"},
            { data: "airline_name"},
            { data: "airline_url_logo_img"},
            { data: "airline_url_api"},
            { data: "airline_pic_name_full"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-document").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_dokumen(data=null) {
    $.ajax({
        url:"/master/get_data_document",
        method:"get",
        success:function(e){
            $('#container_dokumen').html("");
            e = JSON.parse(e);
            e["data"].forEach(item => {
                 $('#container_dokumen').append(`<input type="checkbox" class="form-check-input me-1 dokumen" value="${item.document_id}" document_name="${item.document_name}" style="font-size:12pt" name="document">
                 <label class="form-check-label me-4 mt-1" style="font-size:9pt">${item.document_name}</>`);
            });
            if(data!=null){
                data.forEach(item => {
                    $('.dokumen').each(function(){
                        if($(this).val()==item.document_id){
                            $(this).prop("checked",true);
                        }
                    });
                });

            }
        }
    });
}
