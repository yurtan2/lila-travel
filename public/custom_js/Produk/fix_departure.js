get_data();
var mode=1; //1= insert, 2= delete

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Country");
    $('.add').val("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Country");
    var data = $('#tb-country').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#nama').val(data.name);
    $('#code').val(data.iso3);
    $('#phone').val(data.phonecode);
    $('#modalInsert').attr("edit_id",data.id);
    $('#modalInsert').modal("show");
})


$(document).on("click",".btn-view",function(){
    var data = $('#tb-country').DataTable().row($(this).parents('tr')).data();

    $('#view_visa_name').html(`${data.visa_name}`);
    $('#view_visa_detail').html(`${data.visa_detail}`);
    $('#view_visa_dokumen').html("");
    if(JSON.parse(data.visa_need).length<=0)$('#view_visa_dokumen').html("<label>-</label>");
    JSON.parse(data.visa_need).forEach((item,index) => {
        $('#view_visa_dokumen').append(`<label class="me-3">${index+1}. ${item.document_name}</label>`);
    });
    $('#modalView').attr("visa_id",data.visa_id);
    $('#modalView').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-departure').DataTable().row($(this).parents('tr')).data();

    $('#modalDelete').attr("delete_id",data.tour_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/product/tour/fix_departure/deleteFixDepature",
        method:"post",
        data:{
            tour_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            toastr.success("Berhasil delete data","Berhasil Delete");
            get_data();
            $('.modal').modal("hide");
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var code = $('#code').val();
    var phone = $('#phone').val();
    var valid = 1;
    var url = "insertCountry";
    var data = {
        nama:nama,
        code:code,
        phone:phone,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(code==""||code==null){
        $('#code').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(phone==""||phone==null){
        $('#phone').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editCountry";
        data.country_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Country sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-departure").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/product/tour/fix_departure/get_data_fix",
            type: "get",
            data:{
                tour_name:$('#nama_filter').val(),
                date:$('#filter_date').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <a class='btn btn-warning btn-edit' href="/product/tour/fix_departure/detailFixDepature/${json.data[i].tour_id}">Edit</a>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    json.data[i].tour_validity_text = moment(json.data[i].tour_date_start).format("DD MMM YYYY")+" - "+moment(json.data[i].tour_date_end).format("DD MMM YYYY")
                    json.data[i].tour_status = json.data[i].status==1?"Active":json.data[i].status==2?"Expired":json.data[i].status==4?"Not Publish":"Draf";
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "tour_name"},
            { data: "tour_category"},
            { data: "tour_type"},
            { data: "tour_validity_text"},
            { data: "tour_status"},
            { data: "action",class:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-departure").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
