/*
    Note :
    - kalau bisa card jangan ada card kayak tumpuk" pertama makan space
    karena ada padding, kedua kek aneh ae gak cocok gtu ganggu mata client karena client suka yang clean
    - button ambil yang sedang or small jangan besar soale pertama kali aku buka salfok sama button e buka

*/

// inisial variabel
var inputDestination = 0;
var countDestination = 0;
var countInclusion = 1;
var countExclusion = 1;
var countTermCondition = 1;
var countDeparture = 0;
var tab = 1;
var days = 2;
var inputTourDays = 2;

var itineraryUpdateMode = true;
var itineraryQuil = [];

var termQuil = [];
var inclusionQuil = [];
var exclusionQuil = [];

// Windows Load
$(function () {
    init();
    init2();
    //kalau mau paten mending dari js biar nanti pas edit lebih gampang
    autocompleteKeywords('#input_keyword', null);
    $('#input_tour_date_start').val(getCurrentDateTime());
    addDayTime('#input_tour_date_start', 2, "#input_tour_date_end");


    if(mode==2){
        //edit mode
        $('#input_tour_name').val(tour.tour_name);
        $('#input_tour_category').val(tour.tour_category).trigger("change");
        $('#input_tour_type').val(tour.tour_type).trigger("change");
        $('#input_tour_date_start').val(tour.tour_date_start).trigger("change");
        $('#input_tour_date_end').val(tour.tour_date_end).trigger("change");
        //tour & keyword
        tour.keyword.forEach(item => {
            $('#input_keyword').append(`<option value="${item.keyword_id}" selected>${item.keyword_name}</option>`).trigger("change");
        });
        if(tour.displayer)$('#select_displayer').append(`<option value="${tour.displayer.displayer_id}">${tour.displayer.displayer_title}</option>`).trigger("change");
        //destinasi
        tour.destinasi.forEach(item => {
            $('#btn_add_destination').click();
            $('#destination_'+(inputDestination-1)).append(`<option value="${item.id}" >${item.name}</option>`).trigger("change");
            $('#supplier_'+(inputDestination-1)).append(`<option value="${item.supplier_id}">${item.supplier_name}</option>`).trigger("change");
            $('#offline_code_'+(inputDestination-1)).val(item.offline_code);
            $('#tour_destination_id'+(inputDestination-1)).val(item.tour_destination_id);
            if(item.with_visa==1)$('#visa_'+(inputDestination-1)).prop("checked",true);
            else $('#visa_'+(inputDestination-1)).prop("checked",false);
        });

        //include
        if(tour.tour_air_ticket==1)$('#tour_air_ticket').prop("checked",true);
        if(tour.tour_hotel==1)$('#tour_hotel').prop("checked",true);
        if(tour.tour_meal==1)$('#tour_meal').prop("checked",true);
        if(tour.tour_tour==1)$('#tour_tour').prop("checked",true);
        if(tour.tour_land_trans==1)$('#tour_land_trans').prop("checked",true);
        if(tour.tour_water_trans==1)$('#tour_water_trans').prop("checked",true);
        if(tour.tour_visa==1)$('#tour_visa').prop("checked",true);
        if(tour.tour_insurance==1)$('#tour_insurance').prop("checked",true);

        //day & night
        $('#input_tour_days').val(tour.tour_days);
        $('#input_tour_nights').val(tour.tour_nights);



        //inclsion
        tour.tour_inclusion.forEach((item,index) => {
            console.log(item);
            $('#btn_add_inclusion').click();
            inclusionQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-inclusion'+index).append(`<option value="${item.term_id}">${item.term_name}</option>`);
            $('#select-inclusion'+index).attr("tour_terms_id",item.tour_term_id);

            if(item.tour_terms_date!="[]"){
                JSON.parse(item.tour_terms_date).forEach(element => {
                    $(`#select-tanggal-inclusion${index} option[value='${element}']`).attr("selected", "selected");
                });
            }

        });
        if(tour.tour_inclusion.length<=0) $('#btn_add_inclusion').click();

        //exclusion
        tour.tour_exclusion.forEach((item,index) => {
            $('#btn_add_exclusion').click();
            exclusionQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-exclusion'+index).append(`<option value="${item.term_id}">${item.term_name}</option>`);
            $('#select-exclusion'+index).attr("tour_terms_id",item.tour_term_id);
            if(item.tour_terms_date!="[]"){
                JSON.parse(item.tour_terms_date).forEach(element => {
                    $(`#select-tanggal-exclusion${index} option[value='${element}']`).attr("selected", "selected");
                });
            }
        });
        if(tour.tour_exclusion.length<=0) $('#btn_add_exclusion').click();

        //term & condition
        tour.tour_term.forEach((item,index) => {
            $('#btn_add_term_condition').click();
            termQuil[index].pasteHTML(item.tour_terms_value);
            $('#select-term'+index).append(`<option selected value="${item.terms_condition_id}">${item.terms_condition_name}</option>`);
            $('#select-term'+index).attr("tour_terms_id",item.tour_term_id);
            if(item.tour_terms_date!="[]"){
                console.log(JSON.parse(item.tour_terms_date));
                JSON.parse(item.tour_terms_date).forEach(element => {
                    $(`#select-tanggal-term${index} option[value='${element}']`).attr("selected", "selected");
                });
            }
        });
        if(tour.tour_term.length<=0) $('#btn_add_term_condition').click();

        //validity & price
        refreshDeparture();
        tour.tour_validity.forEach((item,index) => {
            var departure = `.departure_date:eq(${index})`;
            $('#btn-tambah-jadwal').click();
            $('.tour_departure_date_start').eq(index).val(item.tour_departure_date_start).trigger("change");
            $('.tour_departure_date_end').eq(index).val(item.tour_departure_date_end).trigger("change");
            $('.deposit').eq(index).val(formatRupiah(item.tour_departure_deposit+"","")).trigger("change");

            //masukan default price + extra bed
            item.default_price.forEach(value => {
                let name = `[price_name="${value.tour_departure_detail_name}"]`;
                if(value.tour_departure_detail_name=="extra_bed"&&value.tour_departure_detail_price&&value.tour_departure_detail_price>0) {
                    $('.check_extra_bed').eq(index).prop('checked',true).trigger("change");
                }
                if(value.tour_departure_detail_price)$(`${departure} ${name}`).find('.price').val(formatRupiah(value.tour_departure_detail_price));
                if(value.tour_departure_detail_disc)$(`${departure} ${name}`).find('.disc').val(formatRupiah(value.tour_departure_detail_disc));
                if(value.tour_departure_detail_total)$(`${departure} ${name}`).find('.total').val(formatRupiah(value.tour_departure_detail_total));
            });

            //check + masukan add on
            if(item.add_on.length>0){
                $('.check_addon_price').eq(index).prop('checked',true).trigger("change");
                item.add_on.forEach((value_add_on,index_add_on) => {
                    if(value_add_on.tour_departure_detail_name)$(`${departure} .add_on_type`).eq(index_add_on).val(value_add_on.tour_departure_detail_name);
                    if(value_add_on.tour_departure_detail_price)$(`${departure} .add_on_price_input`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_price)).trigger("change");
                    if(value_add_on.tour_departure_detail_disc)$(`${departure} .add_on_disc_input`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_disc)).trigger("change");
                    if(value_add_on.tour_departure_detail_total)$(`${departure} .add_on_total`).eq(index_add_on).val(formatRupiah(value_add_on.tour_departure_detail_total)).trigger("change");
                    if(index_add_on+1<item.add_on.length){
                        $('.btn_add_on').eq(index).click();
                    }
                });
            }
        });
        //publish
        //1 = active
        //2 = expired
        //3 = draft
        //0 = delete
        if(tour.status==1){
            $('#inlineRadio1').prop("checked",true);
            $('#inlineRadio2').prop("checked",false);
        }
        else {
            $('#inlineRadio1').prop("checked",false);
            $('#inlineRadio2').prop("checked",true);
        }
    }
    else{
        $('#btn_add_inclusion').click();
        $('#btn_add_exclusion').click();
        $('#btn_add_term_condition').click();
        $('#btn_add_destination').click();
        $('#btn-tambah-jadwal').click();
        refreshItinerary();
    }
});

function init() {
    autocompleteCountry(`#destination_${inputDestination}`);
    autocompleteSupplier(`#supplier_${inputDestination++}`);
    autocompleteDisplayer(`#select_displayer`, null);
    // autocompleteExclusion(`#select_displayer`,null);
    countDestination++;
    refreshDeparture();
}

function init2() {
    $(".js-example-basic-multiple").select2();
}

$(document).on("click", ".tab-page", function () {
    tab = $(this).val();
    // alert(tab);
});

$(document).on("change",'#input_file_cover',function(){

});

$(document).on("click", "#btn_add_destination", function () {
    var btn_delete = `
    <div class="col-lg-1">
            <div class="row">
                <label for="">&nbsp;</label>
            </div>
            <div class="row justify-content-start">
                <div class="col-2">
                    <button type="button" class="btn close-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-square"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></svg>
                    </button>
                </div>
            </div>
        </div>
    `;
    if (inputDestination == 1) btn_delete = "";
    $element = $(`
    <div class="row mb-3 mt-3 input_destination">
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="basicpill-phoneno-input" class="form-label">Destination</label>
                <select class="form-select select2 country need-check" name="input_tour_destination[]" index="${inputDestination}" id="destination_${inputDestination}">
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="mb-3 text-center">
                <div class="row h-100">
                    <label for="basicpill-email-input" class="form-label">Visa Required</label>
                </div>
                <div class="row justify-content-center py-2">
                    <input class="form-check visa" type="checkbox" name="input_tour_visa[]"  id="visa_${inputDestination}">
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="basicpill-phoneno-input" class="form-label">Supplier</label>
                <select class="form-select select2 need-check" name="input_tour_supplier[]" id="supplier_${inputDestination}">
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="basicpill-firstname-input" class="form-label">Offline Code</label>
                <input type="text" class="form-control need-check" name="input_tour_offline_code[]" id="offline_code_${inputDestination}">
            </div>
        </div>
        ${btn_delete}
        <input type="hidden" class="tour_destination_id" name="tour_destination_id[]" id="tour_destination_id${inputDestination}">
    </div>
    `);

    $(".input_destination:last").after($element);
    init();
});

//supplier berdasarkan supplier
$(document).on("change", ".country", function () {
    var idx = $(this).attr("index");
    //dapetin data supplier berdasarkan country
    autocompleteSupplier('#supplier_' + idx, null, $(this).val());
    //cek ada visa endak
    var temp = $(this).select2("data")[0];
    if (temp.with_visa == 1) $(`#visa_${idx}`).prop("checked", true);
    else $(`#visa_${idx}`).prop("checked", false);
    $(`#visa_${idx}`).attr("with_visa", temp.with_visa);
})

$(document).on("click", ".close-btn", function () {
    $(this).closest(".input_destination").remove();
    countDestination--;
});

$(document).on("change", "#input_tour_days", function(){
    $("#input_tour_nights").val($(this).val()-1);
})

$(document).on("blur", "#input_tour_days, #input_tour_nights", function () {
    if (inputTourDays == $("#input_tour_days").val()) {
        return;
    }
    itineraryUpdateMode = true;
    inputTourDays = $("#input_tour_days").val();
    $(".tour_departure_date_start").each(function(index){
        let newDate = moment(addDaysToDate($(this).val(), inputTourDays)).format("YYYY-MM-DD");
        $(".tour_departure_date_end").eq(index).val(newDate);
    })
});

$(document).on("change", ".input-gambar", function(){
    let reader = new FileReader();
    let inputElement = $(this);

    reader.onload = function(e) {
        inputElement.prev().attr("src", e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
})

$(document).on("click", ".btn-delete-photo", function(){
    let inputFile = $(this).closest(".row");
    inputFile.find("input[type='file']").val('');
    inputFile.find("img").attr("src", uploadImageUrl);
})

$(document).on("click", ".btn-next", function () {
    $(window).scrollTop(0);
    if (tab == 1) {
        $("#pills-itinerary-tab").click();
    } else if (tab == 2) {
        $("#pills-price-tab").click();
    } else if (tab == 3) {
        $("#pills-term-tab").click();
    }
});
$(document).on("click", ".btn-back", function () {
    $(window).scrollTop(0);
    $("#pills-price-tab").click();
});

$(document).on("click", "#pills-itinerary-tab", function () {
    if (!itineraryUpdateMode) return;
    refreshItinerary();
});

function refreshItinerary() {
    itineraryUpdateMode = false;
    itineraryQuil = [];
    $("#input_itenerary").html("");
    for (let i = 1; i <= $("#input_tour_days").val(); i++) {
        $element = $(`
            <div id="row${i}" class="row-itinerary">
                <div class="row mb-3" >
                    <div class="col-1">
                        <label for="">Days</label>
                        <div class="form-control" id="day${i}">
                        ${i.toString().padStart(2, "0")}
                        </div>
                    </div>
                    <div class="col-10">
                        <label for="">Highlight</label>
                        <input type="text" class="form-control need-check" id="input-highligt${i}" name="input_tour_highlight[]" placeholder="Tour Highlight">
                    </div>
                    <div class="col-1 pt-4 text-end">
                        <button type="button" class="btn btn-outline-danger btn-sm w-100 mt-1 btn-trash" id="trash${i}" index="${i}">
                            <ion-icon name="trash-outline" style="font-size:14pt;padding-top:2px"></ion-icon>
                        </button>
                    </div>
                </div>
                <div class="row mb-3 justify-content-between">
                    <div class="col-3">
                        <label for="">Short Description</label>
                    </div>
                    <div class="col-5">
                        <div class="row">
                            <div class="col-2"><label for="">Meals</label></div>
                            <div class="col-4">
                                <input class="form-check-input" type="checkbox" name="input_tour_breakfast[]" id="breakfast${i}">
                                <label class="form-check-label" for="breakfast${i}">
                                    Breakfast
                                </label>
                            </div>
                            <div class="col-3">
                                <input class="form-check-input" type="checkbox" name="input_tour_lunch[]" id="lunch${i}">
                                <label class="form-check-label" for="lunch${i}">
                                    Lunch
                                </label>
                            </div>
                            <div class="col-3">
                                <input class="form-check-input" type="checkbox" name="input_tour_dinner[]" id="dinner${i}" >
                                <label class="form-check-label" for="dinner${i}">
                                    Dinner
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-floating mb-4">
                        <div id="detail-${i}" style="height: 125px"></div>
                    </div>
                </div>
                <hr>
            </div>

        `);
        $("#input_itenerary").append($element);
        itineraryQuil.push(
            new Quill(`#detail-${i}`, {
                theme: "snow",
            })
        );
        if(mode==2){
            var temp = JSON.parse(tour.tour_itinerary);
            $('#input-highligt'+i).val(temp[i-1].highlight);
            itineraryQuil[i-1].pasteHTML(temp[i-1].detail);
            if(temp[i-1].breakfast==1)$('#breakfast'+i).prop("checked",true);
            else $('#breakfast'+i).prop("checked",false);

            if(temp[i-1].lunch==1)$('#lunch'+i).prop("checked",true);
            else $('#lunch'+i).prop("checked",false);

            if(temp[i-1].dinner==1)$('#dinner'+i).prop("checked",true);
            else $('#dinner'+i).prop("checked",false);
        }
    }
}
$(document).on("change", ".radio-departure", function () {
    id = $(this).attr('id').split('_');
    id = id[id.length-1];
    type = ($(this).hasClass('inclusion')? 'inclusion' : $(this).hasClass('exclusion')? 'exclusion' : 'term')
    if($(this).val() == 'disabled'){
        $(`#select-tanggal-${type}${id}`).prop('disabled', true);
    }else{
        $(`#select-tanggal-${type}${id}`).prop('disabled', false);
    }
});

$(document).on("click", "#btn_add_inclusion", function () {

    $btnDel =`
        <div class="col-1">
            <button type="button" class="btn btn-outline-secondary float-end btn_del_inclusion" index="${countInclusion-1}" aria-label="Close">X</button>
        </div>
    `;

    if(countInclusion == 1)$btnDel = '';
    $element = $(`
        <div class="row inclusion mb-3" id="inclusion_${countInclusion-1}">
            <div class="row mb-3 justify-content-between align-items-center">
                <div class="col-3">
                    <label for="basicpill-namecard-input" class="form-label">Inclusion ${countInclusion}</label>
                </div>
                ${$btnDel}
            </div>
            <div class="row ">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-inclusion" index="${countInclusion - 1}" id="select-inclusion${countInclusion - 1}">
                            </select>
                        </div>
                        <div class="col-2 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-3 align-self-center">
                            <input class="form-check-input radio-departure inclusion" type="radio" name="input_tour_inclusion_${countInclusion - 1}[]" id="in_all_departure_${countInclusion-1}" checked value="disabled">
                            <label class="form-check-label" for="in_all_departure_${countInclusion-1}">
                                All Departure
                            </label>
                        </div>
                        <div class="col-1 align-self-center justify-content-center">
                            <input class="form-check-input float-end radio-departure inclusion" type="radio" name="input_tour_inclusion_${countInclusion - 1}[]" id="in_not_all_departure_${countInclusion-1}" value="enabled">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        <div id="inclusion-text-${countInclusion - 1}" style="height: 250px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="row h-100">
                        <div class="col h-100 align-self-center justify-content-center">
                            <select class="h-100 form-select select-tanggal select-tanggal-inclusion" index="${countInclusion - 1}" id="select-tanggal-inclusion${countInclusion - 1}" style="height: 300px;" name="input_tour_tanggal_inclusion[]" multiple disabled>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <small class="text-muted">*Pisahkan dengan enter</small>
        </div>
    `);
    $(".container-inclusion").append($element);
    autocompleteInclusion(`#select-inclusion${countInclusion - 1}`, null);
    RefreshTanggal(`#select-tanggal-inclusion${countInclusion - 1}`);
    inclusionQuil.push(
        new Quill(`#inclusion-text-${countInclusion - 1}`, {
            theme: "snow",
        })
    );
    if (countInclusion > 1) {
        //jika duplicate maka cek tanggal" yang sudah dipilih sebelumnya
        for (let i = countInclusion; i >= 2; i--) {
            var last = i - 2;
            var select = $(`#select-tanggal-inclusion${last}`).val();
            select.forEach(item => {
                $(`#select-tanggal-inclusion${countInclusion - 1} option[value='${item}']`).hide();
            });

        }
    }
    countInclusion++;
});

$(document).on("click", ".btn_del_inclusion", function(){
    index = $(this).attr('index');
    $(`#inclusion_${index}`).remove();
    inclusionQuil.splice(index, 1);
    if(mode==2&&tour.tour_inclusion[index]) tour.tour_inclusion.splice(index,1);
    // refreshInclusion();
})

function refreshInclusion() {
    $parentElement = $(".row.inclusion");
    index = 0;
    $parentElement.each(function(index){
        $(this).attr('id', 'inclusion_'+index);
        $(this).find('.form-label').text('Inclusion ' + (index+1));
        $(this).find('.select-inclusion').attr('id', 'select-inclusion'+index);
        $(this).find('.form-check-input').attr('name', 'input_tour_inclusion_' +index + '[]')
    })
}

$(document).on("change", ".select-inclusion", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr('index');
    var value = "<ul>";

    data_select.detail.forEach(item => {
        value += "<li>"+item.inclusion_detail+"</li>";
    });
    value +="</ul>";
    $('#select-inclusion'+index).attr("tour_terms_id",null);
    inclusionQuil[index].pasteHTML(value);
});

$(document).on("click", "#btn_add_exclusion", function () {
    $btnDel =`
        <div class="col-1">
            <button type="button" class="btn btn-outline-secondary float-end btn_del_exclusion" index="${countExclusion-1}" aria-label="Close">X</button>
        </div>
    `;

    $element = $(`
        <div class="row exclusion mb-3" id='exclusion_${countExclusion - 1}'>
            <div class="row">
                <label for="basicpill-namecard-input" class="form-label">Exclusion ${countExclusion}</label>
            </div>
            <div class="row">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-exclusion" index="${countExclusion - 1}" id="select-exclusion${countExclusion - 1}">
                            </select>
                        </div>
                        <div class="col-2 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-3 align-self-center">
                            <input class="form-check-input radio-departure exclusion" type="radio" name="input_tour_exclusion_${countExclusion - 1}[]" id="ex_all_departure_${countExclusion-1}" checked value="disabled">
                            <label class="form-check-label" for="ex_all_departure_${countExclusion-1}">
                                All Departure
                            </label>
                        </div>
                        <div class="col-1 align-self-center justify-content-center">
                            <input class="form-check-input float-end radio-departure exclusion" type="radio" name="input_tour_exclusion_${countExclusion - 1}[]" id="ex_not_all_departure_${countExclusion-1}" value="enabled">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="exclusion-text-${countExclusion - 1}" style="height: 250px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="row h-100">
                        <div class="col h-100 align-self-center justify-content-center">
                            <select class="h-100 form-select select-tanggal select-tanggal-exclusion" index="${countExclusion - 1}" id="select-tanggal-exclusion${countExclusion - 1}" name="input_tour_tanggal_exclusion[]" multiple disabled>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <small class="text-muted">*Pisahkan dengan enter</small>
        </div>
    `);

    $(".container-exclusion").append($element);
    autocompleteExclusion(`#select-exclusion${countExclusion - 1}`, null);
    RefreshTanggal(`#select-tanggal-exclusion${countExclusion - 1}`);
    exclusionQuil.push(
        new Quill(`#exclusion-text-${countExclusion - 1}`, {
            theme: "snow",
        })
    );

    if (countExclusion > 1) {
        //jika duplicate maka cek tanggal" yang sudah dipilih sebelumnya
        for (let i = countExclusion; i >= 2; i--) {
            var last = i - 2;
            var select = $(`#select-tanggal-exclusion${last}`).val();
            select.forEach(item => {
                $(`#select-tanggal-exclusion${countExclusion - 1} option[value='${item}']`).hide();
            });

        }
    }
    countExclusion++;
});

$(document).on("click", ".btn_del_exclusion", function(){
    index = $(this).attr('index');
    $(`#exclusion${index}`).remove();
    if(mode==2&&tour.tour_exclusion[index]) tour.tour_exclusion.splice(index,1);
})

function RefreshTanggal(id) {
    var select = $(id);
    var temp = select.val();//ambil data jika ada option yang select
    select.html("");//kosongkan
    $('.tour_departure_date_start').each(function(index){
        var date = new moment($(this).val());
        select.append(`<option value="${date.format('MMM DD')}">${date.format('MMM DD')}</option>`);
    })

    if (temp && temp.length > 0) {//cek jika ada data select lama
        temp.forEach(item => {
            $(`${id} option[value='${item}']`).attr("selected", "selected");
        });
    }
}


$(document).on("change", ".select-exclusion", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr('index');
    var value = "<ul>";

    data_select.detail.forEach(item => {
        value += "<li>"+item.inclusion_detail+"</li>";
    });
    value +="</ul>";
    exclusionQuil[index].pasteHTML(value);
});

$(document).on("click", "#btn_add_term_condition", function () {
    $element = $(`
        <div class="row term_condition mb-3">
            <div class="row">
                <label for="basicpill-namecard-input" class="form-label">Term & Condition ${countTermCondition}</label>
            </div>
            <div class="row">
                <div class="col-9 align-self-center">
                    <div class="row mb-3">
                        <div class="col-6">
                            <select class="form-select select-term" index="${countTermCondition - 1}" id="select-term${countTermCondition - 1}">
                            </select>
                        </div>
                        <div class="col-2 align-self-center">
                            <label for="" class="form-check-label">Apply to</label>
                        </div>
                        <div class="col-3 align-self-center">
                            <input class="form-check-input radio-departure term-condition" type="radio" name="input_tour_term_${countTermCondition - 1}[]" id="term_all_departure_${countTermCondition-1}" checked value="disabled">
                            <label class="form-check-label" for="term_all_departure_${countTermCondition-1}">
                                All Departure
                            </label>
                        </div>
                        <div class="col-1 align-self-center justify-content-center">
                            <input class="form-check-input float-end radio-departure term-condition" type="radio" name="input_tour_term_${countTermCondition - 1}[]" id="term_not_all_departure_${countTermCondition-1}" value="enabled">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="term-${countTermCondition}" style="height: 250px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="row h-100">
                        <div class="col h-100 align-self-center justify-content-center">
                            <select class="h-100 form-select select-tanggal select-tanggal-term" index="${countTermCondition - 1}" id="select-tanggal-term${countTermCondition - 1}" name="input_tour_tanggal_term_condition[]" multiple disabled>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);
    $(".container_term_condition").append($element);
    termQuil.push(
        new Quill(`#term-${countTermCondition}`, {
            theme: "snow",
        })
    );
    autocompleteTermCondition(`#select-term${countTermCondition - 1}`, null);
    RefreshTanggal(`#select-tanggal-term${countTermCondition - 1}`);
    if (countTermCondition > 1) {
        //jika duplicate maka cek tanggal" yang sudah dipilih sebelumnya
        for (let i = countTermCondition; i >= 2; i--) {
            var last = i - 2;
            var select = $(`#select-tanggal-term${last}`).val();
            select.forEach(item => {
                $(`#select-tanggal-term${countTermCondition - 1} option[value='${item}']`).hide();
            });

        }
    }
    countTermCondition++;
});

$(document).on("click", ".select-tanggal option", function () {
    //masih bug
    alert("test");
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr("index");
    var jumlah = countInclusion;
    var id = "select-tanggal-inclusion";
    if (data_select.term_jenis && data_select.term_jenis == 2) { jumlah = countExclusion; id = "select-tanggal-exclusion"; }
    if (data_select.terms_condition_id) { jumlah = countTermCondition; id = "select-tanggal-term"; }
    $(`.${id} option`).show();

    for (let i = 0; i < jumlah; i++) {
        if (i != index) {
            $(this).val().forEach(item => {
                $(`.${id}${i} option[value='${item}']`).hide();
            });
        }
    }
});

$(document).on("change", ".select-term", function () {
    var data_select = $(this).select2("data")[0];
    var index = $(this).attr("index");
    var value = "";

    termQuil[index].pasteHTML(data_select.terms_condition_detail);
    $(this).closest(".exclusion").find(".text-area").val(value);
});

$(document).on("click", ".btn-trash", function () {
    var index = parseInt($(this).attr("index"));
    if (parseInt($("#input_tour_days").val()) - 1 <= 0) {
        toastr.error("Minimal 1 Hari / 1 Itinerary", "Gagal Insert");
        return false;
    }
    $("#input_tour_days").val(parseInt($("#input_tour_days").val()) - 1);
    $("#input_tour_nights").val(parseInt($("#input_tour_nights").val()) - 1);
    $('#row' + index).remove();

    //reindexing
    for (let i = index + 1; i <= parseInt($("#input_tour_days").val()) + 1; i++) {
        var new_val = i - 1;
        $('#day' + i).html((new_val + "").padStart(2, "0"));
        $('#row' + i).attr("id", "row" + new_val);
        $('#trash' + i).attr("index", new_val);
    }
    //scroll ke terakhir
    window.location.href = "#input-highligt" + $("#input_tour_days").val();
});

$(document).on("click", ".btn-close-departure", function () {
    var index = $(this).index(".btn-close-departure");
    if ($(".departure_date").length - 1 <= 0) {
        toastr.error("Minimal 1 Depature", "Gagal Insert");
        return false;
    }
    $(this).closest(".departure_date").remove();
    if(mode==2&&tour.tour_validity[index]) tour.tour_validity.splice(index,1);
    refreshDeparture();
});

$(document).on("change", ".check_addon_price", function () {
    $element = $(this).closest(".departure_date").find(".add-on-price");
    if (!$(this).is(":checked")) {
        $element
            .find(".add_on_price_input, .add_on_disc_input, .add_on_type")
            .prop("disabled", true);
    } else {
        $element
            .find(".add_on_price_input, .add_on_disc_input, .add_on_type")
            .prop("disabled", false);
    }
});

$(document).on("change", ".check_extra_bed", function () {
    $element = $(this).closest(".departure_date").find(".extra-bed");
    if (!$(this).is(":checked")) {
        $element
            .find(".extra_bed_price_input, .extra_bed_disc_input")
            .prop("disabled", true);
    } else {
        $element
            .find(".extra_bed_price_input, .extra_bed_disc_input")
            .prop("disabled", false);
    }
});

$(document).on("click", ".btn-tambah-hari", function () {
    $("#input_tour_days").val(parseInt($("#input_tour_days").val()) + 1);
    $("#input_tour_nights").val(parseInt($("#input_tour_nights").val()) + 1);
    refreshItinerary();
    window.location.href = "#input-highligt" + $("#input_tour_days").val();
});

$(document).on("click", ".btn_add_on", function () {
    $element = $(this).closest(".row").prev(".add-on-price").clone();
    $element.find(".add_on_disc_input, .add_on_price_input, .add_on_total, .add_on_type").val('');
    $element.find(".btn_del_add_on").prop("hidden", false);
    $element.find(".add_on_disc_input").val("0");
    $(this).closest(".row").prev(".add-on-price").after($element);
});

$(document).on("click", ".btn_add_on_duplicate", function () {
    $element = $(this).closest('.departure_date');
    $cloneElement = $element.clone();
    $cloneElement.find('.is-invalid').removeClass('is-invalid')
    $element.after($cloneElement);
    $cloneElement.focus()
    refreshDeparture();
    countDeparture++;
    for (let i = 0; i < countInclusion; i++) {
        RefreshTanggal('#select-tanggal-inclusion' + i);
    }
    for (let i = 0; i < countExclusion; i++) {
        RefreshTanggal('#select-tanggal-exclusion' + i);
    }
    for (let i = 0; i < countTermCondition; i++) {
        RefreshTanggal('#select-tanggal-term' + i);
    }
});

$(document).on("click", "#btn-tambah-jadwal", function () {
    let today = getCurrentDate();
    let later = moment(addDaysToDate(today, inputTourDays)).format('YYYY-MM-DD');
    $('.container-depature').append(`
        <div class="departure_date mt-0" >
            <div class="row">
                <div class="col-11">
                    <h5 class="my-0  departure_title" style="color:#43006a!important"><i class="mdi mdi-bullseye-arrow me-3"></i>Departure Date 1</h5>
                </div>
                <div class="col-1 text-end">
                    <button type="button" class="btn btn-outline-secondary btn-close-departure" aria-label="Close">X</button>
                </div>
            </div>
            <div class="mt-3">
                <div class="row header-price">
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="basicpill-vatno-input" class="form-label">Date</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input class="form-control tour_departure_date_start" type="date" value="${today}" name="tour_departure_date_start[]">
                                </div>
                                <div class="col-lg-6">
                                    <input class="form-control tour_departure_date_end" type="date" value="${later}"  name="tour_departure_date_end[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="basicpill-vatno-input" class="form-label">Deposit Amount</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal deposit" name="tour_deposit_amount[]" placeholder="" step="10000">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Tour Price</b></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Selling Price</b></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Discount</b></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Publish Price</b></label>
                        </div>
                    </div>
                </div>
                <div class="row row-price" price_name="adult_single_sharing_price">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Adult Single Sharing" name="adult_single_sharing[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal price" name="adult_single_sharing_price[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal disc" value="0" name="adult_single_sharing_disc[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="publishSingle" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-price" price_name="adult_twin">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Adult Twin/Triple Sharing" name="adult_twin[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal price" id="adult_twin_price[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal disc" value="0" name="adult_twin_disc[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="publishTwin" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-price" price_name="child_with_bed">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Child With Bed" name="child_with_bed[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal price" name="child_with_bed_price[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal disc" value="0" name="child_with_bed_disc[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="publishChildBed" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-price" price_name="child_no_bed">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Child No Bed" name="child_no_bed[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal price" name="child_no_bed_price[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal disc" value="0" name="child_no_bed_disc[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="publishChild" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-price" price_name="infant">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Infant" name="infant[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control need-check nominal price" name="infant_price[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal disc" value="0" name="infat_disc[]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="publishInfant" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-check form-switch form-switch-md mb-3" dir="ltr">
                            <label class="form-check-label" for="customSwitchsizemd">Extra Bed</label>
                            <input type="checkbox" class="form-check-input check_extra_bed">
                        </div>
                    </div>
                </div>
                <div class="row extra-bed row-price" price_name="extra_bed">
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Guest/Room Type</label>
                            <input type="text" class="form-control nominal" value="Extra Bed" name="extra_bed[]" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input id="priceExtraBed" type="text" class="form-control need-check extra_bed_price_input nominal price" name="extra_bed_price[]" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Disc (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input id="discExtraBed" type="text" class="form-control extra_bed_disc_input disc" value="0" nominal disc" name="extra_bed_disc[]" placeholder="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Price</label>
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control nominal total" id="" placeholder=""  disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-check form-switch form-switch-md mb-3" dir="ltr">
                            <label class="form-check-label" for="customSwitchsizemd">Add on Price</label>
                            <input type="checkbox" class="form-check-input check_addon_price">
                        </div>
                    </div>
                </div>
                <div class="row add-on-price">
                    <div class="col-12">
                        <div class="row add-on-price-detail">
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Guest/Room Type</label>
                                    <input type="text" class="form-control need-check add_on_type" name="add_on_room_type[]" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Price</label>
                                    <div class="input-group">
                                        <div class="input-group-text">Rp</div>
                                        <input type="text" class="form-control need-check add_on_price_input nominal price" name="add_on_price[]" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Disc (Rp)</label>
                                    <div class="input-group">
                                        <div class="input-group-text">Rp</div>
                                        <input type="text" class="form-control add_on_disc_input nominal disc " name="add_on_disc[]" placeholder="" value="0" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="" class="form-label float-start">Price</label>
                                    <button type="button" class="btn-close float-end btn_del_add_on" hidden></button>
                                    <div class="input-group">
                                        <div class="input-group-text">Rp</div>
                                        <input type="text" class="form-control add_on_total nominal total" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ">
                        <button type="button" class="btn btn-light btn-rounded float-start btn_add_on">Add On Price +</button>
                        <button type="button" class="btn btn-light btn-rounded float-end btn_add_on_duplicate">Duplicate to Next</button>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    `);
    refreshDeparture();
    countDeparture++;
    for (let i = 0; i < countInclusion; i++) {
        RefreshTanggal('#select-tanggal-inclusion' + i);
    }
    for (let i = 0; i < countExclusion; i++) {
        RefreshTanggal('#select-tanggal-exclusion' + i);
    }
    for (let i = 0; i < countTermCondition; i++) {
        RefreshTanggal('#select-tanggal-term' + i);
    }
});

$(document).on("change", ".tour_departure_date_start", function () {
    for (let i = 0; i < countInclusion; i++) {
        RefreshTanggal('#select-tanggal-inclusion' + i);
    }
    for (let i = 0; i < countExclusion; i++) {
        RefreshTanggal('#select-tanggal-exclusion' + i);
    }
    for (let i = 0; i < countTermCondition; i++) {
        RefreshTanggal('#select-tanggal-term' + i);
    }
});

$(document).on("click", ".btn_del_add_on", function () {
    $(this).closest(".row.add-on-price").remove();
});

$(document).on("keyup", ".price", function () {
    var pr = convertToAngka($(this).val());
    var disc = convertToAngka($(this).closest(".row").find('.disc').val());
    $(this).closest(".row").find('.total').val(formatRupiah((pr - disc) + ""));
});

$(document).on("keyup", ".disc", function () {
    var disc = convertToAngka($(this).val());
    var pr = convertToAngka($(this).closest(".row").find('.price').val());
    $(this).closest(".row").find('.total').val(formatRupiah((pr - disc) + ""));
});

function refreshDeparture() {
    $(".departure_date").each(function (index) {
        $(this)
            .find(".departure_title")
            .text(`Departure Date ${index + 1}`);
        $(this)
            .find(".add_on_type")
            .attr("name", `add_on_room_type_${index}[]`);
        $(this)
            .find(".add_on_price_input")
            .attr("name", `add_on_price_${index}[]`);
        $(this)
            .find(".add_on_disc_input")
            .attr("name", `add_on_disc_${index}[]`);
    });
}

$(document).on("keyup", ".nominal", function (e) {
    //setiap berbau nominal or rupiah dalam input ataupun dalam table harus ada Rp. dan sperator
    $(this).val(formatRupiah(convertToAngka($(this).val()) + ""), "Rp.");
});

$(document).on("click", "#btn_submit", function(){
    $("#tour_form").submit();
})

function isValid()
{
    let check = true;
    let toastSelect = false;
    let $tab = '';
    let $tab2 = '';
    let $element;
    $('.need-check').removeClass('is-invalid');
    $('.need-check').each(function()
    {
        isEmpty = ($(this).val() == '' || $(this).val() == 0) && !$(this).prop('disabled');
        if(isEmpty)
        {
            check = false;
            if($(this).is('input'))
            {
                $(this).toggleClass('is-invalid', isEmpty);
            }
            else
            {
                toastSelect = true;
            }

            if($tab == '')
            {
                $tab = $(`#${$(this).closest('.tab-pane').attr('aria-labelledby')}`);
                $element = $(this);
            }
        }
    })

    $(".select-inclusion").each(function(){
        if($(this).select2("data").length == 0){
            check = false;
            toastr.error('Ada inclusion yang belum terpilih!');
            return false;
        }
    })

    for (let i = 0; i < inclusionQuil.length; i++)
    {
        const element = inclusionQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check = false;
            toastr.error('Ada Inclusion Kosong!');
            break;
        }
    }

    $(".select-exclusion").each(function(){
        if($(this).select2("data").length == 0){
            check = false;
            toastr.error('Ada Exclusion yang belum terpilih!');
            return false;
        }
    })

    for (let i = 0; i < exclusionQuil.length; i++)
    {
        const element = exclusionQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check = false;
            toastr.error('Ada Exclusion Kosong!');
            break;
        }
    }

    $(".select-term").each(function(){
        if($(this).select2("data").length == 0){
            check = false;
            toastr.error('Ada Term & Condition yang belum terpilih!');
            return false;
        }
    })

    for (let i = 0; i < termQuil.length; i++)
    {
        const element = termQuil[i].getContents();
        const isEmpty = element.ops.length === 1 && element.ops[0].insert === '\n';
        if(isEmpty)
        {
            check = false;
            toastr.error('Ada Term & Condition Kosong!');
            break;
        }
    }

    if(!check)
    {
        $tab.click();
        $('html, body').animate({
            scrollTop: $element.offset().top
        }, 10);
        $element.focus();
        if(toastSelect)
        {
            toastr.error('Mohon pilih data');
        }
        toastr.error('Harap isi semua field', "Gagal Insert");
    }
    return check;
}

$(document).on("change", ".visa", function (e) {
    $(this).prop("checked",!$(this).is(":checked"))
});

$(document).on("submit", "#tour_form", function (e) {
    e.preventDefault();
    submit(0);
});

$(document).on("click", "#btn-safe-draf", function (e) {
    console.log("test");
    submit(1);
});

function submit(draf=0) {

    var status=1;
    if(draf==0&&$("#inlineRadio1").is(":checked")&&!isValid())
    {
        return;
    }
    if($("#inlineRadio2").is(":checked"))status=4;
    if(draf==1)status=3;
    tour_destination = [];
    itinerary = [];

    // Insert Tour Destination
    $("select[name='input_tour_destination[]']").each(function (index) {
        var temp = $(this).select2("data")[0];
        if(temp){
            tour_destination.push({
                country_name: temp.name,
                country_id: temp.id,
                is_visa: temp.with_visa == 1 ? true : false
            });
        }
    });

    $("select[name='input_tour_supplier[]']").each(function (index) {
       var temp = $(this).select2("data")[0];
        if(temp){
            tour_destination[index].supplier_nama = temp.name;
            tour_destination[index].supplier_id = temp.id;
        }
        /*else{
            tour_destination.push({
                supplier_nama:"",
                supplier_id: temp.id,
                offline_code :""
            });
        }*/
    });

    $("input[name='input_tour_offline_code[]']").each(function (index) {
        if(tour_destination[index])tour_destination[index].offline_code = $(this).val();
        else{
           /* tour_destination.push({
                offline_code :$(this).val()
            });*/
        }
    });

    //get id destinasi
    if(mode==2){
        $(".tour_destination_id").each(function (index) {
            console.log($(this).val())
            tour_destination[index].tour_destination_id = $(this).val();
        });
    }

    // Insert Itinerary
    var itinerary = [];
    $(".row-itinerary").each(function (index) {
        var idx = index+1;
        itinerary.push({
            highlight: $(`#input-highligt`+idx).val(),
            breakfast:$(`#breakfast`+idx).is(":checked"),
            lunch:$(`#lunch`+idx).is(":checked"),
            dinner:$(`#dinner`+idx).is(":checked"),
            detail: itineraryQuil[index].root.innerHTML
        });
    });

    // insert Validity Price
    var validity = [];
    $(".departure_date").each(function (index) {
        var row = $(this);
        validity.push({
            tour_start_date: row.find('.tour_departure_date_start').val(),
            tour_end_date: row.find('.tour_departure_date_end').val(),
            tour_departure_deposit: convertToAngka(row.find('.deposit').val()),
            price:[],
         });

         if(mode==2&&tour.tour_validity[index])validity[index].tour_departure_id = tour.tour_validity[index].tour_departure_id;

         row.find(`.row-price`).each(function (idx) {
            validity[index].price.push({
                "tour_departure_detail_name":$(this).attr("price_name"),
                "tour_departure_detail_price":convertToAngka($(this).find('.price').val()),
                "tour_departure_detail_disc":convertToAngka($(this).find('.disc').val()),
                "tour_departure_detail_total":convertToAngka($(this).find('.total').val()),
                "tour_departure_detail_jenis":1 //default_price
            });
            if(mode==2&&tour.tour_validity[index]&&tour.tour_validity[index].default_price[idx]) validity[index].price[idx].tour_departure_detail_id =  tour.tour_validity[index].default_price[idx].tour_departure_detail_id;
        });

        row.find(`.add-on-price-detail`).each(function (idx) {
            validity[index].price.push({
                "tour_departure_detail_name":$(this).find(".add_on_type").val(),
                "tour_departure_detail_price":convertToAngka($(this).find('.price').val()),
                "tour_departure_detail_disc":convertToAngka($(this).find('.disc').val()),
                "tour_departure_detail_total":convertToAngka($(this).find('.total').val()),
                "tour_departure_detail_jenis":2 //add on
            });
            if(mode==2&&tour.tour_validity[index]&&tour.tour_validity[index].add_on[idx]) validity[index].price[idx].tour_departure_detail_id =  tour.tour_validity[index].add_on[idx].tour_departure_detail_id;
        });

    });

    // Insert Term & Condition
    var term_condition = [],inclusion = [], exclusion = [];
    $(`.select-tanggal-inclusion`).each(function (index) {
        inclusion.push({tour_terms_date: $(this).val(),tour_terms_jenis:1});
        if(mode==2&&tour.tour_inclusion[index])inclusion[index].tour_terms_id = tour.tour_inclusion[index].tour_terms_id;
    });
    $(`.select-tanggal-exclusion`).each(function (index) {
        exclusion.push({tour_terms_date: $(this).val(),tour_terms_jenis:2});
        if(mode==2&&tour.tour_exclusion[index])exclusion[index].tour_terms_id = tour.tour_exclusion[index].tour_terms_id;
    });
    $(`.select-tanggal-term`).each(function (index) {
        term_condition.push({tour_terms_date: $(this).val(),tour_terms_jenis:3});
        if(mode==2&&tour.tour_term[index])term_condition[index].tour_terms_id = tour.tour_term[index].tour_terms_id;
    });

    $(`.select-inclusion`).each(function (index) {
        inclusion[index].id = $(this).val();
    });
    $(`.select-exclusion`).each(function (index) {
        exclusion[index].id = $(this).val();
    });
    $(`.select-term`).each(function (index) {
        term_condition[index].id = $(this).val();
    });

    for (let i = 0; i < termQuil.length; i++) {
        const item = termQuil[i];
        term_condition[i].tour_terms_value = item.root.innerHTML;
    }

    for (let i = 0; i < inclusionQuil.length; i++) {
        const item = inclusionQuil[i];
        inclusion[i].tour_terms_value = item.root.innerHTML;
    }

    for (let i = 0; i < exclusionQuil.length; i++) {
        const item = exclusionQuil[i];
        exclusion[i].tour_terms_value = item.root.innerHTML;
    }

    data = {
        tour_jenis:1,
        tour_name: $("#input_tour_name").val(),
        tour_category: $("#input_tour_category").val(),
        tour_type: $("#input_tour_type").val(),
        tour_destination: JSON.stringify(tour_destination),
        tour_days: $("#input_tour_days").val(),
        tour_nights: $("#input_tour_nights").val(),
        tour_air_ticket: $("#tour_air_ticket").is(":checked"),
        tour_hotel: $("#tour_hotel").is(":checked"),
        tour_meal: $("#tour_meal").is(":checked"),
        tour_tour: $("#tour_tour").is(":checked"),
        tour_land_trans: $("#tour_land_trans").is(":checked"),
        tour_water_trans: $("#tour_water_trans").is(":checked"),
        tour_visa: $("#tour_visa").is(":checked"),
        tour_insurance: $("#tour_insurance").is(":checked"),
        tour_date_start: $("#input_tour_date_start").val(),
        tour_date_end: $("#input_tour_date_end").val(),
        keyword_id: JSON.stringify($("select[name='input_tour_keyword[]']").val()),
        tour_itinerary: JSON.stringify(itinerary),
        tour_inclusion: JSON.stringify(inclusion),
        tour_exclusion: JSON.stringify(exclusion),
        tour_term_condition: JSON.stringify(term_condition),
        displayer_id: $('#select_displayer').val(),
        status: status,
        validity: JSON.stringify(validity),
        _token : token
    };

    //mode edit
    var url = "insertFixDepature";
    if(mode==2){
        url = "updateFixDepature";
        data.tour_id = tour.tour_id;
        data.displayer_detail_id = tour.displayer_detail_id;
    }

    //convert data -> form data
    const fd = new FormData();
    for (const [key, value] of Object.entries(data)) {
        fd.append(key, value);
    }

    fd.append("tour_cover", $('#input_file_cover')[0].files[0]=="undefined"?null:$('#input_file_cover')[0].files[0]);
    fd.append("tour_banner", $('#input_file_banner')[0].files[0]=="undefined"?null:$('#input_file_cover')[0].files[0]);
    $.each($("#input_file_galery")[0].files, function(i, file) {
        fd.append('tour_galery[]', file);
    });

    //add loading
    $('#btn_submit').append(`
        <div class="spinner-border text-light ms-2 " id="" role="status" style="width: 10px;
        height: 10px;
        font-size: 10px;
        color: white!important;">
            <span class="visually-hidden">Loading...</span>
        </div>
    `);

    $('#btn_submit').attr("disabled","disabled");
    $.ajax({
        url: "/product/tour/fix_departure/"+url,
        method: "post",
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        data: fd,
        success: function () {
            $('#btn_submit').removeAttr("disabled");
            $('#btn_submit').html("Submit Data");
            localStorage.setItem("success","Berhasil Edit Data!");
            window.location.href = "/product/tour/fix_departure";
        },
        error: function (e) {
            console.log(e);
            $('#btn_submit').html("Submit Data");
            $('#btn_submit').removeAttr("disabled");
            toastr.error("", "Terjadi Kesalahan");
        },
    });
}
window.onmousedown = function (e) {
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();

        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');

        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
}
