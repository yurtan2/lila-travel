var mode=1; //1= insert, 2= delete

get_data();
autocompleteCountry('#country','#modalInsert');
autocompleteCountry('#country_filter');

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteVisa",
        method:"post",
        data:{
            visa_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Visa sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var country = $('#country').val();
    var nama = $('#nama').val();
    var detail = quill.root.innerHTML;
    var valid = 1;
    var url = "insertVisa";
    var data = {
        nama:nama,
        country:country,
        dokumen:[],
        detail:detail,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(country==""||country==null){
        $('#country').closest('.form-group').addClass("has-danger");
        valid=0;
    }

    if(detail==""||detail==null){
        $('#syarat').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    $('.dokumen').each(function(e){
        if($(this).is(':checked')) {
            data.dokumen.push({
                document_id:$(this).val(),
                document_name:$(this).attr("document_name")
            });
        }
    });
    console.log(data.dokumen);
    if(mode==2){
        url = "editVisa";
        data.visa_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Visa sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-document").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_visa",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                country:$('#country_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view'>View</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "hotels_name"},
            { data: "location"},
            { data: "starting_price"},
            { data: "rooms_count"},
            { data: "status"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-document").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_dokumen(data=null) {
    $.ajax({
        url:"/master/get_data_document",
        method:"get",
        success:function(e){
            $('#container_dokumen').html("");
            e = JSON.parse(e);
            e["data"].forEach(item => {
                 $('#container_dokumen').append(`<input type="checkbox" class="form-check-input me-1 dokumen" value="${item.document_id}" document_name="${item.document_name}" style="font-size:12pt" name="document">
                 <label class="form-check-label me-4 mt-1" style="font-size:9pt">${item.document_name}</>`);
            });
            if(data!=null){
                data.forEach(item => {
                    $('.dokumen').each(function(){
                        if($(this).val()==item.document_id){
                            $(this).prop("checked",true);
                        }
                    });
                });

            }
        }
    });
}
