autocompleteCountry('#country');


$(function(){
    $(".btn-tambah-room").click();
})

var quill = new Quill('#detail', {
    theme: 'snow'
});

var quillRoom = [];
let hotelFilesArray = [];
let roomsFilesArray = [];
var pillTab = 1;

function newRoom() {
    count = quillRoom.length;
    generalService = ``;
    bathroomFacilities = ``;
    roomFacilites = ``;
    for (let i = 0; i < 10; i++) {
        generalService += `
        <label class="input-group py-1">
            <input type="checkbox" class="h5 me-2" name="" id="">
            <div class="h5">Apa sih</div>
        </label>
        `
        roomFacilites += `
        <label class="input-group py-1">
            <input type="checkbox" class="h5 me-2" name="" id="">
            <div class="h5">Apa sih</div>
        </label>
        `
    }
    for (let i = 0; i < 2; i++) {
        bathroomFacilities += `
        <label class="input-group py-1">
            <input type="checkbox" class="h5 me-2" name="" id="">
            <div class="h5">Apa sih</div>
        </label>
        `
    }
    hidden = (count == 0? "hidden": '');
    $newElement = $(`
    <div class="list_room mt-0" >
        <input type="hidden" value="${count}" class="counter-hidden">
        <div class="row">
            <div class="col-11">
                <h5 class="my-0 room_title" style="color:#43006a!important"></h5>
            </div>
            <div class="col-1 text-end">
                <button type="button" class="btn btn-outline-secondary btn-close-room" aria-label="Close" ${hidden}>X</button>
            </div>
        </div>
        <div class="mt-3">
            <div class="row header-price mb-3">
                <div class="col-lg-6">
                    <div class="">
                        <label for="basicpill-vatno-input" class="form-label">Room Name</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Enter Room's Name">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="basicpill-vatno-input" class="form-label">Price</label>
                        <div class="input-group">
                            <div class="input-group-text">Rp</div>
                            <input type="text" class="form-control need-check nominal" name="" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Room Type</label>
                        <select class="form-select" name="" id=""></select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Meal Type</label>
                        <select class="form-select" name="" id=""></select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Max Adult</label>
                        <input type="text" class="form-control" name="" id="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Max Child</label>
                        <input type="text" class="form-control" name="" id="">
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12">
                    <div class="detail" id="detail-${count}" style="height: 250px"></div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-2">
                    <div class="h5 float-start">Cover</div>
                    <button type="button" class="btn btn-close btn-delete-photo float-end" hidden></button>
                    <label class="float-start">
                        <img src="${uploadImageUrl}" class="w-100">
                        <input type="file" class="upload_single" name="upload_cover" id="" style="display: none;">
                    </label>
                </div>
                <div class="col-lg-2">
                    <div class="h5">Banner</div>
                    <label class="float-start">
                        <img src="${uploadImageUrl}" class="w-100">
                        <input type="file" class="upload_multiple_2" name="upload_banner" id="" accept="image/*,video/*" multiple style="display: none;">
                    </label>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4">
                    <div class="form-label ms-3">General Service</div>
                    <div class="card overflow-auto p-3" style="max-height: 200px;">
                        ${generalService}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-label ms-3">Bathroom Facilities</div>
                    <div class="card overflow-auto p-3" style="max-height: 200px;">
                        ${bathroomFacilities}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-label ms-3">Room Facilities</div>
                    <div class="card overflow-auto p-3" style="max-height: 200px;">
                        ${roomFacilites}
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
    `);
    $("#list_room").append($newElement)
    quillRoom.push(new Quill(`#detail-${count}`, {
        theme: 'snow'
    }))
}

function refreshRoom() {
    $(".list_room").each(function(index){
        $(this).find(".room_title").html(`
            <i class="mdi mdi-bullseye-arrow me-3"></i>Rooms ${index+1}
        `);
    })
}

$(document).on("change", ".upload_single", function(){
    let reader = new FileReader();
    let inputElement = $(this);

    reader.onload = function(e) {
        inputElement.prev().attr("src", e.target.result);
    }
    inputElement.parent().prev().prop("hidden", false);
    reader.readAsDataURL(this.files[0]);
})

$(document).on("change", ".upload_multiple_1", function(){
    let reader = new FileReader();
    let inputElement = $(this);
    let file = this.files[0];

    // Add the file to the hotelFilesArray
    hotelFilesArray.push(file);

    reader.onload = function(e) {
        let newElement = $(`
        <div class="col-lg-2">
            <button type="button" class="h5 btn btn-close btn-delete-photos float-end"></button>
            <img src="${e.target.result}" class="w-100 float-start">
        </div>
        `);

        newElement.find('.btn-delete-photos').on('click', function() {
            // Find the index of the file and remove it from hotelFilesArray
            let index = hotelFilesArray.indexOf(file);
            if(index > -1) {
                hotelFilesArray.splice(index, 1);
            }

            // Remove the preview element
            newElement.remove();
        });

        inputElement.closest(".col-lg-2").after(newElement);
    }

    reader.readAsDataURL(file);
});

$(document).on("change", ".upload_multiple_2", function(){
    let reader = new FileReader();
    let inputElement = $(this);
    let file = this.files[0];

    // Add the file to the roomsFilesArray
    roomsFilesArray.push(file);

    reader.onload = function(e) {
        let newElement = $(`
        <div class="col-lg-2">
            <button type="button" class="h5 btn btn-close btn-delete-photos float-end"></button>
            <img src="${e.target.result}" class="w-100 float-start">
        </div>
        `);

        newElement.find('.btn-delete-photos').on('click', function() {
            // Find the index of the file and remove it from roomsFilesArray
            let index = roomsFilesArray.indexOf(file);
            if(index > -1) {
                roomsFilesArray.splice(index, 1);
            }

            // Remove the preview element
            newElement.remove();
        });

        inputElement.closest(".col-lg-2").after(newElement);
    }

    reader.readAsDataURL(file);
});

$(document).on("click", ".btn-delete-photo", function(){
    $(this).prop("hidden", true);
    let inputFile = $(this).parent();
    inputFile.find("input[type='file']").val('');
    inputFile.find("img").attr("src", uploadImageUrl);
})

$(document).on("click", ".btn-next", function (){
    if(pillTab == 1){
        $('#pills-rooms-tab').click();
    }else if(pillTab == 2){
        $('#pills-setting-tab').click();
    }
})

$(document).on("click", ".btn-tambah-room", function(){
    newRoom();
    refreshRoom();
})

$(document).on("click", ".btn-close-room", function(){
    quillRoom[$(this).closest(".list_room").find('.counter-hidden').val()] = null;
    $(this).closest('.list_room').remove();
    refreshRoom();
})

$(document).on("click", "#pills-general-tab, #pills-rooms-tab, #pills-setting-tab", function(){
    if($(this).attr('id') == 'pills-general-tab'){
        pillTab = 1;
    }else if($(this).attr('id') == 'pills-rooms-tab'){
        pillTab = 2;
    }else{
        pillTab = 3;
    }
    $("html, body").scrollTop(0);
})

$(document).on("keyup", ".nominal", function(){
    $(this).val(formatRupiah($(this).val()));
})
