var list_pic = [];
function refreshPIC() {
    $('#body-pic').html("");
    console.log(list_pic);
    list_pic.forEach((item,index) => {
        $('#body-pic').append(`
            <tr>
                <td class="td-name-${index}">${item.corporate_name}</td>
                <td class="td-name-${index}">${item.pic_jabatan}</td>
                <td class="td-name-${index} text-center"><input type="radio" class="default-corporate" id="rd-default-${index}" index="${index}" name="default"></td>
                <td class="text-center"><button class="btn btn-outline-danger btn-sm me-2 btn-delete-pic" index="${index}" type="button">Delete</button>
                </td>
            </tr>
        `);
        if(item.default==1)$(`#rd-default-${index}`).prop("checked",true);
        $('.edit-mode').hide();
    });
}

autocompleteCorporate('#pic_name');
autocompletePosition('#pic_jabatan');


$(document).on("click",".btn-add-pic",function(){
    var valid=1;

    $('.has-danger').removeClass("has-danger");
    $('.input-pic .pic-check').each(function(){
        if($(this).select2("data").length == 0){
            $(this).closest('.form-group').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }


    list_pic.push({
        "corporate_name":$('#pic_name').select2("data")[0]["text"],
        "corporate_id":$('#pic_name').select2("data")[0]["id"],
        "jabatan_id":$('#pic_jabatan').select2("data")[0]["id"],
        "pic_jabatan":$('#pic_jabatan').select2('data')[0]["text"],
        "default":0
    });

    $('.input-pic .pic-check').val("");
    $('#pic_name').empty().trigger("change");
    $('#pic_jabatan').empty().trigger("change");

    refreshPIC();
});

$(document).on("click",".btn-delete-pic",function(){
    var idx = $(this).attr("index");
    console.log(idx);
    list_pic.splice(idx,1);
    refreshPIC();
});

$(document).on("click",".default-corporate",function(){
    var idx = $(this).attr("index");
    list_pic.forEach((item,index) => {
        list_pic[index].default=0;
        if(index==idx) list_pic[idx].default=1;
    });
});

function calculateAge(birthDateString) {
    const birthDate = new Date(birthDateString);
    const today = new Date();

    // Get days of each month
    const daysInMonth = [31, (today.getFullYear() % 4 === 0 ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let years = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    let days = today.getDate() - birthDate.getDate();

    // Handle negative days (borrow days from the month)
    if (days < 0) {
        months--;
        days += daysInMonth[(today.getMonth() - 1 + 12) % 12]; // Get last month days count
    }

    // Handle negative months (borrow months from the year)
    if (months < 0) {
        years--;
        months += 12;
    }

    return `${years} Tahun, ${months} Bulan, ${days} Hari`;
}

function isValid() {
    let cek = true;
    let element = '';
    $(".need-check").each(function(){
        let isEmpty = $(this).val() == '';
        $(this).toggleClass('is-invalid', isEmpty);
        if(isEmpty && cek){
            cek = false;
            element = $(this);
        }
    })

    if(element != ''){
        $('html, body').animate({
            scrollTop: element.parent().offset().top
        }, 1000);
    }

    return cek;
}

get_data();
var mode=1; //1= insert, 2= delete
var file_name = "";

$(document).on("change", "#customer_tanggal_lahir", function(){
    $('#umur').val(calculateAge($(this).val()));
})

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val(null).trigger("change");
    $('#filter_passport_status').val(0).trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Customer");
    $('.add').val("");
    $('input[type="file"]').val(null).trigger("change");
    $('.file').val(null).trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
    list_pic = [];
    refreshPIC();
    $("#customer_tanggal_lahir").attr('max',today);
    $("#customer_tanggal_lahir").val(today);
    $("#umur").val('0 Tahun');
    $("#inlineRadio1").prop("checked", true);
    $(".date_passport").val(today);
    $(".date_passport").attr('max', today);
    $(".pic-check").empty().trigger('change');
})

$(document).on("change", ".pic-check", function(){
    let isEmpty = false;
    $(".pic-check").each(function(){
        if($(this).val() == '' || $(this).val() == null)isEmpty = true;
    })

    if(!isEmpty){
        $(".btn-add-pic").click();
    }
})

$(document).on('hidden.bs.modal', '#modalInsert', function(e){
    $('.is-invalid').removeClass('is-invalid');
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Customer");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    console.log(data.list_pic);
    if(data.customer_gender=="Male")$('#inlineRadio1').prop("checked",true);
    else if(data.customer_gender=="Female")$('#inlineRadio2').prop("checked",true);
    if(data.customer_jenis=="Indonesia")$('#jenis_customer_indo').prop("checked",true);
    else if(data.customer_jenis=="Other")$('#jenis_customer_other').prop("checked",true);
    $('#customer_first_name').val(data.customer_first_name);
    $('#customer_last_name').val(data.customer_last_name);
    $('#customer_phone').val(data.customer_nomor);
    $('#customer_nik').val(data.customer_nik);
    $('#customer_tanggal_lahir').val(data.customer_tanggal_lahir).trigger("change");
    $('#umur').val(data.customer_umur+" Tahun");
    $('#customer_alamat').val(data.customer_alamat);
    $('#customer_religion').val(data.customer_religion);
    $('#customer_email').val(data.customer_email);
    $('#customer_food').val(data.customer_referensi_makanan);
    $('#customer_passport_number').val(data.customer_passport_number);
    $('#customer_poi').val(data.customer_poi).trigger("change");
    $('#customer_doi').val(data.customer_doi).trigger("change");
    $('#customer_doe').val(data.customer_doe).trigger("change");
    $('input[type="file"]').val(null);
    list_pic = [];
    data.list_pic.forEach(item => {
        var defaults=0;
        if(data.corporate_pic_id==item.corporate_pic_id)defaults=1;
        list_pic.push({
            "corporate_name":item.corporate_name,
            "corporate_id":item.corporate_id,
            "jabatan_id":item.corporate_position_id,
            "pic_jabatan":item.corporate_position_name,
            "corporate_pic_id":item.corporate_pic_id,
            "default":defaults
        });
    });
    refreshPIC();
    $('#modalInsert').attr("edit_id",data.customer_id);
    $('#modalInsert').modal("show");
})

$(document).on("keyup","#customer_passport_number",function(){
    $(this).val($(this).val().toUpperCase());
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.customer_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click",".btn-view",function(){
    $('#title').html("Detail Customer");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#view_name').html(data.customer_first_name+" "+data.customer_last_name);
    $('#view_phone').html(data.customer_nomor_text);
    $('#view_gender').html(data.customer_gender);
    $('#view_nik').html(data.customer_nik);
    $('#view_tanggal_lahir').html(data.customer_tanggal_lahir_text);
    $('#view_age').html(data.customer_umur+" Tahun");
    $('#view_address').html(data.customer_alamat);
    $('#view_referensi').html(data.customer_referensi_makanan);
    $('#view_passport_name').html(data.customer_passport_number);
    $('#view_poi').html(data.customer_poi);
    $('#view_doi').html(data.customer_doi_text);
    $('#view_status').html(data.passport_status);
    $('#view_doe').html(data.customer_doe_text);
    $('#view_create').html(data.created_at);
    $('#view_last').html(data.updated_at);
    $('#view_ktp').attr("src",public+data.customer_ktp).attr("file_name",data.customer_ktp);
    $('#view_visa').attr("src",public+data.customer_visa).attr("file_name",data.customer_visa);
    $('#view_passport').attr("src",public+data.customer_passport).attr("file_name",data.customer_passport);
    $('#view_pic').html(" ");
    data.list_pic.forEach(item => {
        var defaults  = `<ion-icon name="close-outline" class="text-danger"></ion-icon>`;
        if(item.corporate_pic_id==data.corporate_pic_id) defaults =`<ion-icon name="checkmark-outline" class="text-success"></ion-icon>`;
        $('#view_pic').append(`
            <tr>
                <td>${item.corporate_name}</td>
                <td>${item.corporate_position_name}</td>
                <td class="text-center" style="font-weight:bold;font-size:15pt">${defaults}</td>
            </tr>
        `);
    });
    $('#modalView').modal("show");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataCustomer?customer_name="+ $('#filter_customer_name').val()+"&&customer_tanggal_lahir="+ $('#filter_customer_tanggal_lahir').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCustomer",
        method:"post",
        data:{
            customer_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("change","#customer_first_name, #customer_last_name, #customer_phone, #customer_nik,#customer_prefix_nomor",function(){
    $.ajax({
        url:"/master/cekCustomer",
        method:"post",
        data:{
            customer_first_name :$('#customer_last_name').val(),
            customer_last_name :$('#customer_first_name').val(),
            customer_nomor :$('#customer_phone').val(),
            customer_prefix_nomor :$('#customer_prefix_nomor').val(),
            customer_nik :$('#customer_nik').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==-1){
                toastr.warning("Customer Sudah Terdaftar","Perhatian!");
            }
        }
    });
})

$(document).on("input", '.need-check', function(){
    let isEmpty = $(this).val() == '';
    $(this).toggleClass('is-invalid', isEmpty);
})

$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "insertCustomer";
    var gender ="Male";
    var customer_jenis = "Indonesia";
    if($('#jenis_customer_other').is(":checked"))customer_jenis="Other";

    if(!isValid()){
        toastr.error("Ada field yang belum terisi!","Gagal Insert");
        return;
    }

    $('.has-danger').removeClass("has-danger");

    $('#formInsert input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            $(this).closest('.form-floating').addClass("has-danger");
            valid=0;
        }
    });

    if(customer_jenis=="Indonesia"){
        var nik_valid=1;
        if($('#customer_nik').val().length<16){
            nik_valid=0;
            toastr.error("NIK harus 16 digit!","Terjadi Kesalahan");

        }
        if($('#customer_nik').val().length==""){
            nik_valid=0;
            toastr.error("NIK harus diisi!","Terjadi Kesalahan");
        }
        if(nik_valid==0){
            $('#customer_nik').toggleClass('is-invalid', isEmpty);
            return false;
        }
    }

    if(list_pic.length<=0){
        toastr.error("Minimal 1 PIC!","Terjadi Kesalahan");
        return false;
    }

    var ada= 0;
    list_pic.forEach(element => {
        if(element.default==1)ada=1;
    });

    if(ada==0) list_pic[0].default=1;

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if($('#inlineRadio2').is(":checked"))gender="Female";

    var age = parseInt($('#umur').val().split(" ")[0]);
    var param = new FormData();
    param.append( 'customer_title',$('#customer_title').val());
    param.append( 'customer_first_name',$('#customer_first_name').val());
    param.append( 'customer_last_name',$('#customer_last_name').val());
    param.append( 'customer_nik',$('#customer_nik').val());
    param.append( 'customer_email',$('#customer_email').val());
    param.append( 'customer_religion',$('#customer_religion').val());
    param.append( 'customer_prefix_nomor',$('#customer_prefix_nomor').val());
    param.append( 'customer_phone',$('#customer_phone').val());
    param.append( 'customer_tanggal_lahir',$('#customer_tanggal_lahir').val());
    param.append( 'customer_tanggal_lahir',$('#customer_tanggal_lahir').val());
    param.append( 'customer_gender',gender);
    param.append( 'customer_umur',age);
    param.append( 'customer_alamat',$('#customer_alamat').val());
    param.append( 'customer_referensi_makanan',$('#customer_food').val());
    param.append( 'customer_passport_number',$('#customer_passport_number').val());
    param.append( 'customer_poi',$('#customer_poi').val());
    param.append( 'customer_doi',$('#customer_doi').val());
    param.append( 'customer_doe',$('#customer_doe').val());
    param.append( 'customer_jenis',customer_jenis);
    param.append( 'list_pic',JSON.stringify(list_pic));
    param.append( 'visa', $( '#visa' )[0].files[0] );
    param.append( 'ktp', $( '#ktp' )[0].files[0] );
    param.append( 'passport', $( '#passport' )[0].files[0] );
    param.append( '_token',$('meta[name="csrf-token"]').attr('content'));

    if(mode==2){
        url = "editCustomer";
        param.append( 'customer_id', $('#modalInsert').attr("edit_id"));
    }

    $('#btn-insert').append(`
        <div class="spinner-border text-light" role="status" style="width:10px;height:10px">
            <span class="visually-hidden">Loading...</span>
        </div>`);
    $('#btn-insert').attr("disabled","disabled");

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:param,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success:function(e){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();

                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
})
$(document).on("click", "#info-passport", function(){
    $("#gambar_paspor").modal("show");
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_customer",
            type: "get",
            data:{
                corporate_id:$('#filter_corporate_id').val(),
                family_id:$('#filter_customer_family_id').val(),
                passport_status:$('#filter_passport_status').val(),
                customer_name:$('#filter_customer_name').val(),
                customer_nik:$('#filter_customer_nik').val(),
                customer_nomor:$('#filter_customer_nomor').val(),
                customer_passport_number:$('#filter_customer_passport_number').val(),
                customer_tanggal_lahir:$('#filter_customer_tanggal_lahir').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button type="button" class="btn btn-info btn-view">View</button>
                        <button type="button" class="btn btn-warning btn-edit">Edit</button>
                        <button type="button" class="btn btn-danger btn-delete">Delete</button>
                    `;
                    json.data[i].customer_nomor_text = json.data[i].customer_prefix_nomor+json.data[i].customer_nomor;
                    json.data[i].customer_name = json.data[i].customer_first_name+" "+json.data[i].customer_last_name;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                    json.data[i].customer_tanggal_lahir_text = moment(json.data[i].customer_tanggal_lahir).format('DD MMM YYYY');
                    if(json.data[i].customer_doe)json.data[i].customer_doe_text = moment(json.data[i].customer_doe).format('DD MMM YYYY');
                    if(json.data[i].customer_doi)json.data[i].customer_doi_text = moment(json.data[i].customer_doi).format('DD MMM YYYY');

                    if(json.data[i].corporate_id)json.data[i].corporate_id_text = 'C' + (json.data[i].corporate_id+"").toString().padStart(5, '0');
                    else json.data[i].corporate_id_text = "-";

                    if(json.data[i].customer_family_tree_id != "-") json.data[i].family_id = 'FT' + json.data[i].customer_family_tree_id.toString().padStart(5, '0');
                    else json.data[i].family_id = "-";
                    if(json.data[i].customer_doe){
                        var exp = moment(json.data[i].customer_doe).subtract(6,"months",true);
                        var now = moment(new Date());
                        console.log(exp.format("MMM d"));
                        if(now.isAfter(exp)){
                            json.data[i].passport_status = `<span class="badge bg-danger" style="font-size: 10pt;">Expired</span>`;
                        }
                        else{
                            json.data[i].passport_status = `<span class="badge bg-success" style="font-size: 10pt;">Active</span>`;
                        }
                    }
                    else{
                        json.data[i].passport_status = `<span class="badge bg-danger" style="font-size: 10pt;">Expired</span>`;
                    }
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "corporate_id_text"},
            { data: "family_id",className:"text-center"},
            { data: "customer_name"},
            { data: "customer_nomor_text"},
            { data: "customer_gender"},
            { data: "customer_tanggal_lahir_text",className:"text-center"},
            { data: "passport_status",className:"text-center"},
            { data: "action" ,className:"text-center",width:"20%"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".detail",function(){
    var name = $(this).attr("name")+"_"+$('#view_name').html();
    $('#detail_dokumen').attr("src",$(this).attr("src"));
    $('#btn-download').attr("href",$(this).attr("src"));
    $('#btn-download').attr("download",name);
    $("#modalViewDetail").modal("show");
})

$(document).on("click","#btn-download",function(){
    e.preventDefault();
    /*
    $.ajax({
        url:"/master/downloadDocument",
        method:"post",
        data:{
            nama:$(this).attr("nama")
        },
        success:function(data){
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'myfile.pdf';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
*/

})

$('.language').select2({
    placeholder: "Language",
    closeOnSelect: true,
    theme: "bootstrap-5",
    width:"100%",
    dropdownParent: "#modalInsert",
});
