get_data();
var mode=1; //1= insert, 2= delete

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Country");
    $('.add').val("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Country");
    var data = $('#tb-country').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#nama').val(data.name);
    $('#code').val(data.iso3);
    $('#phone').val(data.phonecode);
    $('#modalInsert').attr("edit_id",data.id);
    $('#modalInsert').modal("show");
})


$(document).on("click",".btn-view",function(){
    var data = $('#tb-country').DataTable().row($(this).parents('tr')).data();

    $('#view_visa_name').html(`${data.visa_name}`);
    $('#view_visa_detail').html(`${data.visa_detail}`);
    $('#view_visa_dokumen').html("");
    if(JSON.parse(data.visa_need).length<=0)$('#view_visa_dokumen').html("<label>-</label>");
    JSON.parse(data.visa_need).forEach((item,index) => {
        $('#view_visa_dokumen').append(`<label class="me-3">${index+1}. ${item.document_name}</label>`);
    });
    $('#modalView').attr("visa_id",data.visa_id);
    $('#modalView').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-country').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#modalDelete').attr("delete_id",data.id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataCountry?nama="+ $('#nama_filter').val();
})


$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfVisa/"+$('#modalView').attr("visa_id");
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCountry",
        method:"post",
        data:{
            country_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var code = $('#code').val();
    var phone = $('#phone').val();
    var valid = 1;
    var url = "insertCountry";
    var data = {
        nama:nama,
        code:code,
        phone:phone,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(code==""||code==null){
        $('#code').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(phone==""||phone==null){
        $('#phone').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editCountry";
        data.country_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Country sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-country").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_country",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    var dis = "";
                    if(json.data[i].with_visa==0)dis = "disabled='disabled'";
                    json.data[i].action = `
                        <button class='btn btn-info btn-view' ${dis}>Syarat Visa</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    var status = "Tidak Pakai Visa";
                    var color = "bg-dark";
                    if(json.data[i].with_visa==1){
                        status = " Pakai Visa";
                        color = "bg-success";
                    }
                    json.data[i].with_visa = `<div class='badge ${color} p-2' style="border-radius:100px;font-size:8pt">${status}</badge>`;

                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "iso3",width:"20%"},
            { data: "name",width:"30%"},
            { data: "with_visa",className:"text-center",width:"10%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-country").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
