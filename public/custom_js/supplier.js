get_data();
var mode=1; //1= insert, 2= delete
var list_pic = [];

autocompleteSupplierCategory('#supplier_category','#modalInsert');
autocompleteSupplierCategory('#supplier_category_filter');

autocompleteCountry('#country','#modalInsert');
autocompleteCountry('#country_filter');


$(document).on("click","#btn-search",function(){
    get_data();
})

$(document).on("click","#btn-reset",function(){
   $('#inner-filter input').val("");
   $('#inner-filter select').val("").trigger("change");
   get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    list_pic=[];
    refreshPIC();
    $('#title').html("Tambah Supplier");
    $('.add').val("");
    $('#file').val(null);
    $('.select2').val("").trigger("change");
    $('.has-danger').removeClass("has-danger");
    readOnly(-1);
    $("#template").clone().appendTo("#container-supplier");
    $("#container-supplier .input-pic").css("visibility","visible");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn_import",function(){
    $('#modal_import').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    list_pic = JSON.parse(data.supplier_pic);
    $('.add').val("");
    refreshPIC();
    readOnly(-1);
    $('#title').html("Edit Supplier");
    $('#supplier_name').val(data.supplier_name);
    $('#supplier_phone').val(data.supplier_phone);
    $('#supplier_email').val(data.supplier_email);
    $('#supplier_address').val(data.supplier_address);
    $('#supplier_pic_name').val(data.supplier_pic_name);
    $('#supplier_pic_phone').val(data.supplier_pic_phone);
    $('#supplier_pic_email').val(data.supplier_pic_email);
    $('#supplier_bank_name').val(data.supplier_bank_name);
    $('#supplier_benificary_name').val(data.supplier_benificary_name);
    $('#supplier_bank_account').val(data.supplier_bank_account);
    $('#supplier_swift_code').val(data.supplier_swift_code);
    $('#supplier_curency').val(data.supplier_curency).trigger("change");
    $('#supplier_category').append(`<option value="${data.sp_category_id}">${data.sp_category_nama}</option>`).trigger("change");
    $('#country').empty().trigger("change");
    $('#country').append(`<option value="${data.country_id}">${data.country_name}</option>`).trigger("change");
    $('#view_country').val(data.country_name);
    $('#view_category').val(data.sp_category_nama);
    $('#supplier_id').val(data.supplier_id);
    $('#modalInsert').attr("edit_id",data.supplier_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.supplier_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})


$(document).on("click",".btn-view",function(){
    mode=2;
    $('#title').html("Detail Visa");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    var index = $(this).attr("index");
    $('#edit'+index).click();
    readOnly(1);
    $('#modalInsert').attr("supplier_id",data.supplier_id);
    $('#modalInsert').modal("show");
})


$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataSupplier?nama="+ $('#nama_filter').val()+"&sp_category_id="+ $('#supplier_category_filter').val()+"&country_id="+ $('#country_filter').val();
})

$(document).on("click","#btn-export-pic",function(){
    window.location.href="/master/exportDataSupplierPIC?nama="+ $('#nama_filter').val()+"&sp_category_id="+ $('#supplier_category_filter').val()+"&country_id="+ $('#country_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteSupplier",
        method:"post",
        data:{
            supplier_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
        }
    });
})

$("#form_import").submit(function(e){
    console.log("test");
    e.preventDefault();

    var formData = new FormData();
    formData.append('file', $('#file')[0].files[0]);
    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

    $.ajax({
        url:"/master/importSupplier",
        method:"post",
        data:formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success:function(e){
            if(e.status==1){
                toastr.success("Berhasil Import data","Berhasil Insert");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error(e.msg,"Gagal Insert");
            }
        }
    });
    return false;
})

$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "insertSupplier";


    $('.has-danger').removeClass("has-danger");

    $('#formInsert input').each(function(){
        if($(this).attr("id")!="supplier_id"&&$(this).attr("id")!="supplier_pic"&&$(this).attr("id")!="view_category"&&$(this).attr("id")!="view_country"){
            if($(this).val()==null||$(this).val()==""){
                console.log($(this).attr("id"));
                $(this).closest('.form-group').addClass("has-danger");
                valid=0;
            }
        }
    });

    if(list_pic.length<=0){
        toastr.error("Harap tambahkan PIC Supplier!","Terjadi Kesalahan");
        return false;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }
    if(mode==2){
        url = "editSupplier";
    }

    //cek apakah data pic lupa di add pic
    var valid_add = 1;
    $('.input-pic input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            valid_add=0;
        }
    });

    if(valid_add==1){
        list_pic.push({
            "pic_name":$('#pic_name').val(),
            "pic_phone":$('#pic_phone').val(),
            "pic_email":$('#pic_email').val(),
            "pic_jabatan":$('#pic_jabatan').val(),
        });
    }

    $('#supplier_pic').val(JSON.stringify(list_pic));
    var data = $('#formInsert').serialize();
    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_supplier",
            type: "get",
            data:{
                "nama":$('#nama_filter').val(),
                "country_id":$('#country_filter').val(),
                "sp_category_id":$('#supplier_category_filter').val()
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view' index="${i}">View</button>
                        <button class='btn btn-warning btn-edit' id="edit${i}">Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "supplier_name",width:"10%"},
            { data: "supplier_phone"},
            { data: "sp_category_nama"},
            { data: "country_name"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".btn-add-pic",function(){
    var valid=1;

    $('.has-danger').removeClass("has-danger");

    $('.input-pic input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    list_pic.push({
        "pic_name":$('#pic_name').val(),
        "pic_phone":$('#pic_phone').val(),
        "pic_email":$('#pic_email').val(),
        "pic_jabatan":$('#pic_jabatan').val(),
    });
    $('.input-pic input').val("");
    refreshPIC();
});

$(document).on("click",".btn-delete-pic",function(){
    var idx = $(this).attr("index");
    console.log(idx);
    list_pic.splice(idx,1);
    refreshPIC();
});

$(document).on("click",".btn-edit-pic",function(){
    var idx = $(this).attr("index");
    var dt = list_pic[idx];
    $('.td-name-'+idx).html(`<input type="text" id="input_name_${idx}" class="form-control" value="${dt.pic_name}">`);
    $('.td-phone-'+idx).html(`<input type="text" id="input_phone_${idx}" class="form-control" value="${dt.pic_phone}">`);
    $('.td-email-'+idx).html(`<input type="text" id="input_email_${idx}" class="form-control" value="${dt.pic_email}">`);
    $('.td-jabatan-'+idx).html(`<input type="text" id="input_jabatan_${idx}" class="form-control" value="${dt.pic_jabatan}">`);
    $('#btn-cancel-'+idx).show();
    $('#btn-save-'+idx).show();
    $(this).hide();
});

$(document).on("click",".btn-cancel",function(){
    var idx = $(this).attr("index");
    var dt = list_pic[idx];
    $('.td-name-'+idx).html(`${dt.pic_name}`);
    $('.td-phone-'+idx).html(`${dt.pic_phone}`);
    $('.td-email-'+idx).html(`${dt.pic_email}`);
    $('.td-jabatan-'+idx).html(`${dt.pic_jabatan}`);
    $('#btn-cancel-'+idx).hide();
    $('#btn-save-'+idx).hide();
    $('#btn-edit-'+idx).show();
});

$(document).on("click",".btn-save",function(){
    var idx = $(this).attr("index");
    list_pic[idx].pic_name = $('#input_name_'+idx).val();
    list_pic[idx].pic_phone = $('#input_phone_'+idx).val();
    list_pic[idx].pic_email = $('#input_email_'+idx).val();
    list_pic[idx].pic_jabatan = $('#input_jabatan_'+idx).val();

    var dt = list_pic[idx];
    $('.td-name-'+idx).html(`${dt.pic_name}`);
    $('.td-phone-'+idx).html(`${dt.pic_phone}`);
    $('.td-email-'+idx).html(`${dt.pic_email}`);
    $('.td-jabatan-'+idx).html(`${dt.pic_jabatan}`);
    $('#btn-cancel-'+idx).hide();
    $('#btn-save-'+idx).hide();
    $('#btn-edit-'+idx).show();
});

function refreshPIC() {
    $('#body-pic').html("");
    list_pic.forEach((item,index) => {
        $('#body-pic').append(`
            <tr>
                <td class="td-name-${index}">${item.pic_name}</td>
                <td class="td-phone-${index}">${item.pic_phone}</td>
                <td class="td-email-${index}">${item.pic_email}</td>
                <td class="td-jabatan-${index}">${item.pic_jabatan}</td>
                <td class="text-center"><button class="btn btn-outline-danger btn-sm me-2 btn-delete-pic" index="${index}" type="button">Delete</button>
                <button class="btn btn-outline-warning btn-sm me-2 btn-edit-pic" id="btn-edit-${index}" index="${index}" type="button">Edit</button>
                <button class="btn btn-outline-secondary btn-sm me-2 edit-mode btn-cancel" id="btn-cancel-${index}" index="${index}" type="button">Cancel</button>
                <button class="btn btn-outline-success btn-sm me-2 edit-mode btn-save" id="btn-save-${index}" index="${index}" type="button">Save</button>
                </td>
            </tr>
        `);
        $('.edit-mode').hide();
    });
}

function readOnly(type) {
    if(type==1){
        $('#formInsert input').css("border","0px");
        $('#formInsert select').css("border","0px");
        $('#formInsert input').attr("disabled","disabled");
        $('#formInsert select').attr("disabled","disabled");
        $('#formInsert input').css("background-color","white");
        $('#formInsert select').css("background-color","white");
        $('.input-pic').hide();
        $('#btn-insert').hide();
        $('#country').closest('.form-group').hide();
        $('#supplier_category').closest('.form-group').hide();
        $('#view_country').show();
        $('#view_category').show();
    }
    else{
        console.log("test");
        $('#formInsert input').css("border","");
        $('#formInsert select').css("border","");
        $('#formInsert input').removeAttr("disabled");
        $('#formInsert select').removeAttr("disabled");
        $('#formInsert input').css("background-color","white");
        $('#formInsert select').css("background-color","white");
        $('.input-pic').show();
        $('#btn-insert').show();
        $('#country').closest('.form-group').show();
        $('#supplier_category').closest('.form-group').show();
        $('#view_country').hide();
        $('#view_category').hide();
    }

}
