get_data();
var mode=1; //1= insert, 2= delete


$(document).on("click","#btn-search",function(){
    get_data();
})

$(document).on("click","#btn-reset",function(){
   $('#inner-filter input').val("");
   $('#inner-filter select').val("").trigger("change");
   get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Corporate Position");
    $('.add').val("");
    $('.select2').val("").trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#title').html("Edit Corporate Position");
    $('#nama').val(data.corporate_position_name);
    $('#modalInsert').attr("edit_id",data.corporate_position_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.corporate_position_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataCorporatePosition?nama="+ $('#nama_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCorporatePosition",
        method:"post",
        data:{
            corporate_position_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Position sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var valid = 1;
    var url = "insertCorporatePosition";
    var data = {
        nama:nama,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");
    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }
    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }
    if(mode==2){
        url = "editCorporatePosition";
        data.corporate_position_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_corporate_position",
            type: "get",
            data:{
                "nama":$('#nama_filter').val()
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "corporate_position_name"},
            { data: "action", width: "20%", className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
