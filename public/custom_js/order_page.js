var room = JSON.parse(localStorage.getItem("room"));
var date = (localStorage.getItem("date"));

$(function(){
    init();
    $('.select2').select2({
        placeholder: "Language",
        closeOnSelect: true,
        theme: "bootstrap-5",
        width:"100%",
        dropdownParent: null,
    });
})

function calculateAge(birthDateString) {
    const birthDate = new Date(birthDateString);
    const today = new Date();

    // Get days of each month
    const daysInMonth = [31, (today.getFullYear() % 4 === 0 ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let years = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    let days = today.getDate() - birthDate.getDate();

    // Handle negative days (borrow days from the month)
    if (days < 0) {
        months--;
        days += daysInMonth[(today.getMonth() - 1 + 12) % 12]; // Get last month days count
    }

    // Handle negative months (borrow months from the year)
    if (months < 0) {
        years--;
        months += 12;
    }

    return `${years} Tahun, ${months} Bulan, ${days} Hari`;
}

function init() {
    adult = 0;
    children = 0;
    room.forEach((item) => {
        adult+= Number(item.adult);
        children += Number(item.children);
    })
    $("#date").text(date);
    $("#room").text(room.length + " " + "Room" + (room.length==1? "" : "s"));
    $("#adult").text(adult + " " + "Adult" +  (adult ==1? "" : "s"));
    if(children == 0){
        $("#children").closest(".row").prop("hidden", true);
    }else{
        $("#children").text(children + " " + "Children" +  (children ==1? "" : "s"));
    }

    let isVisa = needVisa();
    console.log(isVisa);
    isVisa = (isVisa? '' : 'hidden');
    let today = new Date().toISOString().substring(0, 10);
    room.forEach((item, index) => {
        adult = '';
        children = '';
        for (let i = 0; i < Number(item.adult) + Number(item.children); i++) {
            if(i+1 > item.adult){
                children = 'Children ' + (i+1 - (item.adult));
            }else{
                children = 'Adult ' + (i + 1);
            }
            adult += `
            <div class="card-header">
                <div class="h5 mb-3">${children}</div>
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="input_name" class="form-label">First Name</label>
                            <input type="text"  class="form-control need-check" placeholder="Budi">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="input_tour_type" class="form-label">Last Name</label>
                            <input type="text" class="form-control need-check" placeholder="Hartono">
                        </div>
                    </div>
                    <div class="col-lg-6 col-birthdate">
                        <div class="mb-3">
                            <label for="input_tour_category" class="form-label">Birthdate *</label>
                            <input type="date" class="form-control birthdate" id="tanggal_lahir" value="${today}" max="${today}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mb-3">
                            <label for="input_tour_type" class="form-label">Age</label>
                            <input type="text" class="form-control age" id="umur" value="0 Tahun" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3" ${isVisa}>
                        <div class="mb-3 text-center">
                            <div class="row h-100">
                                <label for="basicpill-email-input" class="form-label">Punya Visa</label>
                            </div>
                            <div class="row justify-content-center py-2">
                                <input class="form-check" type="checkbox" style="background-color: #7C41FF">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3 has-validation">
                            <label for="input_tour_type" class="form-label">Passport Number</label>
                            <input type="text" class="form-control add is-passport" id="customer_passport_number"  placeholder="Enter Your Passport Number">
                            <div class="invalid-feedback">
                                Passport Number must 16 character
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label for="input_tour_type" class="form-label">DOE</label>
                            <input type="date" class="form-control" value="${today}">
                        </div>
                    </div>
                </div>
            </div>
            `
        }
        let element = $(`
            <div class="card">
                <div class="card-header">
                    <div class="h5 text-purple mt-1">Room ${index+1}</div>
                </div>
                ${adult}
            </div>
        `);

        $("#list_room").append(element);
    });
}

function needVisa() {
    data.destinasi.forEach(element => {
        if(element.with_visa == 1)return true;
    });
    return false;
}

function isValid() {
    let cek = true;
    let element = '';
    $(".need-check").each(function(){
        let isEmpty =  $(this).val() == "";
        $(this).toggleClass("is-invalid",isEmpty);
        if(isEmpty){
            if(cek){
                cek = false;
            }
            if(element == ''){
                element = $(this);
            }
        }
    })

    if(element != ""){
        $('html, body').animate({
            scrollTop: element.parent().offset().top - 50
        }, 100);
    }

    return cek;
}

$(document).on("keyup", ".need-check", function(){
    let isEmpty = $(this).val() == "";
    $(this).toggleClass("is-invalid", isEmpty);
})

$(document).on("click", ".btn-submit", function(){
    let cekIsValid = isValid();
    if(!cekIsValid) return;
    $("#pills-contact-tab").prop("disabled", !cekIsValid);
    $("#pills-contact-tab").click()
    $("#pills-contact-tab").prop("disabled", cekIsValid);
})

$(document).on("change", ".birthdate", function(){
    $(this).closest(".col-birthdate").next().find(".age").val(calculateAge($(this).val()));
})

