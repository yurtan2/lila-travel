get_data();
var mode=1; //1= insert, 2= delete
var file_name = "";

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val(null).trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah TL / Guide / Driver");
    $('.add').val("");
    $('#lg1').val("Abkhaz").trigger("change");
    $('#lg2').val("None").trigger("change");
    $('#lg3').val("None").trigger("change");
    $('input[type="file"]').val(null).trigger("change");
    $('.file').val(null).trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit TL / Guide / Driver");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('.file').val(null).trigger("change");
    $('#nama').val(data.worker_name);
    $('#phone').val(data.worker_phone);
    $('#email').val(data.worker_email);
    $('#address').val(data.worker_address);
    $('#type').val(data.worker_type).trigger("change");
    $('#lg1').val(data.worker_language1).trigger("change");
    $('#lg2').val(data.worker_language2).trigger("change");
    $('#lg3').val(data.worker_language3).trigger("change");
    $('#modalInsert').attr("edit_id",data.worker_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.worker_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click",".btn-view",function(){
    $('#title').html("Detail TL / Guide / Driver");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#view_name').html(data.worker_name);
    $('#view_phone').html(data.worker_phone);
    $('#view_email').html(data.worker_email);
    $('#view_address').html(data.worker_address);
    $('#view_type').html(data.worker_type);
    $('#view_ktp').attr("src",public+data.worker_ktp).attr("file_name",data.worker_ktp);
    $('#view_photo').attr("src",public+data.worker_photo).attr("file_name",data.worker_photo);
    $('#view_guide').attr("src",public+data.worker_guide).attr("file_name",data.worker_guide);
    $('#view_drive').attr("src",public+data.worker_drive).attr("file_name",data.worker_drive);
    $('#view_passport').attr("src",public+data.worker_passport).attr("file_name",data.worker_passport);
    $('#view_language').html(data.worker_language1+", "+data.worker_language2+", "+data.worker_language3);
    $('#modalView').modal("show");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataWorker?nama="+ $('#nama_filter').val()+"&&type="+ $('#type_filter').val()+"&&id="+ $('#id_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteWorker",
        method:"post",
        data:{
            worker_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("TL / Guide / Driver sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "insertWorker";

    $('.has-danger').removeClass("has-danger");

    $('#formInsert input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            $(this).closest('.form-floating').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    var param = new FormData();
    param.append( 'nama',$('#nama').val());
    param.append( 'type',$('#type').val());
    param.append( 'phone',$('#phone').val());
    param.append( 'email',$('#email').val());
    param.append( 'address',$('#address').val());
    param.append( 'language1',$('#lg1').val());
    param.append( 'language2',$('#lg2').val());
    param.append( 'language3',$('#lg3').val());
    param.append( 'photo', $( '#photo' )[0].files[0] );
    param.append( 'ktp', $( '#ktp' )[0].files[0] );
    param.append( 'passport', $( '#passport' )[0].files[0] );
    param.append( 'drive', $( '#drive' )[0].files[0] );
    param.append( 'guide', $( '#guide' )[0].files[0] );
    param.append( '_token',$('meta[name="csrf-token"]').attr('content'));

    if(mode==2){
        url = "editWorker";
        param.append( 'worker_id', $('#modalInsert').attr("edit_id"));
    }

    $('#btn-insert').append(`
        <div class="spinner-border text-light" role="status" style="width:10px;height:10px">
            <span class="visually-hidden">Loading...</span>
        </div>`);
    $('#btn-insert').attr("disabled","disabled");

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:param,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success:function(e){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();

                $('.modal').modal("hide");
            }
            else{
                toastr.error("TL / Guide / Driver sudah terdaftar","Gagal Insert");
            }
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_worker",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                kode:$('#id_filter').val(),
                type:$('#type_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view'>View</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;

                    json.data[i].worker_language_text = json.data[i].worker_language1;
                    if(json.data[i].worker_language2!=null)json.data[i].worker_language_text += ", "+json.data[i].worker_language2.substr(0,4)+"...";
                    else if(json.data[i].worker_language3!=null)json.data[i].worker_language_text += ", "+json.data[i].worker_language3.substr(0,4)+"...";

                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "worker_kode"},
            { data: "worker_name"},
            { data: "worker_type"},
            { data: "worker_language_text"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".detail",function(){
    var name = $(this).attr("name")+"_"+$('#view_name').html();
    $('#detail_dokumen').attr("src",$(this).attr("src"));
    $('#btn-download').attr("href",$(this).attr("src"));
    $('#btn-download').attr("download",name);
    $("#modalViewDetail").modal("show");
})

$(document).on("click","#btn-download",function(){
    e.preventDefault();
    /*
    $.ajax({
        url:"/master/downloadDocument",
        method:"post",
        data:{
            nama:$(this).attr("nama")
        },
        success:function(data){
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'myfile.pdf';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
*/

})

$('.language').select2({
    placeholder: "Language",
    closeOnSelect: true,
    theme: "bootstrap-5",
    width:"100%",
    dropdownParent: "#modalInsert",
});
