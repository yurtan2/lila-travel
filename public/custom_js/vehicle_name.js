get_data();
var mode=1; //1= insert, 2= delete

autocompleteVehicleType('#vehicle_type','#modalInsert');
autocompleteVehicleType('#vehicle_type_filter');

$(document).on("click","#btn-search",function(){
    get_data();
})

$(document).on("click","#btn-reset",function(){
   $('#inner-filter input').val("");
   $('#inner-filter select').val("").trigger("change");
   get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Vehicle");
    $('.add').val("");
    $('.select2').val("").trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    var data = $('#tb-vehicle').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#title').html("Edit Vehicle");
    $('#nama').val(data.vehicle_name);
    $('#vehicle_type').append(`<option value="${data.vehicle_type_id}">${data.vehicle_type_name}</option>`).trigger("change");
    $('#modalInsert').attr("edit_id",data.vehicle_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-vehicle').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.vehicle_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataVehicle?nama="+ $('#nama_filter').val()+"&vehicle_type_id="+ $('#vehicle_type_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteVehicle",
        method:"post",
        data:{
            vehicle_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var product = $('#vehicle_type').val();
    var valid = 1;
    var url = "insertVehicle";
    var data = {
        nama:nama,
        vehicle_type_id:product,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");
    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }
    if(product==""||product==null){
        $('#product_category').closest('.form-select').addClass("has-danger");
        valid=0;
    }
    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }
    if(mode==2){
        url = "editVehicle";
        data.vehicle_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama vehicle sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-vehicle").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_vehicle",
            type: "get",
            data:{
                "vehicle_type_id":$('#vehicle_type_filter').val(),
                "vehicle_name":$('#nama_filter').val()
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "vehicle_type_name"},
            { data: "vehicle_name"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: true,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-vehicle").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
