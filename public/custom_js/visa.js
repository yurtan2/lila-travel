var mode=1; //1= insert, 2= delete

get_data();
autocompleteCountry('#country','#modalInsert');
autocompleteCountry('#country_filter');

//inisialisasi Editor
var quill = new Quill('#detail', {
    theme: 'snow'
  });

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#country_filter').val("").trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    get_dokumen();
    $('#title').html("Tambah Visa");
    $('.add').val("");
    $('#country').val("").trigger("change");
    quill.pasteHTML("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Visa");
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();

    $('#country').append(`<option value="${data.country_id}">${data.country_name}</option>`).trigger("change");
    $('#nama').val(data.visa_name);
    quill.pasteHTML(data.visa_detail);
    get_dokumen(JSON.parse(data.visa_need));

    $('#modalInsert').attr("edit_id",data.visa_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-view",function(){
    mode=2;
    $('#title').html("Detail Visa");
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();

    $('#view_country').html(`${data.country_name}`);
    $('#view_visa_name').html(`${data.visa_name}`);
    $('#view_visa_detail').html(`${data.visa_detail}`);
    $('#view_visa_dokumen').html("");
    if(JSON.parse(data.visa_need).length<=0)$('#view_visa_dokumen').html("<label>-</label>");
    JSON.parse(data.visa_need).forEach((item,index) => {
        $('#view_visa_dokumen').append(`<label class="me-3">${index+1}. ${item.document_name}</label><br>`);
    });
    $('#modalView').attr("visa_id",data.visa_id);
    $('#modalView').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.visa_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataVisa?nama="+ $('#nama_filter').val()+"&country_id="+$('#country_id_filter').val();
})

$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfVisa/"+$('#modalView').attr("visa_id");
})


$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteVisa",
        method:"post",
        data:{
            visa_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Visa sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var country = $('#country').val();
    var nama = $('#nama').val();
    var detail = quill.root.innerHTML;
    var valid = 1;
    var url = "insertVisa";
    var data = {
        nama:nama,
        country:country,
        dokumen:[],
        detail:detail,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(country==""||country==null){
        $('#country').closest('.form-group').addClass("has-danger");
        valid=0;
    }

    if(detail==""||detail==null){
        $('#syarat').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    $('.dokumen').each(function(e){
        if($(this).is(':checked')) {
            data.dokumen.push({
                document_id:$(this).val(),
                document_name:$(this).attr("document_name")
            });
        }
    });
    console.log(data.dokumen);
    if(mode==2){
        url = "editVisa";
        data.visa_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Visa sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-document").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_visa",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                country:$('#country_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view'>View</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "country_name",width:"20%"},
            { data: "visa_name",width:"20%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-document").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_dokumen(data=null) {
    $.ajax({
        url:"/master/get_data_document",
        method:"get",
        success:function(e){
            $('#container_dokumen').html("");
            e = JSON.parse(e);
            e["data"].forEach(item => {
                 $('#container_dokumen').append(`<input type="checkbox" class="form-check-input me-1 dokumen" value="${item.document_id}" document_name="${item.document_name}" style="font-size:12pt" name="document">
                 <label class="form-check-label me-4 mt-1" style="font-size:9pt">${item.document_name}</>`);
            });
            if(data!=null){
                data.forEach(item => {
                    $('.dokumen').each(function(){
                        if($(this).val()==item.document_id){
                            $(this).prop("checked",true);
                        }
                    });
                });

            }
        }
    });
}
