autocompleteAirport("#daerah_asal",null,"Daerah Asal");
autocompleteAirport("#daerah_tujuan",null,"Daerah Tujuan");
autocompleteCountry("#tujuan");
autocompleteCountry("#things_tujuan");
autocompleteCountry("#travel_document");
autocompleteLocation("#location");
reset();

// var swiper = new Swiper(".mySwiper", {
//     cssMode: true,
//     navigation: {
//       nextEl: ".swiper-button-next",
//       prevEl: ".swiper-button-prev",
//     },
//     pagination: {
//       el: ".swiper-pagination",
//     },
//     mousewheel: true,
//     keyboard: true,
// });

$(document).on("click",".menu-search",function(){
    reset();
});

$(document).on("change","#checkin, #durasi",function () {
    addDays("#checkin",$(durasi).val(),'#checkout');
})

function reset() {
    $('.select-select2').empty().trigger("change");
    $('#tanggal_awal').val(getCurrentDate());
    $('#tour_tanggal_pergi').val(getCurrentDate());
    $('#tirtayatra_tanggal_pergi').val(getCurrentDate());
    $('#checkin').val(getCurrentDate());
    $('#tujuan').val(getCurrentDate());
    $('#things_tanggal_pergi').val(getCurrentDate());
    addDays("#checkin",$(durasi).val(),'#checkout');
    addDays("#tanggal_awal",1,'#tanggal_pulang');
    addDays("#tirtayatra_tanggal_pergi",1,'#tirtayatra_tanggal_pulang');
    addDays("#tour_tanggal_pergi",1,'#tour_tanggal_pulang');
    addDays("#things_tanggal_pergi",1,'#things_tanggal_pulang');
    $('#dewasa, #anak, #bayi,#hotel_dewasa, #hotel_anak').val("0");
    $('#room, #durasi').val(1);
}

$(document).on("click", ".link-card", function() {
    window.location.href = "https://wa.link/5xj9l0";
})

$(document).on("click", "#btn-tour-search", function(){
    window.location.href = "/landing_page/tour";
})
