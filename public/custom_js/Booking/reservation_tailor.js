var quill;
var jenis ="Leads";

// Inisialisasi Variabel
$(function(){
    init();
})

function init(){
    $(".btn-tambah-peserta").click();
    quill = new Quill(`#remark_editor`, {
        theme: "snow",
    })
    autocompleteCustomer("#rmh", null);
    autocompleteCustomer("#search", null);
}

function calculateAge(birthDateString) {
    const birthDate = new Date(birthDateString);
    const today = new Date();

    // Get days of each month
    const daysInMonth = [31, (today.getFullYear() % 4 === 0 ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let years = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    let days = today.getDate() - birthDate.getDate();

    // Handle negative days (borrow days from the month)
    if (days < 0) {
        months--;
        days += daysInMonth[(today.getMonth() - 1 + 12) % 12]; // Get last month days count
    }

    // Handle negative months (borrow months from the year)
    if (months < 0) {
        years--;
        months += 12;
    }

    return `${years} Tahun, ${months} Bulan, ${days} Hari`;
}

function newPassenger() {
    let today = new Date().toISOString().substring(0, 10);
    let $newElement = $(`
        <div class="row list_peserta mb-4">
            <div class="row mb-3">
                <div class="col-11">
                    <h5 class="my-0 passenger_title" style="color:#43006a!important"></h5>
                </div>
                <div class="col-1 text-end">
                    <button type="button" class="btn btn-outline-secondary btn-close-passenger" aria-label="Close">X</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_category" class="form-label">Passanger Category</label>
                        <select class="form-select" name="" id="category">
                            <option value="Dewasa">Dewasa</option>
                            <option value="Anak">Anak</option>
                            <option value="Bayi ">Bayi</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">Search </label>
                        <select class="form-select search" name="" id="search"></select>
                    </div>
                </div>
            </div>
            <hr class="hr">
            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_category" class="form-label">First Name *</label>
                        <input type="text"  class="form-control first_name" placeholder="Enter Your First Name">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">Last Name *</label>
                        <input type="text" class="form-control last_name" placeholder="Enter Your Last Name">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_category" class="form-label">NIK *</label>
                        <input type="text" class="form-control nik" placeholder="Enter Your NIK">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Phone Number</label>
                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <select class="select-2 form-select" name="" id="">
                                <option value="+1">+1</option>
                                <option value="+44">+44</option>
                                <option value="+62" selected>+62</option>
                                <option value="+91">+91</option>
                                <option value="+86">+86</option>
                                <option value="+81">+81</option>
                                <option value="+61">+61</option>
                                <option value="+7">+7</option>
                                <option value="+34">+34</option>
                                <option value="+55">+55</option>
                                <option value="+49">+49</option>
                                <option value="+33">+33</option>
                                <option value="+39">+39</option>
                                <option value="+60">+60</option>
                                <option value="+63">+63</option>
                                <option value="+27">+27</option>
                                <option value="+82">+82</option>
                                <option value="+65">+65</option>
                                <option value="+90">+90</option>
                                <option value="+971">+971</option>
                                <option value="+84">+84</option>
                                <option value="+213">+213</option>
                                <option value="+376">+376</option>
                                <option value="+244">+244</option>
                                <option value="+266">+266</option> <!-- Lesotho -->
                                <option value="+231">+231</option> <!-- Liberia -->
                                <option value="+218">+218</option> <!-- Libya -->
                                <option value="+423">+423</option> <!-- Liechtenstein -->
                                <option value="+370">+370</option> <!-- Lithuania -->
                                <option value="+352">+352</option> <!-- Luxembourg -->
                                <option value="+261">+261</option> <!-- Madagascar -->
                                <option value="+265">+265</option> <!-- Malawi -->
                                <option value="+960">+960</option> <!-- Maldives -->
                                <option value="+223">+223</option> <!-- Mali -->
                                <option value="+356">+356</option> <!-- Malta -->
                                <option value="+692">+692</option> <!-- Marshall Islands -->
                                <option value="+222">+222</option> <!-- Mauritania -->
                                <option value="+230">+230</option> <!-- Mauritius -->
                                <option value="+52">+52</option>   <!-- Mexico -->
                                <option value="+373">+373</option> <!-- Moldova -->
                                <option value="+377">+377</option> <!-- Monaco -->
                                <option value="+976">+976</option> <!-- Mongolia -->
                                <option value="+382">+382</option> <!-- Montenegro -->
                                <!-- ... You can continue to add more ... -->

                            </select>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" class="form-control add need-check number-only" id="customer_phone" placeholder="08xxxxxxxx">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_category" class="form-label">Birthdate *</label>
                        <input type="date" class="form-control birthdate" id="tanggal_lahir" value="${today}" max="${today}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">Age</label>
                        <input type="text" class="form-control age" id="umur" value="0 Tahun" disabled>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_category" class="form-label">Address *</label>
                        <input type="text" class="form-control address" placeholder="Jl. Sudirman No. 1...">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">Food Reference</label>
                        <input type="text" class="form-control referensi" placeholder="Enter Your Food Reference">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg">
                    <div class="h5 ">
                        Passport Information
                        <i class="bi bi-info-circle"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                <div class="mb-3 has-validation">
                    <label for="input_tour_type" class="form-label">Passport Number</label>
                    <input type="text" class="form-control add passport_number is-passport" id="customer_passport_number"  placeholder="Enter Your Passport Number">
                    <div class="invalid-feedback">
                        Passport Number must 16 character
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">POI</label>
                        <input type="text" class="form-control" placeholder="Jakarta">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">DOI</label>
                        <input type="date" class="form-control doi" value="${today}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label for="input_tour_type" class="form-label">DOE</label>
                        <input type="date" class="form-control doe" value="${today}">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-6">
                    <div class="h5">Upload KTP / NIK</div>
                    <label class="float-start">
                        <img src="${uploadImageUrl}" style="height: 100px;">
                        <input type="file" class="upload_gambar" name="upload_ktp" id="ktp" style="display: none;">
                    </label>
                    <button type="button" class="btn-close btn-delete-photo"></button>

                </div>
                <div class="col-lg-6">
                    <div class="h5">Upload Passport</div>
                    <label class="float-start">
                        <img src="${uploadImageUrl}" style="height: 100px;">
                        <input type="file" class="upload_gambar" name="upload_paspor" id="passport" style="display: none;">
                    </label>
                    <button type="button" class="btn-close btn-delete-photo"></button>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg">
                    <label for="" class="form-label">Note</label>
                    <textarea class="form-control note" name="" id="" cols="30" rows="5"></textarea>
                </div>
            </div>
            <hr class="hr mt-5 mb-5">
        </div>
    `);
    $(".container-passanger").append($newElement);
    autocompleteCustomer(".search", null);
}

function refreshPassenger(){
    $(".passenger_title").each(function(index){
        $(this).text('Passanger  ' + (index+1));
    })
    $(".btn-close-passenger:first").prop('hidden', true);
    $(".btn-close-passenger:not(:first)").prop('hidden', false);
}

$(document).on("change", ".birthdate", function(){
    $(this).closest(".list_peserta").find(".age").val(calculateAge($(this).val()));
})

$(document).on("change", "#rmh", function(){
    var dt =$(this).select2("data")[0];
    $('#input-address').val(dt.customer_alamat);
    $('#input-nomor').val(dt.customer_nomor);
})

$(document).on("change", "#search", function(){
    var dt =$(this).select2("data")[0];
    //console.log(dt.customer_first_name);
    var parent = $(this).closest(".list_peserta");
    console.log(parent);
    parent.find(".first_name").val(dt.customer_first_name);
    parent.find(".last_name").val(dt.customer_last_name);
    parent.find(".nik").val(dt.customer_nik);
    parent.find(".nomor").val(dt.customer_nomor);
    parent.find(".birthdate").val(dt.customer_tanggal_lahir).trigger("change");
    parent.find(".address").val(dt.customer_alamat);
    parent.find(".referensi").val(dt.customer_referensi_makanan);
    parent.find(".passport_number").val(dt.customer_passport_number).trigger("change");
    parent.find(".doi").val(dt.customer_doi).trigger("change");
    parent.find(".doe").val(dt.customer_doe).trigger("change");
    parent.find(".poi").val(dt.customer_poi).trigger("change");

})

$(document).on("click", ".btn-tambah-peserta", function(){
    newPassenger();
    refreshPassenger();
})

$(document).on("click", ".btn-close-passenger", function(){
    $(this).closest(".list_peserta").remove();
    refreshPassenger();
})

$(document).on("click", ".btn-next", function(){
    $("#pills-passanger-tab").click();
})

$(document).on("change", ".upload_gambar", function(){
    let reader = new FileReader();
    let inputElement = $(this);

    reader.onload = function(e) {
        inputElement.prev().attr("src", e.target.result)
    }

    reader.readAsDataURL(this.files[0]);
})

$(document).on("click", ".btn-delete-photo", function(){
    let inputFile = $(this).prev();
    inputFile.find("input[type='file']").val('');
    inputFile.find("img").attr("src", uploadImageUrl);
})

$(document).on("click", ".bi-info-circle", function(){
    $("#gambar_paspor").modal("show");
})

$(document).on("click", "#btn-simpan", function(){
    var valid = 1;
    var url = "insertReservationTailor";

    $('.has-danger').removeClass("has-danger");
    console.log("test");
    $('#formInsert input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            $(this).closest('.form-floating').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    var data = {
        reservation_tailor_request : $('#reservation_tailor_request').val(),
        reservation_tailor_customer_id : $('#rmh').val(),
        reservation_tailor_phone : $('#input-nomor').val(),
        reservation_tailor_email : $('#input-email').val(),
        reservation_tailor_address : $('#input-address').val(),
        reservation_tailor_dewasa : $('#dewasa').val(),
        reservation_tailor_anak :  $('#anak').val(),
        reservation_tailor_bayi :  $('#bayi').val(),
        reservation_tailor_remark :  quill.root.innerHTML,
        _token :token,
        detail : []
    };

    $('.list_peserta').each(function(){
        data.detail.push({
            "reservation_tailor_detail_category":$(this).find("#category").val(),
            "reservation_tailor_detail_customer_id":$(this).find("#search").val(),
            "reservation_tailor_detail_first_name":$(this).find(".first_name").val(),
            "reservation_tailor_detail_last_name":$(this).find(".last_name").val(),
            "reservation_tailor_detail_nik":$(this).find(".nik").val(),
            "reservation_tailor_detail_nomor":$(this).find(".nomor").val(),
            "reservation_tailor_detail_tanggal_lahir":$(this).find(".birthdate").val(),
            "reservation_tailor_detail_umur":$(this).find(".age").val(),
            "reservation_tailor_detail_referensi_makan":$(this).find(".referensi").val(),
            "reservation_tailor_detail_passport_number":$(this).find(".passport_number").val(),
            "reservation_tailor_detail_address":$(this).find(".address").val(),
            "reservation_tailor_detail_poi":$(this).find(".poi").val(),
            "reservation_tailor_detail_doi":$(this).find(".doi").val(),
            "reservation_tailor_detail_doe":$(this).find(".doe").val(),
            "reservation_tailor_detail_note":$(this).find(".note").val(),
            "reservation_tailor_detail_ktp":$(this).find("#ktp")[0].files[0],
            "reservation_tailor_detail_passport":$(this).find("#passport")[0].files[0]
        });
    });

    data.detail = JSON.stringify(data.detail);

    if(mode==2){
        url = "updateReservationTailor";
    }

    //convert data -> form data
    const fd = new FormData();
    for (const [key, value] of Object.entries(data)) {
        fd.append(key, value);
    }

    $.ajax({
        url:"/booking/tour/"+url,
        method:"post",
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        data:fd,
        success:function(e){
            if(mode==1){
                localStorage.setItem("success","Berhasil Insert Data!");
                toastr.success("Berhasil tambah data baru","Berhasil Insert");
            }
            else if(mode==2){
                localStorage.setItem("success","Berhasil Update Data!");
                toastr.success("Berhasil edit data","Berhasil Update");
            }
            window.location.href = "/booking/tour/";
        }
    });
})
