// Inisialisasi Variabel
var url = "/booking/tour/reservation_fit";
var jenis = "Leads";

$(function(){
    get_data_fit();
})

$(document).on('click', '.tab-page', function(){
    const isThree = ($(this).val() == 3? true:false);
    const ID = ($(this).val() == 3? "Booking ID":"Tour Code");
    const Header = ($(this).val() == 3? "Request":"Product");

    $("#header_id").text(ID);
    $("#header_2nd").text(Header);
    $("#id_filter").attr("placeholder", ID);
    $("#text_filter").attr("placeholder", Header);
    $(".tailor").prop('hidden', !isThree);
    $(".fix-fit").prop('hidden', isThree);

    $("#leads-tab").click();
    if($(this).val() == 1){
        get_data_fit();
        url = "/booking/tour/reservation_fit";
    }else if ($(this).val() == 2){
        get_data_fix();
        url = "/booking/tour/reservation_fix";
    }else{
        get_data_tailor();
        url = "/booking/tour/reservation_tailor";
    }
})

$(document).on('click', '#btn-move-page', function(){
    window.location.href = url;
})

function get_data_fix() {
    $("#tb-departure").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/product/tour/fix_departure/get_data_fix",
            type: "get",
            data:{
                tour_name:$('#nama_filter').val(),
                date:$('#filter_date').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <a class='btn btn-warning btn-edit' href="/product/tour/fix_departure/detailFixDepature/${json.data[i].tour_id}">Edit</a>
                        <button class='btn btn-outline-primary btn-delete'>Download</button>
                        <button class='btn btn-outline-success btn-delete'>Email</button>
                    `;
                    json.data[i].tour_validity_text = moment(json.data[i].tour_date_start).format("DD MMM YYYY")+" - "+moment(json.data[i].tour_date_end).format("DD MMM YYYY")
                    json.data[i].tour_status = json.data[i].status==1?"Active":"Expired";
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "tour_name"},
            { data: "tour_category"},
            { data: "tour_type"},
            { data: "tour_validity_text"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "action",class:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-departure").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_data_fit() {
    $("#tb-departure").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/product/tour/fit_departure/get_data_fit",
            type: "get",
            data:{
                tour_name:$('#nama_filter').val(),
                date:$('#filter_date').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    json.data[i].tour_validity_text = moment(json.data[i].tour_date_start).format("DD MMM YYYY")+" - "+moment(json.data[i].tour_date_end).format("DD MMM YYYY")
                    json.data[i].tour_status = json.data[i].status==1?"Active":"Expired";
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "tour_name"},
            { data: "tour_category"},
            { data: "tour_type"},
            { data: "tour_validity_text"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "action"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-departure").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_data_tailor() {
    $("#tb-departure").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/booking/tour/get_data_tailor",
            type: "get",
            data:{
                reservation_tailor_status:jenis,
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    json.data[i].tour_validity_text = moment(json.data[i].tour_date_start).format("DD MMM YYYY")+" - "+moment(json.data[i].tour_date_end).format("DD MMM YYYY")
                    json.data[i].tour_status = json.data[i].status==1?"Active":"Expired";
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "tour_name"},
            { data: "tour_category"},
            { data: "tour_type"},
            { data: "tour_validity_text"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "tour_status"},
            { data: "action"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-departure").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
