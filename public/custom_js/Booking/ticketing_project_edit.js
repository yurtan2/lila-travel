$(function(){
    init();
    $('.select-2').select2({
        placeholder: "Choose Corporate First",
        closeOnSelect: true,
        theme: "bootstrap-5",
        width:"100%",
        dropdownParent: null,
    });
})


function init(){
    autocompleteCorporate("#corporate-name");
    autocompleteCustomer("#customer-select");
}

$(document).on("click", "#btn-save-project", function(){
    $(".need-check").removeClass("is-invalid");

    let cek = true;
    $(".need-check").each(function(){
        if($(this).val() == "" || $(this).val() == null){
            $(this).addClass("is-invalid");
            cek = false;
        }
    });

    if(!cek) return;

    var data = {
        ticketing_project_name:$('#project_name').val(),
        corporate_id:$('#corporate-name').val(),
        customer_id:$('#pic-name').val(),
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $.ajax({
        url:"/insertTicketingProject",
        method:"post",
        data:data,
        success:function(e){
            if(e!=-1){
                localStorage.setItem("success","Berhasil Insert Data!");
                toastr.success("Berhasil tambah data baru","Berhasil Insert");
                window.location.href = "/ticketingProject/detail/"+e;
            }
            else{
                toastr.error("Nama Project sudah terdaftar!","Gagal Insert");
            }

        }
    });
})

$(document).on("change", "#corporate-name", function(){
    let corporate_id = $(this).select2("data")[0].corporate_id;
    autocompleteCorporatePIC("#pic-name", null, corporate_id);
    $("#pic-name").prop("disabled", false);
})
