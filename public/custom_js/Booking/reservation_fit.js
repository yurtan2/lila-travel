var tab = $("#pills-information-tab").val();

$(function(){
    init();
    $('.select-2').select2({
        placeholder: "Language",
        closeOnSelect: true,
        theme: "bootstrap-5",
        width:"100%",
        dropdownParent: null,
    });
})


function init(){
    let today = getCurrentDate();

    $(".btn-add-ticket").click();
    $(".btn-tambah-peserta").click();
    $("#departure_date").val(today);
    $("#departure_date").attr('max', today);
    autocompleteCustomer('.customer_name');
    autocompleteCountry('#select-departure');
}

function calculateAge(birthDateString) {
    const birthDate = new Date(birthDateString);
    const today = new Date();

    // Get days of each month
    const daysInMonth = [31, (today.getFullYear() % 4 === 0 ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let years = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    let days = today.getDate() - birthDate.getDate();

    // Handle negative days (borrow days from the month)
    if (days < 0) {
        months--;
        days += daysInMonth[(today.getMonth() - 1 + 12) % 12]; // Get last month days count
    }

    // Handle negative months (borrow months from the year)
    if (months < 0) {
        years--;
        months += 12;
    }

    return `${years} Tahun, ${months} Bulan, ${days} Hari`;
}

function newPassanger() {
    let today = new Date().toISOString().substring(0, 10);
    let $newElement = $(`
    <div class="row list_peserta mb-4">
        <div class="row mb-3">
            <div class="col-11">
                <h5 class="my-0 passanger_title" style="color:#43006a!important"></h5>
            </div>
            <div class="col-1 text-end">
                <button type="button" class="btn btn-outline-secondary btn-close-passanger" aria-label="Close">X</button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_category" class="form-label">Passanger Category</label>
                    <select class="form-select" name="" id="">
                        <option value="Dewasa">Dewasa</option>
                        <option value="Anak">Anak</option>
                        <option value="Bayi ">Bayi</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Search </label>
                    <select class="form-select customer_name" name="" id=""></select>
                </div>
            </div>
        </div>
        <hr class="hr">
        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_category" class="form-label">First Name *</label>
                    <input type="text"  class="form-control first_name" placeholder="Enter Your First Name">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Last Name *</label>
                    <input type="text" class="form-control last_name" placeholder="Enter Your Last Name">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_category" class="form-label">NIK *</label>
                    <input type="text" class="form-control nik" placeholder="Enter Your NIK">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Phone Number</label>
                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <select class="select-2 form-select" name="" id="">
                                <option value="+1">+1</option>
                                <option value="+44">+44</option>
                                <option value="+62" selected>+62</option>
                                <option value="+91">+91</option>
                                <option value="+86">+86</option>
                                <option value="+81">+81</option>
                                <option value="+61">+61</option>
                                <option value="+7">+7</option>
                                <option value="+34">+34</option>
                                <option value="+55">+55</option>
                                <option value="+49">+49</option>
                                <option value="+33">+33</option>
                                <option value="+39">+39</option>
                                <option value="+60">+60</option>
                                <option value="+63">+63</option>
                                <option value="+27">+27</option>
                                <option value="+82">+82</option>
                                <option value="+65">+65</option>
                                <option value="+90">+90</option>
                                <option value="+971">+971</option>
                                <option value="+84">+84</option>
                                <option value="+213">+213</option>
                                <option value="+376">+376</option>
                                <option value="+244">+244</option>
                                <option value="+266">+266</option> <!-- Lesotho -->
                                <option value="+231">+231</option> <!-- Liberia -->
                                <option value="+218">+218</option> <!-- Libya -->
                                <option value="+423">+423</option> <!-- Liechtenstein -->
                                <option value="+370">+370</option> <!-- Lithuania -->
                                <option value="+352">+352</option> <!-- Luxembourg -->
                                <option value="+261">+261</option> <!-- Madagascar -->
                                <option value="+265">+265</option> <!-- Malawi -->
                                <option value="+960">+960</option> <!-- Maldives -->
                                <option value="+223">+223</option> <!-- Mali -->
                                <option value="+356">+356</option> <!-- Malta -->
                                <option value="+692">+692</option> <!-- Marshall Islands -->
                                <option value="+222">+222</option> <!-- Mauritania -->
                                <option value="+230">+230</option> <!-- Mauritius -->
                                <option value="+52">+52</option>   <!-- Mexico -->
                                <option value="+373">+373</option> <!-- Moldova -->
                                <option value="+377">+377</option> <!-- Monaco -->
                                <option value="+976">+976</option> <!-- Mongolia -->
                                <option value="+382">+382</option> <!-- Montenegro -->
                                <!-- ... You can continue to add more ... -->

                            </select>
                        </div>
                        <div class="col-lg-9">
                            <input type="tel" class="form-control number-only need-check" placeholder="81xxxxxxxxx">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_category" class="form-label">Birthdate *</label>
                    <input type="date" class="form-control birthdate" id="tanggal_lahir" value="${today}" max="${today}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Age</label>
                    <input type="text" class="form-control age" id="umur" value="0 Tahun" disabled>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_category" class="form-label">Address *</label>
                    <input type="text" class="form-control address" placeholder="Jl. Sudirman No. 1...">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">Food Reference</label>
                    <input type="text" class="form-control referensi" placeholder="Enter Your Food Reference">
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg">
                <div class="h5 ">
                    Passport Information
                    <i class="bi bi-info-circle text-primary cursor"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
            <div class="mb-3 has-validation">
                <label for="input_tour_type" class="form-label">Passport Number</label>
                <input type="text" class="form-control add passport_number is-passport" id="customer_passport_number"  placeholder="Enter Your Passport Number">
                <div class="invalid-feedback">
                    Passport Number must 16 character
                </div>
            </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">POI</label>
                    <select class="select-2 form-select" name="" id="">
                        <option value="+1">+1</option>
                        <option value="+44">+44</option>
                        <option value="+62" selected>+62</option>
                        <option value="+91">+91</option>
                        <option value="+86">+86</option>
                        <option value="+81">+81</option>
                        <option value="+61">+61</option>
                        <option value="+7">+7</option>
                        <option value="+34">+34</option>
                        <option value="+55">+55</option>
                        <option value="+49">+49</option>
                        <option value="+33">+33</option>
                        <option value="+39">+39</option>
                        <option value="+60">+60</option>
                        <option value="+63">+63</option>
                        <option value="+27">+27</option>
                        <option value="+82">+82</option>
                        <option value="+65">+65</option>
                        <option value="+90">+90</option>
                        <option value="+971">+971</option>
                        <option value="+84">+84</option>
                        <option value="+213">+213</option>
                        <option value="+376">+376</option>
                        <option value="+244">+244</option>
                        <option value="+266">+266</option> <!-- Lesotho -->
                        <option value="+231">+231</option> <!-- Liberia -->
                        <option value="+218">+218</option> <!-- Libya -->
                        <option value="+423">+423</option> <!-- Liechtenstein -->
                        <option value="+370">+370</option> <!-- Lithuania -->
                        <option value="+352">+352</option> <!-- Luxembourg -->
                        <option value="+261">+261</option> <!-- Madagascar -->
                        <option value="+265">+265</option> <!-- Malawi -->
                        <option value="+960">+960</option> <!-- Maldives -->
                        <option value="+223">+223</option> <!-- Mali -->
                        <option value="+356">+356</option> <!-- Malta -->
                        <option value="+692">+692</option> <!-- Marshall Islands -->
                        <option value="+222">+222</option> <!-- Mauritania -->
                        <option value="+230">+230</option> <!-- Mauritius -->
                        <option value="+52">+52</option>   <!-- Mexico -->
                        <option value="+373">+373</option> <!-- Moldova -->
                        <option value="+377">+377</option> <!-- Monaco -->
                        <option value="+976">+976</option> <!-- Mongolia -->
                        <option value="+382">+382</option> <!-- Montenegro -->
                        <!-- ... You can continue to add more ... -->

                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">DOI</label>
                    <input type="date" class="form-control doi" value="${today}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="input_tour_type" class="form-label">DOE</label>
                    <input type="date" class="form-control doe" value="${today}">
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-6">
                <div class="h5">Upload KTP / NIK</div>
                <label class="float-start">
                    <img src="${uploadImageUrl}" style="height: 100px;">
                    <input type="file" class="upload_gambar" name="upload_ktp" id="ktp" style="display: none;">
                </label>
                <button type="button" class="btn-close btn-delete-photo"></button>

            </div>
            <div class="col-lg-6">
                <div class="h5">Upload Passport</div>
                <label class="float-start">
                    <img src="${uploadImageUrl}" style="height: 100px;">
                    <input type="file" class="upload_gambar" name="upload_paspor" id="passport" style="display: none;">
                </label>
                <button type="button" class="btn-close btn-delete-photo"></button>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg">
                <label for="" class="form-label">Note</label>
                <textarea class="form-control note" name="" id="" cols="30" rows="5"></textarea>
            </div>
        </div>
        <hr class="hr mt-5">
    </div>
    `);
    $(".container-passanger").append($newElement);
    autocompleteCustomer('.customer_name');
}

function refreshPassanger() {
    $(".passanger_title").each(function(index){
        $(this).text('Peserta ' + (index+1));
    })

    if($(".list_peserta").length == 1){
        $(".btn-close-passanger").prop('hidden', true);
    }else {
        $(".btn-close-passanger").prop('hidden', false);
    }
}

function newTabPassanger(){
    let $newElement = $(`
    <div class="row mt-3 list_ticket_passanger align-items-center">
        <div class="col-1">
            <label for="" class="form-label">No.</label>
            <div class="form-control number-ticket"></div>
        </div>
        <div class="col-6">
            <label for="input_tour_type" class="form-label">Passanger Name</label>
            <select class="form-select customer_name" name="" id=""></select>
        </div>
        <div class="col-2">
            <label for="input_tour_type" class="form-label">Ticket No.</label>
            <input type="email" class="form-control" placeholder="Enter Ticket no.">
        </div>
        <div class="col-2">
            <label for="input_tour_type" class="form-label">Visa No.</label>
            <input type="email" class="form-control" placeholder="Enter Visa no.">
        </div>
        <div class="col-1 pt-4 text-end">
            <button type="button" class="btn btn-outline-danger btn-sm w-100 mt-1 btn-trash" id="">
                <ion-icon name="trash-outline" style="font-size:14pt;padding-top:2px"></ion-icon>
            </button>
        </div>
    </div>
    `);

    $('.container-ticket-passanger').append($newElement);
    autocompleteCustomer(".customer_name");
}

function refreshTabPassanger() {
    $('.list_ticket_passanger').each(function(index){
        $(this).find('.number-ticket').text(index+1);
    })
}

$(document).on("click", '#pills-information-tab, #pills-passanger-tab, #pills-ticket-tab, #pills-payment-tab', function(){
    tab = $(this).val();
})

$(document).on("click", '.btn-next', function(){
    if(tab == 1){
        $('#pills-passanger-tab').click();
    }else if(tab == 2){
        $('#pills-ticket-tab').click();
    }else if(tab == 3){
        $("#pills-payment-tab").click();
    }
})

$(document).on("change", ".birthdate", function(){
    $(this).closest(".list_peserta").find(".age").val(calculateAge($(this).val()));
})

$(document).on("click", ".btn-tambah-peserta", function(){
    newPassanger();
    refreshPassanger();
})

$(document).on("click", ".btn-close-passanger", function(){
    $(this).closest(".list_peserta").remove();
    refreshPassanger();
})

$(document).on("click", ".bi-info-circle", function(){
    $("#gambar_paspor").modal("show");
})

$(document).on("change", ".upload_gambar", function(){
    let reader = new FileReader();
    let inputElement = $(this);

    reader.onload = function(e) {
        inputElement.prev().attr("src", e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
})

$(document).on("click", ".btn-delete-photo", function(){
    let inputFile = $(this).prev();
    inputFile.find("input[type='file']").val('');
    inputFile.find("img").attr("src", uploadImageUrl);
})

$(document).on("click", ".btn-add-ticket", function(){
    newTabPassanger();
    refreshTabPassanger();
})

$(document).on("click", ".btn-trash", function(){
    $(this).closest('.list_ticket_passanger').remove();
    refreshTabPassanger();
})
