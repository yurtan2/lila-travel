var bulan = moment().format('M');
$(function(){
    init();
    get_data();
    $('.tab-page').removeClass("active");
    console.log(bulan);
    $(`[index="${(bulan)}"]`).addClass("active");
})


function init(){
    let today = moment(getCurrentDate()).format('YYYY');
    $("#input-year").text(today);

    autocompleteCorporate("#corporate-name")
    // const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
    // const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
}

$(document).on("click", "#last-year, #next-year", function(){
    let changeYear = $("#input-year").text();
    if($(this).attr("id") == 'last-year'){
        changeYear = moment(changeYear + "-01-01").subtract(1, 'years').format('YYYY');
    }else{
        changeYear = moment(changeYear + "-01-01").add(1, 'years').format('YYYY');
    }
    $("#input-year").text(changeYear);
    get_data();
})

$(document).on("click", "#btn-save", function(){
    $(".is-invalid").removeClass("is-invalid");

    let cek = true;
    $(".need-check").each(function(){
        if($(this).val() == "" || $(this).val() == null){
            $(this).addClass("is-invalid");
            cek = false;
        }
    })

    if(!cek) return;
})

$(document).on("click", ".tab-page", function(){
    bulan = $(this).attr('index');
   get_data();
})


function get_data() {
    $("#table_ticket").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/get_data_ticketing_project",
            type: "get",
            data:{
                ticketing_project_name:$('#nama_filter').val(),
                bulan:bulan,
                tahun:parseInt($('#input-year').html()),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <a class='btn btn-warning btn-edit' href="/ticketingProject/detail/${json.data[i].ticketing_project_id}">Edit</a>
                        <button class='btn btn-outline-primary btn-delete'>Download</button>
                        <button class='btn btn-outline-success btn-delete'>Email</button>
                    `;
                    json.data[i].ticketing_project_total_amount = formatRupiah(json.data[i].ticketing_project_total_amount,"Rp.");
                    json.data[i].customer_phone_text = json.data[i].customer_prefix_nomor+json.data[i].customer_nomor;
                    json.data[i].index = i+1;
                    json.data[i].customer_full = json.data[i].customer_first_name+" "+json.data[i].customer_last_name;
                    json.data[i].total_pax = json.data[i].ticketing_project_adult+json.data[i].ticketing_project_child+json.data[i].ticketing_project_infant;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {
                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "index"},
            { data: "ticketing_project_name"},
            { data: "corporate_name"},
            { data: "customer_full"},
            { data: "customer_phone_text"},
            { data: "updated_by",class:"text-center"},
            { data: "total_pax",class:"text-center"},
            { data: "ticketing_project_total_amount"},
            { data: "action",class:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#table_ticket").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
