var today = moment(getCurrentDate()).format('YYYY-MM-DD');
var jumlah_destinasi = 1;
var dewasa = '';
var anak = '';
var bayi = '';
var data_delete = [];

$(function() {
    console.log(routes);
    var total_all_routes=0;
    routes.forEach((item,index) => {
          // Ambil data Route untuk dimasukan ke html
          let from = '';
          let to = '';
          let dep = '';
          let rtn = '';
          for (let i = 0; i < item.destinasi.length; i++) {

              from += `<div class="h5">${item.destinasi[i].from_city_name} - ${item.destinasi[i].from_airport_name} - ${item.destinasi[i].from_city_code}</div>`;
              to += `<div class="h5">${item.destinasi[i].to_city_name} - ${item.destinasi[i].to_airport_name} - ${item.destinasi[i].to_city_code}</div>`;

              if(item.destinasi[i].departure_date == '-' ||item.destinasi[i].departure_date == null){
                  dep += `<div class="h5">-</div>`;
              }else{
                  dep += `<div class="h5">${moment(item.destinasi[i].departure_date).format('DD MMM YYYY')}</div>`
              }

              if(item.destinasi[i].return_date == '-'|| item.destinasi[i].return_date == null){
                  rtn += `<div class="h5">-</div>`;
              }else{
                  rtn += `<div class="h5">${moment(arr_date[i]).format('DD MMM YYYY')}</div>`
              }
          }

          let dep_rtn;

          if(item.tpr_jenis == 1 || item.tpr_jenis == 2){
              dep_rtn = `
                  <div class="col-lg-2">
                      <label for="">Dep. Date :</label>
                      <div class="h5">${dep}</div>
                  </div>
                  <div class="col-lg-2">
                      <label for="">Rtn. Date :</label>
                      <div class="h5">${rtn}</div>
                  </div>
              `
          }else{
              dep_rtn = `
                  <div class="col-lg-4">
                      <label for="">Dep. Date :</label>
                      <div class="h5">${dep}</div>
                  </div>
              `
          }
          //jumlah pax
          var total_pax =0;
          total_pax+= item.tpr_dewasa+item.tpr_bayi+item.tpr_anak;

          //jenis route
          var jenis = "One Way";
          if(item.tpr_jenis==2) jenis = "Return";
          else if(item.tpr_jenis==3) jenis = "Multi City";


            let table = "";
            var total = 0;
            item.peserta["data"].forEach(peserta => {
                total +=peserta.selling_price;
                table += `
                <tr class="row-table align-middle row-peserta" tpp_id="${peserta.ticket_projecting_peserta_id}">
                    <td>
                        <select class="form-select customer-name" name="" id=""></select>
                    </td>
                    <td>
                        <div class="first-name" style="width: 100px;">${peserta.first_name}</div>
                    </td>
                    <td>
                        <div class="last-name" style="width: 100px;">${peserta.last_name}</div>
                    </td>
                    <td>
                        <div class="nik">${peserta.nik}</div>
                    </td>
                    <td>
                        <div class="birth-date" style="width: 100px;">${peserta.tanggal_lahir}</div>
                    </td>
                    <td>
                        <div class="jabatan" style="width: 100px;">${peserta.jabatan}</div>
                    </td>
                    <td>
                        <select class="form-select airline" name="" id="" >
                        </select>
                    </td>
                    <td>
                    <input class="form-control flight" style="width: 170px;" type="text" name="" id="" value="${peserta.flight}">
                    </td>
                    <td>
                    <input class="form-control class-flight" style="width: 170px;" type="text" name="" id="" value="${peserta.class}">
                    </td>
                    <td>
                        <input class="form-control booking" style="width: 170px;" type="text" name="" id="" value="${peserta.booking_code}">
                    </td>
                    <td>
                        <select class="form-select status" name="" id=""  style="width: 100px;" >
                            <option value="" selected hidden></option>
                            <option value="Active">Active</option>
                            <option value="Issued">Issued</option>
                            <option value="Cancel">Cancel</option>
                            <option value="Expired">Expired</option>
                        </select>
                    </td>
                    <td>
                        <input type="datetime-local" class="form-control time-limit" id="" name="" disabled>
                    </td>
                    <td>
                        <input type="time" class="form-control etd" name="" id="" value="${peserta.etd}">
                    </td>
                    <td>
                        <input type="time" class="form-control eta" name="" id="" value="${peserta.eta}">
                    </td>
                    <td>
                        <input class="form-control nets-price text-end" style="width: 170px;" type="text" name="" id="" value="${formatRupiah(peserta.net_price+"")}">
                    </td>
                    <td>
                        <input class="form-control mark-up text-end" style="width: 170px;" type="text" name="" id="" value="${formatRupiah(peserta.markup_price+"")}">
                    </td>
                    <td>
                        <input class="form-control selling-price text-end" style="width: 170px;" type="text" name="" id="" value="${formatRupiah(peserta.selling_price+"")}">
                    </td>
                    <td>
                        <input class="form-control invoice text-start" style="width: 200px;" type="text" name="" id="" value="${peserta.invoice}">
                    </td>
                    <td>
                        <input class="form-control remark text-start" style="width: 200px;" type="text" name="" id="" value="${peserta.remark}">
                    </td>
                    <td>
                        <button class="btn btn-outline-danger btn-del-row"  tpp_id="${peserta.ticket_projecting_peserta_id}" tpr_id="${peserta.tpr_id}">DELETE</button>
                    </td>
                </tr>
            `
            });

            tables = `
                <div class="table-responsive mb-3">
                    <table class="table table-striped">
                        <tbody class="table-body-passanger">
                            <tr class="align-middle">
                                <td>#</td>
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>NIK</td>
                                <td>Birth Date</td>
                                <td>Jabatan</td>
                                <td>Airline</td>
                                <td>Flight #</td>
                                <td>Class</td>
                                <td>Booking Code</td>
                                <td style="width: 100px;">Status</td>
                                <td>Time Limit</td>
                                <td>ETD</td>
                                <td>ETA</td>
                                <td>Net Price</td>
                                <td>Mark Up</td>
                                <td>Selling Price</td>
                                <td>Invoice #</td>
                                <td>Remark</td>
                                <td>Action</td>
                            </tr>
                            ${table}
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 mb-3">
                    <button class="btn btn-purple float-end ms-2 btn-save-passanger" tpr_id="${item.tpr_id}">Save</button>
                    <button class="btn btn-secondary float-end btn-add-passanger">Add Passanger</button>
                </div>
            `;
            // Masukin ke HTML
            var row_route = `
            <div class="row mb-3 row_route row_route_in_table" editable="false">
                <div class="row justify-content-between mb-3 align-items-center">
                    <div class="col-lg-3 mb-3">
                        <div class="row mb-3">
                            <div class="col-lg-8">
                                <div class="h5 route-title mb-3 float-start">Route ${(index+1)} : ${jenis}</div>
                                <input type="hidden" class="route-type" value="${jenis}">
                            </div>
                            <div class="col">
                                <i class="bi bi-caret-right-fill btn-slide-show"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <div class="row price-row">
                            <div class="col-lg-5 mb-3">
                                <div class="h5 route-total float-end">Total Sales :</div>
                            </div>
                            <div class="col-lg-5 mb-3">
                                <div class="h5 route-price float-end">IDR ${formatRupiah(item.tpr_total_sales)}</div>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-close btn-close-route"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-3">
                        <label for="">From :</label>
                        ${from}
                    </div>
                    <div class="col-lg-3">
                        <label for="">To :</label>
                        ${to}
                    </div>
                    ${dep_rtn}
                    <div class="col-lg-2">
                        <label for="">Total Pax :</label>
                        <div class="h5 total-pax-title">${total_pax} Pax</div>
                        <input type="hidden" class="total-pax" value="${total_pax}">
                    </div>
                </div>
                ${tables}
                <hr>
            </div>
            `;
            $('#list_route').append(row_route);
            autocompleteCustomer('.customer-name');
            autocompleteAirlines(".airline");

            $(".row_route_in_table:last").find(".selling-price").trigger("keyup");
            $(".row_route_in_table:last").find(".net-price").trigger("keyup");

            $(".row_route_in_table:last").find(".status").each(function(index){
                console.log(index);
                $(this).val(item.peserta["data"][index].status_peserta).trigger("change");
            });

            $(".row_route_in_table:last").find(".airline").each(function(index){
                $(this).append(`<option value="${item.peserta["data"][index].airline_id}">${item.peserta["data"][index].airline_name}</option>`).trigger("change");
            });


            $(".row_route_in_table:last").find(".customer-name").each(function(index){
                console.log(index);
                $(this).append(`<option value="${item.peserta["data"][index].customer_id}">${item.peserta["data"][index].first_name+" "+item.peserta["data"][index].last_name}</option>`);
            });
            $('.row_route_in_table:last .route-price').html(formatRupiah(total+"","Rp."));
            total_all_routes+=total;
            list_arilines.push({id: item.airline_id, text: item.airline_name});

    });
    $('#total_all_routes').html(formatRupiah(total_all_routes+"","Rp."));
});
function hitungTotal() {
    var total = 0;
    $('.route-price').each(function(){
        total += convertToAngka($(this).html());
    });
    $('#total_all_routes').html(formatRupiah(total+"","Rp."));
}
for (let i = 0; i < 21; i++) {
    if(i < 11){
        anak += `<option value="${i}">${i}</option>`;
    }
    if(i < 6){
        bayi += `<option value="${i}">${i}</option>`;
    }
    dewasa += `<option value="${i}">${i}</option>`;
}

var oneWayRoute = `
    <div class="row mb-3 isi-route">
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">From *</label>
            <select class="form-select select2 need-check dept-airport" name="" id="">
            </select>
        </div>
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">To *</label>
                <select class="form-select select2 need-check arr-airport" name="" id="">
                </select>
        </div>
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Date *</label>
            <input type="date" class="form-control dept-date" name="" id="departure_date_one_way" value="${today}">
        </div>
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Air Line Preference</label>
            <select class="form-select select2 need-check airline" name="" id="">
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Dewasa</label>
            <select name="dewasa" id="dewasa" class="form-select count-check dewasa">
                ${dewasa}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Anak</label>
            <select name="anak" id="anak" class="form-select count-check anak">
                ${anak}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Bayi</label>
            <select name="bayi" id="bayi" class="form-select count-check bayi">
                ${bayi}
            </select>
        </div>
        <div class="col-lg-5 mb-3">
            <div class="row">
                <label for="">&nbsp;</label>
            </div>
            <button class="btn btn-outline-purple">+ BULK UPLOAD</button>
        </div>
        <div class="col-lg-12 mb-3">
            <button class="btn btn-purple float-end ms-2 btn-save-route">Save</button>
            <button class="btn btn-secondary float-end btn-del-route">Cancel</button>
        </div>
    </div>
`;

var returnRoute = `
    <div class="row mb-3 isi-route">
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">From *</label>
            <select class="form-select select2 need-check dept-airport" name="" id="dept-airport-return">
            </select>
        </div>
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">To *</label>
            <select class="form-select select2 need-check arr-airport" name="" id="return-airport-return">
            </select>
        </div>
        <div class="col-lg-3 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Departure Date *</label>
            <input type="date" class="form-control need-check dept-date" name="" id="r-departure-date" value="${today}">
        </div>
        <div class="col-lg-3 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Return Date *</label>
            <input type="date" class="form-control need-check arr-date" name="" id="r-return-date" value="${moment(today).add(1,'days').format("YYYY-MM-DD")}">
        </div>
        <div class="col-lg-6 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Air Line Preference</label>
            <select class="form-select select2 need-check airline" name="" id="">
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Dewasa</label>
            <select name="dewasa" id="dewasa" class="form-select count-check dewasa">
                ${dewasa}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Anak</label>
            <select name="dewasa" id="dewasa" class="form-select count-check anak">
                ${anak}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Bayi</label>
            <select name="dewasa" id="dewasa" class="form-select count-check bayi">
                ${bayi}
            </select>
        </div>
        <div class="col-lg-5 mb-3">
            <div class="row">
                <label for="">&nbsp;</label>
            </div>
            <button class="btn btn-outline-purple">+ BULK UPLOAD</button>
        </div>
        <div class="col-lg-12 mb-3">
            <button class="btn btn-purple float-end ms-2 btn-save-route">Save</button>
            <button class="btn btn-secondary float-end btn-del-route">Cancel</button>
        </div>
    </div>
`;

var rowDestination = `
<div class="row row-destination">
    <div class="col-lg-4 mb-3">
        <label for="basicpill-phoneno-input" class="form-label">From *</label>
        <select class="form-select select2 need-check dept-airport multi-dept-airport" name="" id="dept${jumlah_destinasi}">
        </select>
    </div>
    <div class="col-lg-4 mb-3">
        <label for="basicpill-phoneno-input" class="form-label">To *</label>
        <select class="form-select select2 need-check arr-airport multi-arr-airport" name="" id="arr${jumlah_destinasi}">
        </select>
    </div>
    <div class="col-lg-3 mb-3">
        <label for="basicpill-phoneno-input" class="form-label">Departure Date *</label>
        <input type="date" class="form-control multi-departure-date dept-date need-check" name="" id="date${jumlah_destinasi}" value="${today}">
    </div>
    <div class="col-1 pt-4 text-end">
        <button type="button" class="btn btn-outline-danger btn-sm w-100 mt-1 btn-trash">
            <ion-icon name="trash-outline" style="font-size:14pt;padding-top:2px"></ion-icon>
        </button>
    </div>
</div>
`

var multiCityRoute = `
    <div class="row mb-3 isi-route">
        ${rowDestination}
        <div class="col-lg-12 mb-3">
            <button class="btn btn-secondary btn-tambah-city">+ Add Route</button>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Dewasa</label>
            <select name="dewasa" id="dewasa" class="form-select count-check dewasa">
                ${dewasa}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Anak</label>
            <select name="dewasa" id="dewasa" class="form-select count-check anak">
                ${anak}
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <label for="">Bayi</label>
            <select name="dewasa" id="dewasa" class="form-select count-check bayi">
                ${bayi}
            </select>
        </div>
        <div class="col-lg-4 mb-3">
            <label for="basicpill-phoneno-input" class="form-label">Air Line Preference</label>
            <select class="form-select select2 need-check airline" name="" id="">
            </select>
        </div>
        <div class="col-lg-2 mb-3">
            <div class="row">
                <label for="">&nbsp;</label>
            </div>
            <button class="btn btn-outline-purple">+ BULK UPLOAD</button>
        </div>
        <div class="col-lg-12 mb-3">
            <button class="btn btn-purple float-end ms-2 btn-save-route">Save</button>
            <button class="btn btn-secondary float-end btn-del-route">Cancel</button>
        </div>
    </div>
`

var list_route = [];
var list_arilines = [];
var list_passanger = [];

$(function(){
    init();
    $('.select-2').select2({
        placeholder: "Placeholder",
        closeOnSelect: true,
        theme: "bootstrap-5",
        width:"100%",
        dropdownParent: null,
    });
})


function init(){

}

function newRoute(){
    let $element = $(`
    <div class="row mb-3 row_route row_route_in_table" editable="true">
        <div class="row justify-content-between mb-3 align-items-center">
            <div class="col-lg-3 mb-3">
                <div class="row mb-3">
                    <div class="col-lg-8">
                        <div class="h5 route-title">Route</div>
                        <input type="hidden" class="route-type" value="">
                    </div>
                    <div class="col">
                        <i class="bi bi-caret-right-fill btn-slide-show" hidden></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-3">
                <div class="row price-row" hidden>
                    <div class="col-lg-5 mb-3">
                        <div class="h5 route-total float-end">Total Sales :</div>
                    </div>
                    <div class="col-lg-5 mb-3">
                        <div class="h5 route-price float-end">IDR 0</div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-close btn-close-route"></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3 form-route">
            <div class="col-lg-2 mb-4">
                <label class="form-check-label" >
                    <input class="form-check-input input-route" id="one" value="One Way" type="radio" name="inputRoute_${list_route.length}" checked>
                    One Way
                </label>
            </div>
            <div class="col-lg-2 mb-4">
                <label class="form-check-label">
                    <input class="form-check-input input-route" id="return" value="Return" type="radio" name="inputRoute_${list_route.length}">
                    Return
                </label>
            </div>
            <div class="col-lg-2 mb-4">
                <label class="form-check-label">
                    <input class="form-check-input input-route" id="multi" value="Multi City" type="radio" name="inputRoute_${list_route.length}">
                    Multi City
                </label>
            </div>
            ${oneWayRoute}
        </div>
        <hr>
    </div>
    `);
    list_route.push(1);
    $("#list_route").append($element);
    refreshRoute();

    //toastr.success("Berhasil tambah Route");
}

function refreshRoute() {
    $(".row_route").each(function(index){
        $(this).find(".input-route").attr("name", "inputRoute_" + index);
        if($(this).attr("editable") == "false"){
            $(this).find(".route-title").text(`Route ${index+1} : ${$(this).find(".route-type").val()}`)
        }
    })
    autocompleteAirport(".dept-airport", null, "Keberangkatan");
    autocompleteAirport(".arr-airport", null, "Destinasi");
    autocompleteAirlines(".airline", null);
}

// Harusnya namanya isEmpty
function isValid(parent) {
    let isEmpty = false;

    if(parent.attr("editable") == "false"){
        return isEmpty;
    }

    parent.find(".need-check, .count-check").removeClass("is-invalid");

    parent.find(".need-check").each(function(){
        if($(this).val() == "" || $(this).val() == null){
            isEmpty = true;
            $(this).addClass("is-invalid");
        }
    })

    let count = 0;
    parent.find(".count-check").each(function(){
        count += Number($(this).val());
    })
    if(count == 0){
        parent.find(".count-check").addClass('is-invalid');
        isEmpty = true;
    }

    return isEmpty;
}

$(document).on("click", "#btn-tambah-route", function(){
    let isEmpty = isValid($(".row_route:last"));
    if(isEmpty && $(".row_route").length != 0){
        toastr.error("Anda harus mengisi route yang ada terlebih dahulu", "Gagal Insert");
        $('html, body').animate({
            scrollTop: $(".row_route:last").offset().top - 50
        }, 1000);
    }else{
        jumlah_destinasi=1;
        newRoute();
        refreshRoute();
    }

});

$(document).on("click", ".btn-trash",function(){
    let row_destination = $(this).closest('.row-destination');
    if($(".row-destination").length == 1){
        toastr.error("Minimal harus ada 1 route", "Gagal Delete");
    }else{
        row_destination.remove();
    }
})

$(document).on("change", ".input-route", function(){
    let rowIndex = $(".form-route").index($(this).closest(".form-route"));
    list_route[rowIndex] = $(this).val();
    $(this).closest(".row_route").find(".isi-route").remove();
    if($(this).val() == "One Way"){
        $(this).closest(".form-route").append($(oneWayRoute));
    }else if ($(this).val() == "Return"){
        $(this).closest(".form-route").append($(returnRoute));
    }else{
        $(this).closest(".form-route").append($(multiCityRoute));
    }
    refreshRoute();
})

$(document).on("click", ".btn-tambah-city", function(){
    $(".row-destination:last").after(rowDestination);
    autocompleteAirport(".dept-airport", null, "Keberangkatan");
    autocompleteAirport(".arr-airport", null, "Destinasi");
    // refreshRoute();
})

$(document).on("click", ".btn-del-route", function(){
    let rowIndex = $(".row_route").index($(this).closest(".row_route"));
    list_route.splice(rowIndex, 1);
    list_arilines.splice(rowIndex, 1);
    $(this).closest(".row_route").remove();
    refreshRoute();
    toastr.success("Berhasil hapus Route");
})

$(document).on("click", ".btn-save-route", function(){
    let isEmpty = isValid($(this).closest(".row_route"));
    let jenis = 1;
    if(isEmpty){
        toastr.error("Ada field yang masih kosong!", "Gagal Save")
        return;
    }
    if($('#return').is(":checked")) jenis=2;
    else if($('#multi').is(":checked")) jenis=3;

    var data = {
        ticketing_project_id:ticketing_project_id,
        tpr_jenis:jenis,
        tpr_airline:$('.airline').val(),
        tpr_dewasa:$('.dewasa').val(),
        tpr_anak:$('.anak').val(),
        tpr_bayi:$('.bayi').val(),
        destinasi:[],
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    list_arilines.push({id: $(".airline").val(), text: $(".airline").text()})
    list_passanger.push({
        adult: Number(data.tpr_dewasa),
        child: Number(data.tpr_anak),
        infant: Number(data.tpr_bayi)
    });

    if(jenis==1){
        data.destinasi.push({
            "from_id":$('.dept-airport').val(),
            "to_id":$('.arr-airport').val(),
            "departure_date":$('#departure_date_one_way').val(),
        });
    }

    else if(jenis==2){
        data.destinasi.push({
            "from_id":$('.dept-airport').val(),
            "to_id":$('.arr-airport').val(),
            "departure_date":$('#r-departure-date').val(),
            "return_date":$('#r-return-date').val(),
        });
    }

    else if(jenis==3){
        $('.row-destination').each(function(index){
            data.destinasi.push({
                "from_id":$(this).find('.multi-dept-airport').val(),
                "to_id":$(this).find('.multi-arr-airport').val(),
                "departure_date":$(this).find('.multi-departure-date').val(),
            });
        });
    }

    data.destinasi = JSON.stringify(data.destinasi);
    // console.log(data);
    let row_route = $(this).closest(".row_route");
    let idx_route = $(".row_route").index(row_route);
    let type = row_route.find(`input[name="inputRoute_${idx_route}"]:checked`).val();

    row_route.find('i').prop("hidden", false);

    $.ajax({
        url:"/insertRoute",
        method:"post",
        data:data,
        success:function(e){
            console.log(e);
             // Ambil Row satu Route

            row_route.find(".route-type").val(type);

            // Ambil data Airline
            let airline = $(".airline").select2("data")[0];
            console.log(airline);

            // Ambil Data untuk tiap tujuan airport dan tanggal keberangkatan
            let arr_airport = [];
            let dept_airport = [];
            let dep_date = [];
            let arr_date = [];

            $(".dept-airport").each(function(index){
                arr_airport.push($(this).select2("data")[0]);
                dept_airport.push($(`.arr-airport:eq(${index})`).select2("data")[0]);

                let dep_date_temp = $(`.dept-date:eq(${index})`).val();
                let arr_date_temp = $(`.arr-date:eq(${index})`).val();
                dep_date.push(dep_date_temp == undefined? '-' : dep_date_temp);
                arr_date.push(arr_date_temp == undefined? '-' : arr_date_temp);
            })

            // Ambil Data Passanger
            let total_pax = 0;
            let total_adult = row_route.find(".dewasa");
            let total_child = row_route.find(".anak");
            let total_baby = row_route.find(".bayi");
            $(".count-check").each(function(){
                total_pax += Number($(this).val());
            })

            // Ambil data Route untuk dimasukan ke html
            let from = '';
            let to = '';
            let dep = '';
            let rtn = '';
            for (let i = 0; i < arr_airport.length; i++) {
                const arr_temp = arr_airport[i];
                const dep_temp = dept_airport[i];

                from += `<div class="h5">${dep_temp.city_name} - ${dep_temp.airport_name} - ${dep_temp.city_code}</div>`;
                to += `<div class="h5">${arr_temp.city_name} - ${arr_temp.airport_name} - ${arr_temp.city_code}</div>`;

                if(dep_date[i] == '-'){
                    dep += `<div class="h5">${dep_date[i]}</div>`;
                }else{
                    dep += `<div class="h5">${moment(dep_date[i]).format('DD MMM YYYY')}</div>`
                }

                if(arr_date[i] == '-'){
                    rtn += `<div class="h5">${arr_date[i]}</div>`;
                }else{
                    rtn += `<div class="h5">${moment(arr_date[i]).format('DD MMM YYYY')}</div>`
                }
            }

            let dep_rtn;

            if(jenis == 1 || jenis == 2){
                dep_rtn = `
                    <div class="col-lg-2">
                        <label for="">Dep. Date :</label>
                        <div class="h5">${dep}</div>
                    </div>
                    <div class="col-lg-2">
                        <label for="">Rtn. Date :</label>
                        <div class="h5">${rtn}</div>
                    </div>
                `
            }else{
                dep_rtn = `
                    <div class="col-lg-4">
                        <label for="">Dep. Date :</label>
                        <div class="h5">${dep}</div>
                    </div>
                `
            }

            // Menghilangkan HTML yang sudah tidak terpakai
            row_route.attr("editable", false);
            row_route.find(".price-row").prop("hidden", false);
            row_route.find(".route-title").text(`Route ${(Number(idx_route) + 1)} : ${type}`);
            row_route.find(".form-route").remove();

            // Masukin ke HTML
            row_route.append(`
                <div class="row mb-3">
                    <div class="col-lg-3">
                        <label for="">From :</label>
                        ${from}
                    </div>
                    <div class="col-lg-3">
                        <label for="">To :</label>
                        ${to}
                    </div>
                    ${dep_rtn}
                    <div class="col-lg-2">
                        <label for="">Total Pax :</label>
                        <div class="h5 total-pax-title">${total_pax} Pax</div>
                        <input type="hidden" class="total-pax" value="${total_pax}">
                    </div>
                </div>
            `)
            let table = "";
            for (let i = 0; i < total_pax; i++) {
                table += `
                    <tr class="row-table align-middle row-peserta" tpp_id="${e.tpp_id[i]}">
                        <td>
                            <select class="form-select customer-name" name="" id=""></select>
                        </td>
                        <td>
                            <div class="first-name" style="width: 100px;">-</div>
                        </td>
                        <td>
                            <div class="last-name" style="width: 100px;">-</div>
                        </td>
                        <td>
                            <div class="nik">-</div>
                        </td>
                        <td>
                            <div class="birth-date" style="width: 100px;">-</div>
                        </td>
                        <td>
                            <div class="jabatan" style="width: 100px;">-</div>
                        </td>
                        <td>
                            <select class="form-select airline" name="" id="" disabled>
                            </select>
                        </td>
                        <td>
                        <input class="form-control flight" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                        <input class="form-control class-flight" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control booking" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <select class="form-select status" name="" id=""  style="width: 100px;" disabled>
                                <option value="" selected hidden></option>
                                <option value="Active">Active</option>
                                <option value="Issued">Issued</option>
                                <option value="Cancel">Cancel</option>
                                <option value="Expired">Expired</option>
                            </select>
                        </td>
                        <td>
                            <input type="datetime-local" class="form-control time-limit" id="" name="" disabled>
                        </td>
                        <td>
                            <input type="time" class="form-control etd" name="" id="" disabled>
                        </td>
                        <td>
                            <input type="time" class="form-control eta" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control nets-price text-end" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control mark-up text-end" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control selling-price text-end" style="width: 170px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control invoice text-start" style="width: 200px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <input class="form-control remark text-start" style="width: 200px;" type="text" name="" id="" disabled>
                        </td>
                        <td>
                            <button class="btn btn-outline-danger btn-del-row"  tpp_id="${e.tpp_id}" tpr_id="${e.tpr_id}">DELETE</button>
                        </td>
                    </tr>
                `
            }

            row_route.append(`
                <div class="table-responsive mb-3">
                    <table class="table table-striped">
                        <tbody class="table-body-passanger">
                            <tr class="align-middle">
                                <td>#</td>
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>NIK</td>
                                <td>Birth Date</td>
                                <td>Jabatan</td>
                                <td>Airline</td>
                                <td>Flight #</td>
                                <td>Class</td>
                                <td>Booking Code</td>
                                <td style="width: 100px;">Status</td>
                                <td>Time Limit</td>
                                <td>ETD</td>
                                <td>ETA</td>
                                <td>Net Price</td>
                                <td>Mark Up</td>
                                <td>Selling Price</td>
                                <td>Invoice #</td>
                                <td>Remark</td>
                                <td>Action</td>
                            </tr>
                            ${table}
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 mb-3">
                    <button class="btn btn-purple float-end ms-2 btn-save-passanger" tpr_id="${e.tpr_id}">Save</button>
                    <button class="btn btn-secondary float-end btn-add-passanger">Add Passanger</button>
                </div>
            `)

            autocompleteCustomer('.customer-name');

            $(".airline").append(`<option value="${airline.airline_id}">${airline.text}</option>`).trigger("change");
            autocompleteAirlines(".airline");

           // $('.row_route_in_table:last').find(".btn-close-route").attr("tpr_id",e.tpr_id);
           // $('.row_route_in_table:last').find(".float-end btn-add-passanger").attr("tpr_id",e.tpr_id);

            toastr.success("Route Berhasil Ditambahkan", "Berhasil Tambah!");
        },
        error:function(e){
            toastr.error(" ", "Terjadi Kesalahan!");
        }
    });


})

$(document).on("click", ".btn-save-passanger", function(){
    console.log(data_delete);
    var row = $(this).closest(".row_route_in_table").find(".row-peserta");
    var price = $(this).closest(".row_route_in_table").find(".route-price");
    var final = [];
    var tpr_id =$(this).attr("tpr_id");
    row.each(function(){
        data = {
            customer_id : $(this).find(".customer-name").val(),
            first_name : $(this).find(".first-name").html(),
            last_name : $(this).find(".last-name").html(),
            nik : $(this).find(".nik").html(),
            tpr_id : tpr_id,
            tanggal_lahir : $(this).find(".birth-date").html(),
            jabatan : $(this).find(".jabatan").html(),
            airline : $(this).find(".airline").val(),
            flight : $(this).find(".flight").val(),
            class : $(this).find(".class-flight").val(),
            booking_code :$(this).find(".booking").val(),
            status_peserta : $(this).find(".status").val(),
            time_limit : $(this).find(".time_limit").val(),
            etd : $(this).find(".etd").val(),
            eta : $(this).find(".eta").val(),
            net_price : convertToAngka($(this).find(".nets-price").val()),
            mark_up : convertToAngka($(this).find(".mark-up").val()),
            selling_price : convertToAngka($(this).find(".selling-price").val()),
            invoice : $(this).find(".invoice").val(),
            remark : $(this).find(".remark").val(),
            tpp_id : $(this).attr("tpp_id"),
        };
        final.push(data);
    });


    var dt_delete = [];
    data_delete.forEach((item,index) => {
        if(item.tpr_id == tpr_id){
            dt_delete.push(item.tpp_id);
            data_delete.splice(index,1);
        }
    });

    $.ajax({
        url:"/managementPeserta",
        method:"post",
        data:{
            "tpr_id":$(this).attr("tpr_id"),
            "ticketing_project_id":ticketing_project_id,
            "data":JSON.stringify(final),
            "data_delete":JSON.stringify(dt_delete),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            console.log(e);
            price.html(formatRupiah(e.header.tpr_total_sales+"","Rp."));

            row.each(function(index){
                console.log(e.detail[index]);
                $(this).attr("tpp_id",e.detail[index]);
            });

            hitungTotal();
            refreshRoute();
            toastr.success("Data Berhasil Disimpan", "Berhasil Simpan Data!");
        },
        error:function(e){
            toastr.error(" ", "Terjadi Kesalahan!");
        }
    });
});

$(document).on("click", ".btn-close-route", function(){
    $(this).closest(".row_route").remove();

    $.ajax({
        url:"/deleteRoute",
        method:"post",
        data:{
            "trp_id":$(this).attr("tpr_id"),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            refreshRoute();
            toastr.success("Route Berhasil Dihapus", "Berhasil Hapus!");
        },
        error:function(e){
            toastr.error(" ", "Terjadi Kesalahan!");
        }
    });
})

$(document).on("change", ".customer-name", function(){
    let row_parent = $(this).closest(".row-table");
    let data = $(this).select2("data")[0];

    row_parent.find(".first-name").text(data.customer_first_name);
    row_parent.find(".last-name").text(data.customer_last_name);
    row_parent.find(".nik").text(data.customer_nik);
    row_parent.find(".birth-date").text(moment(data.customer_tanggal_lahir).format("D MMM YYYY"))
    row_parent.find(".jabatan").text(data.jabatan);
    row_parent.find(".selling-price").val(formatRupiah(0));
    row_parent.find(".nets-price").val(formatRupiah(0));
    row_parent.find(".mark-up").val(formatRupiah(0));
    row_parent.find(".status").val('Active');
    row_parent.find(".airline").attr('disabled', false);
    row_parent.find(".flight").attr('disabled', false);
    row_parent.find(".class-flight").attr('disabled', false);
    row_parent.find(".booking").attr('disabled', false);
    row_parent.find(".status").attr('disabled', false);
    row_parent.find(".time-limit").attr('disabled', false);
    row_parent.find(".eta").attr('disabled', false);
    row_parent.find(".etd").attr('disabled', false);
    row_parent.find(".selling-price").attr('disabled', false);
    row_parent.find(".nets-price").attr('disabled', false);
    row_parent.find(".invoice").attr('disabled', false);
    row_parent.find(".remark").attr('disabled', false);
    // console.log(data);
})

$(document).on("change", ".time-limit", function(){
    let row_parent = $(this).closest(".row-table");

    let time_limit = moment($(this).val());
    let time_now = moment(getCurrentDateTime());

    if(time_limit.isAfter(time_now)){
        if(row_parent.find(".status").val() == "Active"){
            row_parent.find(".status").val("Expired");
        }
    }else{
        if(row_parent.find(".status").val() == "Expired"){
            row_parent.find(".status").val("Active");
        }
    }
})

$(document).on("keyup", ".selling-price, .nets-price", function(){
    $(this).val(formatRupiah($(this).val(), "Rp."));

    let parent = $(this).closest(".row-table");

    let sell_price = convertToAngka(parent.find(".selling-price").val());
    let nets_price = convertToAngka(parent.find(".nets-price").val());

    parent.find(".mark-up").val(formatRupiah(sell_price - nets_price, "Rp."));
})

$(document).on("blur", ".selling-price, .nets-price", function(){

})

$(document).on("click", ".btn-del-row", function(){
    let row_route = $(this).closest(".row_route");
    let parent = $(this).closest(".row-table");

    parent.remove();
    row_route.find(".total-pax").val(row_route.find(".total-pax").val()-1);
    row_route.find(".total-pax-title").text(row_route.find(".total-pax").val() + " Pax");
    data_delete.push({
        "tpp_id":$(this).attr("tpp_id"),
        "tpr_id":$(this).attr("tpr_id")
    })
    console.log(data_delete);
})

$(document).on("click", ".btn-add-passanger", function(){
    let row_route = $(this).closest(".row_route");
    let rowIndex = $(".row_route").index(row_route);
    console.log(rowIndex);
    let table_row = row_route.find(".table-body-passanger");
    let new_row = $(`
        <tr class="row-table align-middle row-peserta" tpp_id="${null}">
            <td>
                <select class="form-select customer-name" name="" id=""></select>
            </td>
            <td>
                <div class="first-name" style="width: 100px;">-</div>
            </td>
            <td>
                <div class="last-name" style="width: 100px;">-</div>
            </td>
            <td>
                <div class="nik">-</div>
            </td>
            <td>
                <div class="birth-date" style="width: 100px;">-</div>
            </td>
            <td>
                <div class="jabatan" style="width: 100px;">-</div>
            </td>
            <td>
                <select class="form-select airline" name="" id="" disabled>
                </select>
            </td>
            <td>
            <input class="form-control flight" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
            <input class="form-control class-flight" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control booking" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <select class="form-select status" name="" id=""  style="width: 100px;" disabled>
                    <option value="" selected hidden></option>
                    <option value="Active">Active</option>
                    <option value="Issued">Issued</option>
                    <option value="Cancel">Cancel</option>
                    <option value="Expired">Expired</option>
                </select>
            </td>
            <td>
                <input type="datetime-local" class="form-control time-limit" id="" name="" disabled>
            </td>
            <td>
                <input type="time" class="form-control etd" name="" id="" disabled>
            </td>
            <td>
                <input type="time" class="form-control eta" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control nets-price text-end" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control mark-up text-end" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control selling-price text-end" style="width: 170px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control invoice text-start" style="width: 200px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <input class="form-control remark text-start" style="width: 200px;" type="text" name="" id="" disabled>
            </td>
            <td>
                <button class="btn btn-outline-danger btn-del-row" tpp_id="${null}" tpr_id="${null}">DELETE</button>
            </td>
        </tr>
    `)
    new_row.find(".airline").append(`<option value="${list_arilines[rowIndex].id}">${list_arilines[rowIndex].text}</option>`).trigger("change");
    table_row.append(new_row);
    autocompleteCustomer('.customer-name');
    autocompleteAirlines(".airline");

    row_route.find(".total-pax").val(Number(row_route.find(".total-pax").val())+1);
    row_route.find(".total-pax-title").text(row_route.find(".total-pax").val() + " Pax");
})

$(document).on("click", ".btn-slide-show", function(){
    let row_route = $(this).closest(".row_route");

    if($(this).hasClass("bi-caret-right-fill")){
        $(this).removeClass("bi-caret-right-fill");
        $(this).addClass("bi-caret-down-fill");
    }else{
        $(this).removeClass("bi-caret-down-fill");
        $(this).addClass("bi-caret-right-fill");
    }
    row_route.find(".table-responsive").slideToggle();
})
