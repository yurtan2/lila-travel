var mode=1; //1= insert, 2= delete

get_data();

//inisialisasi Editor

var quill = new Quill('#syarat', {
    theme: 'snow'
  });

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Passport");
    $('.add').val("");
    quill.pasteHTML("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Passport");
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();

    $('#nama').val(data.passport_name);
    quill.pasteHTML(data.passport_syarat);
    $('#modalInsert').attr("edit_id",data.passport_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-view",function(){
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();
    $('#view_passport_syarat').html(`${data.passport_syarat}`);
    $('#modalView').attr("passport_id",data.passport_id);
    $('#modalView').modal("show");
})


$(document).on("click",".btn-delete",function(){
    var data = $('#tb-document').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.passport_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataPassport?nama="+ $('#nama_filter').val();
})


$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfPassport/"+$('#modalView').attr("passport_id");
})


$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deletePassport",
        method:"post",
        data:{
            passport_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Jenis Passport sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var syarat = quill.root.innerHTML;
    var valid = 1;
    var url = "insertPassport";
    var data = {
        nama:nama,
        syarat:syarat,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");
    console.log(syarat);
    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(syarat==""||syarat==null){
        $('#syarat').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editPassport";
        data.passport_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Jenis Passport sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-document").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_passport",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view'>View Syarat</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "passport_name",width:"20%"},
            { data: "passport_syarat_text",width:"20%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-document").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
