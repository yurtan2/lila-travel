var mode=1; //1= insert, 2= delete
var list_data = [];
var list_delete = [];

get_data();
autocompleteProductCategory('#product_category','#modalInsert');
autocompleteProductCategory('#product_category_filter');


$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#product_category_filter').val("").trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    list_data=[];
    refreshExclusion();
    $('#title').html("Tambah Exclusion");
    $('.add').val("");
    $('#product_category').empty().trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Exclusion");
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    $('#product_category').append(`<option value="${data.pr_category_id}">${data.pr_category_nama}</option>`).trigger("change");
    $('#nama').val(data.term_name);
    list_data= data.detail;
    refreshExclusion();
    $('#modalInsert').attr("edit_id",data.term_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-view",function(){
    mode=2;
    $('#title').html("Detail Exclusion");
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#view_product').html(`${data.pr_category_nama}`);
    $('#view_term_name').html(`${data.term_name}`);
    $('#view_term_detail').html(``);
    data.detail.forEach((item,index) => {
        $('#view_term_detail').append(`${index+1}. ${item.inclusion_detail}<br>`);
    });

    $('#modalView').attr("view_id",data.terms_condition_id);
    $('#modalView').attr("index",$(this).attr("index"));
    $('#modalView').modal("show");
})

$(document).on("click",".btn-direct-edit",function(){
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    $('.modal').modal("hide");
    $('#btnEdit'+$('#modalView').attr("index")).click();
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.term_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataExclusion?nama="+ $('#nama_filter').val()+"&pr_category_id="+$('#product_category_filter').val();
})

$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfExclusion/"+$('#modalView').attr("view_id");
})


$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");
    $.ajax({
        url:"/master/deleteExclusion",
        method:"post",
        data:{
            term_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Terms & Condition Name sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click",".btn-add-inclusion",function(){
    $('.has-danger').removeClass("has-danger");
    var nama = $('#insert-exclusion').val();
    var valid=1;

    if(nama==""||nama==null){
        $('#insert-exclusion').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    list_data.push({
        "term_detail_id":"-",
        "inclusion_detail":nama
    });
    $('#insert-exclusion').val("");
    refreshExclusion();
})

function refreshExclusion() {
    $('#body-exclusion').html("");
    list_data.forEach((item,index) => {
        $('#body-exclusion').append(`
            <tr>
                <td>${item.inclusion_detail}</td>
                <td class="text-center"><button class="btn btn-outline-danger btn-sm me-2 btn-delete-inclusion" index="${index}" type="button">Delete</button></td>
            </tr>
        `)
    });
}

$(document).on("click",".btn-delete-inclusion",function(){
    var idx = $(this).attr("index");
    if(list_data[idx].term_detail_id!="-")list_delete.push(list_data[idx]);
    list_data.splice(idx,1);
    refreshExclusion();
});

$(document).on("click","#btn-insert",function(){
    var category = $('#product_category').val();
    var nama = $('#nama').val();
    var valid = 1;
    var url = "insertExclusion";

    $('.has-danger').removeClass("has-danger");
    $('.error').removeClass("error");

    $('#error_product').html("");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(category==""||category==null){
        $('#product_category').addClass("error");
        $('#error_product').html("*Harap isi field ini");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if($('#insert-exclusion').val()!=""&&$('#insert-exclusion').val()!=null){
        list_data.push({
            "term_detail_id":"-",
            "inclusion_detail":$('#insert-exclusion').val()
        });
    }

    if(list_data.length<=0){
        toastr.error("Harap tambahkan detail exclusion!","Terjadi Kesalahan");
        return false;
    }

    var data = {
        nama:nama,
        pr_category_id:category,
        jenis:2,
        detail:JSON.stringify(list_data),
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    if(mode==2){
        url = "editExclusion";
        data.term_id = $('#modalInsert').attr("edit_id");
        data.list_delete = JSON.stringify(list_delete);
    }


    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Exclusion Name sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-term").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_exclusion",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                pr_category_id:$('#product_category_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view' index='${i}'>View</button>
                        <button class='btn btn-warning btn-edit' id="btnEdit${i}">Edit</button>
                        <button class='btn btn-danger btn-delete' >Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "term_name",width:"20%"},
            { data: "pr_category_nama",width:"20%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-term").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
