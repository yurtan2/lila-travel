get_data();


$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#filter-select-category').val("-1").trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Displayer");
    $('.add').val("");
    $('#select-category').val("Flash Sale").trigger("change");
    $('#input-validity').val(getCurrentDateTime()).trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})


$(document).on("click",".btn-delete",function(){
    var data = $('#tb-displayer').DataTable().row($(this).parents('tr')).data();

    $('#modalDelete').attr("delete_id",data.displayer_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href=`/master/exportDisplayer?nama=${$('#filter-select-category').val()}&validty=${$('#filter-input-validity').val()}`;
})


$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfVisa/"+$('#modalView').attr("visa_id");
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteDisplayer",
        method:"post",
        data:{
            displayer_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#input-title').val();
    var order = $('#input-order').val();
    var valid = 1;
    var url = "insertDisplayer";
    var data = {
        displayer_title:nama,
        displayer_order:order,
        displayer_validity:$('#input-validity').val(),
        displayer_category:$('#select-category').val(),
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#input-title').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(order==""||order==null){
        $('#input-order').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editDisplayer";
        data.displayer_id = $('#displayer_id').val();
        data.displayer_total = detail.length;
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2){
                    localStorage.setItem("success","Berhasil Edit Data!");
                    
                    window.location.href = "/master/displayer";
                }
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Country sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-displayer").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_displayer",
            type: "get",
            data:{
                displayer_category:$('#filter-select-category').val(),
                displayer_validity:$('#filter-input-validity').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <a href="/master/displayer_detail/${json.data[i].displayer_id}" class='btn btn-warning btn-edit'>Edit</a>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    
                    json.data[i].ordering = `<div style="color:rgb(124,77,255); padding-left: 10px; float: left; font-size: 20px; cursor: pointer;" title="change display order">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                    </div>`;
                    json.data[i].displayer_validity_text = moment( json.data[i].displayer_validity).format('MM/DD/YYYY HH:mm');
                    $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                }
                
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "displayer_title"},
            { data: "displayer_category"},
            { data: "displayer_validity_text"},
            { data: "displayer_total",className:"text-center"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-displayer").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
     
}
