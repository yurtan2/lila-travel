autocompleteCustomer("#customer_id","#modalInsert",null,1);
var jumlahMember=0;
var dataMember = '';

$(document).ready(function(){

})

$(document).on("click","#btn-add-member",function(){
    addMember();
});

function calculateAge(birthDateString) {
    const birthDate = new Date(birthDateString);
    const today = new Date();

    // Get days of each month
    const daysInMonth = [31, (today.getFullYear() % 4 === 0 ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let years = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    let days = today.getDate() - birthDate.getDate();

    // Handle negative days (borrow days from the month)
    if (days < 0) {
        months--;
        days += daysInMonth[(today.getMonth() - 1 + 12) % 12]; // Get last month days count
    }

    // Handle negative months (borrow months from the year)
    if (months < 0) {
        years--;
        months += 12;
    }

    return `${years} Tahun, ${months} Bulan, ${days} Hari`;
}

function addMember() {
    var btn_delete = ` <button class=" btn btn-outline-danger mt-4" id="btn-delete-member">Delete</button>`;
    var element = `<div class="row row-member mt-2" index="${jumlahMember}">
                        <div class="col-5">
                            <label>Member Name</label>
                            <select class="form-select member-name" name="" id="member${jumlahMember}" class="form-select"></select>
                            <input type="hidden"  id="member_id${jumlahMember}">
                        </div>
                        <div class="col-3">
                            <label>Member Category</label>
                            <select class="form-select category_member" class="" id="type${jumlahMember}" class="form-select">
                                <option selected>Aunt</option>
                                <option>Brother</option>
                                <option>Cousin (Female)</option>
                                <option>Cousin (Male)</option>
                                <option>Daughter</option>
                                <option>Father</option>
                                <option>Grandfather</option>
                                <option>Grandmother</option>
                                <option>Husband</option>
                                <option>Mother</option>
                                <option>Nephew</option>
                                <option>Niece</option>
                                <option>Sister</option>
                                <option>Son</option>
                                <option>Uncle</option>
                                <option>Wife</option>
                            </select>
                        </div>
                        <div class="col-2 d-grid gap-2 p-2 pt-1 text-center">
                            <button class=" btn btn-outline-secondary mt-4 detail-btn" index="${jumlahMember}" disabled id="">Detail</button>
                        </div>
                        <div class="col-2 d-grid gap-2 p-2 pt-1 text-center">
                           ${btn_delete}
                        </div>
                        <input type="hidden" class="customer_family_tree_detail_id" id="customer_family_tree_detail_id${jumlahMember}" value="-1">
                    </div>`;
    $('#container_member').append(element);
    autocompleteCustomer(`#member${jumlahMember}`,"#modalInsert",null,1);
    jumlahMember++;
}

$(document).on("click", "#btn-delete-member", function(){
    $(this).parent().parent().remove();
})

$(document).on("change", "#customer_tanggal_lahir", function(){
    $('#umur').val(calculateAge($(this).val()));
})

$(document).on("change", "#customer_id",function(){
    var data = $(this).closest('.row').find('#customer_id').select2("data")[0];
    if(data&&data.customer_gender=="Female"){
        $(this).empty().trigger("change");
        toastr.error("Head Of Family hanya bisa Gender Laki","Gagal Insert");
        return false;
    }
    if($(this).val() != null){
        $(this).closest('.row').find('.detail-head-btn').prop('disabled',false);
    }else{
        $(this).closest('.row').find('.detail-head-btn').prop('disabled',true);
    }
})

$(document).on("change", ".member-name", function(){
    if($(this).val() != ''){
        $(this).closest('.row').find('.detail-btn').prop('disabled',false);
    }else{
        $(this).closest('.row').find('.detail-btn').prop('disabled',true);
    }
    console.log($(this).select2("data")[0]);
})

$(document).on("click", ".detail-head-btn", function(){
    getDetail($('#customer_id').val());
});

function getDetail(id) {
    $.ajax({
        url:"/master/getDetailFamilyTree",
        method:"get",
        data:{
            customer_id :id
        },
        success:function(e){
            $('#titleView').html("Detail Customer");
            var data = JSON.parse(e);
            console.log(e);
            $('#view_name').html(data.customer_first_name+" "+data.customer_last_name);
            $('#view_phone').html(data.customer_nomor);
            $('#view_gender').html(data.customer_gender);
            $('#view_nik').html(data.customer_nik);
            $('#view_tanggal_lahir').html(data.customer_tanggal_lahir);
            $('#view_age').html(data.customer_umur+" Tahun");
            $('#view_address').html(data.customer_alamat);
            $('#view_referensi').html(data.customer_referensi_makanan);
            $('#view_passport_name').html(data.customer_passport_number);
            $('#view_poi').html(data.customer_poi);
            $('#view_doi').html(data.customer_doi);
            $('#view_doe').html(data.customer_doe);
            $('#view_ktp').attr("src",public+data.customer_ktp).attr("file_name",data.customer_ktp);
            $('#view_visa').attr("src",public+data.customer_visa).attr("file_name",data.customer_visa);
            $('#view_passport').attr("src",public+data.customer_passport).attr("file_name",data.customer_passport);
            $('#modalView').modal("show");
        }
    });
}
$(document).on("click", ".detail-btn", function(){
    getDetail($('#member'+$(this).attr("index")).val());
})

get_data();
var mode=1; //1= insert, 2= delete
var file_name = "";

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val(null).trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    jumlahMember=0;
    $('#customer_id').empty().trigger("change");
    $('#container_member').html(" ");
    addMember();
    $('#title').html("Tambah Family Tree");
    $('.add').val("");
    $('input[type="file"]').val(null).trigger("change");
    $('.file').val(null).trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    jumlahMember=0;
    $('#customer_id').empty().trigger("change");
    $('#container_member').html(" ");
    $('#title').html("Edit Family Tree");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();

    data.detail.forEach((item,index) => {
        addMember();
        var nm = item.customer_first_name+" "+item.customer_last_name;
 
        if(item.customer_status==0) nm +=" (Deleted)";
        
        $('#member'+index).append(`<option value='${item.customer_id}'>${nm}</option>`).trigger("change");
       
        //$(//'#member'+index).select2('val', item,true);
        $('#type'+index).val(`${item.customer_family_tree_detail_position}`).trigger("change");
        $('#customer_family_tree_detail_id'+index).val(`${item.customer_family_tree_detail_id}`);
        $('#customer_family_tree_detail_id'+index).attr("data",`${item}`);
    });

    $('#customer_id').append(`<option value='${data.customer_id}'>${data.customer_name}</option>`).trigger("change");
    $('#modalInsert').attr("edit_id",data.customer_family_tree_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.customer_family_tree_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click",".btn-view",function(){
    $('#titleView').html("Detail Family Tree");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#view_head').html(data.customer_first_name+" "+data.customer_last_name);
    $('#container_member_view').html(" ");
    data.detail.forEach(item => {
        $('#container_member_view').append(`
            <div class="col-6">
                <p> ${item.customer_first_name+" "+item.customer_last_name}</p>
            </div>
            <div class="col-6">
                <p>${item.customer_family_tree_detail_position}</p>
            </div>
            <br><br>
        `);
    });
    /*


    $('#view_phone').html(data.customer_nomor);
    $('#view_gender').html(data.customer_gender);
    $('#view_nik').html(data.customer_nik);
    $('#view_tanggal_lahir').html(data.customer_tanggal_lahir);
    $('#view_age').html(data.customer_umur+" Tahun");
    $('#view_address').html(data.customer_alamat);
    $('#view_referensi').html(data.customer_referensi_makanan);
    $('#view_passport_name').html(data.customer_passport_number);
    $('#view_poi').html(data.customer_poi);
    $('#view_doi').html(data.customer_doi);
    $('#view_doe').html(data.customer_doe);
    $('#view_ktp').attr("src",public+data.customer_ktp).attr("file_name",data.customer_ktp);
    $('#view_visa').attr("src",public+data.customer_visa).attr("file_name",data.customer_visa);
    $('#view_passport').attr("src",public+data.customer_passport).attr("file_name",data.customer_passport);
    */$('#modalViewHead').modal("show");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataFamilyTree?customer_name="+ $('#filter_customer_name').val()+"&&customer_tanggal_lahir="+ $('#filter_customer_tanggal_lahir').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteFamilyTree",
        method:"post",
        data:{
            customer_family_tree_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function isValid() {
    cek = true;
    if($("#customer_id").select2("data").length == 0){
        cek = false;
    }

    $(".member-name").each(function(){
        if($(this).select2("data").length == 0) cek = false;
    })

    return cek;
}


$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "insertFamilyTree";
    var gender ="Male";

    if(!isValid()){
        toastr.error("Pastikan untuk memeriksa semua nama customer!","Terjadi Kesalahan");
        return false;
    }

    $('.has-danger').removeClass("has-danger");

    $('#formInsert input').each(function(){
        if($(this).val()==null||$(this).val()==""){
            $(this).closest('.form-group').addClass("has-danger");
            $(this).closest('.form-floating').addClass("has-danger");
            valid=0;
        }
    });

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if($(".row-member").length == 0){
        toastr.error("Minimal harus ada 1 Member","Terjadi Kesalahan");
        return;
    }

    var data = {
        customer_id :$('#customer_id').val(),
        detail :[],
        _token :token
    };

    $('.row-member').each(function(index){
        console.log($('#member'+index).val());
        data.detail.push({
            "customer_family_tree_detail_id": $('#customer_family_tree_detail_id'+index).val(),
            "customer_id": $('#member'+index).val(),
            "customer_family_tree_detail_position":$('#type'+index).val(),
        });
    });
    data.detail = JSON.stringify(data.detail);

    if(mode==2){
        url = "editFamilyTree";
        data.customer_family_tree_id = $('#modalInsert').attr("edit_id");
    }

    $('#btn-insert').append(`
        <div class="spinner-border text-light" role="status" style="width:10px;height:10px">
            <span class="visually-hidden">Loading...</span>
        </div>`);
    $('#btn-insert').attr("disabled","disabled");

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();

                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_family_tree",
            type: "get",
            data:{
                customer_name:$('#filter_customer_name').val(),
                customer_family_id:$('#filter_customer_family_id').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button type="button" class="btn btn-info btn-view">View</button>
                        <button type="button" class="btn btn-warning btn-edit">Edit</button>
                        <button type="button" class="btn btn-danger btn-delete">Delete</button>
                    `;
                    json.data[i].customer_name = json.data[i].customer_first_name+" "+json.data[i].customer_last_name;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                    json.data[i].customer_tanggal_lahir_text = moment(json.data[i].customer_tanggal_lahir).format('DD/MM/YYYY');
                    json.data[i].family_id = 'FT' + json.data[i].customer_family_tree_id.toString().padStart(5, '0');
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "family_id"},
            { data: "customer_name"},
            { data: "total"},
            { data: "action" ,className:"text-center",width:"20%"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".detail",function(){
    var name = $(this).attr("name")+"_"+$('#view_name').html();
    $('#detail_dokumen').attr("src",$(this).attr("src"));
    $('#btn-download').attr("href",$(this).attr("src"));
    $('#btn-download').attr("download",name);
    $("#modalViewDetail").modal("show");
})

$(document).on("click","#btn-download",function(){
    e.preventDefault();
    /*
    $.ajax({
        url:"/master/downloadDocument",
        method:"post",
        data:{
            nama:$(this).attr("nama")
        },
        success:function(data){
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'myfile.pdf';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
*/

})
