var mode=1; //1= insert, 2= delete

get_data();
autocompleteProductCategory('#product_category','#modalInsert');
autocompleteProductCategory('#product_category_filter');

//inisialisasi Editor
var quill = new Quill('#detail', {
    theme: 'snow'
  });

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#product_category_filter').val("").trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Term & Condition");
    $('.add').val("");
    $('#product_category_filter').val("").trigger("change");
    quill.pasteHTML("");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Terms & Condition");
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#product_category').append(`<option value="${data.pr_category_id}">${data.pr_category_nama}</option>`).trigger("change");
    $('#nama').val(data.terms_condition_name);
    quill.pasteHTML(data.terms_condition_detail);

    $('#modalInsert').attr("edit_id",data.terms_condition_id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-view",function(){
    mode=2;
    $('#title').html("Detail Terms & Condition");
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();

    $('#view_product').html(`${data.pr_category_nama}`);
    $('#view_term_name').html(`${data.terms_condition_name}`);
    $('#view_term_detail').html(`${data.terms_condition_detail}`);
    $('#modalView').attr("view_id",data.terms_condition_id);
    $('#modalView').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-term').DataTable().row($(this).parents('tr')).data();
    $('#modalDelete').attr("delete_id",data.terms_condition_id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataTerms?nama="+ $('#nama_filter').val()+"&pr_category_id="+$('#product_category_filter').val();
})

$(document).on("click","#btn-export-pdf",function(){
    window.location.href="/exportPdfTerms/"+$('#modalView').attr("view_id");
})


$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteTerm",
        method:"post",
        data:{
            terms_condition_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Terms & Condition Name sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var category = $('#product_category').val();
    var nama = $('#nama').val();
    var detail = quill.root.innerHTML;
    var valid = 1;
    var url = "insertTerm";
    var data = {
        nama:nama,
        pr_category_id:category,
        detail:detail,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(category==""||category==null){
        $('#country').closest('.form-group').addClass("has-danger");
        valid=0;
    }

    if(detail==""||detail==null){
        $('#syarat').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editTerm";
        data.terms_condition_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Terms & Condition Name sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-term").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_term",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                pr_category_id:$('#product_category_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button class='btn btn-info btn-view'>View</button>
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "terms_condition_name",width:"20%"},
            { data: "pr_category_nama",width:"20%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-term").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
