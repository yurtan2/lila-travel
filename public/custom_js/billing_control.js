$(function(){
    init();
})

function init() {
    let today = moment(getCurrentDate()).format("YYYY-MM");
    $("#input_month").val(today).trigger("change");
}

$(document).on("change", "#input_month", function(){
    $("#table-body").html('');
    const [year, month] = $(this).val().split('-').map(Number);

    const startOfMonth = new Date(year, month - 1, 1); // Month is 0-indexed
    const endOfMonth = new Date(year, month, 0);

    const weeks = [];

    let counter = 1;
    let string = '';
    for(let day = startOfMonth; day <= endOfMonth; day.setDate(day.getDate() + 1)) {
        if(string == ''){
            string += moment(new Date(day)).format("DD");
        }
        if(day.getDay() === 6) {
            string += " - " + moment(new Date(day)).format("DD");
            $("#table-body").append(`
                <tr>
                    <td>${counter++}</td>
                    <td>${string}</td>
                    <td>0 Bill</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-rounded btn-detail">detail</button>
                    </td>
                </tr>
            `)
            string = '';
        }
    }
})

$(document).on("click", ".btn-detail, .btn-card", function(){
    $('#modalInsert').modal("show");
})
