var insert_data = [];
var delete_data = [];

$(document).ready(function(){
    refreshDetail();
});

autocompleteTour("#select-tour",null,data);

$(document).on("click",".btn-tour",function(){
   var temp  = $('#select-tour').select2("data")[0];
   data.push(temp);
   insert_data.push(temp);
   $('#select-tour').empty().trigger("change");
   refreshDetail();
})


function refreshDetail() {
    $('#tb-detail').html("");

    data.forEach((item,index) => {
        $('#tb-detail').append(`
            <tr>
                <td>${item.tour_name}</td>
                <td>${item.tour_category}</td>
                <td>${item.tour_jenis==1?"Fix Departure":"Fit Departure"}</td>
                <td>${moment(item.tour_date_start).format("DD MMM YYYY")+" - "+moment(item.tour_date_end).format("DD MMM YYYY")}</td>
                <td>${ item.status==1?"Active":item.status==2?"Expired":item.status==4?"Not Publish":"Draf"}</td>
                <td><button class="btn btn-outline-danger btn-delete-detail" index="${index}">Del</button></td>
            </tr>
        `);
    });
}

$(document).on("change", "#select-tour", function(){
    let isEmpty = ($(this).val() == '');
    $(".btn-tour").prop("disabled", isEmpty);

})

$(document).on("click",".btn-delete-detail",function(){
    var idx = $(this).attr("index");
    delete_data.push(data[idx]);
    data.splice(idx,1);
    autocompleteTour("#select-tour",null,data);
    refreshDetail();
});

$(document).on("click","#btn-insert",function(){
    var nama = $('#input-title').val();
    var order = $('#input-order').val();
    var valid = 1;
    var url = "";
    var data = {
        displayer_title:nama,
        displayer_order:order,
        displayer_validity:$('#input-validity').val(),
        displayer_category:$('#select-category').val(),
        '_token': token
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#input-title').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(order==""||order==null){
        $('#input-order').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "updateDisplayerDetail";
        data.displayer_id = $('#displayer_id').val();
        data.insert = JSON.stringify(insert_data);
        data.delete = JSON.stringify(delete_data);
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            localStorage.setItem("success","Berhasil Edit Data!");
            window.location.href = "/master/displayer";
        }
    });
})
