get_data();
var view_log_id;
$("#credit-type").trigger("change");
$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val(null).trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah Corporate");
    $('#modalEdit').modal("show");
})

$(document).on('hidden.bs.modal', '#modalEdit', function(e){
    $('.is-invalid').removeClass('is-invalid');
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit Data");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    // alert(public + data.corporate_logo)
    $("#corporate-image").attr("src", public + data.corporate_logo);
    $('#corporate_name').text(data.corporate_name);
    $('#corporate_email').text(data.corporate_email);
    $("#credit-limit").val(formatRupiah(data.corporate_credit_limit));
    $("#credit-used").val("Rp."+formatRupiah(data.corporate_credit_used));

    if(data.corporate_due_type != "" && data.corporate_due_type != null){
        $("#credit-type").val(data.corporate_due_type);
    }
    $("#credit-type").trigger('change');

    if(data.corporate_due_date != "" && data.corporate_due_date != null){
        $("#credit-target").val(data.corporate_due_date);
    }

    $('#modalEdit').attr("edit_id",data.corporate_id);
    $('#modalEdit').modal("show");
})

$(document).on("change", "#credit-type", function(){
    $("#credit-target").html('');
    $("#credit-target").prop("disabled", false);
    if($(this).val() == "Day"){
        let target = '';
        for (let i = 1; i <= 14; i++) {
            target += `<option value="${i} days">${i} Days</option>`
        }
        $("#credit-target").append(`
            ${target}
        `);
    }else if ($(this).val() == "Week"){
        $("#credit-target").append(`
            <option value="Monday">Monday</option>
            <option value="Tuesday">Tuesday</option>
            <option value="Wednesday">Wednesday</option>
            <option value="Thursday">Thursday</option>
            <option value="Friday">Friday</option>
            <option value="Saturday">Saturday</option>
            <option value="Sunday">Sunday</option>
        `);
    }else if ($(this).val() == "Month"){
        let target = '';
        for (let i = 1; i <= 28; i++) {
            target += `<option value="On ${i}">On ${i}</option>`
        }
        $("#credit-target").append(`
            ${target}
        `);
    }else{
        $("#credit-target").prop("disabled", true);
    }
})

$(document).on("click",".btn-list",function(){
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();

    $('#modalList').modal("show");
})

$(document).on("click",".btn-view",function(){
    $('#title').html("Detail Customer");
    var data = $('#tb-category').DataTable().row($(this).parents('tr')).data();
    view_log_id = data.corporate_id;
    get_data_log();
    // var own = hide= "No";
    // if(data.corporate_hide_logo==1)hide="Yes";
    // if(data.corporate_use_own_logo==1)own="Yes";

    // $('#view_hide_logo').html(hide);
    // $('#view_own').html(own);
    // $('#view_name').html(data.corporate_name);
    // $('#view_address').html(data.corporate_address);
    // $('#view_admin_fee').html(formatRupiah(data.corporate_admin_fee+"","Rp."));
    // $('#view_email').html(data.corporate_email);
    // $('#view_nomor').html(data.corporate_prefix+data.corporate_nomor);

    // $('#view_logo').attr("src",public+data.corporate_logo).attr("file_name",data.corporate_logo);
    // $('#view_akta').attr("src",public+data.corporate_akta).attr("file_name",data.corporate_akta);
    // $('#view_nib').attr("src",public+data.corporate_nib).attr("file_name",data.corporate_nib);
    // $('#view_sk').attr("src",public+data.corporate_sk).attr("file_name",data.corporate_sk);
    // $('#view_npwp').attr("src",public+data.corporate_npwp).attr("file_name",data.corporate_npwp);
    // $('#view_pic').html(" ");
    // data.detail_pic.forEach(item => {
    //     $('#view_pic').append(`
    //         <tr>
    //             <td>${item.customer_first_name+" "+item.customer_last_name}</td>
    //             <td>${item.corporate_position_name}</td>
    //         </tr>
    //     `);
    // });
    $('#modalView').modal("show");
})

$(document).on("keyup", '#credit-limit', function(){
    $(this).val(formatRupiah($(this).val()));
})

$(document).on("click","#btn-export",function(){
    window.location.href="/master/exportDataCorporate?customer_name="+ $('#filter_customer_name').val()+"&&customer_tanggal_lahir="+ $('#filter_customer_tanggal_lahir').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCorporate",
        method:"post",
        data:{
            corporate_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Customer sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("input", '.need-check', function(){
    let isEmpty = $(this).val() == '';
    $(this).toggleClass('is-invalid', isEmpty);
})

$(document).on("click","#btn-insert",function(){
    var valid = 1;
    var url = "";

    $('.has-danger').removeClass("has-danger");

    if($('#credit-limit').val()==""&&$('#credit-limit').val()==null){
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    var param = new FormData();
    param.append('corporate_credit_limit', convertToAngka($('#credit-limit').val()));
    param.append('corporate_due_type',$('#credit-type').val());
    param.append('corporate_due_date',$('#credit-target').val());
    param.append('_token',$('meta[name="csrf-token"]').attr('content'));

    if(mode==2){
        url = "editCorporateCredit";
        param.append( 'corporate_id', $('#modalEdit').attr("edit_id"));
    }

    $('#btn-insert').append(`
        <div class="spinner-border text-light" role="status" style="width:10px;height:10px">
            <span class="visually-hidden">Loading...</span>
        </div>`);
    $('#btn-insert').attr("disabled","disabled");

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:param,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success:function(e){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
            $('.modal').modal("hide");
            get_data();

        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
})

function get_data() {
    $("#tb-category").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_corporate",
            type: "get",
            data:{
                filter_corporate_nomor:$('#filter_corporate_nomor').val(),
                filter_corporate_name:$('#filter_corporate_name').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    json.data[i].action = `
                        <button type="button" class="btn btn-warning btn-edit">Edit</button>
                        <button type="button" class="btn btn-danger btn-list">List</button>
                        <button type="button" class="btn btn-info btn-view">Logs</button>
                    `;
                    json.data[i].credit_limit = "Rp." + formatRupiah(json.data[i].corporate_credit_limit);
                    json.data[i].credit_used = "Rp." + formatRupiah(json.data[i].corporate_credit_used);
                    json.data[i].credit_available = "Rp." + formatRupiah(json.data[i].corporate_credit_available);
                    let user = (json.data[i].updated_by != null && json.data[i].updated_by != ""? " - " + json.data[i].updated_by : '');
                    json.data[i].created_at = moment(json.data[i].created_at).format('DD/MM/YYYY HH:mm');
                    json.data[i].updated_at = moment(json.data[i].updated_at).format('DD/MM/YYYY HH:mm')+ user;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "created_at",width:"15%"},
            { data: "updated_at",width:"20%"},
            { data: "corporate_name"},
            { data: "credit_limit"},
            { data: "credit_used"},
            { data: "credit_available"},
            { data: "corporate_due_type"},
            { data: "corporate_due_date"},
            { data: "action" ,className:"text-center",width:"20%"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-category").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

function get_data_log() {
    $("#tblog").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        data:{
            nama:null,
            kota:null
        },
        ajax: {
            url: "/master/get_data_corporate_log",
            type: "get",
            data:{
                corporate_log_category:1,
                corporate_id:view_log_id,
                //filter_corporate_name:$('#filter_corporate_name').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {

                    json.data[i].corporate_log_category_text = "Credit Limit";
                    json.data[i].corporate_log_time = moment(json.data[i].corporate_log_time).format('DD/MM/YYYY HH:mm');
                    if(json.data[i].corporate_log_amount<0)json.data[i].corporate_log_amount_text = `<label class="text-danger">- Rp.${formatRupiah(json.data[i].corporate_log_amount)}</label>`;
                    else json.data[i].corporate_log_amount_text = `<label class="text-success">+ Rp.${formatRupiah(json.data[i].corporate_log_amount)}</label>`;
                }
                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [
            { data: "corporate_log_category_text"},
            { data: "corporate_log_amount_text"},
            { data: "corporate_log_user"},
            { data: "corporate_log_time"},
            { data: "corporate_log_due_date_type"},
            { data: "corporate_log_due_date"},
            { data: "corporate_log_message"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tblog").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}

$(document).on("click",".detail",function(){
    var name = $(this).attr("name")+"_"+$('#view_name').html();
    $('#detail_dokumen').attr("src",$(this).attr("src"));
    $('#btn-download').attr("href",$(this).attr("src"));
    $('#btn-download').attr("download",name);
    $("#modalViewDetail").modal("show");
})

$(document).on("click", "#btn-report", function(){
    $("#modalReport").modal("show");
})

$(document).on("click","#btn-download",function(){
    e.preventDefault();
    /*
    $.ajax({
        url:"/master/downloadDocument",
        method:"post",
        data:{
            nama:$(this).attr("nama")
        },
        success:function(data){
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'myfile.pdf';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        },
        error:function(){
            $('#btn-insert').html("Simpan");
            $('#btn-insert').removeAttr("disabled");
        }
    });
*/

})

