console.log("test");
$('#form_login').submit(function(event){
    var valid=1;
    $('.has-danger').removeClass("has-danger");

    if($('#input-email').val()==null||$('#input-email').val()==""){
        $('#input-email').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if($('#password-input').val()==null||$('#password-input').val()==""){
        $('#password-input').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    $.ajax({
        url:"/mekanismeLogin",
        method:"post",
        data:{
            'email': $('#input-email').val(),
            'password': $('#password-input').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
               window.location.href="/master/product_category";
            }
            else{
                toastr.error("Silahkan cek kembali email atau password anda","Gagal Login");
            }
        }
    });
    return false;
});
