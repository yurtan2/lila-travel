get_data();
var mode=1; //1= insert, 2= delete

autocompleteCountry('#country','#modalInsert');
autocompleteCountry('#country_filter');
autocompleteStates('#state_filter',null,null);

$(document).on("change","#country",function(){
    autocompleteStates('#states','#modalInsert',$(this).val());
});

$(document).on("change","#country_filter",function(){
    $('#state_filter').empty().trigger("change");
    autocompleteStates('#state_filter',null,$(this).val());
});

$(document).on("click","#btn-search",function(){
    get_data();
});

$(document).on("click","#btn-reset",function(){
    $('#inner-filter input').val("");
    $('#inner-filter select').val("").trigger("change");
    get_data();
})

$(document).on("click",".btn-add",function(){
    mode=1;
    $('#title').html("Tambah City");
    $('.add').val("");
    $('#country').empty().trigger("change");
    $('#states').empty().trigger("change");
    $('.has-danger').removeClass("has-danger");
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-edit",function(){
    mode=2;
    $('#title').html("Edit City");
    $('#country').empty().trigger("change");
    $('#states').empty().trigger("change");
    var data = $('#tb-city').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#nama').val(data.name);
    $('#country').append(`<option value="${data.country_id}">${data.country_name}</option>`).trigger("change");
    $('#states').append(`<option value="${data.state_id}">${data.state_name}</option>`).trigger("change");
    $('#modalInsert').attr("edit_id",data.id);
    $('#modalInsert').modal("show");
})

$(document).on("click",".btn-delete",function(){
    var data = $('#tb-city').DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $('#modalDelete').attr("delete_id",data.id);
    showModalDelete("Yakin ingin menghapus data ini?","btn-konfirmasi-delete");
})

$(document).on("click","#btn-export",function(){

    window.location.href="/master/exportDataCity?nama="+ $('#nama_filter').val()+"&country_id="+$('#country_id_filter').val()+"&state_id="+$('#state_filter').val();
})

$(document).on("click","#btn-konfirmasi-delete",function(){
    var data =  $('#modalDelete').attr("delete_id");

    $.ajax({
        url:"/master/deleteCity",
        method:"post",
        data:{
            city_id :data,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(e){
            if(e==1){
                toastr.success("Berhasil delete data","Berhasil Delete");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Category sudah terdaftar","Gagal Insert");
            }
        }
    });
})

$(document).on("click","#btn-insert",function(){
    var nama = $('#nama').val();
    var states = $('#states').val();
    var country = $('#country').val();
    var valid = 1;
    var url = "insertCity";
    var data = {
        nama:nama,
        states:states,
        country_id:country,
        '_token': $('meta[name="csrf-token"]').attr('content')
    };

    $('.has-danger').removeClass("has-danger");

    if(nama==""||nama==null){
        $('#nama').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(states==""||states==null){
        $('#states').closest('.form-floating').addClass("has-danger");
        valid=0;
    }

    if(country==""||country==null){
        $('#country').closest('.form-group').addClass("has-danger");
        valid=0;
    }

    if(valid==0){
        toastr.error("Harap isi Semua field!","Terjadi Kesalahan");
        return false;
    }

    if(mode==2){
        url = "editCity";
        data.city_id = $('#modalInsert').attr("edit_id");
    }

    $.ajax({
        url:"/master/"+url,
        method:"post",
        data:data,
        success:function(e){
            if(e==1){
                if(mode==1)toastr.success("Berhasil tambah data baru","Berhasil Insert");
                else if(mode==2)toastr.success("Berhasil edit data","Berhasil Update");
                get_data();
                $('.modal').modal("hide");
            }
            else{
                toastr.error("Nama Country sudah terdaftar","Gagal Insert");
            }
        }
    });
})

function get_data() {
    $("#tb-city").dataTable({
        dom: 'Bfrtip',
        serverSide: true,
        destroy: true,
        deferLoading: 10,
        deferRender: true,
        ajax: {
            url: "/master/get_data_city",
            type: "get",
            data:{
                nama:$('#nama_filter').val(),
                country_id:$('#country_filter').val(),
                state_id:$('#state_filter').val(),
            },
            dataSrc: function (json) {
                console.log(json);
                for (var i = 0; i < json.data.length; i++) {
                    if(json.data[i].with_visa==0)dis = "disabled='disabled'";
                    json.data[i].action = `
                        <button class='btn btn-warning btn-edit'>Edit</button>
                        <button class='btn btn-danger btn-delete'>Delete</button>
                    `;
                }

                $('.dt-button').addClass("btn btn-outline-primary btn-sm");
                $('.paginate_button').addClass("btn btn-outline-primary");
                return json.data;
            },
            error: function (e) {

                console.log(e.responseText);
            },
        },
        initComplete: (settings, json) => {
        },
        columns: [

            { data: "country_name",width:"20%"},
            { data: "state_name",width:"30%"},
            { data: "name",width:"20%"},
            { data: "action" ,className:"text-center"},
        ],
        searching: false,
        displayLength: 10,
        responsive: true,
        ordering: false,
        scrollX: "auto",
        deferLoading:true
    });
    let table1 = $("#tb-city").DataTable();
    table1
        .one("draw", function () {
            table1.columns.adjust();
        })
        .ajax.reload();
}
