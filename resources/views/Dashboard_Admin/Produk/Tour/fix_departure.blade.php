@extends('layout')

@section('body')
    <div id="main-fixed" class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Tour</a></li>
              <li class="breadcrumb-item active" aria-current="page">Fixed Departure</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">Fixed Departure</h5>
                        <a href="{{route('fix_departure_tambah_baru')}}">
                            <button class="btn btn-success btn-add mt-2">
                                + Tambah Baru
                            </button>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="Tour Name">
                            </div>
                            <div class="col-4">
                                <input type="datetime-local" class="form-control add filter_text" id="filter_date">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-departure">
                    <thead>
                        <tr>
                            <td>Tour Name</td>
                            <td>Category</td>
                            <td>Type</td>
                            <td>Validity</td>
                            <td>Status</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('custom_js')
    <script>

        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();;

        if(localStorage.getItem("success")!=null){
            toastr.success(localStorage.getItem("success"),"Berhasil Edit");
            localStorage.removeItem("success");
        }
    </script>
    <script src="{{asset("custom_js/Produk/fix_departure.js")}}"></script>
@endsection
