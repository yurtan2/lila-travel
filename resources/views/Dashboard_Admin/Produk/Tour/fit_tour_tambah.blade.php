@extends('layout')

@section('body')
<link rel="stylesheet" href="{{ asset('js/listbox/listbox.css') }}">
<style>

    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }

    .form-check-input:checked {
        background-color: #7C41FF;
        border-color: #B376FF;
    }

    .fooSelect .select2-selection__rendered {
        font-size: 1.2em;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {

        line-height: inherit !important;

        font-size: 12px !important;

        padding: 4px !important;

        top: 26px !important;

    }

    .lbjs-list​ {
        height: 350px !important;
    }
</style>
<div id="new-fixed" class="p-3 pt-0">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb ps-0">
            <li class="breadcrumb-item"><a href="">Tour</a></li>
            <li class="breadcrumb-item"><a href="{{ route('fit_tour') }}">FIT Departure</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Baru</li>
        </ol>
    </nav>
    <div class="card px-3">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h2 class="card-title pt-2">Add New FIT Tour</h2>
                </div>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills ms-3 mb-5" id="pills-tab" role="tablist">
                <li class="nav-item me-3" role="presentation">
                    <button type="button" class="nav-link active px-5 tab-page" id="pills-general-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-general" type="button" role="tab" aria-controls="pills-general"
                        aria-selected="true" value="1">
                        General Informatian
                    </button>
                </li>
                <li class="nav-item me-3" role="presentation">
                    <button type="button" class="nav-link px-5 tab-page"id="pills-itinerary-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-itinerary" type="button" role="tab" aria-controls="pills-itinerary"
                        aria-selected="false" value="2">
                        Itinerary
                    </button>
                </li>
                <li class="nav-item me-3" role="presentation">
                    <button type="button" class="nav-link px-5 tab-page" id="pills-price-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-price" type="button" role="tab" aria-controls="pills-price"
                        aria-selected="false" value="3">
                        Validity & Price
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button type="button" class="nav-link px-5" id="pills-term-tab" data-bs-toggle="pill" data-bs-target="#pills-term"
                        type="button" role="tab" aria-controls="pills-term" aria-selected="false">
                        Term & Condition
                    </button>
                </li>
            </ul>
            <form action="/dd" method="post" id="tour_form">
                @csrf
                <div class="tab-content" id="pills-tabContent">
                    {{-- tab General --}}
                    <div class="tab-pane fade show active" id="pills-general" role="tabpanel"
                        aria-labelledby="pills-general-tab">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="mb-3">
                                        <label for="input_tour_name" class="form-label">Tour Name</label>
                                        <input type="text" class="form-control need-check" id="input_tour_name" name="input_tour_name" placeholder="Enter a Name Tour">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-3">
                                        <label for="input_tour_category" class="form-label">Tour Category</label>
                                        <select class="form-select" name="input_tour_category" id="input_tour_category">
                                            <option value="outbond">Outbond</option>
                                            <option value="inbound">Inbond</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-3">
                                        <label for="input_tour_type" class="form-label">Tour Type</label>
                                        <select class="form-select" name="input_tour_type" id="input_tour_type">
                                            <option value="domestic">Domestic</option>
                                            <option value="international">International</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3 mt-3 input_destination">
                            </div>
                            <div class="row mt-3 mb-3">
                                <div class="col-lg-12">
                                    <button type="button" id="btn_add_country"
                                        class="btn btn-light btn-rounded px-5">Add Country +</button>
                                </div>
                            </div>
                            <div class="row mt-3 mb-3">
                                <div class="col-lg-2">
                                    <div class="mb-3">
                                        <label for="basicpill-address-input" class="form-label">Duration (Days)</label>
                                        <input type="number" name="input_tour_days" id="input_tour_days"
                                            class="form-control" min="1" value="2">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="mb-3">
                                        <label for="basicpill-address-input" class="form-label">Night Count</label>
                                        <input type="number" name="input_tour_nights" id="input_tour_nights"
                                            class="form-control" min="1" value="1">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <label for="basicpill-address-input" class="form-label">
                                        Package Include
                                    </label>
                                    <div class="row mb-3">
                                        <div class="row">
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_flight" >
                                                <label class="form-check-label" for="formRadios1">
                                                    Flight
                                                </label>
                                            </div>
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_hotel" >
                                                <label class="form-check-label" for="formRadios2">
                                                    Hotel
                                                </label>
                                            </div>
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_meal">
                                                <label class="form-check-label" for="formRadios3">
                                                    Meal
                                                </label>
                                            </div>
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_tour" >
                                                <label class="form-check-label" for="formRadios4">
                                                    Tour
                                                </label>
                                            </div>
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_trans" >
                                                <label class="form-check-label" for="formRadios5">
                                                    Transport
                                                </label>
                                            </div>
                                            <div class="col-2">
                                                <input class="form-check-input" type="checkbox" id="tour_visa">
                                                <label class="form-check-label" for="formRadios5">
                                                    Visa
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <label for="test" class="form-label font-size-13 text-muted">Keyword</label>
                                        </div>
                                    </div>
                                    <div class="row mb-3 align-items-center">
                                        <div class="col">
                                            <select class="form-select js-example-basic-multiple" name="input_tour_keyword[]" id="input_keyword" multiple="multiple">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row align-items-center">
                                        <div class="col-12 pb-1">
                                            <div class="float-start form-check form-switch">
                                                <input class="form-check-input need-check" id="check_gimmick_price" type="checkbox" role="switch" id="flexSwitchCheckDefault">
                                            </div>
                                            <label for="flexSwitchCheckDefault" class="form-label font-size-13 text-muted float-start">Gimmick Price</label>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <input type="text" class="form-control need-check" name="" id="input_gimmick_price" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="basicpill-firstname-input" class="form-label">From</label>
                                        <input class="form-control" type="datetime-local" id="input_tour_date_start">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="basicpill-lastname-input" class="form-label">To</label>
                                        <input class="form-control" type="datetime-local" id="input_tour_date_end">
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-12 col-md-12">
                                    <div class="mb-3">
                                        <label for="choices-multiple-groups"
                                            class="form-label font-size-13 text-muted">Attach to displayer</label>
                                        <select class="form-select" name="" id="select_displayer">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <div class="mb-3">
                                        <label for="choices-multiple-groups"
                                            class="form-label font-size-13 text-muted" data-bs-toggle="tooltip"
                                            data-bs-placement="right" data-bs-title="Tooltip on right">
                                            Cover
                                        </label>
                                        <input type="file" name="file" id="input_file_cover">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="mb-3">
                                        <label for="choices-multiple-groups"
                                            class="form-label font-size-13 text-muted">Banner</label>
                                        <input type="file" name="file" id="input_file_banner">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="mb-3">
                                        <label for="choices-multiple-groups"
                                            class="form-label font-size-13 text-muted">Gallery</label>
                                        <div class="d-flex align-items-center">
                                            <div class="flex-shrink-0">
                                                <input type="file" name="file" id="input_file_galery">
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-end">
                                    <button type="button" class="btn btn-dark btn-draft" id="btn-safe-draf">Simpan Draft</button>
                                    <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya
                                        >></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Tab Itinerary --}}
                    <div class="tab-pane fade" id="pills-itinerary" role="tabpanel"
                        aria-labelledby="pills-itinerary-tab">
                        <div class="container-fluid" id="input_itenerary">

                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-light btn-tambah-hari">Tambah Hari +</button>
                            </div>
                            <div class="col-6 text-end">
                                <button type="button" class="btn btn-dark btn-draft" id="btn-safe-draf">Simpan Draft</button>
                                <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya >></button>
                            </div>
                        </div>
                    </div>

                    {{-- Tab Validity Price --}}
                    <div class="tab-pane fade" id="pills-price" role="tabpanel" aria-labelledby="pills-price-tab">
                        <div class="container-fluid">
                            <div class="container-depature">
                                <ul class="nav nav-pills mb-3" id="tab-validate" role="tablist">
                                    {{-- <li class="nav-item me-3" role="presentation">
                                        <button class="nav-link active" id="btn_hotel_0" value="0" data-bs-toggle="pill" data-bs-target="#tab_hotel_0" type="button" role="tab" aria-controls="tab_hotel_0" aria-selected="true">Hotel 01</button>
                                    </li> --}}
                                    <li class="nav-item me-3" id="btn-add-tab">
                                        <button type="button" class="btn btn-light btn-rounded border" id="btn_add_tab">Add New +</button>
                                    </li>
                                </ul>
                                <div class="tab-content" id="validate-tabContent">
                                    {{-- <div class="tab-pane fade show active" id="tab_hotel_0" role="tabpanel" aria-labelledby="btn_hotel_0" tabindex="0">

                                    </div> --}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-light" id="btn-tambah-jadwal">Tambah
                                        Jadwal</button>
                                </div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-dark btn-draft" id="btn-safe-draf">Simpan Draft</button>
                                    <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya
                                        >></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Tab Term and Condition --}}
                    <div class="tab-pane fade" id="pills-term" role="tabpanel" aria-labelledby="pills-term-tab">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="container-inclusion">

                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="button" class="btn btn-outline-secondary btn-rounded px-3" id="btn_add_inclusion">Add inclusion +</button>
                                </div>
                            </div>
                            <hr>
                            <div class="row mb-3 mt-3">
                                <div class="container-exclusion">

                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3 ">
                                    <button type="button" class="btn btn-outline-secondary btn-rounded px-3" id="btn_add_exclusion">Add Exclusion +</button>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-3">
                                <div class="container_term_condition">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <button type="button" class="btn btn-outline-secondary btn-rounded px-3" id="btn_add_term_condition">Add Term & Condition +</button>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <h6>Publish This Tour</h6>
                                <p>Publish Tour jika ingin menampilkan produk yang bersangkutan di halaman website</p>
                                <div class="row">
                                    <div class="col-auto">
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" checked name="inlineRadioOptions" id="inlineRadio1" value="Yes">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                      </div>
                                    </div>
                                    <div class="col-auto">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                      </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row mt-5">
                                <div class="col-6">
                                    <button type="button" class="btn btn-light btn-back">Back To Price</button>
                                </div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-dark btn-draft" id="btn-safe-draf">Simpan Draft</button>
                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-save">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@isset($tour_id)
    <input type="hidden" id="tour_id" value="{{$tour_id}}">
@endisset

@isset($tour)
    <input type="hidden" id="data_tour" value="{{$tour}}">
@endisset

<!-- Modal -->
<div class="modal fade" id="modal-save" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Konfirmasi Simpan Data</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Pastikan Anda telah memeriksa data yang dimasukkan dengan seksama sebelum menyimpan.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_submit">Simpan Data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
    var token = "{{csrf_token()}}";
    var mode = "{{$mode}}";
    var tour;
    if(mode==2) tour = JSON.parse($('#data_tour').val());
    console.log(tour);
</script>
<script src="{{ asset('js/listbox/listbox.js') }}"></script>
<script src="{{ asset('custom_js/Produk/fit_tour_tambah.js') }}"></script>
@endsection
