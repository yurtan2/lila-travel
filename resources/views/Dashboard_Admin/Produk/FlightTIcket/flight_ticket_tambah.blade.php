@extends('layout')

@section('body')
    <link rel="stylesheet" href="{{ asset('js/listbox/listbox.css') }}">
    <style>
        .nav-item .active {
            border-radius: 100px;
            padding-left: 30px;
            padding-right: 30px;
            font-family: 'Yaldevi', sans-serif;
            font-weight: bold;
            background-image: linear-gradient(to right, #B376FF, #7C41FF);
            color:white;
            text-decoration: none;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #fff;
            background-color: white !important;
        }

        .form-check-input:checked {
            background-color: #7C41FF;
            border-color: #B376FF;
        }

        .fooSelect .select2-selection__rendered {
            font-size: 1.2em;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {

            line-height: inherit !important;

            font-size: 12px !important;

            padding: 4px !important;

            top: 26px !important;

        }

        .lbjs-list​ {
            height: 350px !important;
        }

        .horizontal-scrollable{
            width: 100%;         /* Adjust width as needed */
            overflow-x: scroll;  /* Makes it scrollable horizontally */
            white-space: nowrap;
        }
    </style>
    <div id="new-fixed" class="p-3 pt-0">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="">Tour</a></li>
                <li class="breadcrumb-item"><a href="{{ route('fix_departure') }}">Hotel</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah Baru</li>
            </ol>
        </nav>
        <div class="card px-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h2 class="card-title pt-2">Add New Hotel</h2>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills ms-3 mb-5" id="pills-tab" role="tablist">
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link active px-5 tab-page" id="pills-general-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-general" type="button" role="tab" aria-controls="pills-general"
                            aria-selected="true" value="1">General Informatian</button>
                    </li>
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link px-5 tab-page" id="pills-rooms-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-rooms" type="button" role="tab" aria-controls="pills-rooms"
                            aria-selected="false" value="2">Add Rooms</button>
                    </li>
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link px-5 tab-page" id="pills-setting-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-setting" type="button" role="tab" aria-controls="pills-setting"
                            aria-selected="false" value="3">Advance Setting</button>
                    </li>
                </ul>
                <form action="/dd" method="post" id="tour_form">
                    @csrf
                    <div class="tab-content" id="pills-tabContent">
                        {{-- tab General --}}
                        <div class="tab-pane fade show active" id="pills-general" role="tabpanel"
                            aria-labelledby="pills-general-tab">
                            <div class="container-fluid">
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <label for="input_tour_name" class="form-label">Hotel Name</label>
                                        <input type="text" class="form-control need-check" id="input_tour_name"
                                            name="input_tour_name" placeholder="Enter a Hotel's Name">
                                    </div>
                                    <div class="col-lg-5">
                                        <label for="input_tour_category" class="form-label">Rating</label>
                                        <div id="rater-step">

                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <label for="input_tour_name" class="form-label">Hotel Name</label>
                                        <select class="form-select" name="" id="country"></select>
                                    </div>
                                    <div class="col-lg-5">
                                        <label for="input_tour_category" class="form-label">Address</label>
                                        <input type="text" class="form-control" name="" id="" placeholder="Jalan Sudirman no 1...">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label for="" class="form-label">Detail</label>
                                    <div class="col-lg-12">
                                        <div id="detail" style="height: 250px;"></div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-2">
                                        <div class="h5 float-start">Cover</div>
                                        <button type="button" class="btn btn-close btn-delete-photo float-end" hidden></button>
                                        <label class="float-start">
                                            <img src="{{ asset('assets/upload/image-cards.png') }}" class="w-100">
                                            <input type="file" class="upload_single" name="upload_ktp" id="" style="display: none;">
                                        </label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="h5">Banner</div>
                                        <label class="float-start">
                                            <img src="{{ asset('assets/upload/image-cards.png') }}" class="w-100">
                                            <input type="file" class="upload_multiple_1" name="upload_ktp" id="" accept="image/*,video/*" multiple style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="form-label" for="">Special Facility</label>
                                    <div class="row px-2 overflow-auto">
                                        @for ($j = 0; $j < 20; $j++)
                                        <div class="col-lg-4 align-items-center">
                                            <label class="input-group py-1 mb-2">
                                                <input type="checkbox" class="h5 me-2" name="" id="">
                                                <div class="h5">Apa sih</div>
                                            </label>
                                        </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Itinerary --}}
                        <div class="tab-pane fade" id="pills-rooms" role="tabpanel" aria-labelledby="pills-rooms-tab">
                            <div class="h1"></div>
                            <div class="container-fluid" id="list_room">

                            </div>
                            <div class="row">
                                <div class="col-lg-12 ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-light btn-tambah-hari"><< Sebelumnya</button>
                                </div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-outline-success btn-tambah-room">+ Tambah Room</button>
                                    <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya >></button>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Validity Price --}}
                        <div class="tab-pane fade" id="pills-setting" role="tabpanel" aria-labelledby="pills-setting-tab">
                            <div class="container-fluid">
                                <div class="container-depature">
                                    <div class="row mb-3">
                                        <label for="" class="form-label">
                                            Special Request
                                        </label>
                                        <div class="row overflow-auto bg-light p-2" style="height: 250px">
                                            @foreach ($data['data'] as $key)
                                            <div class="col-lg-4 align-items-center">
                                                <label class="input-group py-1">
                                                    <input type="checkbox" class="h5 me-2" name="" id="">
                                                    <div class="h5">{{$key->name}}</div>
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="pin">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light btn" id="btn-tambah-jadwal">Tambah Jadwal</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Selanjutnya >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Term and Condition --}}
                        <div class="tab-pane fade" id="pills-term" role="tabpanel" aria-labelledby="pills-term-tab">
                            <div class="container-fluid">

                                <div class="">
                                    <div class="container-inclusion">

                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-3">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_inclusion">Add inclusion +</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row mb-3 container-exclusion mt-3">

                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-3 ">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_exclusion">Add Exclusion +</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row container_term_condition mt-3">

                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_term_condition">Add Term & Condition +</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <h6>Publish This Tour</h6>
                                    <p>Publish Tour jika ingin menampilkan produk yang bersangkutan di halaman website</p>
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked name="inlineRadioOptions" id="inlineRadio1" value="Yes">
                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                          </div>
                                        </div>
                                        <div class="col-auto">
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
                                            <label class="form-check-label" for="inlineRadio2">No</label>
                                          </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row mt-5">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light btn-back">Back To Price</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-save">Simpan Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @isset($tour_id)
        <input type="hidden" id="tour_id" value="{{$tour_id}}">
    @endisset

    @isset($tour)
        <input type="hidden" id="data_tour" value="{{$tour}}">
    @endisset

    <!-- Modal -->
    <div class="modal fade" id="modal-save" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Konfirmasi Simpan Data</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Pastikan Anda telah memeriksa data yang dimasukkan dengan seksama sebelum menyimpan.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn_submit">Simpan Data</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="token" value="{{csrf_token()}};">
@endsection

@section('custom_js')
    <script>
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var data = @json($data);
    </script>
    <script src="{{ asset('js/listbox/listbox.js') }}"></script>
    <script src="{{ asset('custom_js/Produk/Hotel/hotel_tambah.js') }}"></script>
@endsection
