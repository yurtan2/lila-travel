@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item active" aria-current="page"><a href="#">Flight Ticket</a></li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Flight Ticket</h5>
                        <a href="#">
                            <button class="btn btn-success btn-add mt-2">
                                + Tambah Flight Ticket
                            </button>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control" placeholder="Airlines Name">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-document">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Code</td>
                            <td>Name</td>
                            <td>Image</td>
                            <td>Api Endpoint</td>
                            <td>PIC</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Flight Ticket</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-lg-4">
                            <label for="code-iata">Code IATA</label>
                            <div class="form-floating ">
                                <input type="text" class="form-control add" id="code-iata" name="nama" placeholder="Code IATA">
                                <label for="code-iata">Code IATA</label>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <label for="nama">Name</label>
                            <div class="form-floating">
                                <input type="text" class="form-control add" id="airline_nama" name="nama" placeholder="Name">
                                <label for="nama">Name</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <label for="url-api">Url API</label>
                            <div class="form-floating">
                                <input type="text" class="form-control add" id="url-api" name="nama" placeholder="">
                                <label for="url-api">Url API</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <label for="url-api">Recent Url</label>
                            <div class="form-floating">
                                <div class="p-3" style="background-color: #f8f8f8;border:1px solid rgb(228, 228, 228);border-radius:10px;max-height:150px;overflow-y:auto" id="log_container">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <label class="float-start">
                                <img src="{{ asset('assets/upload/image-cards.png') }}" class="w-100" id="preview">
                                <input type="file" class="upload_gambar" id="logo" name="upload_ktp" id="" style="display: none;">
                            </label>
                        </div>
                        <div class="col-lg-1">
                            <button type="button" class="btn-close btn-delete-photo"></button>
                        </div>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-12 mb-3">
                                    {{-- <label for="pic-name">Pic Name</label> --}}
                                    <div class="form-floating">
                                        <input type="text" class="form-control add" id="pic-name" name="nama" placeholder="">
                                        <label for="pic-name">Pic Name</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 mb-3">
                                    {{-- <label for="pic-email">Pic Email</label> --}}
                                    <div class="form-floating">
                                        <input type="text" class="form-control add" id="pic-email" name="nama" placeholder="">
                                        <label for="pic-email">Pic Email</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" id="btn-batal" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Detail Visa</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <label for="input-username"><b>Country Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_country" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="input-username"><b>Visa Name</b></label><br>
                            <label id="view_visa_name" style="font-weight: 400"></label>
                        </div>
                    </div>
                    <label for="input-username" ><b>Jenis Dokumen Diperlukan</b></label><br>
                    <div id="view_visa_dokumen" class="mb-3" style="font-weight: 400"></div>
                    <label for="input-username"><b>Detail</b></label>
                    <div class="form-floating mb-4">
                        <div id="view_visa_detail" ></div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-outline-success" id="btn-export-pdf" >Export PDF</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->


@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
        var public = "{{asset('')}}";
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
    </script>
    <script src="{{asset("custom_js/Produk/FlightTicket/flight_ticket.js")}}"></script>
@endsection
