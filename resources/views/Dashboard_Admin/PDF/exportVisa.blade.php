<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <label for="input-username"><b>Visa Name</b></label><br>
    <label id="view_visa_name"> {!!$visa->visa_name!!}</label><br><br>
    <label for="input-username"><b>Jenis Dokumen Diperlukan<b></label><br>
    <div id="view_visa_dokumen" class="mb-3">
        @foreach (json_decode($visa->visa_need,true) as $item)
            <label >{{$loop->iteration}}. {{$item["document_name"]}}</label><br>
        @endforeach
    </div><br>
    <label for="input-username"><b>Detail</b></label>
    <div class="form-floating mb-4">
        <div id="view_visa_detail">
            {!!$content!!}
        </div>
    </div>
</body>
</html>
