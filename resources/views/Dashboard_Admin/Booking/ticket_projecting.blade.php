@extends('layout')

@section('body')
<style>
    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }

    .btn-purple{
        background-color: #7C41FF;
        color: white;
    }
    .btn-purple:hover{
        background-color: #6837d2;
        color: white;
    }
    .btn-outline-purple{
        border: 1px solid #7C41FF;
        color: #7C41FF;
    }
    .btn-outline-purple:hover{
        background-color: #7C41FF;
        color: white;
    }
</style>
    @php
        $month = ['JAN', 'FEB', 'MAR', 'APR', 'MEI', 'JUN', 'JUL', 'AGU', 'SEP', 'OKT', 'NOV', 'DES'];
    @endphp
    <div class="container-fluid">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item active" aria-current="page">Ticketing Projecting</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="row mb-4 mt-2 justify-content-between">
                    <div class="col-lg-3">
                        <div class="h5">Ticketing Project</div>
                    </div>
                    <div class="col-lg-2">
                        <div class="row h5">
                            <div class="col">
                                <i class="bi bi-caret-left-fill float-start cursor" id="last-year"></i>
                            </div>
                            <div class="col text-center">
                                <p class="" id="input-year">2023</p>
                            </div>
                            <div class="col">
                                <i class="bi bi-caret-right-fill float-end cursor" id="next-year"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="row nav nav-pills mx-auto mb-3" id="pills-tab" role="tablist">
                    @foreach ($month as $index => $key)
                        <li class="col-lg-2 nav-item" role="presentation">
                            <button class="nav-link  px-5 tab-page" id="pills-information-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-information" type="button" role="tab" aria-controls="pills-information"
                                aria-selected="true" index="{{$index+1}}" value="1">{{$key}}</button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-4">
                        <a href="{{route('ticketingProjectEdit')}}">
                            <button class="btn btn-purple me-3" id="btn-add-project">+ ADD PROJECT</button>
                        </a>
                    </div>
                </div>
                <div class="row px-3">
                    <table class="table" id="table_ticket">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Project Name</th>
                            <th scope="col">Corporate Name</th>
                            <th scope="col">Corporate PIC</th>
                            <th scope="col">PIC Phone #</th>
                            <th scope="col">Sales</th>
                            <th scope="col">Total Pax</th>
                            <th scope="col">Total Amount</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalProject" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Corporate Booking</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <label for="" class="form-label">Project Name *</label>
                            <input type="text" class="form-control need-check" name="" id="">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <label for="" class="form-label">Corporate Name *</label>
                            <select class="form-select need-check" name="" id="corporate-name"></select>
                        </div>
                        <div class="col-lg-6">
                            <label for="" class="form-label">PIC Name *</label>
                            <select class="form-select select-2 need-check" name="" id="pic-name"></select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-save">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var tooltipImageUrl = "{{ asset('assets/passport/passport_example.png') }}";
    </script>
    <script src="{{asset("custom_js/Booking/ticketing_project.js")}}"></script>
@endsection
