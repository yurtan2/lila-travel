@extends('layout')

@section('body')
<style>
    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }
</style>
    <div class="container-fluid">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="{{route('booking_tour')}}">Tour</a></li>
              <li class="breadcrumb-item active" aria-current="page">Tailor</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="h4">Reservation</div>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills ms-3 mb-5" id="pills-tab" role="tablist">
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link active px-5 tab-page" id="pills-information-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-information" type="button" role="tab" aria-controls="pills-information"
                            aria-selected="true" value="1">Tour Information</button>
                    </li>
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link px-5 tab-page" id="pills-passanger-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-passanger" type="button" role="tab" aria-controls="pills-passanger"
                            aria-selected="false" value="2">Passanger List</button>
                    </li>
                </ul>
                <form action="/dd" method="post" id="tour_form">
                    @csrf
                    <div class="tab-content" id="pills-tabContent">
                        {{-- tab General --}}
                        <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-information-tab">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_name" class="form-label">Request</label>
                                            <input type="text" class="form-control" placeholder="Enter Your Request" id="reservation_tailor_request">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_category" class="form-label">Customer Name *</label>
                                            <select class="form-select" name="" id="rmh"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_type" class="form-label">Phone Number</label>
                                            <div class="row align-items-center">
                                                <div class="col-lg-3">
                                                    <select class="select-2 form-select" name="" id="">
                                                        <option value="+1">+1</option>
                                                        <option value="+44">+44</option>
                                                        <option value="+62" selected>+62</option>
                                                        <option value="+91">+91</option>
                                                        <option value="+86">+86</option>
                                                        <option value="+81">+81</option>
                                                        <option value="+61">+61</option>
                                                        <option value="+7">+7</option>
                                                        <option value="+34">+34</option>
                                                        <option value="+55">+55</option>
                                                        <option value="+49">+49</option>
                                                        <option value="+33">+33</option>
                                                        <option value="+39">+39</option>
                                                        <option value="+60">+60</option>
                                                        <option value="+63">+63</option>
                                                        <option value="+27">+27</option>
                                                        <option value="+82">+82</option>
                                                        <option value="+65">+65</option>
                                                        <option value="+90">+90</option>
                                                        <option value="+971">+971</option>
                                                        <option value="+84">+84</option>
                                                        <option value="+213">+213</option>
                                                        <option value="+376">+376</option>
                                                        <option value="+244">+244</option>
                                                        <option value="+266">+266</option> <!-- Lesotho -->
                                                        <option value="+231">+231</option> <!-- Liberia -->
                                                        <option value="+218">+218</option> <!-- Libya -->
                                                        <option value="+423">+423</option> <!-- Liechtenstein -->
                                                        <option value="+370">+370</option> <!-- Lithuania -->
                                                        <option value="+352">+352</option> <!-- Luxembourg -->
                                                        <option value="+261">+261</option> <!-- Madagascar -->
                                                        <option value="+265">+265</option> <!-- Malawi -->
                                                        <option value="+960">+960</option> <!-- Maldives -->
                                                        <option value="+223">+223</option> <!-- Mali -->
                                                        <option value="+356">+356</option> <!-- Malta -->
                                                        <option value="+692">+692</option> <!-- Marshall Islands -->
                                                        <option value="+222">+222</option> <!-- Mauritania -->
                                                        <option value="+230">+230</option> <!-- Mauritius -->
                                                        <option value="+52">+52</option>   <!-- Mexico -->
                                                        <option value="+373">+373</option> <!-- Moldova -->
                                                        <option value="+377">+377</option> <!-- Monaco -->
                                                        <option value="+976">+976</option> <!-- Mongolia -->
                                                        <option value="+382">+382</option> <!-- Montenegro -->
                                                        <!-- ... You can continue to add more ... -->

                                                    </select>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control add need-check number-only" id="customer_phone" placeholder="08xxxxxxxx">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_category" class="form-label">Email *</label>
                                            <input type="email" id="input-email" class="form-control" placeholder="example@example.com">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_type" class="form-label">Address *</label>
                                            <input type="email" id="input-address" class="form-control" placeholder="Jl. Sudirman No. 1...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Dewasa</label>
                                            <select name="dewasa" id="dewasa" class="form-select">
                                                @for ($i = 0; $i <= 20; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Anak</label>
                                            <select name="anak" id="anak" class="form-select">
                                                @for ($i = 0; $i <= 10; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Bayi</label>
                                            <select name="bayi" id="bayi" class="form-select">
                                                @for ($i = 0; $i <= 5; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg">
                                        <div class="mb-3">
                                            <label for="input_tour_type" class="form-label">Remark</label>
                                            <div id="remark_editor" style="height: 250px"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Itinerary --}}
                        <div class="tab-pane fade" id="pills-passanger" role="tabpanel" aria-labelledby="pills-passanger-tab">
                            <div class="container-fluid">
                                <div class="container-passanger">

                                </div>
                                <div class="row mt-3">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light btn-tambah-peserta">Add Passanger Data</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-save" id="btn-simpan">Simpan Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="gambar_paspor" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Gambar Passpor</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('assets/passport/passport_example.png') }}" class="w-100" alt="">
            </div>
          </div>
        </div>
      </div>
@endsection

@section('custom_js')
    <script>
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var tooltipImageUrl = "{{ asset('assets/passport/passport_example.png') }}";
        var token = "{{csrf_token()}}";
        var quill;
        var mode=1;
    </script>
    <script src="{{asset("custom_js/Booking/reservation_tailor.js")}}"></script>
@endsection
