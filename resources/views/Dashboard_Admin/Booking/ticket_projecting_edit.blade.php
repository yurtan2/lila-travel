@extends('layout')

@section('body')
<style>
    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }

    .btn-purple{
        background-color: #7C41FF;
        color: white;
    }
    .btn-purple:hover{
        background-color: #6837d2;
        color: white;
    }
    .btn-outline-purple{
        border: 1px solid #7C41FF;
        color: #7C41FF;
    }
    .btn-outline-purple:hover{
        background-color: #7C41FF;
        color: white;
    }
</style>
    <div class="container-fluid">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
            <li class="breadcrumb-item"><a href="{{ route('ticketingProject') }}">Ticketing Projecting</a></li>
              <li class="breadcrumb-item active" aria-current="page">Create Corporate Booking Project</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h2 class="card-title pt-2">Corporate Booking</h2>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <label for="" class="form-label">Project Name *</label>
                        <input type="text" class="form-control need-check" name="" id="project_name">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <label for="" class="form-label">Corporate Name *</label>
                        <select class="form-select need-check" name="" id="corporate-name"></select>
                    </div>
                    <div class="col-lg-6">
                        <label for="" class="form-label">PIC Name *</label>
                        <select class="form-select select-2 need-check" name="" id="pic-name" disabled></select>
                    </div>
                </div>
                <div class="row mb-3 justify-content-end">
                    <div class="col-lg-2">
                        <button class="btn btn-purple float-end ms-2" id="btn-save-project">Save</button>
                        <a href="{{ route('ticketingProject') }}">
                            <button class="btn btn-secondary float-end">Cancel</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var tooltipImageUrl = "{{ asset('assets/passport/passport_example.png') }}";
    </script>
    <script src="{{asset("custom_js/Booking/ticketing_project_edit.js")}}"></script>
@endsection
