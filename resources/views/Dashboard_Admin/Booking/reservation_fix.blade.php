@extends('layout')

@section('body')
<style>
    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }

    .btn-purple{
        background-color: #7C41FF;
        color: white;
    }
    .btn-purple:hover{
        background-color: #6837d2;
        color: white;
    }
    .btn-outline-purple{
        border: 1px solid #7C41FF;
        color: #7C41FF;
    }
    .btn-outline-purple:hover{
        background-color: #7C41FF;
        color: white;
    }
</style>
    <div class="container-fluid">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="{{route('booking_tour')}}">Tour</a></li>
              <li class="breadcrumb-item active" aria-current="page">FIX Tour</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="h4">Reservation</div>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills ms-3 mb-5" id="pills-tab" role="tablist">
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link active px-5 tab-page" id="pills-information-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-information" type="button" role="tab" aria-controls="pills-information"
                            aria-selected="true" value="1">Tour Information</button>
                    </li>
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link px-5 tab-page"id="pills-passanger-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-passanger" type="button" role="tab" aria-controls="pills-passanger"
                            aria-selected="false" value="2">Passanger List</button>
                    </li>
                    <li class="nav-item me-3" role="presentation">
                        <button class="nav-link px-5 tab-page" id="pills-ticket-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-ticket" type="button" role="tab" aria-controls="pills-ticket"
                            aria-selected="false" value="3">Ticket & Document</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link px-5" id="pills-payment-tab" data-bs-toggle="pill" data-bs-target="#pills-payment"
                            type="button" role="tab" aria-controls="pills-payment" aria-selected="false" value="4">Payment</button>
                    </li>
                </ul>
                <form action="/dd" method="post" id="tour_form">
                    @csrf
                    <div class="tab-content" id="pills-tabContent">
                        {{-- tab General --}}
                        <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-information-tab">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_name" class="form-label">Select Departure*</label>
                                            <select class="form-select" name="" id="select-departure"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_category" class="form-label">Customer Name *</label>
                                            <select class="form-select customer_name" name="" id=""></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_type" class="form-label">Phone Number</label>
                                            <div class="row align-items-center">
                                                <div class="col-lg-3">
                                                    <select class="select-2 form-select" name="" id="">
                                                        <option value="+1">+1</option>
                                                        <option value="+44">+44</option>
                                                        <option value="+62" selected>+62</option>
                                                        <option value="+91">+91</option>
                                                        <option value="+86">+86</option>
                                                        <option value="+81">+81</option>
                                                        <option value="+61">+61</option>
                                                        <option value="+7">+7</option>
                                                        <option value="+34">+34</option>
                                                        <option value="+55">+55</option>
                                                        <option value="+49">+49</option>
                                                        <option value="+33">+33</option>
                                                        <option value="+39">+39</option>
                                                        <option value="+60">+60</option>
                                                        <option value="+63">+63</option>
                                                        <option value="+27">+27</option>
                                                        <option value="+82">+82</option>
                                                        <option value="+65">+65</option>
                                                        <option value="+90">+90</option>
                                                        <option value="+971">+971</option>
                                                        <option value="+84">+84</option>
                                                        <option value="+213">+213</option>
                                                        <option value="+376">+376</option>
                                                        <option value="+244">+244</option>
                                                        <option value="+266">+266</option> <!-- Lesotho -->
                                                        <option value="+231">+231</option> <!-- Liberia -->
                                                        <option value="+218">+218</option> <!-- Libya -->
                                                        <option value="+423">+423</option> <!-- Liechtenstein -->
                                                        <option value="+370">+370</option> <!-- Lithuania -->
                                                        <option value="+352">+352</option> <!-- Luxembourg -->
                                                        <option value="+261">+261</option> <!-- Madagascar -->
                                                        <option value="+265">+265</option> <!-- Malawi -->
                                                        <option value="+960">+960</option> <!-- Maldives -->
                                                        <option value="+223">+223</option> <!-- Mali -->
                                                        <option value="+356">+356</option> <!-- Malta -->
                                                        <option value="+692">+692</option> <!-- Marshall Islands -->
                                                        <option value="+222">+222</option> <!-- Mauritania -->
                                                        <option value="+230">+230</option> <!-- Mauritius -->
                                                        <option value="+52">+52</option>   <!-- Mexico -->
                                                        <option value="+373">+373</option> <!-- Moldova -->
                                                        <option value="+377">+377</option> <!-- Monaco -->
                                                        <option value="+976">+976</option> <!-- Mongolia -->
                                                        <option value="+382">+382</option> <!-- Montenegro -->
                                                        <!-- ... You can continue to add more ... -->

                                                    </select>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control add need-check number-only" id="customer_phone" placeholder="08xxxxxxxx">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_category" class="form-label">Email</label>
                                            <input type="email" class="form-control" placeholder="example@example.com">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="input_tour_type" class="form-label">Address</label>
                                            <input type="email" class="form-control" placeholder="Jl. Sudirman No. 1...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Dewasa</label>
                                            <select name="dewasa" id="dewasa" class="form-select">
                                                @for ($i = 0; $i <= 20; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Anak</label>
                                            <select name="anak" id="anak" class="form-select">
                                                @for ($i = 0; $i <= 10; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label for="">Bayi</label>
                                            <select name="bayi" id="bayi" class="form-select">
                                                @for ($i = 0; $i <= 5; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Itinerary --}}
                        <div class="tab-pane fade" id="pills-passanger" role="tabpanel"
                            aria-labelledby="pills-passanger-tab">
                            <div class="container-fluid">
                                <div class="container-passanger">

                                </div>
                                <div class="row mt-2 mb-5 justify-content-end">
                                    <div class="col-lg-3">
                                        <button type="button" class="btn btn-outline-purple float-end btn-tambah-peserta">Add New Passanger</button>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light"><< Previous</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Validity Price --}}
                        <div class="tab-pane fade" id="pills-ticket" role="tabpanel" aria-labelledby="pills-ticket-tab">
                            <div class="container-fluid">
                                <div class="container-ticket">
                                    <div class="row">
                                        <div class="col-3">
                                            <label for="" class="form-label">Airline</label>
                                            <select class="form-select" name="" id=""></select>
                                        </div>
                                        <div class="col-3">
                                            <label for="input_tour_type" class="form-label">PNR #</label>
                                            <input type="email" class="form-control" placeholder="Enter PNR Code">
                                        </div>
                                        <div class="col-2">
                                            <label for="input_tour_type" class="form-label">Number of Pax</label>
                                            <input type="email" class="form-control" placeholder="Number of Passanger">
                                        </div>
                                        <div class="col-4">
                                            <label for="input_tour_type" class="form-label">Time Limit Ticket</label>
                                            <input type="datetime-local" class="form-control" id="ailrine-limit">
                                        </div>
                                    </div>
                                </div>
                                <div class="container-ticket-passanger">

                                </div>
                                <div class="row mt-3 mb-5 justify-content-end">
                                    <div class="col-lg-3">
                                        <button type="button" class="btn btn-outline-purple float-end btn-add-ticket">Add New Passanger</button>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light btn" id="btn-tambah-jadwal"><< Previous</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Tab Term and Condition --}}
                        <div class="tab-pane fade" id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                            <div class="container-fluid">
                                <div class="">
                                    <div class="container-inclusion">

                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-3">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_inclusion">Add inclusion +</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row mb-3 container-exclusion mt-3">

                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-3 ">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_exclusion">Add Exclusion +</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row container_term_condition mt-3">

                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <button type="button" class="btn btn-outline-secondary btn-rounded px-3"
                                                id="btn_add_term_condition">Add Term & Condition +</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <h6>Publish This Tour</h6>
                                    <p>Publish Tour jika ingin menampilkan produk yang bersangkutan di halaman website</p>
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked name="inlineRadioOptions" id="inlineRadio1" value="Yes">
                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                          </div>
                                        </div>
                                        <div class="col-auto">
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
                                            <label class="form-check-label" for="inlineRadio2">No</label>
                                          </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row mt-5">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-light btn-back">Back To Price</button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-room" role="tabpanel"
                            aria-labelledby="pills-room-tab">
                            <div class="container-fluid" id="input_itenerary">

                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-light btn-tambah-hari">Tambah Hari +</button>
                                </div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-seat" role="tabpanel"
                            aria-labelledby="pills-seat-tab">
                            <div class="container-fluid" id="input_itenerary">

                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-light btn-tambah-hari">Tambah Hari +</button>
                                </div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-secondary btn btn-next">Next >></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="gambar_paspor" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Gambar Passpor</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="{{ asset('assets/passport/passport_example.png') }}" class="w-100" alt="">
                </div>
            </div>
        </div>
      </div>
@endsection

@section('custom_js')
    <script>
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var tooltipImageUrl = "{{ asset('assets/passport/passport_example.png') }}";
    </script>
    <script src="{{asset("custom_js/Booking/reservation_fit.js")}}"></script>
@endsection
