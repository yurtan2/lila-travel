@extends('layout')

@section('body')
    <style>
        .nav-item .active {
            border-radius: 100px;
            font-family: 'Yaldevi', sans-serif;
            font-weight: bold;
            background-image: linear-gradient(to right, #B376FF, #7C41FF);
            color:white;
            text-decoration: none;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #fff;
            background-color: white !important;
        }
    </style>

    <div id="main-fixed" class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Tour</a></li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-body">

                <div class="row mb-3">
                    <h5 class="card-title pt-2">Tour Reservation</h5>
                </div>
                <div class="row mb-5 align-items-center">
                    <div class="col-3 col-md-8">
                        <button class="btn btn-success btn-add mt-2" id="btn-move-page">
                            + Tambah Baru
                        </button>
                    </div>
                    <div class="col-3 col-md-4 text-end">
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                    </div>
                    <!-- inside filter -->
                    <div class="row w-100 mx-auto"  id="inner-filter" style="display: none">
                        <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                            <div class="row">
                                <div class="col-3">
                                    <input type="search" name="nama_filter" id="id_filter" class="form-control" placeholder="Tour Code">
                                </div>
                                <div class="col-3">
                                    <input type="search" name="nama_filter" id="text_filter" class="form-control" placeholder="Product">
                                </div>
                                <div class="col-3">
                                    <input type="datetime-local" class="form-control add" id="filter_date">
                                </div>
                                <div class="col-3">
                                    <button class="btn btn-outline-secondary waves-effect waves-light" id="btn-reset">
                                        <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                    </button>
                                    <button class="btn btn-outline-success waves-effect waves-light" id="btn-search">
                                        <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-n3">
                    <ul class="nav nav-pills nav-fill ms-3 mb-5" id="pills-tab" role="tablist">
                        <li class="nav-item me-3" role="presentation">
                            <button class="nav-link active px-5 tab-page" id="pills-fit-tab" data-bs-toggle="pill" data-bs-target="#pills-fit" type="button" role="tab" aria-controls="pills-fit" aria-selected="true" value="1">FIT TOUR</button>
                        </li>
                        <li class="nav-item me-3" role="presentation">
                            <button class="nav-link px-5 tab-page"id="pills-fix-tab" data-bs-toggle="pill" data-bs-target="#pills-fit" type="button" role="tab" aria-controls="pills-fit" aria-selected="false" value="2">FIXED DEPARTURE</button>
                        </li>
                        <li class="nav-item me-3" role="presentation">
                            <button class="nav-link px-5 tab-page" id="pills-tailor-tab" data-bs-toggle="pill" data-bs-target="#pills-fit" type="button" role="tab" aria-controls="pills-fit" aria-selected="false" value="3">TAILOR MADE BOOKING</button>
                        </li>
                    </ul>
                </div>
                <div class="row mt-n3">
                    <div class="h5">Daftar Pemesanan Tour</div>
                </div>
                <div class="row">
                    <div class="tab-content" id="pills-tabContent2">
                        <div class="tab-pane fade show active" id="pills-fit" role="tabpanel" aria-labelledby="pills-fit-tab" tabindex="0">
                            <ul class="nav nav-pills mb-5" id="pills-fit-tab" role="tablist">
                                <li class="nav-item me-3 fix-fit-tailor" role="presentation">
                                    <button class="nav-link active btn-sm" id="leads-tab" data-bs-toggle="pill">LEADS</button>
                                </li>
                                <li class="nav-item me-3 fix-fit" role="presentation">
                                    <button class="nav-link btn-sm" btn_name="Definit" data-bs-toggle="pill">DEFINIT</button>
                                </li>
                                <li class="nav-item me-3 fix-fit" role="presentation">
                                    <button class="nav-link btn-sm" btn_name="Done" data-bs-toggle="pill">DONE</button>
                                </li>
                                <li class="nav-item me-3 fix-fit" role="presentation">
                                    <button class="nav-link btn-sm" btn_name="Postpone" data-bs-toggle="pill">POSTPONE</button>
                                </li>
                                <li class="nav-item me-3 fix-fit" role="presentation">
                                    <button class="nav-link btn-sm" btn_name="Cancel" data-bs-toggle="pill">CANCEL</button>
                                </li>
                                <li class="nav-item me-3 fix-fit" role="presentation">
                                    <button class="nav-link btn-sm" btn_name="Refund" data-bs-toggle="pill">REFUND</button>
                                </li>
                                <li class="nav-item me-3 tailor" role="presentation" hidden>
                                    <button class="nav-link btn-sm" btn_name="Follow Up" data-bs-toggle="pill">FOLLOW UP</button>
                                </li>
                                <li class="nav-item me-3 tailor" role="presentation" hidden>
                                    <button class="nav-link btn-sm" btn_name="Confirm" data-bs-toggle="pill">CONFIRM</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <table class="table table-hover" id="tb-departure">
                    <thead>
                        <tr>
                            <td id="header_id">Tour Code</td>
                            <td id="header_2nd">Product</td>
                            <td>Dept. Date</td>
                            <td>Party Size</td>
                            <td>Source</td>
                            <td>Customer CP.</td>
                            <td>Sales</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();;

        $("#btn-tambah-baru").click(function(){

        })
    </script>
    <script src="{{asset("custom_js/Booking/tour.js")}}"></script>
@endsection
