@extends('layout')

@section('body')
<style>
    .form-check-input:checked {
        background-color: #7C41FF;
        border-color: #B376FF;
    }

    .nav-item .active {
        border-radius: 100px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
        text-decoration: none;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #fff;
        background-color: white !important;
    }

    .btn-purple{
        background-color: #7C41FF;
        color: white;
    }
    .btn-purple:hover{
        background-color: #6837d2;
        color: white;
    }
    .btn-outline-purple{
        border: 1px solid #7C41FF;
        color: #7C41FF;
    }
    .btn-outline-purple:hover{
        background-color: #7C41FF;
        color: white;
    }
</style>
    @php
        $month = ['JAN', 'FEB', 'MAR', 'APR', 'MEI', 'JUN', 'JUL', 'AGU', 'SEP', 'OKT', 'NOV', 'DES'];
    @endphp
    <div class="container-fluid">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
            <li class="breadcrumb-item"><a href="{{ route('ticketingProject') }}">Ticketing Projecting</a></li>
              <li class="breadcrumb-item active" aria-current="page">Create Corporate Booking Details</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h2 class="card-title pt-2">Corporate Booking Details</h2>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mb-3 align-items-center justify-content-between">
                    <div class="col-lg-5">
                        <div class="row align-items-center">
                            <div class="col-lg-4 mb-3 fw-bold">Project Name :</div>
                            <div class="col-lg-6 mb-3">{{$header->ticketing_project_name}}</div>
                            <div class="col-lg-4 mb-3 fw-bold">Corporate Name :</div>
                            <div class="col-lg-6 mb-3">{{$header->corporate_name}}</div>
                            <div class="col-lg-4 mb-3 fw-bold">Total Pax :</div>
                            <div class="col-lg-6 mb-3">{{str_pad($header->ticketing_project_adult,2,"0",STR_PAD_LEFT)}} Adult | {{str_pad($header->ticketing_project_child,2,"0",STR_PAD_LEFT)}} Child | {{str_pad($header->ticketing_project_infant,2,"0",STR_PAD_LEFT)}} Infant</div>
                        </div>
                    </div>
                    <div class="col-lg-5 justify-content-end">
                        <div class="row align-items-center">
                            <div class="col-lg-6 mb-3 fw-bold">Corporate Contact :</div>
                            <div class="col-lg-4 mb-3">{{$header->corporate_prefix." ".$header->corporate_nomor}}</div>
                            <div class="col-lg-6 mb-3 fw-bold">Mobile Phone:</div>
                            <div class="col-lg-4 mb-3">{{$header->customer_prefix_nomor." ".$header->customer_nomor}}</div>
                            <div class="col-lg-6 mb-3 fw-bold">Total Sales :</div>
                            <div class="col-lg-4 mb-3 d-grid">
                                <button class="btn btn-purple-gradient text-start" style="border-radius: 10px;font-weight:bold" id="total_all_routes">IDR 0</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn btn-secondary mb-3 px-5" id="btn-tambah-route">+ Add Route</button>
                    </div>
                </div>
                <hr class="mb-3">
                <div class="container-fluid mb-3" id="list_route">

                </div>
                {{-- <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr class="align-middle">
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>NIK</td>
                                <td>Birth Date</td>
                                <td>Jabatan</td>
                                <td>Airline</td>
                                <td>Flight #</td>
                                <td>Class</td>
                                <td>Booking Code</td>
                                <td style="width: 100px;">Status</td>
                                <td>Time Limit</td>
                                <td>ETD</td>
                                <td>ETA</td>
                                <td>Net Price</td>
                                <td>Mark Up</td>
                                <td>Selling Price</td>
                                <td>Invoice #</td>
                                <td>Remark</td>
                            </tr>
                            <tr class="row-table align-middle">
                                <td>
                                    <select class="form-select customer-name" name="" id=""></select>
                                </td>
                                <td>
                                    <div class="last-name" style="width: 100px;">-</div>
                                </td>
                                <td>
                                    <div class="nik">-</div>
                                </td>
                                <td>
                                    <div class="birth-date" style="width: 100px;">-</div>
                                </td>
                                <td>
                                    <div class="jabatan" style="width: 100px;">-</div>
                                </td>
                                <td>Airline</td>
                                <td>Flight #</td>
                                <td>Class</td>
                                <td>Booking Code</td>
                                <td>
                                    <select class="form-select" name="" id=""  style="width: 100px;">
                                        <option value="Active" selected>Active</option>
                                        <option value="Issued">Issued</option>
                                        <option value="Cancel">Cancel</option>
                                        <option value="Expired">Expired</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="datetime-local" class="form-control" id="" name="" placeholder="DD-MM-YYYYThh:mm">
                                </td>
                                <td>
                                    <input type="time" class="form-control" name="" id="">
                                </td>
                                <td>
                                    <input type="time" class="form-control" name="" id="">
                                </td>
                                <td>
                                    <input class="form-control nets-price text-end" style="width: 170px;" type="text" name="" id="">
                                </td>
                                <td>
                                    <input class="form-control mark-up text-end" style="width: 170px;" type="text" name="" id="" value="Rp.0" disabled>
                                </td>
                                <td>
                                    <input class="form-control selling-price text-end" style="width: 170px;" type="text" name="" id="">
                                </td>
                                <td>Invoice #</td>
                                <td>Remark</td>
                            </tr>
                        </tbody>
                    </table>
                </div> --}}
                {{-- <div class="contaier-fluid">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>

                            </tr>
                        </table>
                      </div>
                </div> --}}
                {{-- <div class="container-fluid">
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <tr class="row-table">
                                <colgroup>
                                    <col style="width: 30%">
                                </colgroup>

                                <td class="customer-name">First Name</td>
                                <td>Last Name</td>
                                <td>NIK</td>
                                <td>Birth Date</td>
                                <td>Jabatan</td>
                                <td>Airline</td>
                                <td>Flight #</td>
                                <td>Class</td>
                                <td>Booking Code</td>
                                <td>
                                    <select class="form-select w-100" name="" id="">
                                        <option value="Active">Active</option>
                                        <option value="Issued">Issued</option>
                                        <option value="Cancel">Cancel</option>
                                        <option value="Expired">Expired</option>
                                    </select>
                                </td>
                                <td>Time Limit</td>
                                <td>ETD</td>
                                <td>ETA</td>
                                <td>Net Price</td>
                                <td>Mark Up</td>
                                <td>Selling Price</td>
                                <td>Invoice #</td>
                                <td>Remark</td>
                            </tr>
                        </table>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalProject" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Add New Project</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <label for="" class="form-label">Project Name *</label>
                            <input type="text" class="form-control need-check" name="" id="">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <label for="" class="form-label">Corporate Name *</label>
                            <select class="form-select need-check" name="" id="corporate-name"></select>
                        </div>
                        <div class="col-lg-6">
                            <label for="" class="form-label">PIC Name *</label>
                            <select class="form-select select-2 need-check" name="" id="pic-name"></select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-2">
                            <label for="" class="form-label">Dewasa *</label>
                            <select class="form-select" name="" id="">
                                @for ($i = 0; $i < 21; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <label for="" class="form-label">Anak *</label>
                            <select class="form-select" name="" id="">
                                @for ($i = 0; $i < 11; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <label for="" class="form-label">Bayi *</label>
                            <select class="form-select" name="" id="">
                                @for ($i = 0; $i < 6; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var ticketing_project_id = "{{ $ticketing_project_id }}";
        var routes = @json($routes);
        var uploadImageUrl = "{{ asset('assets/upload/image-cards.png') }}";
        var tooltipImageUrl = "{{ asset('assets/passport/passport_example.png') }}";
    </script>
    <script src="{{asset("custom_js/Booking/ticketing_project_detail.js")}}"></script>
@endsection
