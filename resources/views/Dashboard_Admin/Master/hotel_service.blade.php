@extends('layout')

@section('body')
    <style>

    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Category</a></li>
              <li class="breadcrumb-item active" aria-current="page">Hotel Service</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Hotel Service</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Hotel Service</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-8">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="Hotel Service">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Hotel Service Name</td>
                            <td>Category</td>
                            <td>Icon</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @php
    $hotelIcons = [
        "bell","bag", "key", "house-door", "tsunami", "tv", "wifi", "bell",
        "briefcase", "geo-alt", "calendar", "credit-card", "check", "phone", "people",
        "person-plus", "person-lines-fill", "bank2", "gift", "umbrella", "person-badge",
        "tools", "camera", "file-text", "tv", "truck", "binoculars", "globe2", "dribbble",
        "water","truck", "sun", "tree", "bicycle",
        "airplane", "building", "calendar","bus-front","car-front","cup-hot","door-open","envelope-at","fuel-pump","mortarboard","p-circle","plugin","reception-4","signpost-split",
        "ticket-detailed","train-front",""
    ];

    @endphp
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Hotel Service</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <label for="input-username">Nama Hotel Service</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="nama" placeholder="Hotel Service">
                        <label for="input-username">Nama Hotel Service</label>
                    </div>
                    <label for="input-username">Category</label>
                    <div class="form-floating mb-4">
                        <select class="form-select" name="" id="category">
                            <option value="Master" selected>Master</option>
                            <option value="General">General</option>
                            <option value="Bathroom">Bathroom</option>
                        </select>
                        <label for="">Catogory Option</label>
                    </div>
                    <label for="input-username">Icon</label>
                    <div class="row px-2">
                        <div class="container">
                            @for ($i = 0; $i < sizeof($hotelIcons); $i++)
                            <label>
                                <i class="icon-hover p-2 bi bi-{{$hotelIcons[$i]}}" style="font-size:25px"></i>
                                <input class="icon-check" type="radio" name="icon-service" id="{{$hotelIcons[$i]}}" nama_icon="{{$hotelIcons[$i]}}" style="display: none;" name="" >
                            </label>
                            @endfor
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/hotel_service.js")}}"></script>
@endsection
