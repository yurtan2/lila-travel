@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="/master/displayer">Displayer</a></li>
                <li class="breadcrumb-item"><a href="#">Displayer Details</a></li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-body">
                <h4 class="pt-2">Displayer</h4>
                <hr>
                <div class="row mt-4s">
                    <div class="col-12 col-md-6">
                        <label for="input-username">Title</label>
                        <input type="text" class="form-control add" id="input-title" value="{{$data->displayer_title}}" placeholder="Title Displayer...">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="input-username">Category</label>
                        <div class="form-group mb-4">
                            <select name="" id="select-category" class="form-select">
                                <option value="Flash Sale">Flash Sale</option>
                                <option value="Promo">Promo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="input-username">Validity</label>
                        <div class="form-group mb-4">
                            <input type="datetime-local" class="form-control add" id="input-validity"  value="{{$data->displayer_validity}}">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="input-username">Order</label>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control add" id="input-order" placeholder="Order" value="{{$data->displayer_order}}">
                        </div>
                    </div>
                </div>
                <input type="hidden" id="displayer_id" value="{{$data->displayer_id}}">
                <h4 class="pt-2">Product List</h4>
                <hr>
                <div class="row mt-4 mb-4">
                    <div class="col-10">
                        <select name="select-tour" id="select-tour"></select>
                    </div>
                    <div class="col-2">
                        <button class="btn btn-outline-dark w-100 btn-tour" disabled> + Add Tour</button>
                    </div>
                </div>
                <table class="table mb-5">
                    <thead>
                        <tr>
                            <th>Tour Name</th>
                            <th>Displayer Name</th>
                            <th>Type</th>
                            <th>Validity</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="tb-detail">

                    </tbody>
                </table>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6">
                        <a href="/master/displayer" class="btn btn-outline-secondary">Back</a>
                    </div>
                    <div class="col-6 text-end">
                        <button class="btn btn-success" id="btn-insert">Save Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('custom_js')
    <script>
        var token = "{{csrf_token()}}";
        var mode=2; //1= insert, 2= update
        var data = @json($data_detail);
    </script>
    <script src="{{asset("custom_js/displayer_detail.js")}}"></script>
@endsection
