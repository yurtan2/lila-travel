@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
        .tooltip {
            z-index: 1000000;
        }
        .icon{
            color: gray;
        }
        .icon-danger:hover{
            color:#F17A7A!important;
        }
        .icon-info:hover{
            color:#16daf1!important;
        }
        .icon-warning:hover{
            color:#ffcc5a!important;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="#">Customer Management</a></li>
                <li class="breadcrumb-item active" aria-current="page">Corporate</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Corporate</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Corporate</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_corporate_name" class="form-control" placeholder="Corporate Name">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_corporate_nomor" class="form-control" placeholder="Corporate No. Telephone">
                            </div>

                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>

                            <td>Name</td>
                            <td>Phone</td>
                            <td>Admin Fee</td>
                            <td>Total PIC</td>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Corporate</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row mb-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label for="" class="form-label">Corporate Name *</label>
                                    <input type="text" class="form-control need-check add" id="corporate_name" placeholder="Enter Corporate's Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Email *</label>
                                    <input type="text" class="form-control add need-check" id="corporate_email" placeholder="Enter Your Email">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Phone Number</label>
                                    <div class="input-group">
                                        <select class="form-select select2" name="" id="corporate_prefix">
                                            <option value="+1">+1</option>
                                            <option value="+44">+44</option>
                                            <option value="+62" selected>+62</option>
                                            <option value="+91">+91</option>
                                            <option value="+86">+86</option>
                                            <option value="+81">+81</option>
                                            <option value="+61">+61</option>
                                            <option value="+7">+7</option>
                                            <option value="+34">+34</option>
                                            <option value="+55">+55</option>
                                            <option value="+49">+49</option>
                                            <option value="+33">+33</option>
                                            <option value="+39">+39</option>
                                            <option value="+60">+60</option>
                                            <option value="+63">+63</option>
                                            <option value="+27">+27</option>
                                            <option value="+82">+82</option>
                                            <option value="+65">+65</option>
                                            <option value="+90">+90</option>
                                            <option value="+971">+971</option>
                                            <option value="+84">+84</option>
                                            <option value="+213">+213</option>
                                            <option value="+376">+376</option>
                                            <option value="+244">+244</option>
                                            <option value="+266">+266</option> <!-- Lesotho -->
                                            <option value="+231">+231</option> <!-- Liberia -->
                                            <option value="+218">+218</option> <!-- Libya -->
                                            <option value="+423">+423</option> <!-- Liechtenstein -->
                                            <option value="+370">+370</option> <!-- Lithuania -->
                                            <option value="+352">+352</option> <!-- Luxembourg -->
                                            <option value="+261">+261</option> <!-- Madagascar -->
                                            <option value="+265">+265</option> <!-- Malawi -->
                                            <option value="+960">+960</option> <!-- Maldives -->
                                            <option value="+223">+223</option> <!-- Mali -->
                                            <option value="+356">+356</option> <!-- Malta -->
                                            <option value="+692">+692</option> <!-- Marshall Islands -->
                                            <option value="+222">+222</option> <!-- Mauritania -->
                                            <option value="+230">+230</option> <!-- Mauritius -->
                                            <option value="+52">+52</option>   <!-- Mexico -->
                                            <option value="+373">+373</option> <!-- Moldova -->
                                            <option value="+377">+377</option> <!-- Monaco -->
                                            <option value="+976">+976</option> <!-- Mongolia -->
                                            <option value="+382">+382</option> <!-- Montenegro -->
                                            <!-- ... You can continue to add more ... -->

                                        </select>
                                        <input type="text" class="form-control w-75 add need-check number-only" id="corporate_nomor" placeholder="08xxxxxxxx">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3 has-validation">
                                    <label for="input_tour_category" class="form-label">Corporate Address</label>
                                    <input type="text" class="form-control need-check add " id="corporate_address" placeholder="Enter Your Address">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3 has-validation">
                                    <label for="input_tour_category" class="form-label">Corporate Admin Fee</label>
                                    <input type="text" class="form-control need-check add number-only" id="corporate_admin_fee" placeholder="Enter Your Admin Fee">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Logo</label>
                                    <div class="row align-items-center py-2">
                                        <div class="col-auto">
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="inlineRadio1">
                                            <label class="form-check-label" for="inlineRadio1">Hide Logo Lila</label>
                                          </div>
                                        </div>
                                        <div class="col-auto">
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="inlineRadio2">
                                            <label class="form-check-label" for="inlineRadio2">Use Own Logo</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">Corporate Logo *</label>
                                    <input type="file" class="form-control add birthdate need-check" id="corporate_logo" id="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">Akta Perusahaan</label>
                                    <input type="file" class="form-control add birthdate need-check" id="corporate_akta" id="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">NIB</label>
                                    <input type="file" class="form-control add birthdate need-check" id="corporate_nib" id="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">SK Menkumham</label>
                                    <input type="file" class="form-control add birthdate need-check" id="corporate_sk" id="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">NPWP</label>
                                    <input type="file" class="form-control add birthdate need-check" id="corporate_npwp" id="">
                                </div>
                            </div>
                            <hr>
                            <h6 class="mb-3">PIC Information</h6>
                            <table class="table" id="table-pic">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Name</td>
                                        <td>Jabatan</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody id="body-pic">

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="input-username">Name</label>
                            </div>
                            <div class="col-lg-4">
                                <label for="input-username">Jabatan</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="input-username">&nbsp;</label>
                            </div>
                        </div>
                        <div class="row align-items-center input-pic mb-4">
                            <div class="col-lg-6 border">
                                <div class="form-group">
                                    <select class="form-select pic-check" name="" id="pic_name"></select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select class="form-select jabatan_select2 pic-check" name="" id="pic_jabatan">
                                        <option value="">Belum ada jabatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-outline-success btn-md btn-add-pic" type="button"><i data-feather="plus" class="fw-bold" style="height: 10px; width: 10px;"></i> Add PIC</button>
                            </div>
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Corporate Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Email</b></label><br>
                            <label id="view_email" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Phone Number</b></label><br>
                            <label id="view_nomor" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Corporate Address</b></label><br>
                            <label id="view_address" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Corporate Admin Fee</b></label><br>
                            <label id="view_admin_fee" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Hide Logo Lila</b></label><br>
                            <label id="view_hide_logo" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Use Own Logo</b></label><br>
                            <label id="view_own" style="font-weight: 400"></label>
                        </div>
                    </div>
                    <hr>
                    <br>

                    <label for="input-username"><b>Document</b></label>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Corporate Logo</label>
                                <img src="" alt="" id="view_logo" style="width: 100%" class="detail"name="KTP_KITAS">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Akta Perusahaan</label>
                                <img src="" alt="" id="view_akta" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">NIB</label>
                                <img src="" alt="" id="view_nib" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">SK Menkumham</label>
                                <img src="" alt="" id="view_sk" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">NPWP</label>
                                <img src="" alt="" id="view_npwp" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <label for="input-username"><b>PIC Information</b></label><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Name</td>
                                <td>Jabatan</td>
                            </tr>
                        </thead>
                        <tbody id="view_pic">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Detail -->
    <div class="modal fade bs-example-modal-center"  id="modalViewDetail">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="detail_dokumen" style="width: 100%">
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="btn-download">Download</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        var public = "{{asset('')}}";
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
        $('#modalInsert').on('shown.bs.modal', function() {
            console.log("modal show");
            $('.tooltips').tooltip({
                container: $(this)
            });
        });
    </script>
    <script src="{{asset("custom_js/corporate.js")}}"></script>
@endsection
