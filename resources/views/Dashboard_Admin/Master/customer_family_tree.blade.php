@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
        .icon{
            color: gray;
        }
        .icon:hover{
            color:#F17A7A!important;
        }
        .icon{
            color: gray;
        }
        .icon-danger:hover{
            color:#F17A7A!important;
        }
        .icon-info:hover{
            color:#16daf1!important;
        }
        .icon-warning:hover{
            color:#ffcc5a!important;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="#">Customer Management</a></li>
                <li class="breadcrumb-item active" aria-current="page">Family Tree</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Family Tree</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Family Tree</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_family_id" class="form-control filter_text" placeholder="Family ID">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_name" class="form-control filter_text" placeholder="Customer Name">
                            </div>
                            <div class="col-3">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Family ID</td>
                            <td>Head Of Family</td>
                            <td>Count Member</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Family Tree</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row">
                        <div class="col-10">
                            <label for="input_tour_category" class="form-label">Head Of Family</label>
                            <select name="" id="customer_id" class="form-select"></select>
                        </div>
                        <div class="col-2 d-grid gap-2 p-2 pt-1 text-center">
                            <button class=" btn btn-outline-secondary mt-4 detail-head-btn" disabled id="">Detail</button>
                        </div>
                    </div>
                    <hr>
                    <label for="input_tour_category" class="form-label">Member Of Family</label>
                    <div id="container_member"></div>
                    <div class="text-end  mt-3">
                        <button class="btn btn-success" id="btn-add-member">Add New Member</button>
                    </div>

                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <!-- Modal View Family-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalViewHead">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titleView">Detail Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <label for="input-username"><b>Head Of Member</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_head" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label for="input-username"><b>List Of Member</b></label><br>
                        </div>
                        <div class="col-6">
                            <label for="input-username"><b>Member</b></label><br>

                        </div>
                        <div class="col-6">
                            <label for="input-username"><b>Member Type</b></label><br>

                        </div>
                    </div>
                    <div class="row" id="container_member_view" ></div>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Full Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Phone</b></label><br>
                            <label id="view_phone" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Gender</b></label><br>
                            <label id="view_gender" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>NIK</b></label><br>
                            <label id="view_nik" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Birthdate</b></label><br>
                            <label id="view_tanggal_lahir" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Age</b></label><br>
                            <label id="view_age" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Address</b></label><br>
                            <label id="view_address" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Food Reference</b></label><br>
                            <label id="view_referensi" style="font-weight: 400"></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Passport Number</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_passport_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>POI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_poi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOE</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doe" style="font-weight: 400"></label>
                            </div>
                        </div>
                    </div>
                    <label for="input-username"><b>Document</b></label>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <img src="" alt="" id="view_ktp" style="width: 100%" class="detail"name="KTP_KITAS">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <img src="" alt="" id="view_passport" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Visa</label>
                                <img src="" alt="" id="view_visa" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


@endsection
@section('custom_js')
    <script>
        var mode=1;
        var token = "{{csrf_token()}}";
        var public = "{{asset('')}}";
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/customer_family_tree.js")}}"></script>
@endsection
