@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
        .tooltip {
            z-index: 1000000;
        }
        .icon{
            color: gray;
        }
        .icon-danger:hover{
            color:#F17A7A!important;
        }
        .icon-info:hover{
            color:#16daf1!important;
        }
        .icon-warning:hover{
            color:#ffcc5a!important;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item active" aria-current="page">Customer</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Customer</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Customer</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_corporate_id" class="form-control filter_text" placeholder="Corporate ID">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_family_id" class="form-control filter_text" placeholder="Family ID">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_name" class="form-control filter_text" placeholder="Customer Name">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_nik" class="form-control filter_text" placeholder="Customer NIK">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_nomor" class="form-control filter_text" placeholder="Customer No. Telephone">
                            </div>
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_customer_passport_number" class="form-control filter_text" placeholder="Customer Passport Number">
                            </div>
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="date" id="filter_customer_tanggal_lahir" class="form-control filter_text" placeholder="Customer Tanggal Lahir">
                            </div>
                            <div class="col-4">
                                <label class="form-label"></label>
                                <select class="form-select" id="filter_passport_status">
                                    <option value="0" selected>All</option>
                                    <option value="1" >Active</option>
                                    <option value="-1">Expired</option>
                                </select>
                            </div>

                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>Corporate ID</td>
                            <td>Family ID</td>
                            <td>Customer Name</td>
                            <td>Phone</td>
                            <td>Gender</td>
                            <td>Tanggal Lahir</td>
                            <td>Passport Status</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-xl">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Customer</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row mb-2">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="" class="form-label">First Name *</label>
                                    <div class="input-group mb-3">
                                        <select class="form-select" name="" id="customer_title">
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                        </select>
                                        <input type="text" class="form-control add w-75" id="customer_first_name" aria-label="Text input with dropdown button">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Last Name *</label>
                                    <input type="text" class="form-control add need-check" id="customer_last_name" placeholder="Enter Your Last Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3 has-validation">
                                    <label for="input_tour_category" class="form-label">NIK

                                        <a href="#" data-bs-toggle="tooltip" style="z-index: 99999999999999999" data-bs-placement="top" title="Untuk Customer Indonesia, WAJIB Mengisikan NIK">
                                            <i data-feather="info" style="width: 10pt;color:#1c84ee;" ></i>
                                        </a></label>

                                    <input type="text" class="form-control add number-only" id="customer_nik" placeholder="Enter Your NIK">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Phone Number</label>
                                    <div class="input-group">
                                        <select class="form-select select2" name="" id="customer_prefix_nomor">
                                            <option value="+1">+1</option>
                                            <option value="+44">+44</option>
                                            <option value="+62" selected>+62</option>
                                            <option value="+91">+91</option>
                                            <option value="+86">+86</option>
                                            <option value="+81">+81</option>
                                            <option value="+61">+61</option>
                                            <option value="+7">+7</option>
                                            <option value="+34">+34</option>
                                            <option value="+55">+55</option>
                                            <option value="+49">+49</option>
                                            <option value="+33">+33</option>
                                            <option value="+39">+39</option>
                                            <option value="+60">+60</option>
                                            <option value="+63">+63</option>
                                            <option value="+27">+27</option>
                                            <option value="+82">+82</option>
                                            <option value="+65">+65</option>
                                            <option value="+90">+90</option>
                                            <option value="+971">+971</option>
                                            <option value="+84">+84</option>
                                            <option value="+213">+213</option>
                                            <option value="+376">+376</option>
                                            <option value="+244">+244</option>
                                            <option value="+266">+266</option> <!-- Lesotho -->
                                            <option value="+231">+231</option> <!-- Liberia -->
                                            <option value="+218">+218</option> <!-- Libya -->
                                            <option value="+423">+423</option> <!-- Liechtenstein -->
                                            <option value="+370">+370</option> <!-- Lithuania -->
                                            <option value="+352">+352</option> <!-- Luxembourg -->
                                            <option value="+261">+261</option> <!-- Madagascar -->
                                            <option value="+265">+265</option> <!-- Malawi -->
                                            <option value="+960">+960</option> <!-- Maldives -->
                                            <option value="+223">+223</option> <!-- Mali -->
                                            <option value="+356">+356</option> <!-- Malta -->
                                            <option value="+692">+692</option> <!-- Marshall Islands -->
                                            <option value="+222">+222</option> <!-- Mauritania -->
                                            <option value="+230">+230</option> <!-- Mauritius -->
                                            <option value="+52">+52</option>   <!-- Mexico -->
                                            <option value="+373">+373</option> <!-- Moldova -->
                                            <option value="+377">+377</option> <!-- Monaco -->
                                            <option value="+976">+976</option> <!-- Mongolia -->
                                            <option value="+382">+382</option> <!-- Montenegro -->
                                            <!-- ... You can continue to add more ... -->

                                        </select>
                                        <input type="text" class="form-control w-75 add need-check number-only" id="customer_phone" placeholder="08xxxxxxxx">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">Email *</label>
                                    <input type="email" class="form-control add need-check" id="customer_email" placeholder="Enter Your Email">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Religion *</label>
                                    <select class="form-select" id="customer_religion">
                                        <option value="Islam" selected>Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Khonghucu">Khonghucu</option>
                                        <option value="Prefer Not To Say">Prefer Not To Say</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">Birthdate T *</label>
                                    <input type="date" class="form-control add birthdate need-check" id="customer_tanggal_lahir">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Age</label>
                                    <input type="text" class="form-control age" id="umur" value="0 Tahun" disabled>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_category" class="form-label">Address *</label>
                                    <input type="text" class="form-control add need-check" id="customer_alamat" placeholder="Jl. Sudirman No. 1...">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Food Reference</label>
                                    <input type="text" class="form-control add"id="customer_food"   placeholder="Enter Your Food Reference">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Gender</label>
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked name="inlineRadioOptions" id="inlineRadio1" value="Male">
                                            <label class="form-check-label" for="inlineRadio1">Male</label>
                                          </div>
                                        </div>
                                        <div class="col-auto">
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Female">
                                            <label class="form-check-label" for="inlineRadio2">Female</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Jenis Customer</label>
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked name="customer_jenis" id="jenis_customer_indo" value="Male">
                                            <label class="form-check-label" for="inlineRadio1">Indonesia</label>
                                          </div>
                                        </div>
                                        <div class="col-auto">
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="customer_jenis" id="jenis_customer_other" value="Female">
                                            <label class="form-check-label" for="inlineRadio2">Other</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg">
                                <div class="h5 ">
                                    Passport Information
                                    <a class="gambar_paspor" title="" href="#"  id="info-passport"><i class="bi bi-info-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="mb-3 has-validation">
                                    <label for="input_tour_type" class="form-label">Passport Number</label>
                                    <input type="text" class="form-control add is-passport" id="customer_passport_number"  placeholder="Enter Your Passport Number">
                                    <div class="invalid-feedback">
                                        Passport Number must 16 character
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">POI</label>
                                    <input type="text" class="form-control add" id="customer_poi" placeholder="Jakarta">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">DOI</label>
                                    <input type="date" class="form-control add date_passport" id="customer_doi">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">DOE</label>
                                    <input type="date" class="form-control add date_passport" id="customer_doe">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h6 class=" mb-3">Document</h6>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <input class="form-control add file" type="file" id="ktp">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <input class="form-control add file" type="file" id="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Visa</label>
                                <input class="form-control add file" type="file" id="visa">
                            </div>
                        </div>
                    </div>
                    <br>
                    <h6 class="mb-3">PIC Information</h6>
                    <table class="table" id="table-pic">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Jabatan</td>
                                <td class="text-center">Default</td>
                                <td class="text-center">Action</td>
                            </tr>
                        </thead>
                        <tbody id="body-pic">
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="input-username">Name</label>
                        </div>
                        <div class="col-lg-4">
                            <label for="input-username">Jabatan</label>
                        </div>
                        <div class="col-lg-2">
                            <label for="input-username">&nbsp;</label>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-lg-6 ">
                            <select class="form-select select2 pic-check w-100" name="" id="pic_name"></select>
                        </div>
                        <div class="col-lg-4">
                            <select class="form-select select2 jabatan_select2 pic-check" name="" id="pic_jabatan">
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <button class="btn btn-outline-success btn-md btn-add-pic" type="button"><i data-feather="plus" class="fw-bold" style="height: 10px; width: 10px;"></i> Add PIC</button>
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     <div class="modal fade bs-example-modal-center" id="gambar_paspor">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Gambar Passpor</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('assets/passport/passport_example.png') }}" class="w-100" alt="">
            </div>
          </div>
        </div>
      </div>
     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Full Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Phone</b></label><br>
                            <label id="view_phone" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Gender</b></label><br>
                            <label id="view_gender" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>NIK</b></label><br>
                            <label id="view_nik" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Birthdate</b></label><br>
                            <label id="view_tanggal_lahir" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Age</b></label><br>
                            <label id="view_age" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Address</b></label><br>
                            <label id="view_address" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Food Reference</b></label><br>
                            <label id="view_referensi" style="font-weight: 400"></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Passport Number</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_passport_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>POI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_poi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOE</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doe" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Status Passport</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_status" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="d-none d-md-block col-4"></div>
                        <div class="col-4">
                            <label for="input-username"><b>Create At</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_create" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Last Update</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_last" style="font-weight: 400"></label>
                            </div>
                        </div>
                    </div>
                    <label for="input-username"><b>Document</b></label>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <img src="" alt="" id="view_ktp" style="width: 100%" class="detail"name="KTP_KITAS">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <img src="" alt="" id="view_passport" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Visa</label>
                                <img src="" alt="" id="view_visa" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                    </div>
                    <br>
                    <label for="input-username"><b>PIC Information</b></label><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Corporate Name</td>
                                <td>Jabatan</td>
                                <td class="text-center">Default</td>
                            </tr>
                        </thead>
                        <tbody id="view_pic">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Detail -->
    <div class="modal fade bs-example-modal-center"  id="modalViewDetail">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="detail_dokumen" style="width: 100%">
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="btn-download">Download</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom_js')
    <script>
        var public = "{{asset('')}}";
        var today = new Date().toISOString().split('T')[0];

        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
        $('#modalInsert').on('shown.bs.modal', function() {
            console.log("modal show");
            $('.tooltips').tooltip({
                container: $(this)
            });
        });
    </script>
    <script src="{{asset("custom_js/customer.js")}}"></script>
@endsection
