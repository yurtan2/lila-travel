@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Displayer</a></li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Displayer</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Displayer</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <select name="" id="filter-select-category" class="form-select filter_select">
                                    <option value="-1">All</option>
                                    <option value="Flash Sale">Flash Sale</option>
                                    <option value="Promo">Promo</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <div class="form-group mb-4">
                                    <input type="datetime-local" class="form-control add filter_text" id="filter-input-validity">
                                </div>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-displayer">
                    <thead>
                        <tr>
                            <td>Title</td>
                            <td>Displayer Name</td>
                            <td>Validity</td>
                            <td class="text-center">Total Product</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody id="body-displayer">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Displayer </h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <label for="input-username">Title</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="input-title" placeholder="Title Displayer...">
                        <label for="input-username">Title</label>
                    </div>
                    <label for="input-username">Displayer Name</label>
                    <div class="form-group mb-4">
                        <select name="" id="select-category" class="form-select">
                            <option value="Flash Sale">Flash Sale</option>
                            <option value="Promo">Promo</option>
                        </select>
                    </div>
                    <label for="input-username">Validity</label>
                    <div class="form-group mb-4">
                        <input type="datetime-local" class="form-control add" id="input-validity">
                    </div>
                    <label for="input-username">Order</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="input-order" placeholder="Order" value="1">
                        <label for="input-username">Order</label>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

@endsection
@section('custom_js')
    <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
    <script>
        var mode=1; //1= insert, 2= update
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();

        if(localStorage.getItem("success")!=null){
            toastr.success(localStorage.getItem("success"),"Berhasil Edit");
            localStorage.removeItem("success");
        }
    </script>
    <script src="{{asset("custom_js/displayer.js")}}"></script>
@endsection
