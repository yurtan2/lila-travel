@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
        .tooltip {
            z-index: 1000000;
        }
        .icon{
            color: gray;
        }
        .icon-danger:hover{
            color:#F17A7A!important;
        }
        .icon-info:hover{
            color:#16daf1!important;
        }
        .icon-warning:hover{
            color:#ffcc5a!important;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="#">Customer Management</a></li>
                <li class="breadcrumb-item active" aria-current="page">Billing Control</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <h5 class="card-title pt-2 mb-3">List Billing</h5>
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-8">
                        <input type="month" class="form-control" id="input_month">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-3">
                    <div class="col-lg-4">
                        <div class="card h-75 btn-card cursor">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-lg-6">
                                        <div class="h5 float-start"><i class="bi bi-exclamation-circle-fill text-danger me-2 float-start"></i>Aging</div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="h3 text-danger float-end">5</div>
                                    </div>
                                </div>
                                <div class="card-text">
                                    Pembayaran melewati tanggal jatuh tempo
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card h-75 btn-card cursor">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-lg-7">
                                        <div class="h5 float-start"><i class="bi bi-exclamation-triangle-fill text-danger float-start me-3"></i>Exception</div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="h3 text-danger float-end">5</div>
                                    </div>
                                </div>
                                <div class="card-text">
                                    Transaksi yang pembayarannya di berikan kebijakan
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card h-75">
                            <div class="card-body btn-card cursor">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-lg-7">
                                        <div class="h5 float-start"><i class="bi bi-dash-circle-fill text-danger float-start me-3"></i>Block</div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="h3 text-danger float-end">5</div>
                                    </div>
                                </div>
                                <div class="card-text">
                                    Bundle invoice user corporate belum tuntas
                                </div>
                            </div>
                            {{-- <div class="card-body">
                            </div> --}}
                        </div>
                    </div>
                </div>
                {{-- <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Family ID</td>
                            <td>Customer Name</td>
                            <td>Phone</td>
                            <td>Gender</td>
                            <td>Tanggal Lahir</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table> --}}
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Date</th>
                          <th scope="col">Bill Control</th>
                          <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-xl">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Detail Billing Control</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Corporate</th>
                              <th scope="col">Date & Time</th>
                              <th scope="col">Category</th>
                              <th scope="col">Transaction</th>
                              <th scope="col">Amount</th>
                              <th scope="col">Due Date</th>
                              <th scope="col">Status</th>
                              <th scope="col">Payment Method</th>
                              <th scope="col">Remarks</th>
                            </tr>
                        </thead>
                        <tbody id="">

                        </tbody>
                    </table>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     <div class="modal fade bs-example-modal-center" id="gambar_paspor">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Gambar Passpor</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('assets/passport/passport_example.png') }}" class="w-100" alt="">
            </div>
          </div>
        </div>
      </div>
     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Full Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Phone</b></label><br>
                            <label id="view_phone" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Gender</b></label><br>
                            <label id="view_gender" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>NIK</b></label><br>
                            <label id="view_nik" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Birthdate</b></label><br>
                            <label id="view_tanggal_lahir" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Age</b></label><br>
                            <label id="view_age" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Address</b></label><br>
                            <label id="view_address" style="font-weight: 400"></label>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Food Reference</b></label><br>
                            <label id="view_referensi" style="font-weight: 400"></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Passport Number</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_passport_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>POI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_poi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOI</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doi" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>DOE</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_doe" style="font-weight: 400"></label>
                            </div>
                        </div>
                    </div>
                    <label for="input-username"><b>Document</b></label>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <img src="" alt="" id="view_ktp" style="width: 100%" class="detail"name="KTP_KITAS">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <img src="" alt="" id="view_passport" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Visa</label>
                                <img src="" alt="" id="view_visa" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Detail -->
    <div class="modal fade bs-example-modal-center"  id="modalViewDetail">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="detail_dokumen" style="width: 100%">
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="btn-download">Download</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom_js')
    <script>
        var public = "{{asset('')}}";
        var today = new Date().toISOString().split('T')[0];

        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
        $('#modalInsert').on('shown.bs.modal', function() {
            console.log("modal show");
            $('.tooltips').tooltip({
                container: $(this)
            });
        });
    </script>
    <script src="{{asset("custom_js/billing_control.js")}}"></script>
@endsection
