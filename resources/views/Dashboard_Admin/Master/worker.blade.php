@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item active" aria-current="page">TL / Guide / Driver</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List TL / Guide / Driver</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah TL / Guide / Driver</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">

                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="id_filter" class="form-control filter_text" placeholder="TL / Guide / Driver ID">
                            </div>
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="TL / Guide / Driver Name">
                            </div>
                            <div class="col-4 mt-3 pt-1">
                                <div class="form-group mb-4">
                                    <select class="form-select select2 filter_select" data-trigger name="choices-single-default"
                                    id="type_filter">
                                        <option hidden value=''>Select Type</option>
                                        <option value="Tour Leader" >Tour Leader</option>
                                        <option value="Guide">Guide</option>
                                        <option value="Driver">Driver</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>ID</td>
                            <td>Nama</td>
                            <td>Type</td>
                            <td>Language</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-xl">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah TL / Guide / Driver</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <h6>General Information</h6>
                    <form action="" method="post" id="formInsert">
                        <div class="row">
                            <div class="col-4">
                                <label for="input-username">Name*</label>
                                <div class="form-floating mb-4">
                                    <input type="text" class="form-control add" id="nama" placeholder="Name TL / Guide / Driver">
                                    <label for="input-username">Nama TL / Guide / Driver</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Phone No*</label>
                                <div class="form-floating mb-4">
                                    <input type="text" class="form-control add" id="phone" placeholder="Phone No">
                                    <label for="input-username">Phone No</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Type*</label>
                                <select class="form-select select2" data-trigger name="choices-single-default"
                                id="type">
                                    <option value="Tour Leader" selected>Tour Leader</option>
                                    <option value="Guide">Guide</option>
                                    <option value="Driver">Driver</option>
                                </select>
                            </div>
                            <div class="col-5">
                                <label for="input-username">Email*</label>
                                <div class="form-floating mb-4">
                                    <input type="text" class="form-control add" id="email" placeholder="Email">
                                    <label for="input-username">Email</label>
                                </div>
                            </div>
                            <div class="col-7">
                                <label for="input-username">Address*</label>
                                <div class="form-floating mb-4">
                                    <input type="text" class="form-control add" id="address" placeholder="Address">
                                    <label for="input-username">Address</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h6>Languange</h6>
                        <div class="row mt-4">
                            <div class="col-4">
                                <label for="input-username">Language 1*</label>
                                <select class="form-select select2 language" data-trigger name="choices-single-default"
                                id="lg1">
                                    @foreach ($language as $item)
                                        @php
                                            if($loop->iteration==0) $selected="selected";
                                            else $selected="";
                                        @endphp
                                        <option value="{{$item}}" {{$selected}}>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Language 2</label>
                                <select class="form-select select2 language" data-trigger name="choices-single-default"
                                id="lg2">
                                    <option value="None" selected>None</option>
                                    @foreach ($language as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Language 3</label>
                                <select class="form-select select2 language" data-trigger name="choices-single-default"
                                id="lg3">
                                    <option value="None" selected>None</option>
                                    @foreach ($language as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                    <br>
                    <h6 class="mt-3 mb-3">Document</h6>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Photo</label>
                                <input class="form-control file" type="file" id="photo">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <input class="form-control file" type="file" id="ktp">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <input class="form-control file" type="file" id="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Drive License</label>
                                <input class="form-control file" type="file" id="drive">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Guide License</label>
                                <input class="form-control file" type="file" id="guide">
                            </div>
                        </div>
                    </div>

                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail TL / Guide / Driver</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label for="input-username"><b>Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_name" style="font-weight: 400"></label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="input-username"><b>Phone</b></label><br>
                            <label id="view_phone" style="font-weight: 400"></label>
                        </div>
                            <div class="col-4">
                                <label for="input-username"><b>Type</b></label><br>
                                <label id="view_type" style="font-weight: 400"></label>
                            </div>
                            <div class="col-4">
                                <label for="input-username"><b>Email</b></label><br>
                                <label id="view_email" style="font-weight: 400"></label>
                            </div>
                            <div class="col-4">
                                <label for="input-username"><b>Address</b></label><br>
                                <label id="view_address" style="font-weight: 400"></label>
                            </div>
                    </div>
                    <br>
                    <label for="input-username" ><b>Languange</b></label><br>
                    <div id="view_language" class="mb-4" style="font-weight: 400"></div>

                    <label for="input-username"><b>Document</b></label>
                    <div class="row">
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Photo</label>
                                <img src="" alt="" id="view_photo" style="width: 100%" class="detail" name="photo">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">KTP / KITAS</label>
                                <img src="" alt="" id="view_ktp" style="width: 100%" class="detail"name="KTP_KITAS">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Passport</label>
                                <img src="" alt="" id="view_passport" style="width: 100%" class="detail" name="passport">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Drive License</label>
                                <img src="" alt="" id="view_drive" style="width: 100%" class="detail" name="Drive_License">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Guide License</label>
                                <img src="" alt="" id="view_guide" style="width: 100%" class="detail" name="Guide_License">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Detail -->
    <div class="modal fade bs-example-modal-center"  id="modalViewDetail">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="detail_dokumen" style="width: 100%">
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="btn-download">Download</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal-->
@endsection
@section('custom_js')
    <script>
        var public = "{{asset('')}}";
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/worker.js")}}"></script>
@endsection
