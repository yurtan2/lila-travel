@extends('layout')

@section('body')
    @php
        $language = ["Abkhaz", "Acehnese", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bhojpuri", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa (Nyanja)", "Chinese (Mandarin)", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi (Maldivian)", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Filipino (Tagalog)", "Finnish", "French", "Fulah", "Galician", "Ganda", "Georgian", "German", "Greek", "Greenlandic", "Guarani", "Gujarati", "Haitian Creole", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue (Occidental)", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu (Gikuyu)", "Kinyarwanda", "Kirundi", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Kyrgyz", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maldivian (Divehi)", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Ndonga", "Nepali", "North Ndebele", "Northern Sami", "Norwegian", "Norwegian Bokmål", "Norwegian Nynorsk", "Nuosu (Sichuan Yi)", "Occitan", "Ojibwe (Ojibwa)", "Old Church Slavonic", "Oriya", "Oromo", "Ossetian", "Pāli", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Pushto", "Quechua", "Romanian", "Romansh", "Russian", "Samoan", "Sango", "Sanskrit", "Sardinian", "Scottish Gaelic", "Serbian", "Shona", "Sichuan Yi (Nuosu)", "Sindhi", "Sinhala (Sinhalese)", "Slovak", "Slovenian", "Somali", "South Ndebele", "Southern Sotho", "Spanish", "Sundanese", "Swahili", "Swati (Swazi)", "Swedish", "Tagalog (Filipino)", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western Frisian", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"];
    @endphp
    <style>
        .modal.fade {
        background: rgba(0, 0, 0, 0.5);
        }

        .modal-backdrop.fade {
        opacity: 0;
        }
        .detail{
            cursor: pointer;
        }
        .tooltip {
            z-index: 1000000;
        }
        .icon{
            color: gray;
        }
        .icon-danger:hover{
            color:#F17A7A!important;
        }
        .icon-info:hover{
            color:#16daf1!important;
        }
        .icon-warning:hover{
            color:#ffcc5a!important;
        }
    </style>
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
                <li class="breadcrumb-item"><a href="#">Customer Management</a></li>
                <li class="breadcrumb-item active" aria-current="page">Credit Corporate</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">Credit Corporate Setting</h5>
                        {{-- <button class="btn btn-success btn-add mt-2">+ Tambah Corporate</button> --}}
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-3">
                                <label class="form-label"></label>
                                <input type="search" id="filter_corporate_name" class="form-control" placeholder="Corporate Name">
                            </div>

                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Corporate Name</td>
                            <td>Credit Limit</td>
                            <td>Credit Used</td>
                            <td>Credit Available</td>
                            <td>Type due Date</td>
                            <td>Due Date</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" id="modalEdit">
         <div class="modal-dialog modal-dialog-centered modal-md">
             <div class="modal-content ">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Edit Corporate</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row mb-3">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="" alt="" id="corporate-image" class="w-100">
                            </div>
                            <div class="col-lg-7">
                                <div class="mb-3">
                                    <div class="h2" id="corporate_name"></div>
                                    <div class="h5" id="corporate_email"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Credit Limit</label>
                                    <div class="input-group">
                                        <span class="input-group-text">Rp.</span>
                                        <input type="text" class="form-control add need-check" id="credit-limit">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Credit Used</label>
                                    <input type="text" class="form-control add need-check" id="credit-used" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label for="input_tour_type" class="form-label">Schedule Due Date</label>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Type</label>
                                    <select class="form-select" name="" id="credit-type">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Cash">Cash</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="input_tour_type" class="form-label">Target</label>
                                    <select class="form-select" name="" id="credit-target"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <!-- Modal View-->
     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Log Credit Corporate</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!--
                    <div class="row justify-content-end">
                        <div class="col-lg-4">
                            <div class="form-floating mb-3">
                                <input type="search" class="form-control" placeholder="search data">
                                <label class="" for=""><b>Searh Data</b></label>
                            </div>
                        </div>
                    </div>-->
                    <table class="table" id="tblog">
                        <thead>
                            <tr>
                                <td>Category</td>
                                <td>Amount</td>
                                <td>User</td>
                                <td>Time</td>
                                <td>Due Type</td>
                                <td>Due Date</td>
                                <td>Message</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal List-->
    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalList">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">List Bill Corporate</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-between align-items-center mb-3">
                        <div class="col-lg-2">
                            <button class="btn btn-purple" type="button" id="btn-report">Report Bill</button>
                        </div>
                        <div class="col-lg-3">
                            <input type="search" class="form-control" placeholder="search data">
                        </div>
                    </div>
                    <label for="input-username"><b>PIC Information</b></label><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Date & TIme</td>
                                <td>Category</td>
                                <td>Transaction</td>
                                <td>Ammount</td>
                                <td>Due Date</td>
                                <td>Status</td>
                                <td>Payment Method</td>
                                <td>Remarks</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" tabindex="-1" role="dialog" aria-hidden="true"  data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal Report-->
    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalReport">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Report Bills</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="input-username"><b>PIC Information</b></label><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Start Date</td>
                                <td>End Date</td>
                                <td>All bills</td>
                                <td>Aging Bills</td>
                                <td>Unpaid Bills</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" tabindex="-1" role="dialog" aria-hidden="true"  data-bs-dismiss="modal">Close</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Detail -->
    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalViewDetail">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="detail_dokumen" style="width: 100%">
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="btn-download">Download</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        var public = "{{asset('/')}}";
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/credit_corporate.js")}}"></script>
@endsection
