@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Location</a></li>
              <li class="breadcrumb-item active" aria-current="page">Country</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Country</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Country</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="Country">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-country">
                    <thead>
                        <tr>
                            <td>Country Code</td>
                            <td>Country</td>
                            <td class="text-center">Visa</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Country </h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <label for="input-username">Country Name</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="nama" placeholder="Country Name">
                        <label for="input-username">Country Name</label>
                    </div>
                    <label for="input-username">Country Code</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="code" placeholder="Country Code">
                        <label for="input-username">Country Code</label>
                    </div>
                    <label for="input-username">Country Phone</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="phone" placeholder="Country Phone">
                        <label for="input-username">Country Phone</label>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->


     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Detail Visa</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="col-12">
                           <label for="input-username"><b>Visa Name</b></label><br>
                           <label id="view_visa_name" style="font-weight: 400"></label>
                       </div>
                   </div>
                   <label for="input-username"><b>Jenis Dokumen Diperlukan</b></label><br>
                   <div id="view_visa_dokumen" class="mb-3" style="font-weight: 400"></div>
                   <label for="input-username"><b>Detail</b></label>
                   <div class="form-floating mb-4">
                       <div id="view_visa_detail" ></div>
                   </div>
                </div>
                <div class="modal-footer">
                   <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                   <button class="btn btn-outline-success" id="btn-export-pdf" >Export PDF</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/country.js")}}"></script>
@endsection
