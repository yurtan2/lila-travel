@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Category</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product Category</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <h5 class="card-title pt-2">List Supplier</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Supplier</button>
                        <button class="btn btn-outline-success btn_import mt-2">+ Import Supplier</button>
                    </div>
                    <div class="col-12 col-md-5 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data Supplier</button>
                        <button class="btn btn-primary btn-rounded waves-effect waves-light" id="btn-export-pic">Export Data PIC</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter filter_text" id="nama_filter" class="form-control filter_text" placeholder="Supplier Name">
                            </div>
                            <div class="col-4 mt-3 pt-1">
                                <div class="form-group mb-4">
                                    <select class="form-select select2 filter_select" data-trigger name="choices-single-default"
                                    id="country_filter">
                                    </select>
                                </div>
                            </div>
                            <div class="col-4 mt-3 pt-1">
                                <div class="form-group mb-4">
                                    <select class="form-select select2 filter_select" data-trigger name="sp_category_id"
                                    id="supplier_category_filter">
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Name</td>
                            <td>Phone No</td>
                            <td>Category</td>
                            <td>Country</td>
                            <td  class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-xl">
             <div class="modal-content me-3">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Supplier </h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <form id="formInsert" method="post">
                        @csrf
                        <h6 class="mb-3">General Information</h6>
                        <div class="row">
                            <div class="col-4">
                                <label for="input-username">Supplier Name</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_name" id="supplier_name" placeholder="Supplier Name">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Phone No.</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_phone" id="supplier_phone" placeholder="Phone No.">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Supplier Category</label>
                                <div class="form-group mb-4">
                                    <select class="form-select select2" data-trigger name="sp_category_id"
                                    id="supplier_category">
                                    </select>
                                </div>
                                <input type="text" class="form-control add" id="view_category"  style="display: none">
                            </div>
                            <div class="col-4">
                                <label for="input-username">Email</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_email"  id="supplier_email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Location</label>
                                <div class="form-group mb-4">
                                    <select class="form-select select2" data-trigger name="country_id"
                                    id="country">
                                    </select>
                                </div>
                                <input type="text" class="form-control add" id="view_country"  style="display: none">
                            </div>
                            <div class="col-4">
                                <label for="input-username">Address</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_address" id="supplier_address" placeholder="Address">
                                </div>
                            </div>
                        </div>
                        <hr>

                        <h6 class="mb-3">Bank Information</h6>
                        <div class="row">
                            <div class="col-4">
                                <label for="input-username">Bank Name</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_bank_name" id="supplier_bank_name" placeholder="Bank Name">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Benificary Name</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_benificary_name" id="supplier_benificary_name" placeholder="Benificary Name">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Account Number</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_bank_account" id="supplier_bank_account" placeholder="Account Number">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">SWIFT Code</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_swift_code" id="supplier_swift_code" placeholder="SWIFT Code">
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="input-username">Currency</label>
                                <div class="form-group mb-4">
                                    <select class="form-select" name="supplier_curency" id="supplier_curency">
                                        <option value="EUR" selected>EUR</option>
                                        <option value="AOA">AOA</option>
                                        <option value="XCD">XCD</option>
                                        <option value="ARS">ARS</option>
                                        <option value="AMD">AMD</option>
                                        <option value="AUD">AUD</option>
                                        <option value="AZN">AZN</option>
                                        <option value="BSD">BSD</option>
                                        <option value="BHD">BHD</option>
                                        <option value="BDT">BDT</option>
                                        <option value="BBD">BBD</option>
                                        <option value="BYN">BYN</option>
                                        <option value="BZD">BZD</option>
                                        <option value="XOF">XOF</option>
                                        <option value="BTN">BTN</option>
                                        <option value="BOB">BOB</option>
                                        <option value="BAM">BAM</option>
                                        <option value="BWP">BWP</option>
                                        <option value="BRL">BRL</option>
                                        <option value="BND">BND</option>
                                        <option value="BGN">BGN</option>
                                        <option value="BIF">BIF</option>
                                        <option value="KHR">KHR</option>
                                        <option value="XAF">XAF</option>
                                        <option value="CAD">CAD</option>
                                        <option value="CVE">CVE</option>
                                        <option value="CLP">CLP</option>
                                        <option value="CNY">CNY</option>
                                        <option value="COP">COP</option>
                                        <option value="KMF">KMF</option>
                                        <option value="CRC">CRC</option>
                                        <option value="CUP">CUP</option>
                                        <option value="CZK">CZK</option>
                                        <option value="CDF">CDF</option>
                                        <option value="DKK">DKK</option>
                                        <option value="DJF">DJF</option>
                                        <option value="DOP">DOP</option>
                                        <option value="USD">USD</option>
                                        <option value="ERN">ERN</option>
                                        <option value="SZL">SZL</option>
                                        <option value="ETB">ETB</option>
                                        <option value="FJD">FJD</option>
                                        <option value="GMD">GMD</option>
                                        <option value="GEL">GEL</option>
                                        <option value="GHS">GHS</option>
                                        <option value="GTQ">GTQ</option>
                                        <option value="GNF">GNF</option>
                                        <option value="GYD">GYD</option>
                                        <option value="HTG">HTG</option>
                                        <option value="HNL">HNL</option>
                                        <option value="HUF">HUF</option>
                                        <option value="ISK">ISK</option>
                                        <option value="INR">INR</option>
                                        <option value="IDR">IDR</option>
                                        <option value="IRR">IRR</option>
                                        <option value="IQD">IQD</option>
                                        <option value="ILS">ILS</option>
                                        <option value="JMD">JMD</option>
                                        <option value="JPY">JPY</option>
                                        <option value="JOD">JOD</option>
                                        <option value="KZT">KZT</option>
                                        <option value="KES">KES</option>
                                        <option value="KPW">KPW</option>
                                        <option value="KRW">KRW</option>
                                        <option value="KWD">KWD</option>
                                        <option value="KGS">KGS</option>
                                        <option value="LAK">LAK</option>
                                        <option value="LBP">LBP</option>
                                        <option value="LSL">LSL</option>
                                        <option value="LRD">LRD</option>
                                        <option value="LYD">LYD</option>
                                        <option value="MGA">MGA</option>
                                        <option value="MWK">MWK</option>
                                        <option value="MYR">MYR</option>
                                        <option value="MVR">MVR</option>
                                        <option value="MRO">MRO</option>
                                        <option value="MUR">MUR</option>
                                        <option value="MXN">MXN</option>
                                        <option value="MDL">MDL</option>
                                        <option value="MNT">MNT</option>
                                        <option value="MAD">MAD</option>
                                        <option value="MZN">MZN</option>
                                        <option value="MMK">MMK</option>
                                        <option value="NAD">NAD</option>
                                        <option value="NPR">NPR</option>
                                        <option value="NZD">NZD</option>
                                        <option value="NIO">NIO</option>
                                        <option value="NGN">NGN</option>
                                        <option value="MKD">MKD</option>
                                        <option value="NOK">NOK</option>
                                        <option value="OMR">OMR</option>
                                        <option value="PKR">PKR</option>
                                        <option value="PAB">PAB</option>
                                        <option value="PGK">PGK</option>
                                        <option value="PYG">PYG</option>
                                        <option value="PEN">PEN</option>
                                        <option value="PHP">PHP</option>
                                        <option value="PLN">PLN</option>
                                        <option value="QAR">QAR</option>
                                        <option value="RON">RON</option>
                                        <option value="RUB">RUB</option>
                                        <option value="RWF">RWF</option>
                                        <option value="WST">WST</option>
                                        <option value="STD">STD</option>
                                        <option value="SAR">SAR</option>
                                        <option value="RSD">RSD</option>
                                        <option value="SCR">SCR</option>
                                        <option value="SLL">SLL</option>
                                        <option value="SGD">SGD</option>
                                        <option value="SBD">SBD</option>
                                        <option value="SOS">SOS</option>
                                        <option value="ZAR">ZAR</option>
                                        <option value="SSP">SSP</option>
                                        <option value="LKR">LKR</option>
                                        <option value="SDG">SDG</option>
                                        <option value="SRD">SRD</option>
                                        <option value="SEK">SEK</option>
                                        <option value="CHF">CHF</option>
                                        <option value="SYP">SYP</option>
                                        <option value="TWD">TWD</option>
                                        <option value="TJS">TJS</option>
                                        <option value="TZS">TZS</option>
                                        <option value="THB">THB</option>
                                        <option value="TOP">TOP</option>
                                        <option value="TTD">TTD</option>
                                        <option value="TND">TND</option>
                                        <option value="TRY">TRY</option>
                                        <option value="TMT">TMT</option>
                                        <option value="UGX">UGX</option>
                                        <option value="UAH">UAH</option>
                                        <option value="AED">AED</option>
                                        <option value="GBP">GBP</option>
                                        <option value="UYU">UYU</option>
                                        <option value="UZS">UZS</option>
                                        <option value="VUV">VUV</option>
                                        <option value="VEF">VEF</option>
                                        <option value="VND">VND</option>
                                        <option value="YER">YER</option>
                                        <option value="ZMW">ZMW</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="supplier_id" id="supplier_id">
                            <input type="hidden" name="supplier_pic" id="supplier_pic">
                        </div>
                    </form>
                        <hr>
                        <h6 class="mb-3">PIC Information</h6>
                        <table class="table" id="table-pic">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Phone No.</td>
                                    <td>Email</td>
                                    <td>Jabatan</td>
                                    <td class="text-center">Action</td>
                                </tr>
                            </thead>
                            <tbody id="body-pic">

                            </tbody>
                        </table>

                        <div class="row input-pic mb-4">
                            <div class="col-3">
                                <label for="input-username">Name</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_pic_name[]" id="pic_name" placeholder="PIC Name">
                                </div>
                            </div>
                            <div class="col-3">
                                <label for="input-username">Phone No.</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_pic_phone[]" id="pic_phone" placeholder="PIC Phone No.">
                                </div>
                            </div>
                            <div class="col-3">
                                <label for="input-username">Email</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_pic_email[]" id="pic_email" placeholder="PIC Email">
                                </div>
                            </div>
                            <div class="col-3">
                                <label for="input-username">Jabatan</label>
                                <div class="form-group mb-4">
                                    <input type="text" class="form-control add" name="supplier_pic_jabatan[]" id="pic_jabatan" placeholder="PIC Jabatan">
                                </div>
                            </div>
                            <div class="col-12 text-end pt-0">
                                <button class="btn btn-outline-success btn-sm btn-add-pic" type="button"><i data-feather="plus" style="width: 17px"></i> Add PIC</button>
                            </div>

                        </div>


                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

    <!-- Modal Import -->
    <div class="modal" tabindex="-1" id="modal_import">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Import Data Supplier</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/master/importSupplier" id="form_import"  enctype="multipart/form-data" class="dropzone">
                    @csrf
                    <div class="fallback">
                        <input name="file" type="file" id="file">
                    </div>
                </div>
                <div class="modal-footer" style="justify-content:none">
                    <a href="/downloadTemplate" class="btn btn-info">Download Template</a>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>


    <script src="{{asset("custom_js/supplier.js")}}"></script>
    <script>
        var dropzone = new Dropzone("#form_import",{
           maxFilesize: 1,
           acceptedFiles: ".xls,.xlsx"
       });
       dropzone.on("success", function(file,response) {
           dropzone.removeFile(file);
           toastr.success("Berhasil Insert Supplier","Berhasil Insert!");
           get_data();
           $('.modal').modal("hide");
       });
       dropzone.on("complate", function(file,response) {
           dropzone.removeFile(file);
       });
       dropzone.on("error", function(file,response) {
           dropzone.removeFile(file);
           toastr.error(response.message,"Gagal Insert!");

       });
     </script>
@endsection
