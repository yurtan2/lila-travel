@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Terms</a></li>
              <li class="breadcrumb-item active" aria-current="page">Inclusion</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List Inclusion</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Inclusion</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="Inclusion">
                            </div>
                            <div class="col-4 mt-3 pt-1">
                                <div class="form-group mb-4">
                                    <select class="form-select select2 filter_select" data-trigger name="choices-single-default"
                                    id="product_category_filter">
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-term">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Inclusion Name</td>
                            <td>Product Category</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Tambah Inclusion</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">

                    <label for="input-username">Inclusion Name</label>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control add" id="nama" name="nama" placeholder="Term & Condition Name">
                        <label for="input-username">Inclusion Name</label>
                    </div>
                    <div class="form-group mb-4">
                        <label for="input-username">Product Category</label>
                        <select class="form-select select2" data-trigger name="choices-single-default"
                        id="product_category" ><!--multiple="multiple"-->
                        </select>
                    </div>
                    <label for="input-username">Detail</label>
                    <div class="form-floating mb-4">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <td>Inclusion</td>
                                    <td class="text-center">Action</td>
                                </tr>
                            </thead>
                            <tbody  id="body-inclusion">

                            </tbody>
                        </table>
                        <div class="row input-data">
                            <div class="col-8">
                                <label class="form-label"></label>
                                <input type="search" name="insert-inclusion" id="insert-inclusion" class="form-control" placeholder="Inclusion">
                            </div>
                            <div class="col-4 pt-1">
                                <button class="btn btn-outline-success mt-3 btn-add-inclusion">+ Add Inclusion</button>
                            </div>
                        </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalView">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="title">Detail Inclusion</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <label for="input-username"><b>Inclusion Name</b></label><br>
                            <div class="form-group mb-4">
                                <label id="view_term_name"></label>
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="input-username"><b>Product Category</b></label><br>
                            <label id="view_product"></label>
                        </div>
                    </div>
                    <label for="input-username"><b>Detail</b></label>
                    <div class="form-floating mb-4">
                        <div id="view_term_detail"></div>
                    </div>
                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-success btn-direct-edit">Edit</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->


@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/inclusion.js")}}"></script>
@endsection
