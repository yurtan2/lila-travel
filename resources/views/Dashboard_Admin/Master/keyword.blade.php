@extends('layout')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item active" aria-current="page">Keyword</li>
            </ol>
          </nav>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <h5 class="card-title pt-2">List keyword</h5>
                        <button class="btn btn-success btn-add mt-2">+ Tambah Keyword</button>
                    </div>
                    <div class="col-12 col-md-4 text-end">
                        <br><br>
                        <button class="btn btn-light btn-rounded waves-effect waves-light me-3" id="btn-filter">
                            <i class="bx bx-filter-alt font-size-16 align-middle me-2"></i>Filter
                        </button>
                        <button class="btn btn-info btn-rounded waves-effect waves-light" id="btn-export">Export Data</button>
                    </div>
                </div>
                <!-- inside filter -->
                <div class="row p-2"  id="inner-filter" style="display: none">
                    <div class="col-12 bg-light p-3 mt-3" style="border-radius: 10px">
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"></label>
                                <input type="search" name="nama_filter" id="nama_filter" class="form-control filter_text" placeholder="Keyword Name">
                            </div>
                            <div class="col-4 mt-3 pt-1">
                                <div class="form-group mb-4">
                                    <select class="form-select select2 filter_select" data-trigger name="choices-single-default"
                                    id="keyword_filter">
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-secondary waves-effect waves-light mt-3" id="btn-reset">
                                    <i class="bx bx bx-rotate-right font-size-16 align-middle "></i>
                                </button>
                                <button class="btn btn-outline-success waves-effect waves-light mt-3" id="btn-search">
                                    <i class="bx bx-search-alt-2 font-size-16 align-middle "></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="tb-category">
                    <thead>
                        <tr>
                            <td>
                                Created At <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>
                                Last Update <br>
                                <small class="text-muted">(DD/MM/YYYY)</small>
                            </td>
                            <td>Keyword Name</td>
                            <td>Keyword Category</td>
                            <td class="text-center">Keyword</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true" id="modalInsert">
         <div class="modal-dialog modal-dialog-centered modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                    <h4 class="modal-title mb-0 flex-grow-1">Tambah Keyword</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                 <div class="modal-body">
                    <ul class="nav justify-content-end nav-tabs-custom rounded card-header-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#home2" menu="keyword" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Keyword</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#profile2" menu="category" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Category</span>
                            </a>
                        </li>
                    </ul>
                    <br>
                    <!-- Tab panes -->
                    <div class="tab-content text-muted">
                        <div class="tab-pane active" id="home2" role="tabpanel">
                            <label for="input-username">Keyword Name</label>
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control add" id="nama" placeholder="Keyword Name">
                                <label for="input-username">Keyword Name</label>
                            </div>

                            <label for="input-username">Keyword Category</label>
                            <div class="form-group">
                                <select name="category" id="category" class="form-select"></select>
                            </div>
                            <label for="input-username" class="mt-3">Keyword</label>
                            <div class="form-group">
                                <textarea name="keyword" id="keyword" cols="30" rows="10" class="form-control add"
                                placeholder="Bangkok, Pattaya, ..."></textarea>
                            </div>

                        </div>
                        <div class="tab-pane" id="profile2" role="tabpanel">
                            <h6>Tambah Keyword Category</h6>
                            <div class="row">
                                <div class="col-9 form-group">
                                    <input type="text" name="keyword_category_name" id="keyword_category_name" class="form-control" placeholder="Nama Keyword Category">
                                </div>
                                <div class="col-3">
                                    <button type="button" class="btn btn-success" id="btn-add-category">+ Tambah Category</button>
                                </div>
                            </div>
                            <table class="table mt-3">
                                <thead>
                                    <tr>
                                        <td>Nama Category</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody id="body-category">

                                </tbody>
                            </table>
                        </div>

                 </div>
                 <div class="modal-footer">
                    <button class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button class="btn btn-success" id="btn-insert">Simpan</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
@endsection

@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/keyword.js")}}"></script>
@endsection
