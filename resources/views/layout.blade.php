<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Lila Travel & Tour</title>
        <link rel="icon" type="image/png" href="{{asset('images/logo-gradient.png')}}" />
        <meta name="csrf-token" content="{{ csrf_token() }}"  id="_token"/>
        @include('Libary_Navbar.general_libary')
    </head>
    <body data-topbar="dark">
        <style>
            .default2{

                border-radius: 10px;

                padding-left: 30px;

                padding-right: 30px;

                background-image: linear-gradient(50deg, #FFC876, #FB48FF);

                border:none;

                color:white;

            }
            .default-check{
                border: 2px solid #B376FF;
                background-color: rgb(250, 250, 250);
                border-radius: 5px
            }
            .icon-hover:hover{
                border: 2px solid #B376FF;
                background-color: rgb(250, 250, 250);
                border-radius: 5px
            }
        </style>
        @include('General.modal_general')
        <div id="layout-wrapper">
            @include('Libary_Navbar.navbar')
            @include('Libary_Navbar.sidenav')
            <div class="main-content" style="overflow-y:auto">
                <div class="page-content">
                    @yield('body')
                </div>
            </div>
        </div>
    </body>
</html>
@include('Libary_Navbar.general_js')
@yield('custom_js')
