    <!-- JAVASCRIPT -->
    <script src="{{asset("/libs/jquery/jquery.min.js")}}"></script>
    <script src="{{asset("/libs/bootstrap/js/bootstrap.bundle.min.js")}}"></script>
    <script src="{{asset("/libs/metismenu/metisMenu.min.js")}}"></script>
    <script src="{{asset("/libs/simplebar/simplebar.min.js")}}"></script>
    <script src="{{asset("/libs/node-waves/waves.min.js")}}"></script>
    <script src="{{asset("/libs/feather-icons/feather.min.js")}}"></script>
    <!-- pace js -->
    <script src="{{asset("/libs/pace-js/pace.min.js")}}"></script>

    <!-- apexcharts -->
    <script src="{{asset("/libs/apexcharts/apexcharts.min.js")}}"></script>

    <!-- Plugins js-->
    <script src="{{asset("/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js")}}"></script>
    <script src="{{asset("/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js")}}"></script>
    <!-- dashboard init -->
    <script src="{{asset("/js/pages/dashboard.init.js")}}"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

    <script src="{{asset("/js/app.js")}}"></script>
    <!-- Toastr -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- editor -->
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <!-- dropzone js -->
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>

   <script>
        //custom script
        $('.table').css({"width":"100%"});
        $("input").attr('autocomplete', 'off');
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
        var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
        var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
            return new bootstrap.Popover(popoverTriggerEl)
        })
        //setting toast
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }


        $(document).on("input", ".number-only", function() {
            $(this).val($(this).val().replace(/[^0-9]/g, ''));

            if ($(this).val()[0] === '0') {
                $(this).val($(this).val().substring(1));
            }
        })

        $(document).on("input", ".is-passport", function(){
            $(this).val($(this).val().replace(/[^a-zA-Z0-9]/g, '').toUpperCase());
        })

        $(document).on("keydown", ".filter_text", function(e){
            if(e.which == 13){
                $('#btn-search').click();
            }
        })

        $(document).on("change", ".filter_select", function(){
            $('#btn-search').click();
        })

        //munculin modal delete
        function showModalDelete(text,button_id) {
            //button id ini, id button ketika dikofrimasi delete
            $("#text-delete").html(text);
            $(".btn-konfirmasi").attr("id",button_id);
            $('#modalDelete').modal("show");
        }

        function closeModalDelete() {
            $('#modalDelete').modal("hide");
        }

        //autocomplate
        function autocompleteProductCategory(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteProductCategory",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Product Category",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }
        function autocompleteSupplierCategory(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteSupplierCategory",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Supplier Category",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteSupplier(id,modalParent=null,country=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteSupplier",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            "country_id": country,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Supplier",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteCountry(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteCountry",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Country",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteStates(id,modalParent=null,country_id) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteStates",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            "country_id": country_id,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama State",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteVehicleType(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteVehicleType",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Vehicle Type",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteKeywordCategory(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteKeywordCategory",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },
                placeholder: "Nama Keyword Category",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteKeywords(id,modalParent=null) {
            $(id).select2({
                ajax: {
                    url: "/autocompleteKeyword",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };
                    },
                },

                placeholder: "Nama Keyword",
                closeOnSelect: true,
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }


        function autocompleteDisplayer(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteDisplayer",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                allowClear: true,
                placeholder: "Displayer Title",
                closeOnSelect: true,
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }


        function autocompleteLocation(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteLocation",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                minimumInputLength: 3,
                language: {
                    inputTooShort: function() {
                        return 'Please add up to 3 characters for accurate result';
                    }
                },
                placeholder: "City Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteInclusion(id,modalParent=null) {
            console.log(id);
            $(id).select2({
                ajax: {
                    url: "/autocompleteInclusion",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Inclusion Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteAirport(id,modalParent=null,placeholder) {

            $(id).select2({
                ajax: {
                    url: "/autocompleteAirport",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: placeholder,
                closeOnSelect: true,
                // matcher: matchStart,
                minimumInputLength: 3,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteExclusion(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteExclusion",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Exclusion Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteTermCondition(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteTermCondition",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Term & Condition Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteCorporate(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteCorporate",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Corporate Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompletePosition(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompletePosition",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Jabatan",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteAirlines(id,modalParent=null) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteAirlines",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Select Airlines",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteCustomer(id,modalParent=null,$parent=null,unique=0) {
            //search customer
            $(id).select2({
                ajax: {
                    url: "/autocompleteCustomer",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            "unique": unique,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        if($parent){
                            var idx = -1;
                            data.data.forEach((item,index) => {
                                if(item.customer_id==$parent){
                                    idx = index;
                                }
                            });
                            if(idx!=-1)data.data.splice(idx,1);
                        }
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Customer Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteCorporatePIC(id,modalParent=null,corporate_id) {
            //search customer
            $(id).select2({
                ajax: {
                    url: "/autocompleteCorporatePIC",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            "corporate_id": corporate_id,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                    templateResult: function(data) {
                        return data.html;
                    },
                },

                placeholder: "PIC Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        function autocompleteTour(id,modalParent=null,data_tour) {
            //search country dan city
            $(id).select2({
                ajax: {
                    url: "/autocompleteTour",
                    dataType: "json",
                    type: "post",
                    data: function data(params) {
                        return {
                            "keyword": params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        };
                    },
                    processResults: function processResults(data) {
                        var final = [];
                        console.log(data.data.data);
                        data.data.data.forEach(item => {
                            var ada=-1;
                            data_tour.forEach((item_tour,index) => {
                                if(item.tour_id==item_tour.tour_id){ada=1}
                            });
                            console.log("Test");
                            if(ada==-1)final.push(item);
                        });
                        console.log(final);
                        data.data.data = final;
                        return {
                            results: $.map(data.data, function (item) {
                                return item;
                            }),
                        };

                    },
                },
                placeholder: "Tour Name",
                closeOnSelect: true,
                theme: "bootstrap-5",
                width:"100%",
                dropdownParent: modalParent ? $(modalParent) : "",
            });
        }

        //helper
        function formatRupiah(angka, prefix) {
            angka = angka.toString();
            var number_string = angka.replace(/[^,\d]/g, "").toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
        }

        function convertToAngka(rupiah) {
            return parseInt(rupiah.replace(/,.*|[^0-9]/g, ""), 10);
        }

        function getCurrentDate() {
            var local = new Date();
            local.setMinutes(local.getMinutes() - local.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        }

        function getCurrentDateTime() {
            var local = new Date();
            local.setMinutes(local.getMinutes() - local.getTimezoneOffset());
            console.log(local.toJSON());
            return local.toJSON().slice(0,16);
        }

        function addDays(date,durasi,output) {
            //nambah dari dari date
            var checkin = new Date($(date).val());
            var checkout = new Date();
            checkout.setDate(checkin.getDate()+parseInt(durasi));
            $(output).val(checkout.getFullYear()+"-"+((checkout.getMonth()+1)+"").padStart(2,"0")+"-"+checkout.getDate()).trigger('change');
        }

        function addDayTime(date,durasi,output) {
            //nambah dari dari date
            var date = new moment($(date).val());
            date.add(parseInt(durasi),"days");
            $(output).val(date.format('YYYY-MM-DD HH:mm')).trigger('change');
        }

        function addDaysToDate(date, daysToAdd) {
            const newDate = new Date(date);
            newDate.setDate(newDate.getDate() + Number(daysToAdd));
            return newDate;
        }
    </script>
