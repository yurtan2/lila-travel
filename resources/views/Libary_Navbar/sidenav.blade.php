 <style>
    .disabled-item {
        pointer-events: none;
        opacity: 0.5;
    }
    .menu-title{
        pointer-events:auto!important;
    }

 </style>
 <!-- ========== Left Sidebar Start ========== -->
 <div class="vertical-menu">

     <div data-simplebar class="h-100">

         <!--- Sidemenu -->
         <div id="sidebar-menu">
             <!-- Left Menu Start -->
             <ul class="metismenu list-unstyled" id="side-menu">
                 <li class="menu-title" data-key="t-menu">Menu</li>

                 <li class="disabled-item">
                     <a href="index.html">
                         <i data-feather="home"></i>
                         <span data-key="t-dashboard">Dashboard</span>
                     </a>
                 </li>

                 <li class="menu-title  menu-header" style="cursor: pointer" data-key="t-apps" onclick="expand('Master')" menu="Master" >Master</li>

                 <li class="Master">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="grid"></i>
                         <span data-key="t-ecommerce">Category</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('product_category') }}" key="t-products">Products</a></li>
                         <li><a href="{{ route('supplier_category') }}" key="t-products">Suplier</a></li>
                         <li><a href="{{ route('hotel_room_category') }}" key="t-products">Hotel Room</a></li>
                         <li><a href="{{ route('hotel_meal_category') }}" key="t-products">Hotel Meal</a></li>
                         <li><a href="{{ route('hotel_service') }}" key="t-products">Hotel Service</a></li>
                         <li><a href="{{ route('corporate_position') }}" key="t-products">Corporate Position</a></li>
                     </ul>
                 </li>
                 <li class="Master">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="map-pin"></i>
                         <span data-key="t-ecommerce">Location</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('country') }}" key="t-products">Country</a></li>
                         <li><a href="{{ route('city') }}" key="t-products">City</a></li>
                     </ul>
                 </li>
                 <li class="Master">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="file"></i>
                         <span data-key="t-ecommerce">Travel Documents</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('document') }}" key="t-products">Document</a></li>
                         <li><a href="{{ route('visa') }}" key="t-products">Visa</a></li>
                         <li><a href="{{ route('passport') }}" key="t-products">Passport</a></li>
                     </ul>
                 </li>
                 <li class="Master">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="truck"></i>
                         <span data-key="t-ecommerce">Vehicle</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('vehicle_type') }}" key="t-products">Vehicle Type</a></li>
                         <li><a href="{{ route('vehicle') }}" key="t-products">Vehicle Name</a></li>
                     </ul>

                 </li>
                 <li class="Master">
                     <a href="{{ route('supplier') }}">
                         <i data-feather="users"></i>
                         <span data-key="t-dashboard">Supplier</span>
                     </a>
                 </li>
                 <li class="Master">
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="shopping-bag"></i>
                        <span data-key="t-ecommerce">Customer Management</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('customer') }}" key="t-products">Customer</a></li>
                        <li><a href="{{ route('family_tree') }}" key="t-products">Family Tree</a></li>
                        <li><a href="{{ route('corporate') }}" key="t-products">Corporate</a></li>
                        <li><a href="{{ route('credit_corporate') }}" key="t-products">Credit Corporate Setting</a></li>
                        <li><a href="{{ route('billing_control') }}" key="t-products">Billing Control</a></li>
                    </ul>
                </li>
                 <!--<li>
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="briefcase"></i>
                        <span data-key="t-ecommerce">Corporate</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('customer') }}" key="t-products">Corporate</a></li>
                    </ul>
                </li>-->
                 <li class="Master">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="file-text"></i>
                         <span data-key="t-ecommerce">Terms</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('term_condition') }}" key="t-products">Term & Condition</a></li>
                         <li><a href="{{ route('inclusion') }}" key="t-products">Inclusion</a></li>
                         <li><a href="{{ route('exclusion') }}" key="t-products">Exclusion</a></li>
                     </ul>
                 </li>
                 <li class="Master">
                     <a href="{{ route('user_category') }}">
                         <i data-feather="command"></i>
                         <span data-key="t-ecommerce">User Category</span>
                     </a>
                 </li>
                 <li class="Master">
                     <a href="{{ route('keyword') }}">
                         <i data-feather="tag"></i>
                         <span data-key="t-ecommerce">Keyword</span>
                     </a>
                 </li>
                 <li class="Master">
                     <a href="{{ route('worker') }}">
                         <i data-feather="user"></i>
                         <span data-key="t-ecommerce">TL / Guide / Driver</span>
                     </a>
                 </li>
                 <li class="Master">
                     <a href="{{ route('displayer') }}">
                         <i data-feather="copy"></i>
                         <span data-key="t-ecommerce">Displayer</span>
                     </a>
                 </li>

                 <li class="menu-title menu-header" data-key="t-apps" onclick="expand('Product')"  menu="Product">Product</li>

                 <li class="Product ">
                     <a href="{{route('flight_ticket')}}">
                         <ion-icon name="airplane-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Flight Tiket</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{fit_tour')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product" >
                     <a href="{{ route('hotel') }}" class="has-arrow">
                         <ion-icon name="bed-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Hotel</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <ion-icon name="restaurant-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Restaurant</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <ion-icon name="golf-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Attraction</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <ion-icon name="ticket-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Event</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <ion-icon name="train-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Transport</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="disabled-item Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <ion-icon name="document-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                         <span data-key="t-ecommerce">Travel Document</span>
                     </a>
                     {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('fix_departure')}}" key="t-products">Fixed Departure</a></li>
                        <li><a href="{{route('fit_departure')}}" key="t-products">FIT Departure</a></li>
                    </ul> --}}
                 </li>

                 <li class="Product">
                     <a href="javascript: void(0);" class="has-arrow">
                         <i data-feather="map"></i>
                         <span data-key="t-ecommerce">Tour</span>
                     </a>
                     <ul class="sub-menu" aria-expanded="false">
                         <li><a href="{{ route('fix_departure') }}" key="t-products">Fixed Departure</a></li>
                         <li><a href="{{ route('fit_tour') }}" key="t-products">FIT Tour</a></li>
                     </ul>
                 </li>

                 <li class="menu-title" data-key="t-apps" onclick="expand('Booking')">Booking</li>
                 <li class="Booking">
                    <a href="{{ route('booking_tour') }}">
                        <ion-icon name="albums-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                        <span data-key="t-ecommerce">Tour</span>
                    </a>
                </li>
                <li class="disabled-item Booking" id="text">
                    <a href="{{ route('scanDocument') }}">
                        <ion-icon name="document-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                        <span data-key="t-ecommerce">Scan Document</span>
                    </a>
                </li>
                <li class="Ticketing-Project" id="text">
                    <a href="{{ route('ticketingProject') }}">
                        <ion-icon name="document-outline" style="font-size: 21px;margin-right:9px"></ion-icon>
                        <span data-key="t-ecommerce">Ticketing Project</span>
                    </a>
                </li>
         </div>
         <!-- Sidebar -->
     </div>
 </div>
 <!-- Left Sidebar End -->
<script>
    function expand(menu) {
        $("."+menu).toggle();
        $("."+menu).toggleClass("mm-active");
    }
</script>
