    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset("/images/favicon.ico")}}">

    <!-- plugin css -->
    <link href="{{asset("/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css")}}" rel="stylesheet" type="text/css" />

    <!-- preloader css -->
    <link rel="stylesheet" href="{{asset("/css/preloader.min.css")}}" type="text/css" />

    <!-- Bootstrap Css -->
    <link href="{{asset("/css/bootstrap.min.css")}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset("/css/icons.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset("/css/app.min.css")}}" id="app-style" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>

    {{-- Bootstrap Icon --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
    <script src="https://rawgit.com/moment/moment/2.2.1/min/moment.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    <script src="{{asset('js/pages/rating.init.js')}}"></script>
    <script src="{{asset('libs/rater-js/index.js')}}"></script>
    <style>
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            background-color:#34c38f;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            padding-top:15px;
            font-size:15pt
        }
        .float:hover{
            color:#4c4c4c
        }
        .select2-selection__rendered{
            font-size: .8125rem!important;
        }
        .text-purple{
            color: #7C41FF;
        }
        .btn-purple{
            background-color: #7C41FF;
            color: white;
        }
        .btn-purple-gradient{
            background-image: linear-gradient(to right, #B376FF, #7C41FF);
            color: white;
        }
        .btn-purple:hover{
            background-color: #6837d2;
            color: white;
        }
        .btn-purple-gradient:hover{
            background-image: linear-gradient(to right, #b880fd, #8b56fd);
            color: white;
        }
        .btn-outline-purple{
            border: 1px solid #7C41FF;
            color: #7C41FF;
        }
        .btn-outline-purple:hover{
            background-color: #7C41FF;
            color: white;
        }
        .cursor:hover{
            cursor: pointer;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
    <!-- Include stylesheet -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

        <!-- dropzone css -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css" rel="stylesheet" type="text/css" />

    <!-- Ionic icon-->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
