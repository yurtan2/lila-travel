@extends('layout')

@section('body')
<div class="p-3 pt-0">
    <div class="card">
        <div class="card-body p-4">
            <h4 class="mb-4">Scan Document</h4>
            <form action="/uploadOCR" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" id="file">
                <button class="btn btn-success" type="submit">upload</button>
            </form>
            <h6 class="mt-3">Result Data : </h6>
            <div class="p-3" style="min-height:200px;"></div>
        </div>
    </div>
 
</div>
    
@endsection