@extends('layout_customer')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            </ol>
          </nav>
        <div class="card">

        </div>
    </div>

@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/city.js")}}"></script>
@endsection
