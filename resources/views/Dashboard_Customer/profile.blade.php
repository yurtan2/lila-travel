@extends('layout_customer')

@section('body')
    <div class="p-3 pt-0">
        <nav  style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb ps-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            </ol>
          </nav>
          <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="profile-user"></div>
                </div>
            </div>

            <div class="row">
               <div class="profile-content">
                   <div class="row align-items-end">
                        <div class="col-sm">
                            <div class="d-flex align-items-end mt-3 mt-sm-0">
                                <div class="flex-shrink-0">
                                    <div class="avatar-xxl me-3">
                                        <img src="assets/images/users/avatar-3.jpg" alt="" class="img-fluid rounded-circle d-block img-thumbnail">
                                    </div>
                                </div>
                                <div class="flex-grow-1">
                                    <div>
                                        <h5 class="font-size-16 mb-1">Melinda Rivera</h5>
                                        <p class="text-muted font-size-13 mb-2 pb-2">Full Stack Developer</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>

            <div class="row">
                <div class="col-xl-8 col-lg-7">
                    <div class="tab-content">
                        <div class="tab-pane active" id="overview" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">About</h5>
                                </div>

                                <div class="card-body">
                                    <div>
                                        <div class="pb-3">
                                            <h5 class="font-size-15">Bio :</h5>
                                            <div class="text-muted">
                                                <p class="mb-2">Hi I'm Phyllis Gatlin, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
                                                <p class="mb-2">It is a long established fact that a reader will be distracted by the readable content of a page when looking at it has a more-or-less normal distribution of letters</p>
                                                <p>It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is. The European languages are members of the same family. Their separate existence is a myth.</p>

                                                <ul class="list-unstyled mb-0">
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Donec vitae sapien ut libero venenatis faucibus</li>
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Quisque rutrum aenean imperdiet</li>
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Integer ante a consectetuer eget</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="pt-3">
                                            <h5 class="font-size-15">Experience :</h5>
                                            <div class="text-muted">
                                                <p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is. The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc</p>

                                                <ul class="list-unstyled mb-0">
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Donec vitae sapien ut libero venenatis faucibus</li>
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Quisque rutrum aenean imperdiet</li>
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Integer ante a consectetuer eget</li>
                                                    <li class="py-1"><i class="mdi mdi-circle-medium me-1 text-success align-middle"></i>Phasellus nec sem in justo pellentesque</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end tab pane -->


                        <!-- end tab pane -->
                    </div>
                    <!-- end tab content -->
                </div>
                <!-- end col -->

                <div class="col-xl-4 col-lg-5">

                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Document</h5>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap">
                                    <tbody>
                                        <tr>
                                            <td><h5 class="font-size-14 m-0"><a href="javascript: void(0);" class="text-dark">Passport</a></h5></td>

                                            <td>
                                                <i class="mdi mdi-circle-medium font-size-18 text-success align-middle me-1"></i> Online
                                            </td>
                                            <td>
                                               <button class="btn btn-outline-success">Upload</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h5 class="font-size-14 m-0"><a href="javascript: void(0);" class="text-dark">Visa</a></h5></td>

                                            <td>
                                                <i class="mdi mdi-circle-medium font-size-18 text-success align-middle me-1"></i> Online
                                            </td>
                                            <td>
                                               <button class="btn btn-outline-success">Upload</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h5 class="font-size-14 m-0"><a href="javascript: void(0);" class="text-dark">KTP</a></h5></td>

                                            <td>
                                                <i class="mdi mdi-circle-medium font-size-18 text-success align-middle me-1"></i> Online
                                            </td>
                                            <td>
                                               <button class="btn btn-outline-success">Upload</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>

@endsection
@section('custom_js')
    <script>
        $('#btn-filter').click(function(){
            $('#inner-filter').slideToggle();
        })
        $("#btn-filter").click();
    </script>
    <script src="{{asset("custom_js/city.js")}}"></script>
@endsection
