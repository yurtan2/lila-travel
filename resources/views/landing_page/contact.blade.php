<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @include('Libary_Navbar.general_libary')
    <style>
        .background{
            background-image: url('{{asset("/background_about.jpg")}}');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: right;
            background-size: auto;
        }
        html{font-family: 'Yaldevi', sans-serif;}
    </style>
</head>
<body style="background-color: white">
    <!-- Navbar -->
    @include('landing_page.navbar')
    <div class="">
        <div class="container p-5  background"  style="border-radius:20px;">
            <h1 style="color:white">Lila Travel & Tours</h1>
            <h6 style="color:white">Make your holiday comfort with the local EXPERT</h6>
        </div>
    </div>
    <div class="container mb-5">
        <div class="row mt-5">
            <div class="col-md-5 col-xl-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3943.8681952483353!2d115.252792!3d-8.7040647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd241bc148d5109%3A0x6801428c9c113352!2sJl.%20Bypass%20Ngurah%20Rai%20No.88a%2C%20Sanur%2C%20Denpasar%20Selatan%2C%20Kota%20Denpasar%2C%20Bali%2080227!5e0!3m2!1sen!2sid!4v1689328936554!5m2!1sen!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-md-7 col-xl-6 pt-5" style="margin-left: -80px">
                <div class="card card-body">
                    <h5 style="color: #7C41FF">Contact Us</h5><br>
                    <p style="text-align: justify"> 
                        <i data-feather="smartphone" style="width: 18px" class="me-3"></i> + 62 81 999 58 8000<br><br>

                        <i data-feather="mail" style="width: 18px" class="me-3"></i> info@lila.co.id<br><br>
                        
                        <i data-feather="map-pin" style="width: 18px" class="me-3"></i>  Jl. Bypass Ngurah Rai No.88 A, Sanur, Denpasar Selatan, Kota Denpasar, Bali 80227
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>

@include('Libary_Navbar.general_js')
