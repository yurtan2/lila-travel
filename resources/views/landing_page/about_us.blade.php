<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About Us </title>
    @include('Libary_Navbar.general_libary')
    <style>
        .background{
            background-image: url('{{asset("/background_about.jpg")}}');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: right;
            background-size: auto;
        }
        html{font-family: 'Yaldevi', sans-serif;}
    </style>
</head>
<body style="background-color: white">
    <!-- Navbar -->
    @include('landing_page.navbar')
    <div class="">
        <div class="container p-5  background"  style="border-radius:20px;">
            <h1 style="color:white">Lila Travel & Tours</h1>
            <h6 style="color:white">Make your holiday comfort with the local EXPERT</h6>
        </div>
    </div>
    <div class="container mb-5">
        <div class="row mt-5">
            <div class="col-md-5 col-xl-4">
                <img src="{{asset('/bg1.jpg')}}" alt="" width="100%">
            </div>
            <div class="col-md-7 col-xl-8 pt-5" style="margin-left: -50px">
                <div class="card card-body">
                    <h5 style="color: #7C41FF">About Us</h5>
                    <p style="text-align: justify"> 
                     Didirikan sejak 1997 oleh Subawa, I Made Gede, seorang professional muda asal Bali yang telah berpengalaman dalam berbagai bidang yang berhubungan dengan dunia kepariwisataan, termasuk didalamnya sebagai executive Hotel bertaraf Internasional seperti The Ritz-Carlton. Sebagai putra kelahiran Bali, Subawa sangat bangga dengan kultur dan budaya Bali dan memiliki motivasi kuat untuk memajukan perkembangan Indonesia khususnya Bali sebagai sebuah destinasi kelas dunia.
                     Selain sebagai Tour Operator kami juga telah banyak melayani wisatawan MICE (Meeting, Incentive, Convention and Exhibition) yang penangannya oleh Divisi khusus yang kami namakan Lila Tour, Destination Management Company.
                     Di tahun 1999, PT. Lila Buana Wisata melahirkan Divisi baru yang dinamakan Lila Travel, Agen perjalanan Wisata, terakreditasi IATA (International Air Transport Association), menangani segala kebutuhan perjalanan mulai dari Air-ticketing, Paket Liburan, Paket Religi "Tirtayatra", Hotel Booking, Asuransi Perjalanan serta Dokumen Perjalanan. 
                     <br>
                     Simply service!
                    </p>
                </div>
                <div class="mt-3 text-end ps-5 ms-4">
                    <h5 style="color: #7C41FF">Our Mission</h5>
                    <p style="text-align: right"> Menjadi penyedia Jasa Pariwisata dan Perjalanan terpercaya di Indonesia yang mengutamakan kualitas pelayanan dengan Harga termurah dan dapat memberikan pengalaman yang tak terlupakan.</p>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>