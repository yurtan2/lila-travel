<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lila Travel & Tour</title>
    <link rel="icon" type="image/png" href="{{asset('images/logo-gradient.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}"  id="_token"/>
    @include('Libary_Navbar.general_libary')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link href="{{asset("/css/landing_page.css")}}" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: white">
        <!-- Navbar -->
        @include('landing_page.navbar')
        <!-- Carousel -->
        {{-- {{dd($data)}} --}}
        <div class="container-fluid pe-md-5 ps-md-5" style="z-index: -1;">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" >
                <div class="carousel-inner" style="border-radius:40px">
                    <div class="carousel-item active" style="border-radius:20px;height:405px">
                        <img src="{{asset('/images/slide1.png')}}" class="d-block w-100 banner" alt="...">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/banner1.png')}}" class="d-block w-100 banner" width="100%">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/banner2.jpg')}}" class="d-block w-100 banner" alt="...">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/39390207.png')}}" class="d-block w-100 banner" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>

        <div class="container mt-3" style="">
            <ul class="nav nav-pills small-nav-items mb-3 responsive-scroll" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3 active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-ticket" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tiket Pesawat <i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-hotel" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Hotel<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-tour" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tour<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-tirtayatra" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tirtayatra<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-things" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Things to Do<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-travel" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Travel Document<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                </li>
            </ul>
            <hr>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-12">
                    <div class="h3 fw-bold mb-3">Product Found</div>
                </div>
            </div>
            <div class="row">
                {{-- <div class="col-lg-4">
                    <button class="btn default1 btn-active w-50 mb-n5 ms-n3" style="border-radius:10px;padding-left:0px;padding-right:0px;">
                        <i class="bi bi-star"></i>
                        Star Hotel
                    </button>
                    <div class="card border w-100 mx-auto mt-n3" style="width: 18rem; z-index: 0">
                        <img src="{{ asset('assets/upload/image-cards.png') }}" class="card-img-top" style="width: 304px; height: 220px; border-radius: 25px;">
                        <div class="card-body">
                            <div class="card-title h5" style="height: 30px;">${namaHotel[i][j]}</div>
                            <div class="card-text">
                                <div class="text-dark" style="font-size: 7pt">Starting From</div>
                                <div class="text-danger" style="font-size: 8pt"><del>${formatRupiah(harga_asli[i][j], 'Rp.')}</del></div>
                                <div class="h5">${formatRupiah(harga_disc[i][j], 'Rp.')}</div>
                                <div class="text-dark">Hotel Rate</div>
                                <div class="row justify-content-between">
                                    <div class="col-6">
                                        ${star}
                                    </div>
                                    <div class="col-3">
                                        <i class="bi bi-heart h5 me-1"></i>
                                        <i class="bi bi-share h5"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                @for ($i = 0; $i < 20; $i++)
                <div class="col-lg-4">
                    <div class="card w-100">
                        <button class="btn default1 btn-active w-50 mb-n5 ms-n3" style="border-radius: 10px; z-index: 1;">
                            <i class="bi bi-star"></i>
                            Star Hotel
                        </button>
                        <img src="{{ asset('assets/cover/64cbf186e23d7.jpg') }}" class="card-img-top p-3" alt="...">
                        <div class="card-body">
                            <div class="h4 mt-n4">Nama Hotel</div>
                            <div class="row mt-3">
                                <div class="col-lg-4">
                                    <p>Inclusion</p>
                                    <i class="bi bi-airplane"></i>
                                    <i class="bi bi-airplane"></i>
                                    <i class="bi bi-airplane"></i>
                                    <i class="bi bi-airplane"></i>
                                    <p>Tour Rate</p>
                                    <div class="text-warning">
                                        <i class="bi bi-star-fill"></i>
                                        <i class="bi bi-star-fill"></i>
                                        <i class="bi bi-star-fill"></i>
                                        <i class="bi bi-star-fill"></i>
                                    </div>
                                    <p>3 Reviews</p>
                                </div>
                                <div class="col-lg-8">
                                    <p>Starting From</p>
                                    <div class="h5"><del>Rp1.400.000</del></div>
                                    <div class="h3">Rp.1000.000</div>
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <button class="btn btn-purple btn-rounded w-100">Book Now</button>
                                        </div>
                                        <div class="col-lg-2">
                                            <i class="bi bi-heart h5 me-1"></i>
                                        </div>
                                        <div class="col-lg-2">
                                            <i class="bi bi-share h5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
        <div class="container-fluid mt-5 pt-5 px-5 mb-3">
            <footer class="container-fluid default2 px-5 py-2 text-start">
                <div class="row mt-5 pt-5">
                    <div class="col-lg-6 col-12 mb-3">
                        <div class="row mb-3 mt-n5">
                            <div class="col-lg-2 col-md-2 col-4">
                                <img class="w-100" src="{{asset('assets/landing_page/dp_path004.png')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-12">
                                Didirikan sejak 1997 oleh Subawa, I Made Gede, seorang professional muda asal Bali yang telah berpengalaman dalam berbagai bidang yang berhubungan dengan dunia kepariwisataan, termasuk didalamnya sebagai executive Hotel bertaraf Internasional seperti The Ritz-Carlton. Sebagai putra kelahiran Bali, Subawa sangat bangga dengan kultur dan budaya Bali dan memiliki motivasi kuat untuk memajukan perkembangan Indonesia khususnya Bali sebagai sebuah destinasi kelas dunia.
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="row justify-content-between px-2 mb-3">
                            <div class="col-lg-4 col-md-4 col-12 mx-auto">
                                <p class="h5 text-white fw-bold mb-4">Services</p>
                                <p>Air Ticketing</p>
                                <p>Hotels</p>
                                <p>Tour</p>
                                <p>Tirtayatra</p>
                                <p>Tirtayatra</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <p class="h5 text-white fw-bold mb-4">About</p>
                                <p>Our Story</p>
                                <p>Benefits</p>
                                <p>Team</p>
                                <p>Careers</p>
                                <p></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <p class="h5 text-white fw-bold mb-4">Follow Us</p>
                                <p>
                                    <a href="https://www.facebook.com/LILAtourandtravel/" class="text-white">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                </p>
                                <p>
                                    <a href="https://twitter.com/lilatravelntour/" class="text-white">
                                        <i class="fab fa-twitter"></i>
                                        Twitter
                                    </a>
                                </p>
                                <p>
                                    <a href="https://www.instagram.com/lilabuanawisata/" class="text-white">
                                        <i class="fab fa-instagram"></i>
                                        Instagram
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 pb-3">
                    <div class="col-lg-8 col-12">Copyright © 2020. LogoIpsum. All rights reserved.</div>
                    <div class="col-lg-2">Term & Condition</div>
                    <div class="col-lg-2">Privacy Policy</div>
                </div>
            </footer>
        </div>

    </body>
</html>
@include('Libary_Navbar.general_js')

<script src="{{asset("custom_js/landing_page.js")}}"></script>
<!-- unpkg CDN -->
<script src="https://unpkg.com/bootstrap-detect-breakpoint/src/bootstrap-detect-breakpoint.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
q
