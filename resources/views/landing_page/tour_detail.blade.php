<!DOCTYPE html>
@php
    $image = asset($data["tour_banner"]);
    $itinerary = json_decode($data["tour_itinerary"], false);
@endphp
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lila Travel & Tour</title>
    <link rel="icon" type="image/png" href="{{asset('images/logo-gradient.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}"  id="_token"/>
    @include('Libary_Navbar.general_libary')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link href="{{asset("/css/landing_page.css")}}" rel="stylesheet" type="text/css" />
    <style>
        .responsive-scroll {

            display: flex;

            overflow-x: auto;

            scrollbar-width: thin;

            scrollbar-color: rgba(155, 155, 155, 0.7) transparent;

        }

        .responsive-scroll::-webkit-scrollbar {

            height: 6px;

        }

        .responsive-scroll::-webkit-scrollbar-track {

            background: transparent;

        }

        .responsive-scroll::-webkit-scrollbar-thumb {

            background-color: rgba(155, 155, 155, 0.7);

            border-radius: 20px;

            border: transparent;

        }

        .container-image{
            border-radius:20px;
            height:500px;
            background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.6)), url({{$image}});
            background-size: cover;
        }
        .page{
            background-image: linear-gradient(to left, #B376FF, #7C41FF);
            color: white
        }
        .text-purple{
            color: #7C41FF;
        }
        .text-gray{
            color: gray;
        }
        .btn-purple{
            background-color: #7C41FF;
            color: white;
        }
        .btn-purple:hover{
            background-color: #6837d2;
            color: white;
        }
        .btn-outline-purple{
            border: 1px solid #7C41FF;
            color: #7C41FF;
        }
        .btn-outline-purple:hover{
            background-color: #7C41FF;
            color: white;
        }
        .card{
            border-radius: 10px;
        }

        .bi-x:hover{
            color: red;
            cursor: pointer;
        }

        .cursor:hover{
            cursor: pointer;
        }
        .swiper {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        li{
            display: list-item!important;
        }
    </style>
</head>
<body style="background-color: white">
    @include('landing_page.navbar')
    <div class="container-fluid px-5">
        <div class="container-image">
            <div class="row pt-5 px-5 h-100 align-items-center">
                <div class="col-lg-1">
                    &nbsp;
                </div>
                <div class="col-lg-3 text-start pt-4">
                    <div class="h3 text-white">
                        {{$data["destinasi"][0]["name"]}}
                    </div>
                    <div class="h1 text-white">
                        {{$data["tour_name"]}}
                    </div>
                    <div class="h5 text-white">
                        {{$data["tour_days"]}} Days / {{$data["tour_nights"]}} Night
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-7">
                            <div class="text-white fs-5 fw-bold cursor discover">Discover More <i class="bi bi-arrow-down-circle ms-2"></i></div>
                            <hr  style="height: 10px; color: white;">
                        </div>
                    </div>
                </div>
                <div class="col-2 text-end">
                </div>
            </div>
        </div>
    </div>
    <div id="content"></div>
    <div class="container-fluid w-75 mt-5 p-1">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div class="h5 float-start mt-2 text-purple">Itinerary</div>
                        <button class="btn btn-outline-purple float-end">Export PDF</button>
                    </div>
                    <div class="card-body">
                        <div class="accordion w-100" id="accordionExample">
                            @foreach ($itinerary as $key => $item)
                            <div class="row">
                                <h2 class="accordion-header" id="heading_{{$key}}">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_{{$key}}" aria-expanded="true" aria-controls="collapse_{{$key}}">
                                    <strong>Day {{$key+1}}</strong> : {{$item->highlight}}
                                    </button>
                                </h2>
                                <div id="collapse_{{$key}}" class="accordion-collapse collapse show">
                                    <div class="accordion-body">
                                        {!!$item->detail!!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="h5 float-start mt-2 text-purple">Galery</div>
                    </div>
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            @foreach ($data["tour_galery"] as $key)
                            <div class="swiper-slide" style="
                                background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url({{asset($key["tour_images"])}});
                                width: 300px;
                                height: 300px;
                                background-size: cover;">
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination text-white"></div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <ul class="nav nav-pills mb-3 responsive-scroll" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Inclusion</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Exclusion</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Term & Condition</button>
                                </li>
                            </ul>
                        </div>
                        <br>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                {!!$data["tour_inclusion"][0]["tour_terms_value"]!!}
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                {!!$data["tour_exclusion"][0]["tour_terms_value"]!!}
                            </div>
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                {!!$data["tour_term"][0]["tour_terms_value"]!!}
                            </div>
                          </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 mx-auto">
                <div class="row mb-3">
                    <div class="card p-2">
                        <div class="card-header">
                            <div class="h5 text-purple">Choose Date and Participant</div>
                        </div>
                        <div class="card-body">
                            <div class="container-fluid" id="order-room">

                            </div>
                            <div class="row align-items-center">
                                <button class="btn btn-outline-purple btn-add"><i class="bi bi-plus-circle me-2"></i>Add Room</button>
                            </div>
                            <div class="row mt-3">
                                <button class="btn btn-purple btn-lg" id="btn-book">Book Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@include('Libary_Navbar.general_js')
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
<script src="{{asset("custom_js/tour_detail.js")}}"></script>
<script>
    var data = @json($data);
    var id = @json($data["tour_validity"]);
    var swiper = new Swiper(".mySwiper", {
      pagination: {
        el: ".swiper-pagination",
        type: "fraction",
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
</script>
