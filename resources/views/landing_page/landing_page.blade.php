<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lila Travel & Tour</title>
    <link rel="icon" type="image/png" href="{{asset('images/logo-gradient.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}"  id="_token"/>
    @include('Libary_Navbar.general_libary')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link href="{{asset("/css/landing_page.css")}}" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: white">
        <!-- Navbar -->
        @include('landing_page.navbar')
        <!-- Carousel -->
        {{-- {{dd($data)}} --}}
        <div class="container-fluid pe-md-5 ps-md-5 mb-lg-n5" style="z-index: -1;">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" >
                <div class="carousel-inner" style="border-radius:40px">
                    <div class="carousel-item active" style="border-radius:20px;height:405px">
                        <img src="{{asset('/images/slide1.png')}}" class="d-block w-100 banner" alt="...">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/banner1.png')}}" class="d-block w-100 banner" width="100%">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/banner2.jpg')}}" class="d-block w-100 banner" alt="...">
                    </div>
                    <div class="carousel-item" style="border-radius:20px;height:405px">
                        <img src="{{asset('/39390207.png')}}" class="d-block w-100 banner" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
        <div class="container tab-landing-page text-center" style="z-index:1;position: relative;font-weight:200; margin-bottom: 90px;">
            <!-- search -->
            <div class="scrollable" role="tablist">
                <ul class="nav nav-pills small-nav-items mb-3 responsive-scroll" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                    <button class="btn default1 menu-search me-3 active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-ticket" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tiket Pesawat <i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-hotel" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Hotel<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-tour" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tour<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-tirtayatra" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tirtayatra<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-things" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Things to Do<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="btn default1 menu-search me-3" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-travel" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Travel Document<i data-feather="send" style="width: 12pt;margin-left:3px"></i></button>
                    </li>
                </ul>
            </div>
            <!--content search-->

            <div class="tab-content" id="content-search">
                <!--tiket-->
                <div class="tab-pane active" id="pills-ticket" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4 col-12">
                                    <label for="">Daerah Asal</label>
                                    <select name="daerah_asal" id="daerah_asal" class="form-select select-select2"></select>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <label for="">Daerah Tujuan</label>
                                    <select name="daerah_tujuan" id="daerah_tujuan" class="form-select select-select2"></select>
                                </div>
                                <div class="col-lg-4 pt-4 ps-4">
                                    <div class="form-check form-check-inline mt-2">
                                        <input class="form-check-input input-checked" type="radio" checked="checked" name="flexRadioDefault" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                        Return
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input input-checked" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                        One Way
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-start mt-3 mb-3">
                                <div class="col-lg-2 col-6">
                                    <label for="">Tanggal Pergi</label>
                                    <input type="date" name="tanggal_awal" id="tanggal_awal" class="form-control">
                                </div>
                                <div class="col-lg-2 col-6">
                                    <label for="">Tanggal Kembali</label>
                                    <input type="date" name="tanggal_akhir" id="tanggal_akhir" class="form-control">
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Dewasa</label>
                                    <select name="dewasa" id="dewasa" class="form-select">
                                        @for ($i = 0; $i <= 20; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Anak</label>
                                    <select name="anak" id="anak" class="form-select">
                                        @for ($i = 0; $i <= 10; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Bayi</label>
                                    <select name="bayi" id="bayi" class="form-select">
                                        @for ($i = 0; $i <= 5; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:0px">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search Ticket
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hotel-->
                <div class="tab-pane " id="pills-hotel" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4 col-12">
                                    <label for="">Location</label>
                                    <select name="location" id="location" class="form-select"></select>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Room</label>
                                    <select name="room" id="room" class="form-select">
                                        @for ($i = 1; $i <= 20; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Dewasa</label>
                                    <select name="dewasa" id="hotel_dewasa" class="form-select">
                                        @for ($i = 0; $i <= 20; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <label for="">Anak</label>
                                    <select name="anak" id="hotel_anak" class="form-select">
                                        @for ($i = 0; $i <= 10; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="row text-start mt-3 mb-3">
                                <div class="col-lg-3 col-12">
                                    <label for="">Check In</label>
                                    <input type="date" name="checkin" id="checkin" class="form-control">
                                </div>
                                <div class="col-lg-3 col-12">
                                    <label for="">Durasi</label>
                                    <select name="durasi" id="durasi" class="form-select">
                                        @for ($i = 1; $i <= 20; $i++)
                                            <option value="{{$i}}">{{$i}} Malam</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-3 col-12">
                                    <label for="">Check Out</label>
                                    <input type="date" name="checkout" disabled id="checkout" class="form-control">
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:0px">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search Hotel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tour -->
                <div class="tab-pane " id="pills-tour" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4 col-12">
                                    <label >Tujuan</label>
                                    <select name="tujuan" id="tujuan" class="form-select select-select2"></select>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Pergi</label>
                                    <input type="date" name="tour_tanggal_pergi" id="tour_tanggal_pergi" class="form-control">
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Kembali</label>
                                    <input type="date" name="tour_tanggal_pulang" id="tour_tanggal_pulang" class="form-control">
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:0px" id="btn-tour-search">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search Tour
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tirtayatra -->
                <div class="tab-pane " id="pills-tirtayatra" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4 col-6">
                                    <label for="">Tanggal Pergi</label>
                                    <input type="date" name="tirtayatra_tanggal_pergi" id="tirtayatra_tanggal_pergi" class="form-control">
                                </div>
                                <div class="col-lg-4 col-6">
                                    <label for="">Tanggal Kembali</label>
                                    <input type="date" name="tirtayatra_tanggal_pulang" id="tirtayatra_tanggal_pulang" class="form-control">
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:0px">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search Tirtayatra
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- things -->
                <div class="tab-pane " id="pills-things" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4">
                                    <label >Tujuan</label>
                                    <select name="tujuan" id="things_tujuan" class="form-select select-select2"></select>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Pergi</label>
                                    <input type="date" name="things_tanggal_pergi" id="things_tanggal_pergi" class="form-control">
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Kembali</label>
                                    <input type="date" name="things_tanggal_pulang" id="things_tanggal_pulang" class="form-control">
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:10px">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane " id="pills-travel" role="tabpanel">
                    <div class="card" style="border-radius: 10px">
                        <div class="card-body p-4 ps-5 pe-5">
                            <div class="row text-start">
                                <div class="col-lg-4">
                                    <label >Tujuan</label>
                                    <select name="tujuan" id="travel_document" class="form-select select-select2"></select>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Pergi</label>
                                    <input type="date" name="things_tanggal_pergi" id="things_tanggal_pergi" class="form-control">
                                </div>
                                <div class="col-lg-3 col-6">
                                    <label for="">Tanggal Kembali</label>
                                    <input type="date" name="things_tanggal_pulang" id="things_tanggal_pulang" class="form-control">
                                </div>
                                <div class="col-lg-2 col-12 pt-4">
                                    <button class="btn default1 btn-active w-100" style="border-radius:10px;padding-left:0px;padding-right:10px">
                                        <i data-feather="search" style="width: 12pt;margin-left:3px"></i>
                                        Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-5">
            {{--
            <div class="row mt-5 mb-3">
                <div class="col text-center">
                    <div class="h2">Populer Tour</div>
                    <p>Temukan paket tur populer di berbagai destinasi di seluruh dunia. Telusuri berbagai pilihan untuk menemukan pengalaman wisata yang sempurna untuk Anda. Mulai dari petualangan seru hingga rekreasi santai, kami menyediakan berbagai opsi untuk semua jenis penjelajah.</p>
                </div>
            </div>
            <div class="row">
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        @foreach ($data2["data"] as $key)
                        @php
                            $url = asset($key["tour_cover"]);
                            $title = ($key["tour_jenis"] == 1? "Group Tour":"2 Can Go");
                            $price = number_format($key["tour_price_gimick"]);
                            $tour_name = $key["tour_name"];
                            if(strlen($tour_name)>=24) $tour_name = substr($tour_name,1,24)."...";
                        @endphp
                        <div class="swiper-slide mb-3"  style="overflow-y:hidden; width: auto;">
                            <div class="col-0 ms-5 me-3 text-start" style="">
                                <div class="card text-white" style="width: 268px; height: 380px;">
                                    <a href="/tour/detail/{{$key["tour_id"]}}">
                                        <div class="show_bg_w" style="background-image: linear-gradient(to top, rgba(0, 0, 0, 0.8) 0%, rgba(0, 0, 0, 0.6) 40%, rgba(255, 255, 255, 0.2) 70%), url('{{$url}}');width: 268px; height: 380px;">
                                        </div>
                                        <div class="card-img-overlay shadow ps-4">
                                            <h5 class="card-title mt-2" style="margin-left:-10px">
                                                <div class="btn btn-primary ms-n4 shadow mb-5 default2">{{$title}}</div>
                                            </h5>
                                            <div class="card-text mt-5 pt-2 ms-n2 ">
                                                <div class="btn btn-light btn-sm btn-rounded">{{$key["country"]}}</div>
                                                <div class="h5 text-white mt-2">{{$tour_name}}</div>
                                            </div>
                                            <div class="card-text text-white ms-n2 mt-1" style="min-height:30px">
                                                <div class="text-white" style="font-size: 8pt">Inclusion</div>
                                                @if ($key["tour_air_ticket"] == 1)
                                                <i class='bi bi-ticket-detailed-fill'></i>
                                                @endif
                                                @if ($key["tour_hotel"] == 1)
                                                <i class="bi bi-building-fill"></i>
                                                @endif
                                                @if ($key["tour_meal"] == 1)
                                                <i class="bi bi-egg-fried"></i>
                                                @endif
                                                @if ($key["tour_tour"] == 1)
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-airplane-fill me-1" viewBox="0 0 16 16">
                                                    <path d="M6.428 1.151C6.708.591 7.213 0 8 0s1.292.592 1.572 1.151C9.861 1.73 10 2.431 10 3v3.691l5.17 2.585a1.5 1.5 0 0 1 .83 1.342V12a.5.5 0 0 1-.582.493l-5.507-.918-.375 2.253 1.318 1.318A.5.5 0 0 1 10.5 16h-5a.5.5 0 0 1-.354-.854l1.319-1.318-.376-2.253-5.507.918A.5.5 0 0 1 0 12v-1.382a1.5 1.5 0 0 1 .83-1.342L6 6.691V3c0-.568.14-1.271.428-1.849Z"/>
                                                </svg>
                                                @endif
                                                @if ($key["tour_land_trans"] == 1)
                                                <i class="bi bi-airplane-fill"></i>
                                                @endif
                                                @if ($key["tour_water_trans"] == 1)
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-car-front-fill me-1" viewBox="0 0 16 16">
                                                    <path d="M2.52 3.515A2.5 2.5 0 0 1 4.82 2h6.362c1 0 1.904.596 2.298 1.515l.792 1.848c.075.175.21.319.38.404.5.25.855.715.965 1.262l.335 1.679c.033.161.049.325.049.49v.413c0 .814-.39 1.543-1 1.997V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.338c-1.292.048-2.745.088-4 .088s-2.708-.04-4-.088V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.892c-.61-.454-1-1.183-1-1.997v-.413a2.5 2.5 0 0 1 .049-.49l.335-1.68c.11-.546.465-1.012.964-1.261a.807.807 0 0 0 .381-.404l.792-1.848ZM3 10a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm10 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2ZM6 8a1 1 0 0 0 0 2h4a1 1 0 1 0 0-2H6ZM2.906 5.189a.51.51 0 0 0 .497.731c.91-.073 3.35-.17 4.597-.17 1.247 0 3.688.097 4.597.17a.51.51 0 0 0 .497-.731l-.956-1.913A.5.5 0 0 0 11.691 3H4.309a.5.5 0 0 0-.447.276L2.906 5.19Z"/>
                                                </svg>
                                                @endif
                                                @if ($key["tour_visa"] == 1)
                                                <i class="bi bi-book-fill"></i>
                                                @endif
                                                @if ($key["tour_insurance"] == 1)
                                                <i class="bi bi-coin"></i>
                                                @endif
                                            </div>
                                            <div class="card-text text-white ms-n2 mt-2">
                                                <div class="text-white" style="font-size: 8pt">Starting From</div>
                                                <div class="text-white" style="font-size: 8pt"><del>Rp.{{$price}}</del></div>
                                                <div class="h5 text-white">Rp.{{$price}}</div>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>--}}
        </div>

        <div class="container" id="card_page">

        </div>
        <div class="container-fluid mt-5 pt-5 px-5 mb-3">
            <footer class="container-fluid default2 px-5 py-2 text-start">
                <div class="row mt-5 pt-5">
                    <div class="col-lg-6 col-12 mb-3">
                        <div class="row mb-3 mt-n5">
                            <div class="col-lg-2 col-md-2 col-4">
                                <img class="w-100" src="{{asset('assets/landing_page/dp_path004.png')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-12">
                                Didirikan sejak 1997 oleh Subawa, I Made Gede, seorang professional muda asal Bali yang telah berpengalaman dalam berbagai bidang yang berhubungan dengan dunia kepariwisataan, termasuk didalamnya sebagai executive Hotel bertaraf Internasional seperti The Ritz-Carlton. Sebagai putra kelahiran Bali, Subawa sangat bangga dengan kultur dan budaya Bali dan memiliki motivasi kuat untuk memajukan perkembangan Indonesia khususnya Bali sebagai sebuah destinasi kelas dunia.
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="row justify-content-between px-2 mb-3">
                            <div class="col-lg-4 col-md-4 col-12 mx-auto">
                                <p class="h5 text-white fw-bold mb-4">Services</p>
                                <p>Air Ticketing</p>
                                <p>Hotels</p>
                                <p>Tour</p>
                                <p>Tirtayatra</p>
                                <p>Tirtayatra</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <p class="h5 text-white fw-bold mb-4">About</p>
                                <p>Our Story</p>
                                <p>Benefits</p>
                                <p>Team</p>
                                <p>Careers</p>
                                <p></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <p class="h5 text-white fw-bold mb-4">Follow Us</p>
                                <p>
                                    <a href="https://www.facebook.com/LILAtourandtravel/" class="text-white">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                </p>
                                <p>
                                    <a href="https://twitter.com/lilatravelntour/" class="text-white">
                                        <i class="fab fa-twitter"></i>
                                        Twitter
                                    </a>
                                </p>
                                <p>
                                    <a href="https://www.instagram.com/lilabuanawisata/" class="text-white">
                                        <i class="fab fa-instagram"></i>
                                        Instagram
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 pb-3">
                    <div class="col-lg-8 col-12">Copyright © 2020. LogoIpsum. All rights reserved.</div>
                    <div class="col-lg-2">Term & Condition</div>
                    <div class="col-lg-2">Privacy Policy</div>
                </div>
            </footer>
        </div>

    </body>
</html>
@include('Libary_Navbar.general_js')

<script src="{{asset("custom_js/landing_page.js")}}"></script>
<!-- unpkg CDN -->
<script src="https://unpkg.com/bootstrap-detect-breakpoint/src/bootstrap-detect-breakpoint.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
<script type="text/javascript">
    function initPromo() {
        var head_promo=head_fs=0;
        var data = <?php echo json_encode($data); ?>;

        if(!Array.isArray(data))return;
        card = '';

        data.forEach((element) => {
            //header title (flash sale / promo)
            if(element.displayer_category=="Flash Sale"&&head_fs==0||
            element.displayer_category=="Promo"&&head_promo==0){
                $("#card_page").append($(`
                    <div class="row mt-5 mb-3">
                        <div class="col text-center">
                            <div class="h2">${element.displayer_category}</div>
                            <p>Temukan hotel berperingkat tinggi di berbagai lokasi di seluruh dunia. Telusuri penginapan yang paling dicari untuk menemukan akomodasi yang sempurna untuk Anda. Mulai dari resor bintang lima hingga penginapan dengan harga terjangkau, kami menyajikan semuanya di ujung jari Anda.</p>
                        </div>
                    </div>
                `));
                if(element.displayer_category=="Flash Sale")head_fs=1;
                else if(element.displayer_category=="Promo")head_promo=1;
            }

            $("#card_page").append($(`
                <h5 class="ps-4 mb-4 mt-4">${element.displayer_title}</h5>
            `));

            //foreach per item
            element.detail.forEach((detail, index) => {
                console.log(detail);
                className = (element.displayer_category = "Flash Sale"? 'image-container': 'image-container-2')
                inclusion = '';

                //pengcekan inclusion
                if(detail.tour_ticket == 1)
                    inclusion += `<i class="bi bi-ticket-detailed-fill"></i>`;
                if(detail.tour_hotel == 1)
                    inclusion += `<i class="bi bi-building-fill"></i>`;
                if(detail.tour_meal == 1)
                    inclusion += `<i class="bi bi-egg-fried"></i>`;
                if(detail.tour_tour == 1)
                    inclusion +=
                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-airplane-fill me-1" viewBox="0 0 16 16">
                        <path d="M6.428 1.151C6.708.591 7.213 0 8 0s1.292.592 1.572 1.151C9.861 1.73 10 2.431 10 3v3.691l5.17 2.585a1.5 1.5 0 0 1 .83 1.342V12a.5.5 0 0 1-.582.493l-5.507-.918-.375 2.253 1.318 1.318A.5.5 0 0 1 10.5 16h-5a.5.5 0 0 1-.354-.854l1.319-1.318-.376-2.253-5.507.918A.5.5 0 0 1 0 12v-1.382a1.5 1.5 0 0 1 .83-1.342L6 6.691V3c0-.568.14-1.271.428-1.849Z"/>
                    </svg>`;
                if(detail.tour_land_trans == 1)
                    inclusion += `<i class="bi bi-airplane-fill"></i>`;
                if(detail.tour_water_trans == 1)
                    inclusion +=
                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-car-front-fill me-1" viewBox="0 0 16 16">
                        <path d="M2.52 3.515A2.5 2.5 0 0 1 4.82 2h6.362c1 0 1.904.596 2.298 1.515l.792 1.848c.075.175.21.319.38.404.5.25.855.715.965 1.262l.335 1.679c.033.161.049.325.049.49v.413c0 .814-.39 1.543-1 1.997V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.338c-1.292.048-2.745.088-4 .088s-2.708-.04-4-.088V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.892c-.61-.454-1-1.183-1-1.997v-.413a2.5 2.5 0 0 1 .049-.49l.335-1.68c.11-.546.465-1.012.964-1.261a.807.807 0 0 0 .381-.404l.792-1.848ZM3 10a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm10 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2ZM6 8a1 1 0 0 0 0 2h4a1 1 0 1 0 0-2H6ZM2.906 5.189a.51.51 0 0 0 .497.731c.91-.073 3.35-.17 4.597-.17 1.247 0 3.688.097 4.597.17a.51.51 0 0 0 .497-.731l-.956-1.913A.5.5 0 0 0 11.691 3H4.309a.5.5 0 0 0-.447.276L2.906 5.19Z"/>
                    </svg>`;
                if(detail.tour_visa == 1)
                    inclusion += `<i class="bi bi-book-fill"></i>`;
                if(detail.tour_insurance == 1)
                    inclusion += `<i class="bi bi-coin"></i>`   ;

                //query cardnya
                card += `
                    <div class="swiper-slide mb-3" style="overflow-y:hidden" >
                        <div class="col-0 ms-5 me-3 text-start" style="">
                            <div class="card text-white" style="width: 268px; height: 380px;">
                                <a href="/tour/detail/${detail.tour_id}" class="text-white">
                                    <div class="show_bg_w" style="background-image: linear-gradient(to top, rgb(14 120 127 / 50%) 0%, rgb(14 120 127 / 18%) 40%, rgba(255, 255, 255, 0) 50%), url('{{asset('${detail.tour_cover}')}}');width: 268px; height: 380px;">
                                    </div>
                                    <div class="card-img-overlay shadow ps-4">
                                        <h5 class="card-title mt-2" style="margin-left:-10px">
                                            <div class="btn btn-primary ms-n4 shadow mb-5 default2">${detail.tour_jenis==1?"Group Tour":"2 Can Go"}</div>
                                        </h5>
                                        <div class="card-text mt-5 pt-2 ms-n2 ">
                                            <div class="btn btn-light btn-sm btn-rounded">${detail.country}</div>
                                            <div class="h5 text-white mt-2">${detail.tour_name}</div>
                                        </div>
                                        <div class="card-text ms-n2 mt-1" style="min-height:30px">
                                            <div class="text-white" style="font-size: 8pt">Inclusion</div>
                                            ${inclusion}
                                        </div>
                                        <div class="card-text ms-n2 mt-2">
                                            <div class="text-white" style="font-size: 8pt">Starting From</div>
                                            <div class="text-white" style="font-size: 8pt"><del>${formatRupiah(detail.tour_price_highest_price+"", 'Rp.')}</del></div>
                                            <div class="h5 text-white">${detail.tour_price_gimick == null? 0 : formatRupiah(detail.tour_price_gimick+"", 'Rp.')}</div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                `});

             //masukan ke swiper
            var final = `
                <div class="row mb-3">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            ${card}
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            `;

            card="";
            //apppend ke card page
            $('#card_page').append(final);
        });

    }
    function initPopulerHotel() {
        destinasi = ['Indonesia', 'Malaysia', 'Thailand'];
        namaHotel = [
            ['The Oberoi Lombok', 'Amnaya Resort Kuta', 'Intercontinental Bali Resort', 'Four Seasons Resort Bali at Jimbaran Bay', 'Ashley Tanah Abang', 'Amanjiwo', 'Alila Purnama'],
            ['JW Marriott Kuala Lumpur', 'Grand Millennium Kuala Lumpur', 'PARKROYAL COLLECTION Kuala Lumpur', 'Amari Johor Bahru', 'MOV Hotel', 'Dorsett Kuala Lumpur', 'The Face Suites Kuala Lumpur', 'Hotel Maya Kuala Lumpur'],
            ['Kamalaya Koh Samui', 'Iniala Beach House', 'Phulay Bay, A Ritz-Carlton Reserve', 'Six Senses Yao Noi', 'Akara Hotel']
        ];
        bintang = [
            [5, 4, 5, 5, 4, 5, 5],
            [5, 5, 5, 4, 4, 5, 5, 5],
            [5, 5, 5, 5, 4]
        ]
        harga_asli = [
            [5919362, 10086142, 4111498, 27198584, 1467071, 25652000, 200000000],
            [5919362, 10086142, 4111498, 27198584, 1467071, 25652000, 200000000, 200000000],
            [5919362, 10086142, 4111498, 27198584, 1467071],
        ]
        harga_disc = [
            [4650000, 2480878, 3237388, 15840000, 682100, 17424000, 198617647],
            [4650000, 2480878, 3237388, 15840000, 682100, 17424000, 198617647, 198617647],
            [4650000, 2480878, 3237388, 15840000, 682100]
        ]

        $tab = $(`
            <ul class="nav nav-pills mb-3 justify-content-center responsive-scroll" id="pills-tab" role="tablist">

            </ul>
        `);

        $content = $(`
            <div class="tab-content" id="pills-tabContent">
            </div>
        `);

        for(let i = 0; i < destinasi.length; i++){
            tabContent = '';
            for(j = 0; j < namaHotel[i].length; j++){
                star = '';
                for(k = 0; k < bintang[i][j]; k++){
                    star += `<i class="bi bi-star-fill text-warning me-1 h5"></i>`
                }
                tabContent += `
                <div class="swiper-slide mb-3">
                    <div class="row justify-content-center link-card">
                        <div class="col-10">
                            <div class="row">
                                <div class="col-12 mx-auto justify-content-center">
                                    <button class="btn default1 btn-active w-50 mb-n5 ms-n3" style="border-radius:10px;padding-left:0px;padding-right:0px">
                                        <i class="bi bi-star"></i>
                                        ${bintang[i][j]} Star Hotel
                                    </button>
                                    <div class="card w-100 mx-auto mt-n3" style="width: 18rem; z-index: -1;" style="">
                                        <img src="{{asset('assets/landing_page/${namaHotel[i][j]}.png')}}" class="card-img-top" style="width: 304px; height: 220px; border-radius: 25px;">
                                        <div class="card-body">
                                            <div class="card-title h5" style="height: 30px;">${namaHotel[i][j]}</div>
                                            <div class="card-text">
                                                <div class="text-dark" style="font-size: 7pt">Starting From</div>
                                                <div class="text-danger" style="font-size: 8pt"><del>${formatRupiah(harga_asli[i][j], 'Rp.')}</del></div>
                                                <div class="h5">${formatRupiah(harga_disc[i][j], 'Rp.')}</div>
                                                <div class="text-dark">Hotel Rate</div>
                                                <div class="row justify-content-between">
                                                    <div class="col-6">
                                                        ${star}
                                                    </div>
                                                    <div class="col-3">
                                                        <i class="bi bi-heart h5 me-1"></i>
                                                        <i class="bi bi-share h5"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `
            }
            $elementTab= $(`
                <li class="nav-item mx-3" role="presentation">
                    <button class="btn default1 menu-search${(i == 0? 'active' : '')}" id="pills-tab-${i}" data-bs-toggle="pill" data-bs-target="#pills-${i}" type="button" role="tab" aria-controls="pills-${i}" ${(i == 0? 'aria-selected="true"' : '')}>${destinasi[i]}</button>
                </li>
            `);

            $elementContent = $(`
                <div class="tab-pane fade show ${(i == 0? 'active' : '')}" id="pills-${i}" role="tabpanel" aria-labelledby="pills-tab-${i}" tabindex="0">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            ${tabContent}
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <div class="row">

                    </div>
                </div>
            `)
            //$tab.append($elementTab);
           // $content.append($elementContent);
        }
        /*
        $('#card_page').append($(`
            <div class="row mt-5 mb-3">
                <div class="row text-center">
                    <div class="h2">Populer Hotel Search</div>
                    <p>Temukan hotel berperingkat tinggi di berbagai lokasi di seluruh dunia. Telusuri penginapan yang paling dicari untuk menemukan akomodasi yang sempurna untuk Anda. Mulai dari resor bintang lima hingga penginapan dengan harga terjangkau, kami menyajikan semuanya di ujung jari Anda.</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12" id="tab-populer-hotel">

                </div>
                <div class="col-12">
                    <div class="tab-content" id="pills-tabContent">

                    </div>
                </div>
            </div>
        `));*/
        $("#card_page").find('#tab-populer-hotel').append($tab);
        $("#card_page").find('#pills-tabContent').append($content);

    }
    function initSwiper() {

        var count=3;
        if($(window).width()<= 450) count=1;
        else if($(window).width()> 450&&$(window).width()<= 979) count=2;
        else count = 3;
        var swiper = new Swiper(".mySwiper", {
            cssMode: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
            },
            mousewheel: true,
            keyboard: true,
            slidesPerView: count,
        });
    }
    $(function(){
        initPromo();
        initPopulerHotel();
        initSwiper();
    })

    window.onresize = function(event) {
        initSwiper();
    };
</script>
