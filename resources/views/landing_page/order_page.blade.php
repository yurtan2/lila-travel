<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lila Travel & Tour</title>
    <link rel="icon" type="image/png" href="{{asset('images/logo-gradient.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}"  id="_token"/>
    @include('Libary_Navbar.general_libary')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
    />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="{{asset("/css/landing_page.css")}}" rel="stylesheet" type="text/css" />
    <style>
        .container-image{
            border-radius:20px;
            height:500px;
            background-size: cover;
        }
        .page{
            background-image: linear-gradient(to left, #B376FF, #7C41FF);
            color: white
        }
        .text-purple{
            color: #7C41FF;
        }
        .btn-purple{
            background-color: #7C41FF;
            color: white;
        }
        .btn-purple:hover{
            background-color: #6837d2;
            color: white;
        }
        .btn-outline-purple{
            border: 1px solid #7C41FF;
            color: #7C41FF;
        }
        .btn-outline-purple:hover{
            background-color: #7C41FF;
            color: white;
        }
        .card{
            border-radius: 10px;
        }

        .bi-x:hover{
            color: red;
            cursor: pointer;
        }

        .cursor:hover{
            cursor: pointer;
        }
        .swiper {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .default1:active,.btn-active, #pills-tab .active{
            border-radius: 200px;
            padding-left: 30px;
            padding-right: 30px;
            font-family: 'Yaldevi', sans-serif;
            font-weight: bold;
            background-image: linear-gradient(to right, #B376FF, #7C41FF);
            color:white;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"></script>

</head>
<body style="background-color: white">
    <div class="container py-3" >
        <nav class="navbar navbar-expand-lg bg-body-tertiary" id="navbar" >
            <div class="container-fluid" style="font-weight: bold">
                <a class="navbar-brand " href="{{route('landing_page')}}">
                    <img src="{{asset("/images/logo-gradient.png")}}" height="50" alt="MDB Logo"
                      loading="lazy" />
                </a>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="nav nav-pills mx-auto" id="pills-tab" role="tablist">
                        <li class="nav-item mx-auto" role="presentation">
                          <button class="nav-link active" id="pills-home-tab" disabled data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Fill in Data</button>
                        </li>
                        <li class="nav-item mx-auto" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" disabled data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">></button>
                        </li>
                        <li class="nav-item mx-auto" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" disabled data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Done</button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    {{-- <div class="container pt-3 mb-3" >

    </div> --}}
    <div class="container-fluid p-5 h-100 bg-light">
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="container">
                    <div class="row mb-3 px-3">
                        <div class="col-lg-5">
                            <div class="h4 text-purple">Tour booking</div>
                        </div>
                    </div>
                    <div class="row mb-3 justify-content-lg-between justify-content-md-center">
                        <div class="col-lg-7 col-md-10 order-lg-1 order-md-2">
                            <div class="row mb-3 px-3">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="h5">Contact Information</div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="input_name" class="form-label">First Name</label>
                                                    <input type="text"  class="form-control need-check" placeholder="Budi">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="input_tour_type" class="form-label">Last Name</label>
                                                    <input type="text" class="form-control need-check" placeholder="Hartono">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="input_tour_category" class="form-label">Email</label>
                                                    <input type="email" class="form-control need-check" placeholder="budi@example.com">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="input_tour_type" class="form-label">Phone Number</label>
                                                    <div class="row align-items-center">
                                                        <div class="col-lg-5">
                                                            <select class="select2 form-select" name="" id="">
                                                                <option value="+1">+1</option>
                                                                <option value="+44">+44</option>
                                                                <option value="+62" selected>+62</option>
                                                                <option value="+91">+91</option>
                                                                <option value="+86">+86</option>
                                                                <option value="+81">+81</option>
                                                                <option value="+61">+61</option>
                                                                <option value="+7">+7</option>
                                                                <option value="+34">+34</option>
                                                                <option value="+55">+55</option>
                                                                <option value="+49">+49</option>
                                                                <option value="+33">+33</option>
                                                                <option value="+39">+39</option>
                                                                <option value="+60">+60</option>
                                                                <option value="+63">+63</option>
                                                                <option value="+27">+27</option>
                                                                <option value="+82">+82</option>
                                                                <option value="+65">+65</option>
                                                                <option value="+90">+90</option>
                                                                <option value="+971">+971</option>
                                                                <option value="+84">+84</option>
                                                                <option value="+213">+213</option>
                                                                <option value="+376">+376</option>
                                                                <option value="+244">+244</option>
                                                                <option value="+266">+266</option> <!-- Lesotho -->
                                                                <option value="+231">+231</option> <!-- Liberia -->
                                                                <option value="+218">+218</option> <!-- Libya -->
                                                                <option value="+423">+423</option> <!-- Liechtenstein -->
                                                                <option value="+370">+370</option> <!-- Lithuania -->
                                                                <option value="+352">+352</option> <!-- Luxembourg -->
                                                                <option value="+261">+261</option> <!-- Madagascar -->
                                                                <option value="+265">+265</option> <!-- Malawi -->
                                                                <option value="+960">+960</option> <!-- Maldives -->
                                                                <option value="+223">+223</option> <!-- Mali -->
                                                                <option value="+356">+356</option> <!-- Malta -->
                                                                <option value="+692">+692</option> <!-- Marshall Islands -->
                                                                <option value="+222">+222</option> <!-- Mauritania -->
                                                                <option value="+230">+230</option> <!-- Mauritius -->
                                                                <option value="+52">+52</option>   <!-- Mexico -->
                                                                <option value="+373">+373</option> <!-- Moldova -->
                                                                <option value="+377">+377</option> <!-- Monaco -->
                                                                <option value="+976">+976</option> <!-- Mongolia -->
                                                                <option value="+382">+382</option> <!-- Montenegro -->
                                                                <!-- ... You can continue to add more ... -->

                                                            </select>
                                                        </div>
                                                        <div class="col-lg-7">
                                                            <input type="tel" class="form-control number-only need-check" placeholder="81xxxxxxxxx">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3 px-3" id="list_room">

                            </div>
                            <div class="row mb-3 justify-content-center">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <button class="btn btn-purple btn-submit">Proceed to Booking</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-10 order-lg-2 order-md-1">
                            <div class="row px-3">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row align-items-center">
                                            <div class="col-lg-8 col-md-12">
                                                <div class="h3">{{$data["tour_name"]}}</div>
                                                <label class="text-muted">
                                                    {{$data["tour_days"]}} Days & {{$data["tour_nights"]}} Nights
                                                </label>
                                            </div>
                                            <div class="col-lg-4 col-md-12">
                                                @php
                                                        $round_image = asset($data["tour_cover"]);
                                            
                                                @endphp
                                                <img src="{{$round_image}}" style="width: 75px; height: 75px;" class="rounded-circle">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mb-1 align-items-center">
                                            <div class="col-lg-1">
                                                <div class="h5 mx-auto">
                                                    <i class="fas fa-bed text-purple me-2"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-10">
                                                <div class="h5" id="room">Room</div>
                                            </div>
                                        </div>
                                        <div class="row mb-1 align-items-center">
                                            <div class="col-lg-1">
                                                <div class="h5 mx-auto">
                                                    <i class="fas fa-user text-purple me-2"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-10">
                                                <div class="h5" id="adult">Adults</div>
                                            </div>
                                        </div>
                                        <div class="row mb-1 align-items-center">
                                            <div class="col-lg-1">
                                                <div class="h5 mx-auto">
                                                    <i class="fas fa-child mx-auto text-purple me-2"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-10">
                                                <div class="h5" id="children">Children</div>
                                            </div>
                                        </div>
                                        <div class="row mb-1 align-items-center">
                                            <div class="col-lg-1">
                                                <div class="h5 mx-auto">
                                                    <i class="fas fa-calendar mx-auto text-purple me-2"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-10">
                                                <div class="h5" id="date">Date</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">

            </div>
        </div>

    </div>
</body>
</html>
@include('Libary_Navbar.general_js')
<script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
      pagination: {
          el: ".swiper-pagination",
        type: "fraction",
      },
      navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
    });
    var data = @json($data);
</script>
<script src="{{asset("custom_js/order_page.js")}}"></script>
