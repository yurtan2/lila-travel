<div class="container pt-3 mb-3" >
    <nav class="navbar navbar-expand-lg bg-body-tertiary" id="navbar" >
        <div class="container-fluid" style="font-weight: bold">
            <a class="navbar-brand " href="{{route('landing_page')}}">
                <img src="{{asset("/images/logo-gradient.png")}}" height="50" alt="MDB Logo"
                  loading="lazy" />
            </a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav  mx-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Product
                    </a>
                    <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('aboutUs')}}">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('contact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Carrer</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">FAQ</a>
                </li>
                    @if (!Session::has("user"))
                        <li class="nav-item">
                            <a href="{{route('login')}}" class="btn btn-pill  default1 btn-active" style="margin-right:5px; margin-top: 20px;">Login</a>
                        </li>
                        <li class="nav-item">
                            <button class="btn btn-pill btn-light default1" style="margin-top: 20px;">Sign Up</button>
                        </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="#">My Booking</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('login')}}" class="btn btn-pill default1 btn-active" style="margin-right:5px; margin-top: 20px;">Dashboard</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Yaldevi:wght@400;500;700&display=swap" rel="stylesheet">
<style>
    html{font-family: 'Yaldevi', sans-serif;color:#667080;background-color: white;}
    .nav-link{
        font-size: 11pt;
        margin-right: 25px;
        margin-top: 20px;
    }
    .default1{
        background-color: #f6f6f6;
        border-radius: 200px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        color: #7C41FF
    }
    .default1:active,.btn-active, #pills-tab .active{
        border-radius: 200px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
    }
    .default1:hover{
        border-radius: 200px;
        padding-left: 30px;
        padding-right: 30px;
        font-family: 'Yaldevi', sans-serif;
        font-weight: bold;
        background-image: linear-gradient(to right, #B376FF, #7C41FF);
        color:white;
    }

    .form-check-input:checked{
        background-color: #7C41FF;
        border-color:#7C41FF;
    }

    /* Scrollbar styles */
    ::-webkit-scrollbar {
    width: 12px;
    height: 12px;
    }

    ::-webkit-scrollbar-track {
    background: #f5f5f5;
    border-radius: 10px;
    }

    ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: #ccc;
    }

    ::-webkit-scrollbar-thumb:hover {
    background: #999;
    }
</style>
